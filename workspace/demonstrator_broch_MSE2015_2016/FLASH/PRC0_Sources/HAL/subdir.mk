################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../PRC0_Sources/HAL/mpc5643l_adc.c" \
"../PRC0_Sources/HAL/mpc5643l_clock.c" \
"../PRC0_Sources/HAL/mpc5643l_ctu.c" \
"../PRC0_Sources/HAL/mpc5643l_dspi.c" \
"../PRC0_Sources/HAL/mpc5643l_ecsm.c" \
"../PRC0_Sources/HAL/mpc5643l_etimer.c" \
"../PRC0_Sources/HAL/mpc5643l_fccu.c" \
"../PRC0_Sources/HAL/mpc5643l_flexpwm.c" \
"../PRC0_Sources/HAL/mpc5643l_hal.c" \
"../PRC0_Sources/HAL/mpc5643l_interrupts.c" \
"../PRC0_Sources/HAL/mpc5643l_mode.c" \
"../PRC0_Sources/HAL/mpc5643l_pit.c" \
"../PRC0_Sources/HAL/mpc5643l_power.c" \
"../PRC0_Sources/HAL/mpc5643l_reset.c" \
"../PRC0_Sources/HAL/mpc5643l_siul.c" \
"../PRC0_Sources/HAL/mpc5643l_swg.c" \
"../PRC0_Sources/HAL/mpc5643l_sysstatus.c" \
"../PRC0_Sources/HAL/mpc5643l_uart.c" \
"../PRC0_Sources/HAL/mpc5643l_wakeup.c" \

C_SRCS += \
../PRC0_Sources/HAL/mpc5643l_adc.c \
../PRC0_Sources/HAL/mpc5643l_clock.c \
../PRC0_Sources/HAL/mpc5643l_ctu.c \
../PRC0_Sources/HAL/mpc5643l_dspi.c \
../PRC0_Sources/HAL/mpc5643l_ecsm.c \
../PRC0_Sources/HAL/mpc5643l_etimer.c \
../PRC0_Sources/HAL/mpc5643l_fccu.c \
../PRC0_Sources/HAL/mpc5643l_flexpwm.c \
../PRC0_Sources/HAL/mpc5643l_hal.c \
../PRC0_Sources/HAL/mpc5643l_interrupts.c \
../PRC0_Sources/HAL/mpc5643l_mode.c \
../PRC0_Sources/HAL/mpc5643l_pit.c \
../PRC0_Sources/HAL/mpc5643l_power.c \
../PRC0_Sources/HAL/mpc5643l_reset.c \
../PRC0_Sources/HAL/mpc5643l_siul.c \
../PRC0_Sources/HAL/mpc5643l_swg.c \
../PRC0_Sources/HAL/mpc5643l_sysstatus.c \
../PRC0_Sources/HAL/mpc5643l_uart.c \
../PRC0_Sources/HAL/mpc5643l_wakeup.c \

OBJS += \
./PRC0_Sources/HAL/mpc5643l_adc_c.obj \
./PRC0_Sources/HAL/mpc5643l_clock_c.obj \
./PRC0_Sources/HAL/mpc5643l_ctu_c.obj \
./PRC0_Sources/HAL/mpc5643l_dspi_c.obj \
./PRC0_Sources/HAL/mpc5643l_ecsm_c.obj \
./PRC0_Sources/HAL/mpc5643l_etimer_c.obj \
./PRC0_Sources/HAL/mpc5643l_fccu_c.obj \
./PRC0_Sources/HAL/mpc5643l_flexpwm_c.obj \
./PRC0_Sources/HAL/mpc5643l_hal_c.obj \
./PRC0_Sources/HAL/mpc5643l_interrupts_c.obj \
./PRC0_Sources/HAL/mpc5643l_mode_c.obj \
./PRC0_Sources/HAL/mpc5643l_pit_c.obj \
./PRC0_Sources/HAL/mpc5643l_power_c.obj \
./PRC0_Sources/HAL/mpc5643l_reset_c.obj \
./PRC0_Sources/HAL/mpc5643l_siul_c.obj \
./PRC0_Sources/HAL/mpc5643l_swg_c.obj \
./PRC0_Sources/HAL/mpc5643l_sysstatus_c.obj \
./PRC0_Sources/HAL/mpc5643l_uart_c.obj \
./PRC0_Sources/HAL/mpc5643l_wakeup_c.obj \

OBJS_QUOTED += \
"./PRC0_Sources/HAL/mpc5643l_adc_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_clock_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_ctu_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_dspi_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_ecsm_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_etimer_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_fccu_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_flexpwm_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_hal_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_interrupts_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_mode_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_pit_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_power_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_reset_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_siul_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_swg_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_sysstatus_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_uart_c.obj" \
"./PRC0_Sources/HAL/mpc5643l_wakeup_c.obj" \

C_DEPS += \
./PRC0_Sources/HAL/mpc5643l_adc_c.d \
./PRC0_Sources/HAL/mpc5643l_clock_c.d \
./PRC0_Sources/HAL/mpc5643l_ctu_c.d \
./PRC0_Sources/HAL/mpc5643l_dspi_c.d \
./PRC0_Sources/HAL/mpc5643l_ecsm_c.d \
./PRC0_Sources/HAL/mpc5643l_etimer_c.d \
./PRC0_Sources/HAL/mpc5643l_fccu_c.d \
./PRC0_Sources/HAL/mpc5643l_flexpwm_c.d \
./PRC0_Sources/HAL/mpc5643l_hal_c.d \
./PRC0_Sources/HAL/mpc5643l_interrupts_c.d \
./PRC0_Sources/HAL/mpc5643l_mode_c.d \
./PRC0_Sources/HAL/mpc5643l_pit_c.d \
./PRC0_Sources/HAL/mpc5643l_power_c.d \
./PRC0_Sources/HAL/mpc5643l_reset_c.d \
./PRC0_Sources/HAL/mpc5643l_siul_c.d \
./PRC0_Sources/HAL/mpc5643l_swg_c.d \
./PRC0_Sources/HAL/mpc5643l_sysstatus_c.d \
./PRC0_Sources/HAL/mpc5643l_uart_c.d \
./PRC0_Sources/HAL/mpc5643l_wakeup_c.d \

OBJS_OS_FORMAT += \
./PRC0_Sources/HAL/mpc5643l_adc_c.obj \
./PRC0_Sources/HAL/mpc5643l_clock_c.obj \
./PRC0_Sources/HAL/mpc5643l_ctu_c.obj \
./PRC0_Sources/HAL/mpc5643l_dspi_c.obj \
./PRC0_Sources/HAL/mpc5643l_ecsm_c.obj \
./PRC0_Sources/HAL/mpc5643l_etimer_c.obj \
./PRC0_Sources/HAL/mpc5643l_fccu_c.obj \
./PRC0_Sources/HAL/mpc5643l_flexpwm_c.obj \
./PRC0_Sources/HAL/mpc5643l_hal_c.obj \
./PRC0_Sources/HAL/mpc5643l_interrupts_c.obj \
./PRC0_Sources/HAL/mpc5643l_mode_c.obj \
./PRC0_Sources/HAL/mpc5643l_pit_c.obj \
./PRC0_Sources/HAL/mpc5643l_power_c.obj \
./PRC0_Sources/HAL/mpc5643l_reset_c.obj \
./PRC0_Sources/HAL/mpc5643l_siul_c.obj \
./PRC0_Sources/HAL/mpc5643l_swg_c.obj \
./PRC0_Sources/HAL/mpc5643l_sysstatus_c.obj \
./PRC0_Sources/HAL/mpc5643l_uart_c.obj \
./PRC0_Sources/HAL/mpc5643l_wakeup_c.obj \

C_DEPS_QUOTED += \
"./PRC0_Sources/HAL/mpc5643l_adc_c.d" \
"./PRC0_Sources/HAL/mpc5643l_clock_c.d" \
"./PRC0_Sources/HAL/mpc5643l_ctu_c.d" \
"./PRC0_Sources/HAL/mpc5643l_dspi_c.d" \
"./PRC0_Sources/HAL/mpc5643l_ecsm_c.d" \
"./PRC0_Sources/HAL/mpc5643l_etimer_c.d" \
"./PRC0_Sources/HAL/mpc5643l_fccu_c.d" \
"./PRC0_Sources/HAL/mpc5643l_flexpwm_c.d" \
"./PRC0_Sources/HAL/mpc5643l_hal_c.d" \
"./PRC0_Sources/HAL/mpc5643l_interrupts_c.d" \
"./PRC0_Sources/HAL/mpc5643l_mode_c.d" \
"./PRC0_Sources/HAL/mpc5643l_pit_c.d" \
"./PRC0_Sources/HAL/mpc5643l_power_c.d" \
"./PRC0_Sources/HAL/mpc5643l_reset_c.d" \
"./PRC0_Sources/HAL/mpc5643l_siul_c.d" \
"./PRC0_Sources/HAL/mpc5643l_swg_c.d" \
"./PRC0_Sources/HAL/mpc5643l_sysstatus_c.d" \
"./PRC0_Sources/HAL/mpc5643l_uart_c.d" \
"./PRC0_Sources/HAL/mpc5643l_wakeup_c.d" \


# Each subdirectory must supply rules for building sources it contributes
PRC0_Sources/HAL/mpc5643l_adc_c.obj: ../PRC0_Sources/HAL/mpc5643l_adc.c
	@echo 'Building file: $<'
	@echo 'Executing target #21 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_adc.args" -o "PRC0_Sources/HAL/mpc5643l_adc_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/%.d: ../PRC0_Sources/HAL/%.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

PRC0_Sources/HAL/mpc5643l_clock_c.obj: ../PRC0_Sources/HAL/mpc5643l_clock.c
	@echo 'Building file: $<'
	@echo 'Executing target #22 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_clock.args" -o "PRC0_Sources/HAL/mpc5643l_clock_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_ctu_c.obj: ../PRC0_Sources/HAL/mpc5643l_ctu.c
	@echo 'Building file: $<'
	@echo 'Executing target #23 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_ctu.args" -o "PRC0_Sources/HAL/mpc5643l_ctu_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_dspi_c.obj: ../PRC0_Sources/HAL/mpc5643l_dspi.c
	@echo 'Building file: $<'
	@echo 'Executing target #24 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_dspi.args" -o "PRC0_Sources/HAL/mpc5643l_dspi_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_ecsm_c.obj: ../PRC0_Sources/HAL/mpc5643l_ecsm.c
	@echo 'Building file: $<'
	@echo 'Executing target #25 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_ecsm.args" -o "PRC0_Sources/HAL/mpc5643l_ecsm_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_etimer_c.obj: ../PRC0_Sources/HAL/mpc5643l_etimer.c
	@echo 'Building file: $<'
	@echo 'Executing target #26 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_etimer.args" -o "PRC0_Sources/HAL/mpc5643l_etimer_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_fccu_c.obj: ../PRC0_Sources/HAL/mpc5643l_fccu.c
	@echo 'Building file: $<'
	@echo 'Executing target #27 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_fccu.args" -o "PRC0_Sources/HAL/mpc5643l_fccu_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_flexpwm_c.obj: ../PRC0_Sources/HAL/mpc5643l_flexpwm.c
	@echo 'Building file: $<'
	@echo 'Executing target #28 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_flexpwm.args" -o "PRC0_Sources/HAL/mpc5643l_flexpwm_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_hal_c.obj: ../PRC0_Sources/HAL/mpc5643l_hal.c
	@echo 'Building file: $<'
	@echo 'Executing target #29 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_hal.args" -o "PRC0_Sources/HAL/mpc5643l_hal_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_interrupts_c.obj: ../PRC0_Sources/HAL/mpc5643l_interrupts.c
	@echo 'Building file: $<'
	@echo 'Executing target #30 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_interrupts.args" -o "PRC0_Sources/HAL/mpc5643l_interrupts_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_mode_c.obj: ../PRC0_Sources/HAL/mpc5643l_mode.c
	@echo 'Building file: $<'
	@echo 'Executing target #31 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_mode.args" -o "PRC0_Sources/HAL/mpc5643l_mode_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_pit_c.obj: ../PRC0_Sources/HAL/mpc5643l_pit.c
	@echo 'Building file: $<'
	@echo 'Executing target #32 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_pit.args" -o "PRC0_Sources/HAL/mpc5643l_pit_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_power_c.obj: ../PRC0_Sources/HAL/mpc5643l_power.c
	@echo 'Building file: $<'
	@echo 'Executing target #33 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_power.args" -o "PRC0_Sources/HAL/mpc5643l_power_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_reset_c.obj: ../PRC0_Sources/HAL/mpc5643l_reset.c
	@echo 'Building file: $<'
	@echo 'Executing target #34 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_reset.args" -o "PRC0_Sources/HAL/mpc5643l_reset_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_siul_c.obj: ../PRC0_Sources/HAL/mpc5643l_siul.c
	@echo 'Building file: $<'
	@echo 'Executing target #35 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_siul.args" -o "PRC0_Sources/HAL/mpc5643l_siul_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_swg_c.obj: ../PRC0_Sources/HAL/mpc5643l_swg.c
	@echo 'Building file: $<'
	@echo 'Executing target #36 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_swg.args" -o "PRC0_Sources/HAL/mpc5643l_swg_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_sysstatus_c.obj: ../PRC0_Sources/HAL/mpc5643l_sysstatus.c
	@echo 'Building file: $<'
	@echo 'Executing target #37 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_sysstatus.args" -o "PRC0_Sources/HAL/mpc5643l_sysstatus_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_uart_c.obj: ../PRC0_Sources/HAL/mpc5643l_uart.c
	@echo 'Building file: $<'
	@echo 'Executing target #38 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_uart.args" -o "PRC0_Sources/HAL/mpc5643l_uart_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/HAL/mpc5643l_wakeup_c.obj: ../PRC0_Sources/HAL/mpc5643l_wakeup.c
	@echo 'Building file: $<'
	@echo 'Executing target #39 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/HAL/mpc5643l_wakeup.args" -o "PRC0_Sources/HAL/mpc5643l_wakeup_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '


