################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../PRC0_Sources/drivers/MC33937A.c" \
"../PRC0_Sources/drivers/button.c" \
"../PRC0_Sources/drivers/control.c" \
"../PRC0_Sources/drivers/filter.c" \
"../PRC0_Sources/drivers/math_transforms.c" \
"../PRC0_Sources/drivers/math_trigo.c" \
"../PRC0_Sources/drivers/pmsm_currentVoltage.c" \
"../PRC0_Sources/drivers/pmsm_motors.c" \
"../PRC0_Sources/drivers/pmsm_posSensors.c" \

C_SRCS += \
../PRC0_Sources/drivers/MC33937A.c \
../PRC0_Sources/drivers/button.c \
../PRC0_Sources/drivers/control.c \
../PRC0_Sources/drivers/filter.c \
../PRC0_Sources/drivers/math_transforms.c \
../PRC0_Sources/drivers/math_trigo.c \
../PRC0_Sources/drivers/pmsm_currentVoltage.c \
../PRC0_Sources/drivers/pmsm_motors.c \
../PRC0_Sources/drivers/pmsm_posSensors.c \

OBJS += \
./PRC0_Sources/drivers/MC33937A_c.obj \
./PRC0_Sources/drivers/button_c.obj \
./PRC0_Sources/drivers/control_c.obj \
./PRC0_Sources/drivers/filter_c.obj \
./PRC0_Sources/drivers/math_transforms_c.obj \
./PRC0_Sources/drivers/math_trigo_c.obj \
./PRC0_Sources/drivers/pmsm_currentVoltage_c.obj \
./PRC0_Sources/drivers/pmsm_motors_c.obj \
./PRC0_Sources/drivers/pmsm_posSensors_c.obj \

OBJS_QUOTED += \
"./PRC0_Sources/drivers/MC33937A_c.obj" \
"./PRC0_Sources/drivers/button_c.obj" \
"./PRC0_Sources/drivers/control_c.obj" \
"./PRC0_Sources/drivers/filter_c.obj" \
"./PRC0_Sources/drivers/math_transforms_c.obj" \
"./PRC0_Sources/drivers/math_trigo_c.obj" \
"./PRC0_Sources/drivers/pmsm_currentVoltage_c.obj" \
"./PRC0_Sources/drivers/pmsm_motors_c.obj" \
"./PRC0_Sources/drivers/pmsm_posSensors_c.obj" \

C_DEPS += \
./PRC0_Sources/drivers/MC33937A_c.d \
./PRC0_Sources/drivers/button_c.d \
./PRC0_Sources/drivers/control_c.d \
./PRC0_Sources/drivers/filter_c.d \
./PRC0_Sources/drivers/math_transforms_c.d \
./PRC0_Sources/drivers/math_trigo_c.d \
./PRC0_Sources/drivers/pmsm_currentVoltage_c.d \
./PRC0_Sources/drivers/pmsm_motors_c.d \
./PRC0_Sources/drivers/pmsm_posSensors_c.d \

OBJS_OS_FORMAT += \
./PRC0_Sources/drivers/MC33937A_c.obj \
./PRC0_Sources/drivers/button_c.obj \
./PRC0_Sources/drivers/control_c.obj \
./PRC0_Sources/drivers/filter_c.obj \
./PRC0_Sources/drivers/math_transforms_c.obj \
./PRC0_Sources/drivers/math_trigo_c.obj \
./PRC0_Sources/drivers/pmsm_currentVoltage_c.obj \
./PRC0_Sources/drivers/pmsm_motors_c.obj \
./PRC0_Sources/drivers/pmsm_posSensors_c.obj \

C_DEPS_QUOTED += \
"./PRC0_Sources/drivers/MC33937A_c.d" \
"./PRC0_Sources/drivers/button_c.d" \
"./PRC0_Sources/drivers/control_c.d" \
"./PRC0_Sources/drivers/filter_c.d" \
"./PRC0_Sources/drivers/math_transforms_c.d" \
"./PRC0_Sources/drivers/math_trigo_c.d" \
"./PRC0_Sources/drivers/pmsm_currentVoltage_c.d" \
"./PRC0_Sources/drivers/pmsm_motors_c.d" \
"./PRC0_Sources/drivers/pmsm_posSensors_c.d" \


# Each subdirectory must supply rules for building sources it contributes
PRC0_Sources/drivers/MC33937A_c.obj: ../PRC0_Sources/drivers/MC33937A.c
	@echo 'Building file: $<'
	@echo 'Executing target #12 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/drivers/MC33937A.args" -o "PRC0_Sources/drivers/MC33937A_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/drivers/%.d: ../PRC0_Sources/drivers/%.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

PRC0_Sources/drivers/button_c.obj: ../PRC0_Sources/drivers/button.c
	@echo 'Building file: $<'
	@echo 'Executing target #13 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/drivers/button.args" -o "PRC0_Sources/drivers/button_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/drivers/control_c.obj: ../PRC0_Sources/drivers/control.c
	@echo 'Building file: $<'
	@echo 'Executing target #14 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/drivers/control.args" -o "PRC0_Sources/drivers/control_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/drivers/filter_c.obj: ../PRC0_Sources/drivers/filter.c
	@echo 'Building file: $<'
	@echo 'Executing target #15 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/drivers/filter.args" -o "PRC0_Sources/drivers/filter_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/drivers/math_transforms_c.obj: ../PRC0_Sources/drivers/math_transforms.c
	@echo 'Building file: $<'
	@echo 'Executing target #16 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/drivers/math_transforms.args" -o "PRC0_Sources/drivers/math_transforms_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/drivers/math_trigo_c.obj: ../PRC0_Sources/drivers/math_trigo.c
	@echo 'Building file: $<'
	@echo 'Executing target #17 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/drivers/math_trigo.args" -o "PRC0_Sources/drivers/math_trigo_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/drivers/pmsm_currentVoltage_c.obj: ../PRC0_Sources/drivers/pmsm_currentVoltage.c
	@echo 'Building file: $<'
	@echo 'Executing target #18 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/drivers/pmsm_currentVoltage.args" -o "PRC0_Sources/drivers/pmsm_currentVoltage_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/drivers/pmsm_motors_c.obj: ../PRC0_Sources/drivers/pmsm_motors.c
	@echo 'Building file: $<'
	@echo 'Executing target #19 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/drivers/pmsm_motors.args" -o "PRC0_Sources/drivers/pmsm_motors_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

PRC0_Sources/drivers/pmsm_posSensors_c.obj: ../PRC0_Sources/drivers/pmsm_posSensors.c
	@echo 'Building file: $<'
	@echo 'Executing target #20 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"PRC0_Sources/drivers/pmsm_posSensors.args" -o "PRC0_Sources/drivers/pmsm_posSensors_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '


