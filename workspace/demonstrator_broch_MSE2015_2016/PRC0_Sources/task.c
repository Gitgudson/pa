/******************************************************************************
*
* Master semester project of Jan Huber
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* 	Created on			: 2019 Apr 16
*	Last modification	: 2019 May 5
* 	Author				: Jan Huber
*/

#include <typedefs.h>
#include "task.h"

#include "HAL/mpc5643l_hal.h"
#include "HAL/mpc5643l_clock.h"
#include "HAL/mpc5643l_ecsm.h"
#include "HAL/mpc5643l_siul.h"


void freq_meas_task(void*)
{
	int32_t clockErr = 0;
	
	clock_freq_meter_start();
	clockErr = clock_freq_meter_check();
	
	if(0 != clockErr)
	{
		mode_sw_error();
	}
	else{}
}

void nce_injection_task(void*)
{
	ecsm_nce_gen();
}

void obe_injection_task(void*)
{
	ecsm_obe_gen();
}
