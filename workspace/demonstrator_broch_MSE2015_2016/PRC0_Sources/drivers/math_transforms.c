/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2016 Jan 02
 *	Last modification : 2016 Jan 02
 *      Author        : Nicolas Broch
 */

#include "drivers/math_transforms.h"

void trans_clarke(const Pmsm_abc_float_s *abc, Pmsm_alphaBeta_s *alphaBeta)
{
	/*====================================================
	  Compute matrix transform
	====================================================*/
	alphaBeta->alpha = (0.667F) * abc->a - (0.333F) * abc->b - (0.333F) * abc->c;
	alphaBeta->beta  = (0.5774F) * abc->b - (0.5774F) * abc->c;
}

void trans_clarke_backward(const Pmsm_alphaBeta_s *alphaBeta, Pmsm_abc_float_s *abc)
{
	/*====================================================
	  Compute matrix transform
	====================================================*/
	abc->a = alphaBeta->alpha;
	abc->b = - (0.5F) * alphaBeta->alpha + (0.866F) * alphaBeta->beta;
	abc->c = - (0.5F) * alphaBeta->alpha - (0.866F) * alphaBeta->beta;
}


void trans_park(const Pmsm_alphaBeta_s *alphaBeta, const float angle, Pmsm_dq_s *dq)
{
	float sinVal = 0.0F;
	float cosVal = 0.0F;
	
	/*====================================================
	  Compute sine and cosine of angle
	====================================================*/
	sin_cos_opt(angle, &sinVal, &cosVal);
	
	/*====================================================
	  Compute matrix transform
	====================================================*/
	dq->d = cosVal  * alphaBeta->alpha + sinVal * alphaBeta->beta;
	dq->q = -sinVal * alphaBeta->alpha + cosVal * alphaBeta->beta;
}

void trans_park_backward(const Pmsm_dq_s *dq, const float angle, Pmsm_alphaBeta_s *alphaBeta)
{
	float sinVal = 0.0F;
	float cosVal = 0.0F;
	
	/*====================================================
	  Compute sine and cosine of angle
	====================================================*/
	sin_cos_opt(angle, &sinVal, &cosVal);
	
	/*====================================================
	  Compute matrix transform
	====================================================*/
	alphaBeta->alpha = cosVal * dq->d - sinVal * dq->q; 
	alphaBeta->beta  = sinVal * dq->d + cosVal * dq->q;
}
