/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Dec 30
 *	Last modification : 2015 Dec 30
 *      Author        : Nicolas Broch
 */

#include "drivers/math_trigo.h"
#include "drivers/math_tables.h"

/***************************************************************************//*!
@brief         Return absolute value of X

@param		   X	input value


@return  		uint32_t	absolute value of X

@details       This function returns the absolute value of X
******************************************************************************/
inline uint32_t absInt(int32_t X);



float atan4(int32_t Y, int32_t X)
{
	uint32_t x_abs = 0;
	uint32_t y_abs = 0;
	uint32_t div = 0;
	float angle = 0;
	
	
	/*====================================================
	  First step, compute the atan with the absolute values
	  of Y and X
	====================================================*/
	x_abs = absInt(X);
	y_abs = absInt(Y); 
	
	/*====================================================
	  Case no value x and y = 0
	====================================================*/
	if(x_abs == 0 && y_abs == 0)
	{
		angle = 0;
	}
	
	/*====================================================
	  Case pi/2 
	====================================================*/
	else if(x_abs == 0) // no need to check if y_abs == , see first if case
	{
		angle = PI_2;
	}
	/*====================================================
	  If y_abs >= x_abs -> res = pi/2 - atan2(y/x)
	====================================================*/
	if(y_abs <= x_abs)
	{
		div = (y_abs << 10 )/x_abs;
		
		angle = lut_atan[div-1];
	}
	
	/*====================================================
	  If y_abs < x_abs -> res = atan2(x/y)
	====================================================*/
	else
	{
		div = (x_abs << MATH_SIZE_LUT_TAN )/y_abs;		
		
		angle = PI_2 - lut_atan[div-1];
	}
	
	/*====================================================
	  4-quadrant check
	====================================================*/
	if(X >= 0 && Y >= 0)
	{
		// nothing to do
	}
	else if(X >= 0 && Y < 0)
	{
		angle = -angle;
	}
	else if(X < 0 && Y >= 0)
	{
		angle = 2*PI_2 - angle;
	}
	else // X and Y < 0
	{
		angle = -2*PI_2 + angle;
	}
	
	return angle;
}

float sin_opt(float angle)
{
	float sinVal = 0.0F;
			
	/*====================================================
	  Use the mathematical property cos(pi/2 - x) = sin(x)
	====================================================*/
	sinVal = cos_opt(PI_2 - angle);
	
	return sinVal;
}

float cos_opt(float angle)
{
	float cosVal = 0.0F;
	float angleModAbs = 0.0F;
	float angleAbs = 0.0F;
	uint32_t indexLut = 0; // index of value of cosine in look-at-table
	
	/*====================================================
	  Take the absolute value of the angle
	====================================================*/
	if(angle < 0.0F)
	{
		angleAbs = -angle;
	}
	else
	{
		angleAbs = angle;
	}
	
	/*====================================================
	  Take the modulo of the angle by PI to avoid 
	  overflow. Normally, this function should be useless,
	  but it can avoid failure following an error of
	  computation
	====================================================*/
	angleModAbs = floatmod(angleAbs, PI);
	
	if(angleModAbs > PI_2)
	{
		angleModAbs -= PI_2;
		
		indexLut = (uint32_t) ((angleModAbs * (MATH_SIZE_LUT_COS-1.F)) / PI_2);
		
		if(indexLut >= MATH_SIZE_LUT_COS_I)
		{
			indexLut = MATH_SIZE_LUT_COS_I - 1;
		}
		// MISRA-C compliance
		else
		{
		}

		cosVal = -lut_cos[MATH_SIZE_LUT_COS_I-indexLut];
	}
	// MISRA-C compliance
	else
	{
		indexLut = (uint32_t) ((angleModAbs * (MATH_SIZE_LUT_COS-1.F)) / PI_2);
		
		if(indexLut >= MATH_SIZE_LUT_COS_I)
		{
			indexLut = MATH_SIZE_LUT_COS_I - 1;
		}
		// MISRA-C compliance
		else
		{
		}

		cosVal = lut_cos[indexLut];
	}
	
	return cosVal;
}

void sin_cos_opt(const float angle, float *sin, float *cos)
{
	float angleMod = 0.0F;
	float anglePI_2 = 0.0F;
	uint32_t indexLut = 0; // index of value of cosine in look-at-table
	
	
	/*====================================================
	  Take the modulo of the angle by PI to avoid 
	  overflow. Normally, this function should be useless,
	  but it can avoid failure following an error of
	  computation
	====================================================*/
	angleMod = floatmod(angle, 2*PI);
	
	if(angleMod < -PI)
	{
		angleMod = 2*PI + angleMod;
	}
	else if(angleMod > PI)
	{
		angleMod = -2*PI + angleMod;
	}
	
	// at this point angleModAbs is between -Pi and Pi
	
	if(angleMod > PI_2)
	{
		anglePI_2 = angleMod - PI_2;
		
		/*====================================================
		  Compute the LUT index
		====================================================*/
		indexLut = (uint32_t) ((anglePI_2 * (MATH_SIZE_LUT_COS-1.F)) / PI_2);
		
		/*====================================================
		  Check if indexLut is out of range and correct it
		====================================================*/
		indexLut = indexLut >= MATH_SIZE_LUT_COS_I ? MATH_SIZE_LUT_COS_I : indexLut;

		/*====================================================
		  return sine and cosine values
		====================================================*/
		*cos = -lut_cos[MATH_SIZE_LUT_COS_I-indexLut-1];
		*sin = lut_cos[indexLut];
	}
	
	else if(angleMod < -PI_2)
	{
		anglePI_2 = -angleMod - PI_2;
		
		/*====================================================
		  Compute the LUT index
		====================================================*/
		indexLut = (uint32_t) ((anglePI_2 * (MATH_SIZE_LUT_COS-1.F)) / PI_2);
		
		/*====================================================
		  Check if indexLut is out of range and correct it
		====================================================*/
		indexLut = indexLut > MATH_SIZE_LUT_COS_I ? MATH_SIZE_LUT_COS_I : indexLut;

		/*====================================================
		  return sine and cosine values
		====================================================*/
		*cos = -lut_cos[MATH_SIZE_LUT_COS_I-indexLut-1];
		*sin = -lut_cos[indexLut];
	}
	else if(angleMod < 0)
	{
		anglePI_2 = -angleMod;
		
		/*====================================================
		  Compute the LUT index
		====================================================*/
		indexLut = (uint32_t) ((anglePI_2 * (MATH_SIZE_LUT_COS-1.F)) / PI_2);
		
		/*====================================================
		  Check if indexLut is out of range and correct it
		====================================================*/
		indexLut = indexLut > MATH_SIZE_LUT_COS_I ? MATH_SIZE_LUT_COS_I : indexLut;

		/*====================================================
		  return sine and cosine values
		====================================================*/
		*cos = lut_cos[indexLut];
		*sin = -lut_cos[MATH_SIZE_LUT_COS_I-indexLut-1];
	}
	else // angleMod <= 0 and angleMod <= PI_2
	{
		anglePI_2 = angleMod;
		
		/*====================================================
		  Compute the LUT index
		====================================================*/
		indexLut = (uint32_t) ((anglePI_2 * (MATH_SIZE_LUT_COS-1.F)) / PI_2);
		
		/*====================================================
		  Check if indexLut is out of range and correct it
		====================================================*/
		indexLut = indexLut > MATH_SIZE_LUT_COS_I ? MATH_SIZE_LUT_COS_I : indexLut;

		/*====================================================
		  return sine and cosine values
		====================================================*/
		*cos = lut_cos[indexLut];
		*sin = lut_cos[MATH_SIZE_LUT_COS_I-indexLut-1];
	}
	
	return;
}


float floatmod(float val, float mod)
{
	float valAbs = 0;
	
	/*====================================================
	  take the absolute value of mod
	====================================================*/
	if(mod < 0)
	{
		mod = -mod;
	}
	else
	{
		
		
	}
	
	/*====================================================
	  Compute modulo for negative values
	====================================================*/
	if(val < 0)
	{
		valAbs = -val;
		
		while(valAbs > mod)
		{
			valAbs -= mod;
		}
		
		valAbs = -valAbs;
	}
	/*====================================================
	  Compute modulo for positives values
	====================================================*/
	else
	{
		valAbs = val;
		
		while(valAbs > mod)
		{
			valAbs -= mod;
		}
	}
	
	return valAbs;
}



inline uint32_t absInt(int32_t X)
{
	uint32_t returnVal = 0;
	
	if(X < 0){
		returnVal = (uint32_t) -X;
	}
	else{
		returnVal = (uint32_t) X;
	}
	
	return returnVal;
}


float fastsqrt(float val)  
{
	  union
	  {
	    int i;
	    float x;
	  } u;
	  u.x = val;
	  u.i = (1<<29) + (u.i >> 1) - (1<<22); 
	  
	  // Two Babylonian Steps (simplified from:)
	  // u.x = 0.5f * (u.x + x/u.x);
	  // u.x = 0.5f * (u.x + x/u.x);
	  u.x =       u.x + val/u.x;
	  u.x = 0.25f*u.x + val/u.x;

	  return u.x;
}
