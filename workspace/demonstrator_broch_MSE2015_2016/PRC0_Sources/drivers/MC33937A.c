/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2016 Jan 03
 *	Last modification : 2016 Jan 25
 *      Author        : Nicolas Broch
*/


#include "drivers/MC33937A.h"


int32_t MC33937A_init(const Pmsm_motor_s *motor)
{
	int32_t  returnVal   = 0;
	uint32_t  returnSPI  = 0;
	uint16_t  commandSPI = 0;
	uint32_t  i          = 0;
	
	/*====================================================
	  Configure pads for Motors' H-bridges
	====================================================*/
	// pad enable
    siul_config_pad(motor->config.mc33937A.padEnable,HAL_NO, HAL_NO, 
					SIUL_MODE_GPIO, SIUL_OUTPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
    // pad interrupt
	siul_config_pad(motor->config.mc33937A.padInterr, HAL_YES, HAL_NO,
				SIUL_MODE_GPIO, SIUL_OUTPUT,
				SIUL_SLOW, SIUL_TYPE_NONE, HAL_YES);
    
	/*====================================================
	  Remove reset of H-bridges
	====================================================*/
    if(siul_write_pad(motor->config.mc33937A.padReset, SIUL_HIGH) != 0)
    {
    	// if an error has occurred, return the error code
    	returnVal = -1;
    }
    // MISRA-C compliance
    else
    {
    	/*====================================================
    	  Wait for h-bridge voltage to stabilise
    	====================================================*/
    	for(i = 0; i < 10000; i++) {}
    	
    	/*====================================================
    	  Clear faults of MC33937 chip
    	====================================================*/
    	MC33937A_clearFaults(motor);
    	

		/*====================================================
		  Disable deadtime control of motor 0 chip
		====================================================*/
		if(MC33937A_setDeadTimeNull(motor) !=0)
		{
			returnVal = -2;
		}
		// MISRA-C compliance
		else
		{
		}
    }
    
    return returnVal;
}

int32_t MC33937A_enable(const Pmsm_motor_s *motor)
{
	int32_t returnVal = 0;
	
	if(siul_write_pad(motor->config.mc33937A.padEnable, SIUL_HIGH) != 0)
	{
		// if an error has occurred, return the error code
		returnVal = -1;
	}
	// MISRA-C compliance
	else
	{
		
	}
	
	return returnVal;
}

int32_t MC33937A_disable(const Pmsm_motor_s *motor)
{
	int32_t returnVal = 0;
	
	if(siul_write_pad(motor->config.mc33937A.padEnable, SIUL_LOW) != 0)
	{
		// if an error has occurred, return the error code
		returnVal = -1;
	}
	// MISRA-C compliance
	else
	{
	}
	
	return returnVal;
}


void MC33937A_clearFaults(const Pmsm_motor_s *motor)
{
	uint16_t  spiCommand = 0;
	
	/*====================================================
	  Clear interrupt flags through CLINT0 and CLINT1 
	  SPI commands
	====================================================*/
	spiCommand = MOT_33937A_CINT0;
	dspi_send(motor->config.mc33937A.dspiCtrl, &spiCommand, 1, 
			  motor->config.mc33937A.dspiCS, CTAR0);
	
	spiCommand = MOT_33937A_CINT1;
	dspi_send(motor->config.mc33937A.dspiCtrl, &spiCommand, 1, 
			  motor->config.mc33937A.dspiCS, CTAR0);
}

int32_t MC33937A_setDeadTimeNull(const Pmsm_motor_s *motor)
{
	int32_t returnVal      = 0;
	uint16_t  spiCommand   = 0; // command to send
	uint32_t  spiReceive32 = 0; // received command in 32 bits
	
	/*====================================================
	  Desactive deadtime control
	====================================================*/
	spiCommand = MOT_33937A_DTOFF;
	dspi_send_receive(motor->config.mc33937A.dspiCtrl, &spiCommand, 1, 
					  motor->config.mc33937A.dspiCS, CTAR0, &spiReceive32);
	
	/*====================================================
	  Send command to receive REG1 register to check
	  whether deadtime control is really disabled
	====================================================*/
	spiCommand = MOT_33937A_REG1R;
	dspi_send_receive(motor->config.mc33937A.dspiCtrl, &spiCommand, 1, 
					  motor->config.mc33937A.dspiCS, CTAR0, &spiReceive32);
	/*====================================================
	  Send the NULL command to read the REG1 register
	====================================================*/
	spiCommand = MOT_33937A_REG1R;
	dspi_send_receive(motor->config.mc33937A.dspiCtrl, &spiCommand, 1, 
			          motor->config.mc33937A.dspiCS, CTAR0, &spiReceive32);
	
	/*====================================================
	  Check if the FULLON bit = 1, which means that
	  the deadtime control is disabled
	====================================================*/
	if((spiReceive32 & MOT_33937A_FULLON) != MOT_33937A_FULLON)
	{
		returnVal = -1;
	}
	
	return returnVal;
}


int32_t MC33937A_checkFault(const Pmsm_motor_s *motor0, const Pmsm_motor_s *motor1)
{
	int32_t returnVal      = 0; // return code of the function
	uint16_t  spiCommand   = 0; // command to send
	uint32_t  spiReceive32 = 0; // received command in 32 bits
	
	/*====================================================
	  Check whether there is a fault in 1 chip
	====================================================*/
	if(siul_read_pad(PCR_D_5) > 0) // interrupt activated
	{
		spiCommand = MOT_33937A_REG0R; // command to read Register 0
		
		/*====================================================
		  Check motor 0 chip - send 2 times the command.
		  First time, to send it and second time, to read
		  the value of the register
		====================================================*/
		dspi_send_receive(motor0->config.mc33937A.dspiCtrl, &spiCommand, 1, 
					motor0->config.mc33937A.dspiCS, CTAR0, &spiReceive32);
		dspi_send_receive(motor0->config.mc33937A.dspiCtrl, &spiCommand, 1, 
				    motor0->config.mc33937A.dspiCS, CTAR0, &spiReceive32);
		/*====================================================
		  Check whether there in error in this chip
		====================================================*/
		if(spiReceive32 > 0)
		{
			returnVal = -1;
		}
		else {} // MISRA-C Compliance
		
		/*====================================================
		  Check motor 1 chip - send 2 times the command.
		  First time, to send it and second time, to read
		  the value of the register
		====================================================*/
		dspi_send_receive(motor1->config.mc33937A.dspiCtrl, &spiCommand, 1, 
					motor1->config.mc33937A.dspiCS, CTAR0, &spiReceive32);
		dspi_send_receive(motor1->config.mc33937A.dspiCtrl, &spiCommand, 1, 
				    motor1->config.mc33937A.dspiCS, CTAR0, &spiReceive32);
		/*====================================================
		  Check whether there in error in this chip
		====================================================*/
		if(spiReceive32 > 0 && returnVal == 0) // only chip 1 has a fault
		{
			returnVal = -2; 
		}
		else if(spiReceive32 > 0 && returnVal == 0) // both chips have a fault
		{
			returnVal = -3; 
		}
		else {} // MISRA-C Compliance
		
	}
	else {} // MISRA-C Compliance
	
	return returnVal;
}
