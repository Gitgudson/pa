/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Dec 30
 *	Last modification : 2016 Jan 02
 *      Author        : Nicolas Broch
 */

#include "drivers/pmsm_motors.h"

/***************************************************************************//*!
@brief         Initialise a motor data structure
******************************************************************************/
void pmsmMotors_initDataStruct(Pmsm_motor_s *data);

/*******************************************************************************
* Global variables
*******************************************************************************/
Pmsm_motor_s motorPMSM0;
Pmsm_motor_s motorPMSM1;
uint32_t nbIterIRQ;


int32_t pmsmMotors_init()
{
	int32_t returnVal = 0;
	Dspi_pcs_e padsCsSpi[2];
	int32_t returnFct = 0;
	
	/*====================================================
	  Init the IRQ counter
	====================================================*/
	nbIterIRQ = 0;
	
	/*====================================================
	  Set motor parameters
	====================================================*/
	// motor 0 brake resistor
	motorPMSM0.config.padBrake = PCR_G_6;
	
	// H-bridge driver MC33937A
	motorPMSM0.config.mc33937A.dspiCtrl  = DSPI0;
	motorPMSM0.config.mc33937A.dspiCS    = PCS0;
	motorPMSM0.config.mc33937A.padEnable = PCR_A_11;
	motorPMSM0.config.mc33937A.padReset  = PCR_A_10;
	motorPMSM0.config.mc33937A.padInterr = PCR_D_5;
	
	// motor 1 brake resistor
	motorPMSM1.config.padBrake = PCR_H_14;
	
	// H-bridge driver MC33937A 
	motorPMSM1.config.mc33937A.dspiCtrl  = DSPI0;
	motorPMSM1.config.mc33937A.dspiCS    = PCS1;
	motorPMSM1.config.mc33937A.padEnable = PCR_A_9;
	motorPMSM1.config.mc33937A.padReset  = PCR_A_10;
	motorPMSM1.config.mc33937A.padInterr = PCR_D_5;
	
	/*====================================================
	  Configure Chip select pads of DSPI modules
	====================================================*/
	padsCsSpi[0] = motorPMSM0.config.mc33937A.dspiCS;
	padsCsSpi[1] = motorPMSM1.config.mc33937A.dspiCS;
	
	/*====================================================
	  Initialise data structures
	====================================================*/
	pmsmMotors_initDataStruct(&motorPMSM0);
	pmsmMotors_initDataStruct(&motorPMSM1);
	

	
	/*====================================================
	  Voltage analogic measures
	====================================================*/
	// motor 0 - ADC0_AN[1]
	siul_config_pad(PCR_B_8, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	
	// motor 0 - ADC0_AN[3]
	siul_config_pad(PCR_C_2, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Current analogic measures
	====================================================*/
	// motor 0 - I_A - ADC01_AN[11]
	siul_config_pad(PCR_B_9, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	// motor 0 - I_B - ADC01_AN[12]
	siul_config_pad(PCR_B_10, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	// motor 0 - I_C - ADC0_AN[2]
	siul_config_pad(PCR_C_1, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);

	
	
	// motor 1 - I_A - ADC01_AN[13]
	siul_config_pad(PCR_B_11, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	// motor 1 - I_B - ADC01_AN[14]
	siul_config_pad(PCR_B_12, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	// motor 1 - I_C - ADC1_AN[2]
	siul_config_pad(PCR_B_15, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Brake resistors
	====================================================*/
	// motor 0
	siul_config_pad(motorPMSM0.config.padBrake,HAL_NO, HAL_NO, 
					SIUL_MODE_GPIO, SIUL_OUTPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	
	// motor 1
	siul_config_pad(motorPMSM1.config.padBrake,HAL_NO, HAL_NO, 
					SIUL_MODE_GPIO, SIUL_OUTPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	/*====================================================
	  Reset pad of MC33937A chips (h-bridge drivers)
	====================================================*/
    siul_config_pad(motorPMSM0.config.mc33937A.padReset,HAL_NO, 
    		        HAL_NO, SIUL_MODE_GPIO, SIUL_OUTPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Initialise system with HAL drivers 
	====================================================*/
    
	/*====================================================
	  Initialise system position and speed sensors
	====================================================*/
    pmsmMotors_posSpeedInit();
    
	// init DSPI0 for MC33937 chips
	dspi_init(motorPMSM0.config.mc33937A.dspiCS,padsCsSpi, PMSM_NUM);
	adc_init();
	etimer_init();
	
	flexpwm_init();
	
	ctu_init();
	
	/*====================================================
	  Clear ADC registers
	====================================================*/
	adc_reset_endOfConv();
	
	/*====================================================
	  Configure first H-bridges. This function must be called
	  before flexpwm_init() for normal initialisation 
	====================================================*/
	returnFct = MC33937A_init(&motorPMSM0);
	if(returnFct != 0)
	{
		returnVal = -1;
	}
	else{
		/*====================================================
		  Configure second H-bridges. This function must be called
		  before flexpwm_init() for normal initialisation 
		====================================================*/
		if(MC33937A_init(&motorPMSM1) != 0)
		{
			returnVal = -1;
		}
		else{
		
			/*====================================================
			  Enable H-bridge of motor 0 
			====================================================*/
			if(MC33937A_enable(&motorPMSM0) != 0)
			{
				returnVal = -1;
			}
			else
			{
				/*====================================================
				  Enable H-bridge of motor 1
				====================================================*/
				if(MC33937A_enable(&motorPMSM1) != 0)
				{
					returnVal = -1;
				}
				else
				{		
					/*====================================================
					  Check whether they are fault in the H-bridge drivers
					====================================================*/
					if(MC33937A_checkFault(&motorPMSM0, &motorPMSM1) != 0)
					{
						returnVal = -1;
					}
					else{
						/*====================================================
						  Initialise current and position control systems
						====================================================*/
						pmsmMotors_controlIInit(&motorPMSM0);
						pmsmMotors_controlIInit(&motorPMSM1);
						pmsmMotors_controlWInit(&motorPMSM0);
						pmsmMotors_controlWInit(&motorPMSM1);
					}
				}
			}
		}
	}

	return returnVal;
}

void pmsmMotors_controlWInit(Pmsm_motor_s *motor)
{
	/*====================================================
	  Reset Data
	====================================================*/
	motor->ctrlSpeed.state = CTRL_OFF;
	motor->ctrlSpeed.speedCommand = 0.0F;
	motor->ctrlSpeed.outputTorque = 0.0F;

	/*====================================================
	  Configure position control
	====================================================*/
	motor->ctrlSpeed.regPI.param.Kp = PMSM_KP_W;
	motor->ctrlSpeed.regPI.param.Gi = PMSM_GI_W;
	motor->ctrlSpeed.regPI.param.Gp_w = 1/PMSM_KP_W;
	motor->ctrlSpeed.regPI.param.Te = PMSM_TE_W;
	
	/*====================================================
	  Set Torque limit
	====================================================*/
	motor->ctrlSpeed.regPI.param.maxOutput = PMSM_MAX_I * PMSM_KT;
	motor->ctrlSpeed.regPI.param.minOutput = 
				-motor->ctrlSpeed.regPI.param.maxOutput;
	
	/*====================================================
	  Init PI controller
	====================================================*/
	control_initPI(&motor->ctrlSpeed.regPI);
}

void pmsmMotors_controlIInit(Pmsm_motor_s *motor)
{
	/*====================================================
	  Reset Data
	====================================================*/
	motor->ctrlCurrent.state = CTRL_OFF;
	motor->ctrlCurrent.outputV.Uabc.a = 0.0F;
	motor->ctrlCurrent.outputV.Uabc.b = 0.0F;
	motor->ctrlCurrent.outputV.Uabc.c = 0.0F;
	
	motor->ctrlCurrent.outputV.UalphaBeta.alpha = 0.0F;
	motor->ctrlCurrent.outputV.UalphaBeta.beta  = 0.0F;
	
	motor->ctrlCurrent.outputV.Udq.d = 0.0F;
	motor->ctrlCurrent.outputV.Udq.q = 0.0F;
	
	motor->ctrlCurrent.cCurrent.d = 0.0F;
	motor->ctrlCurrent.cCurrent.q = 0.0F;
	
	/*====================================================
	  Configure current control - D control
	====================================================*/
	motor->ctrlCurrent.controlD.param.Kp = PMSM_KP_ID;
	motor->ctrlCurrent.controlD.param.Gi = PMSM_GI_ID;
	motor->ctrlCurrent.controlD.param.Gp_w = 1/PMSM_KP_ID;
	motor->ctrlCurrent.controlD.param.Te = PMSM_TE_I;
	
	control_initPI(&motor->ctrlCurrent.controlD);
	
	/*====================================================
	  Configure current control - Q control
	====================================================*/
	motor->ctrlCurrent.controlQ.param.Kp = PMSM_KP_IQ;
	motor->ctrlCurrent.controlQ.param.Gi = PMSM_GI_IQ;
	motor->ctrlCurrent.controlQ.param.Gp_w = 1/PMSM_KP_IQ;
	motor->ctrlCurrent.controlQ.param.Te = PMSM_TE_I;
	
	control_initPI(&motor->ctrlCurrent.controlQ);
}

void pmsmMotors_posSpeedInit()
{
	Control_IIRfilter_param_s filterSpeed;
	
	/*====================================================
	  Configure resolver pads
	====================================================*/
	// excitation resolver 0 
	siul_config_pad(PCR_E_14, HAL_NO, HAL_NO, 
			  	  	SIUL_MODE_ALTER_1, SIUL_OUTPUT,
			  	  	SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	// SIN signal of resolver 0 - ADC0_AN[0]
	siul_config_pad(PCR_B_7, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	// COS signal of resolver 0 - ADC1_AN[0]
	siul_config_pad(PCR_B_13, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	
	// excitation resolver 1
	siul_config_pad(PCR_F_13, HAL_NO, HAL_NO, 
				  	SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	// SIN signal of resolver 1 - ADC0_AN[6]
	siul_config_pad(PCR_E_7, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	// COS signal of resolver 1 - ADC1_AN[6]
	siul_config_pad(PCR_E_12, HAL_NO, HAL_YES, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Configure encoders pads
	====================================================*/
	// motor 0 - Phase A - eTimer0_ETC[0]
	siul_config_pad(PCR_A_0,HAL_NO, HAL_NO, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	siul_config_padSelection(35, 0);
	  
	// motor 0 - Phase B - eTimer0_ETC[1]
	siul_config_pad(PCR_A_1,HAL_NO, HAL_NO, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	siul_config_padSelection(36, 0);
	
	// motor 1 - Phase A - eTimer1_ETC[1]
	siul_config_pad(PCR_C_13,HAL_NO, HAL_NO, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	siul_config_padSelection(10, 0);
	  
	// motor 1 - Phase B - eTimer1_ETC[2]
	siul_config_pad(PCR_C_14,HAL_NO, HAL_NO, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	siul_config_padSelection(11, 1);
	
	/*====================================================
	  Initialise parameters of speed computation
	  filter for both motors
	====================================================*/
	filterSpeed.a0 = PMSM_SPEED_FILT_A0;
	filterSpeed.a1 = PMSM_SPEED_FILT_A1;
	filterSpeed.b0 = PMSM_SPEED_FILT_B0;
	filterSpeed.b1 = PMSM_SPEED_FILT_B1;
	
	/*====================================================
	  Initialise sensors of motor 0
	====================================================*/
	pmsmMotors_initPos(&motorPMSM0.meas.pos, PMSM_ETIMER_ENC0, PMSM_ENC_PRF,
					   PMSM_ADC_RESOL0_SIN, PMSM_ADC_RESOL0_COS,
					   PMSM_FE_I, &filterSpeed);
	
	/*====================================================
	  Initialise sensors of motor 1
	====================================================*/
	pmsmMotors_initPos(&motorPMSM1.meas.pos, PMSM_ETIMER_ENC1, PMSM_ENC_PRF,
					   PMSM_ADC_RESOL1_SIN, PMSM_ADC_RESOL1_COS,
					   PMSM_FE_I, &filterSpeed);
	
}


int32_t pmsmMotors_speedControl(Pmsm_motor_s *motor, float speed)
{
	int32_t returnVal = 0;
	float error = 0.0F;
	
	/*====================================================
	  The current command is a function of the
	  state machine of controller
	====================================================*/
	switch(motor->ctrlSpeed.state){
	case CTRL_ON:
		/*====================================================
		  Compute speed commands
		====================================================*/
		motor->ctrlSpeed.speedCommand = speed;
		
		/*====================================================
		  Execute the control algorithm for position
		====================================================*/
		// compute error with turn modulo
		error = motor->ctrlSpeed.speedCommand - 
				motor->meas.pos.posAndSpeed.speed.value;
		
		
		
		motor->ctrlSpeed.outputTorque =
			  control_algoPI(&motor->ctrlSpeed.regPI, 
					  	  	 error);
		break;
	case CTRL_OFF:
		/*====================================================
		  speed command = 0
		====================================================*/
		motor->ctrlSpeed.speedCommand = 0.0F;
		break;
	default: // error
		returnVal = -1;
	}

	
	return returnVal;
}



int32_t pmsmMotors_torqueControl(Pmsm_motor_s *motor, float torque, 
		                          float Id)
{
	int32_t returnVal = 0;
	float error = 0.0F;
	
	/*====================================================
	  The current command is a function of the
	  state machine of controller
	====================================================*/
	switch(motor->ctrlCurrent.state){
	case CTRL_ON:
		/*====================================================
		  Compute currents commands
		====================================================*/
		motor->ctrlCurrent.cCurrent.d = Id;
		motor->ctrlCurrent.cCurrent.q = torque * PMSM_KT_INV;
		
		/*====================================================
		  Limit q current
		====================================================*/
		if(motor->ctrlCurrent.cCurrent.q > PMSM_MAX_I)
		{
			motor->ctrlCurrent.cCurrent.q = PMSM_MAX_I;
		}
		else if(motor->ctrlCurrent.cCurrent.q < -PMSM_MAX_I)
		{
			motor->ctrlCurrent.cCurrent.q = -PMSM_MAX_I;
		}
		else // do nothing - current is in the admitted range
		{
		}
		
		/*====================================================
		  Set maximal and minimal values for D axis
		====================================================*/
		motor->ctrlCurrent.controlD.param.maxOutput = motor->meas.dcBus.maxV_dq;
		motor->ctrlCurrent.controlD.param.minOutput = -motor->meas.dcBus.maxV_dq;
		
		/*====================================================
		  Execute the control algorithm for D axis
		====================================================*/
		// compute current error
		error =	motor->ctrlCurrent.cCurrent.d -
				motor->meas.currents.Idq.d;

		motor->ctrlCurrent.outputV.Udq.d =
			  control_algoPI(&motor->ctrlCurrent.controlD, 
					  error);
		
		/*====================================================
		 Set maximal and minimal values for Q axis -
		 The geometrical sum of both voltage must be
		 under motor->ctrlCurrent.controlD.param.maxOutput
		====================================================*/
		motor->ctrlCurrent.controlQ.param.maxOutput = fastsqrt(motor->ctrlCurrent.controlD.param.maxOutput *
															   motor->ctrlCurrent.controlD.param.maxOutput -
															   motor->ctrlCurrent.outputV.Udq.d *
															   motor->ctrlCurrent.outputV.Udq.d);
		// to avoid rounding errors
		if(motor->ctrlCurrent.controlQ.param.maxOutput > motor->ctrlCurrent.controlD.param.maxOutput)
		{
			motor->ctrlCurrent.controlQ.param.maxOutput = motor->ctrlCurrent.controlD.param.maxOutput;
		}
		
		motor->ctrlCurrent.controlQ.param.minOutput = -motor->ctrlCurrent.controlQ.param.maxOutput;
		
		/*====================================================
		  Execute the control algorithm for Q axis
		====================================================*/
		// compute current error
		error =	motor->ctrlCurrent.cCurrent.q -
				motor->meas.currents.Idq.q;
		
		motor->ctrlCurrent.outputV.Udq.q =
			  control_algoPI(&motor->ctrlCurrent.controlQ, 
					         error);
		
		break;
	case CTRL_OFF:
		/*====================================================
		  Current command = 0
		====================================================*/
		motor->ctrlCurrent.cCurrent.d = 0.0F;
		motor->ctrlCurrent.cCurrent.q = 0.0F;
		break;
	default: // error
		returnVal = -1;
	}
	
	return returnVal;
}

void pmsmMotors_endIRQ()
{
	/*====================================================
	  Clear ADC registers
	====================================================*/
	adc_reset_endOfConv();
	
	/*====================================================
	  Clear IRQ flag
	====================================================*/
	ctu_IRQ_adc_ack();
	
	/*====================================================
	  Increment the IRQ counter iteration
	====================================================*/
	nbIterIRQ++;
}



void pmsmMotors_initDataStruct(Pmsm_motor_s *data)
{
	
	/*====================================================
	  Reset current measures
	====================================================*/
	data->meas.currents.Iabc.a = 0.0F;
	data->meas.currents.Iabc.b = 0.0F;
	data->meas.currents.Iabc.c = 0.0F;
	
	data->meas.currents.Iabc_raw.a = 0;
	data->meas.currents.Iabc_raw.b = 0;
	data->meas.currents.Iabc_raw.c = 0;
	
	data->meas.currents.IalphaBeta.alpha = 0.0F;
	data->meas.currents.IalphaBeta.beta  = 0.0F;
	
	data->meas.currents.Idq.d = 0.0F;
	data->meas.currents.Idq.q = 0.0F;
	
	/*====================================================
	  Reset DCBus values
	====================================================*/
	data->meas.dcBus.maxV_dq   = 0.0F;
	data->meas.dcBus.vbus_adc  = 0;
	data->meas.dcBus.vbus_volt = 0.0F;
	

}

int32_t pmsmMotors_endOfConv()
{
	int32_t returnVal = 0;
	
	/*====================================================
	  If all expected channels are converted, return 1
	====================================================*/
	if((ADC0.CEOCFR0.R & PMSM_ADC0_CONV_CH) == PMSM_ADC0_CONV_CH &&
	   (ADC1.CEOCFR0.R & PMSM_ADC1_CONV_CH) == PMSM_ADC1_CONV_CH)
	{
		returnVal = 1;
	}
	// MISRA-C compliance
	else
	{
	}
	
	return returnVal;
}


int32_t pmsmMotors_checkDCbusVoltage(Pmsm_motor_s *motor)
{
	int32_t returnVal = 0;
	
	/*====================================================
	  If voltage over DCBUS_MAX_VOLTAGE, turn on braking
	  resistor and return 1
	====================================================*/
	if(motor->meas.dcBus.vbus_volt >= DCBUS_MAX_VOLTAGE)
	{
		returnVal = 1;
		siul_write_fast_pad(motor->config.padBrake, SIUL_HIGH);
	}
	/*====================================================
	  If voltage under DCBUS_MAX_VOLTAGE, turn off braking
	  resistor
	====================================================*/
	else
	{
		siul_write_fast_pad(motor->config.padBrake, SIUL_LOW);
	}

	return returnVal; 
}
