/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2016 Jan 02
 *	Last modification : 2016 Jan 02
 *      Author        : Nicolas Broch
 */


#include "drivers/pmsm_currentVoltage.h"


/***************************************************************************//*!
@brief         Convert floating point values into duty cycles

@param		   commandFloat			voltage commands

@param		   dcBusVolt			voltage of the DC Bus

@param		   maxPWM				duty cycle maximum (minimum = -maxPWM)
@param		   dutyCycles			output, duty cycles for PWM


@return  	   void

@details       This function returns the absolute value of X
******************************************************************************/
inline void pmsmMotors_convert_V_dutyCycle(const Pmsm_abc_float_s *commandFloat, const float dcBusVolt,
										   const uint16_t maxPWM, Flexpwm_dutyCycle_s *dutyCycles);

int32_t pmsmMotors_readCurrents(Pmsm_motor_s* mot0, Pmsm_motor_s* mot1)
{
	int32_t returnVal = 0;
	static float   invMax    = 1/(float)ADC_BI_MAX_VAL;
	
	// structures to save the complete ADC registers
	uint32_t rawIa0 = 0, rawIb0 = 0, rawIc0 = 0;
	uint32_t rawIa1 = 0, rawIb1 = 0, rawIc1 = 0;
	
	/*====================================================
	  Read the measures from ADC 
	====================================================*/	
	rawIa0 = *MOT0_IA;
	rawIb0 = *MOT0_IB;
	rawIc0 = *MOT0_IC;
	
	rawIa1 = *MOT1_IA;
	rawIb1 = *MOT1_IB;
	rawIc1 = *MOT1_IC;
	
	/*====================================================
	  Check if datas are all valid
	====================================================*/
	if((rawIa0 & ADC_VALID) == 0 || 
	   (rawIb0 & ADC_VALID) == 0 || 
	   (rawIc0 & ADC_VALID) == 0 || 
	   (rawIa1 & ADC_VALID) == 0 || 
	   (rawIb1 & ADC_VALID) == 0 || 
	   (rawIc1 & ADC_VALID) == 0)
	{
		returnVal = -1;
	}
	/*====================================================
	  If data valid, copy raw measure into motor data
	  structure
	====================================================*/
	else
	{
		mot0->meas.currents.Iabc_raw.a = (PMSM_CURRENT_MID - (int16_t) (rawIa0 & ADC_CDATA_MASK) 
												+ PMSM_CORR_OFFSET_M0_IA);
		mot0->meas.currents.Iabc_raw.b = (PMSM_CURRENT_MID - (int16_t) (rawIb0 & ADC_CDATA_MASK) 
												+ PMSM_CORR_OFFSET_M0_IB);
		mot0->meas.currents.Iabc_raw.c = (PMSM_CURRENT_MID - (int16_t) (rawIc0 & ADC_CDATA_MASK) 
												+ PMSM_CORR_OFFSET_M0_IC);
		
		mot1->meas.currents.Iabc_raw.a = (PMSM_CURRENT_MID - (int16_t) (rawIa1 & ADC_CDATA_MASK) 
												+ PMSM_CORR_OFFSET_M1_IA);
		mot1->meas.currents.Iabc_raw.b = (PMSM_CURRENT_MID - (int16_t) (rawIb1 & ADC_CDATA_MASK) 
												+ PMSM_CORR_OFFSET_M1_IB);
		mot1->meas.currents.Iabc_raw.c = (PMSM_CURRENT_MID - (int16_t) (rawIc1 & ADC_CDATA_MASK) 
												+ PMSM_CORR_OFFSET_M1_IC);
		
		/*====================================================
		  Transform raw ADC currents in currents in [A]
		====================================================*/
		mot0->meas.currents.Iabc.a =  ((float) mot0->meas.currents.Iabc_raw.a * CURRENT_SENS_MAX)
										* invMax;
		mot0->meas.currents.Iabc.b =  ((float) mot0->meas.currents.Iabc_raw.b * CURRENT_SENS_MAX)
										* invMax;
		mot0->meas.currents.Iabc.c =  ((float) mot0->meas.currents.Iabc_raw.c * CURRENT_SENS_MAX)
										* invMax;
		
		mot1->meas.currents.Iabc.a =  ((float) mot1->meas.currents.Iabc_raw.a * CURRENT_SENS_MAX)
										* invMax;
		mot1->meas.currents.Iabc.b =  ((float) mot1->meas.currents.Iabc_raw.b * CURRENT_SENS_MAX)
										* invMax;
		mot1->meas.currents.Iabc.c =  ((float) mot1->meas.currents.Iabc_raw.c * CURRENT_SENS_MAX)
										* invMax;
		
		

		/*====================================================
		  Transform ABC currents into alpha-beta space
		====================================================*/
		trans_clarke(&mot0->meas.currents.Iabc, &mot0->meas.currents.IalphaBeta);
		trans_clarke(&mot1->meas.currents.Iabc, &mot1->meas.currents.IalphaBeta);
		
		
		/*====================================================
		  Transform alpha-beta space into dq-space
		====================================================*/
		
		trans_park(&mot0->meas.currents.IalphaBeta, mot0->meas.pos.posAndSpeed.position.rotor_angle_k *
				  (float)PMSM_NB_POLES, &mot0->meas.currents.Idq);
		trans_park(&mot1->meas.currents.IalphaBeta, mot1->meas.pos.posAndSpeed.position.rotor_angle_k * 
				  (float)PMSM_NB_POLES, &mot1->meas.currents.Idq);
	}
	
	return returnVal;
}

int32_t pmsmMotors_readVoltage(Pmsm_motor_s* mot0, Pmsm_motor_s* mot1)
{
	int32_t returnVal = 0;
	
	// structures to save the complete ADC registers
	uint32_t rawDCBus0 = 0;
	uint32_t rawDCBus1 = 0;
	
	/*====================================================
	  Read values from ADC
	====================================================*/
	rawDCBus0 = *DCBUS0;
	rawDCBus1 = *DCBUS1;
	
	/*====================================================
	  Check if datas are all valid
	====================================================*/
	if((rawDCBus0 & ADC_VALID) == 0 || 
	   (rawDCBus1 & ADC_VALID) == 0)
	{
		returnVal = -1;
	}
	else
	{
		/*====================================================
		  If data valid, copy raw measure into motor data
		  structure
		====================================================*/
		mot0->meas.dcBus.vbus_adc = (uint16_t) (rawDCBus0 & ADC_CDATA_MASK);
		mot1->meas.dcBus.vbus_adc = (uint16_t) (rawDCBus1 & ADC_CDATA_MASK);
		
		/*====================================================
		  Convert raw values into values in voltage
		====================================================*/
		motorPMSM0.meas.dcBus.vbus_volt  = ((float)((int32_t) motorPMSM0.meas.dcBus.vbus_adc * DCBUS_SENS_SCALE)) 
														* (1/(float)ADC_MAX_VAL);
		motorPMSM1.meas.dcBus.vbus_volt  = ((float)((int32_t) motorPMSM1.meas.dcBus.vbus_adc * DCBUS_SENS_SCALE)) 
														* (1/(float)ADC_MAX_VAL);
		
		// 90% of available DCbus recalculated to phase voltage = 0.90*Ubus/sqrt(3)
		motorPMSM0.meas.dcBus.maxV_dq    = (0.51961F)*(motorPMSM0.meas.dcBus.vbus_volt);
		motorPMSM1.meas.dcBus.maxV_dq    = (0.51961F)*(motorPMSM1.meas.dcBus.vbus_volt);
	}
	
	return returnVal;
}


void pmsmMotors_setVoltage(Pmsm_motor_s *motor0, Pmsm_motor_s *motor1)
{
	Flexpwm_dutyCycle_s dutyCycleM0, dutyCycleM1;
		
	/*====================================================
	  Transform backward from dq to alpha-beta space
	====================================================*/
	
	trans_park_backward(&motor0->ctrlCurrent.outputV.Udq, motor0->meas.pos.posAndSpeed.position.rotor_angle_k * 
						(float)PMSM_NB_POLES, &motor0->ctrlCurrent.outputV.UalphaBeta);
	trans_park_backward(&motor1->ctrlCurrent.outputV.Udq, motor1->meas.pos.posAndSpeed.position.rotor_angle_k * 
						(float)PMSM_NB_POLES, &motor1->ctrlCurrent.outputV.UalphaBeta);

	/*====================================================
	  Transform backward from alpha-beta to abc space
	====================================================*/
	trans_clarke_backward(&motor0->ctrlCurrent.outputV.UalphaBeta, 
			              &motor0->ctrlCurrent.outputV.Uabc);
	trans_clarke_backward(&motor1->ctrlCurrent.outputV.UalphaBeta, 
			              &motor1->ctrlCurrent.outputV.Uabc);
	
	/*====================================================
	  Transform [V] commands into FlexPWM raw values for
	  motor
	====================================================*/
	pmsmMotors_convert_V_dutyCycle(&motor0->ctrlCurrent.outputV.Uabc, motor0->meas.dcBus.vbus_volt,
								   FLEXPWM_DUTY_RANGE-FLEXPWM0_DTCNT0_0, &dutyCycleM0);
	
	pmsmMotors_convert_V_dutyCycle(&motor1->ctrlCurrent.outputV.Uabc, motor1->meas.dcBus.vbus_volt,
								   FLEXPWM_DUTY_RANGE-FLEXPWM1_DTCNT0_0, &dutyCycleM1);
	
	/*====================================================
	  Set duty cycle values in FlexPWM modules
	====================================================*/
	flexpwm_change_dutyCycle(FLEXPWM_MOD0, &dutyCycleM0);
	flexpwm_change_dutyCycle(FLEXPWM_MOD1, &dutyCycleM1);
}


inline void pmsmMotors_convert_V_dutyCycle(const Pmsm_abc_float_s *commandFloat, const float dcBusVolt,
											  const uint16_t maxPWM, Flexpwm_dutyCycle_s *dutyCycles)
{
	uint16_t i           = 0;
	int32_t voltDCycle32 = 0;    // temporary variable to compute PWM value
	float   scalePWM     = 0.0F; // value to multiply the command in float to PWM value
	
	/*====================================================
	  Compute the multiplication factor to convert V in
	  PWM value
	====================================================*/
	scalePWM = 2.0F * (float) maxPWM / dcBusVolt;
	
	
	/*====================================================
	  Compute Ua
	====================================================*/
	voltDCycle32 = (int32_t) (commandFloat->a * scalePWM);
	
	if(voltDCycle32 > maxPWM)
	{
		dutyCycles->sub[0] = (int16_t) maxPWM;
	}
	else if(voltDCycle32 < -maxPWM)
	{
		dutyCycles->sub[0] = (int16_t) -maxPWM;
	}
	else
	{
		dutyCycles->sub[0] = (int16_t) voltDCycle32;
	}
	
	/*====================================================
	  Compute Ub
	====================================================*/
	voltDCycle32 = (int32_t) (commandFloat->b * scalePWM);
	
	if(voltDCycle32 > maxPWM)
	{
		dutyCycles->sub[1] = (int16_t) maxPWM;
	}
	else if(voltDCycle32 < -maxPWM)
	{
		dutyCycles->sub[1] = (int16_t) -maxPWM;
	}
	else
	{
		dutyCycles->sub[1] = (int16_t) voltDCycle32;
	}
	
	/*====================================================
	  Compute Uc
	====================================================*/
	voltDCycle32 = (int32_t) (commandFloat->c * scalePWM);
	
	if(voltDCycle32 > maxPWM)
	{
		dutyCycles->sub[2] = (int16_t) maxPWM;
	}
	else if(voltDCycle32 < -maxPWM)
	{
		dutyCycles->sub[2] = (int16_t) -maxPWM;
	}
	else
	{
		dutyCycles->sub[2] = (int16_t) voltDCycle32;
	}
	
	return;
}
