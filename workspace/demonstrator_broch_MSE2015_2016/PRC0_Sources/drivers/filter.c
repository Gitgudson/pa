/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2016 Jan 14
 *	Last modification : 2016 Jan 14
 *      Author        : Nicolas Broch
 */



#include "drivers/filter.h"

void control_initFilterIIR(Control_IIRfilter_s *filter)
{
	/*====================================================
	  Initialise the internal values
	====================================================*/
	
	filter->data.x_k  = 0.0F;
	filter->data.x_k1 = 0.0F;
	filter->data.y_k  = 0.0F;
	filter->data.y_k1 = 0.0F;
	
	/*====================================================
	  Pre-compute divisions
	====================================================*/
	filter->data.b0a1 = filter->param.b0 / filter->param.a1;
	filter->data.b1a1 = filter->param.b1 / filter->param.a1;
	filter->data.a0a1 = filter->param.a0 / filter->param.a1;
}

float control_filterIIR(Control_IIRfilter_s *filter, const float u_k)
{
	/*====================================================
	  Shift input and output data of filter
	====================================================*/
	filter->data.x_k1 = filter->data.x_k;
	filter->data.x_k  = u_k;
	filter->data.y_k1 = filter->data.y_k;
	
	/*====================================================
	  Compute the filter
	====================================================*/
	filter->data.y_k = filter->data.b1a1 * filter->data.x_k +
					   filter->data.b0a1 * filter->data.x_k1 -
					   filter->data.a0a1 * filter->data.y_k1;
	
	return filter->data.y_k;	 
}
