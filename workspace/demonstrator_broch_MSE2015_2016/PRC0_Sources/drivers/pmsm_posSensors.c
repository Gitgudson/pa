/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2016 Jan 02
 *	Last modification : 2016 Jan 02
 *      Author        : Nicolas Broch
 */

#include "drivers/pmsm_posSensors.h"

/***************************************************************************//*!
@brief         Compute speed of the motor

@return  		void
******************************************************************************/
inline void pmsmMotors_computeSpeed(Pmsm_sensorsPosition_s *mot);

/***************************************************************************//*!
@brief         Check if given values of resolver are null

@return  		int32_t\n
				0 if not null
				1 if null
******************************************************************************/
volatile int32_t pmsmMotors_isRes_null(int16_t adc_sin, int16_t adc_cos);

void pmsmMotors_initPos(Pmsm_sensorsPosition_s* mot,
						const vuint16_t *encETimerReg, const float maxValueEnc,
						const vuint32_t *resADCReg_sin, const vuint32_t *resADCReg_cos,
						const float Fe_pos, const Control_IIRfilter_param_s *paramSpeedFilter)
{
	/*====================================================
	  Initialise position and speed registers
	====================================================*/
	mot->posAndSpeed.position.rotor_angle_k = 0.0F;
	mot->posAndSpeed.position.rotor_angle_k1 = 0.0F;
	mot->posAndSpeed.speed.Fe = Fe_pos;
	mot->posAndSpeed.speed.filter.param = *paramSpeedFilter;
	mot->posAndSpeed.state    = PSMS_POS_FIX; // no sensor used
	
	// initialise speed filter
	control_initFilterIIR(&mot->posAndSpeed.speed.filter);
	
	/*====================================================
	  Initialise encoder registers
	====================================================*/
	mot->encoder.param.eTimerReg = (vuint16_t *) encETimerReg;
	mot->encoder.param.maxValueEnc = maxValueEnc;
	
	mot->encoder.meas.pos_inc = 0;
	mot->encoder.meas.pos_rad = 0.0F;
	
	/*====================================================
	  Initialise resolver registers
	====================================================*/
	mot->resolver.param.ADCReg_cos = (vuint32_t *)resADCReg_cos;
	mot->resolver.param.ADCReg_sin = (vuint32_t *)resADCReg_sin;
	
	mot->resolver.param.angle0 = 0.0F;
	
	mot->resolver.meas.adc_cos = 0;
	mot->resolver.meas.adc_sin = 0;
	mot->resolver.meas.pos_rad = 0.0F;
	mot->resolver.meas.pos_rad_raw = 0.0F;
	
	/*====================================================
	  Initialise speed computation
	====================================================*/
	
}

void pmsmMotors_read2Enc(Pmsm_sensorsPosition_s* mot0, Pmsm_sensorsPosition_s* mot1)
{
	/*====================================================
	  Copy the position from eTimers registers 
	====================================================*/
	mot0->encoder.meas.pos_inc = (int16_t)*mot0->encoder.param.eTimerReg;
	mot1->encoder.meas.pos_inc = (int16_t)*mot1->encoder.param.eTimerReg;
	
	/*====================================================
	  Compute position in radian
	====================================================*/
	mot0->encoder.meas.pos_rad = ((float) mot0->encoder.meas.pos_inc / 
										  mot0->encoder.param.maxValueEnc) * PI;
	mot1->encoder.meas.pos_rad = ((float) mot1->encoder.meas.pos_inc / 
										  mot1->encoder.param.maxValueEnc) * PI;
}

void pmsmMotors_readEnc(Pmsm_sensorsPosition_s* mot)
{
	/*====================================================
	  Copy the position from eTimers registers 
	====================================================*/
	mot->encoder.meas.pos_inc = (int16_t)*mot->encoder.param.eTimerReg;
	
	/*====================================================
	  Compute position in radian
	====================================================*/
	mot->encoder.meas.pos_rad = ((float) mot->encoder.meas.pos_inc / 
										 mot->encoder.param.maxValueEnc) * PI;
}


int32_t pmsmMotors_readResolv(Pmsm_sensorsPosition_s* mot)
{
	int32_t returnVal = 0;
	
	// structures to save the complete ADC registers
	uint32_t rawSin = 0, rawCos = 0;

	
	/*====================================================
	  Read the measures from ADC 
	====================================================*/
	rawSin = *mot->resolver.param.ADCReg_sin;
	rawCos = *mot->resolver.param.ADCReg_cos;

	
	/*====================================================
	  Check if data are all valid
	====================================================*/
	if((rawSin & ADC_VALID) == 0 || 
	   (rawCos & ADC_VALID) == 0)
	{
		returnVal = -1;
	}
	
	/*====================================================
	  If data valid, copy raw measure into motor data
	  structure
	====================================================*/
	else
	{
		mot->resolver.meas.adc_sin = ((int16_t) (rawSin & ADC_CDATA_MASK) - PMSM_RES_MID);
		mot->resolver.meas.adc_cos = ((int16_t) (rawCos & ADC_CDATA_MASK) - PMSM_RES_MID);		
		
		/*====================================================
		  Check if measure of SIN and COS signals are null
		====================================================*/
		if(pmsmMotors_isRes_null(mot->resolver.meas.adc_sin, mot->resolver.meas.adc_cos))
		{
			returnVal = -2;
		}
		else{		
			/*====================================================
			  Compute the position in radian
			====================================================*/
			mot->resolver.meas.pos_rad_raw = atan4(mot->resolver.meas.adc_sin, 
					                               mot->resolver.meas.adc_cos);
			mot->resolver.meas.pos_rad     = (float) floatmod(mot->resolver.meas.pos_rad_raw - 
					                                          mot->resolver.param.angle0 + PI, 2*PI) - PI;
		}
	}
	
	return returnVal;
}


void pmsmMotors_setAngleSys(Pmsm_sensorsPosition_s* mot, float angle_mot)
{
	/*====================================================
	  Configure the register of eTimers to set up the
	  current angle
	====================================================*/
	*mot->encoder.param.eTimerReg = (uint16_t)((int16_t)((angle_mot * mot->encoder.param.maxValueEnc) / PI));
	
	/*====================================================
	  Do a new measure to set the 0
	====================================================*/
	pmsmMotors_readEnc(mot);
	
	/*====================================================
	  Configure the current value to be angle angle_motX
	====================================================*/
	mot->resolver.param.angle0 = floatmod(mot->resolver.meas.pos_rad_raw -  angle_mot + PI, 2 * PI) - PI;
	mot->resolver.meas.pos_rad = angle_mot;
}



int32_t pmsmMotors_setPositionSpeed(Pmsm_sensorsPosition_s *mot)
{
	int32_t returnVal = 0;
	float posErrSens = 0.0F;
	
	/*====================================================
	  Shift position of rotor_angle_k in rotor_anglek1
	====================================================*/
	mot->posAndSpeed.position.rotor_angle_k1 = 
			mot->posAndSpeed.position.rotor_angle_k;
	
	/*====================================================
	  Configure position regarding selection of sensor
	====================================================*/
	switch(mot->posAndSpeed.state){
	case PMSM_POS_RESOLVER:
		mot->posAndSpeed.position.rotor_angle_k = 
				mot->resolver.meas.pos_rad;
		break;
	case PMSM_POS_ENCODER:
		mot->posAndSpeed.position.rotor_angle_k = 
				mot->encoder.meas.pos_rad;
		break;
	case PSMS_POS_FIX: // don't touch the position
		break;
	default:
		returnVal = -1;
	}
	
	/*====================================================
	  Compute motor speed
	====================================================*/
	pmsmMotors_computeSpeed(mot);
	
	/*====================================================
	  If there is no error and the current position
	  is computed from sensors, checks whether there
	  is an error between sensors.
	====================================================*/
	if(mot->posAndSpeed.state != PSMS_POS_FIX && 
	   returnVal == 0)
	{
		/*====================================================
		  Check sensor difference if the system is homed
		====================================================*/
		posErrSens = mot->encoder.meas.pos_rad - mot->resolver.meas.pos_rad;
		if(posErrSens > 6)
		{
			posErrSens -= 2.0F*PI;
		}
		else if(posErrSens < -6)
		{
			posErrSens += 2.0F*PI;
		}			
		
		if(posErrSens > PMSM_LIM_ERR_SENS ||
		   posErrSens < -PMSM_LIM_ERR_SENS)
		{
			returnVal = -2;
		}
	}

	return returnVal; 
}

inline void pmsmMotors_computeSpeed(Pmsm_sensorsPosition_s *mot)
{
	float speedNonFilt;
	float diffAngle;
	
	/*====================================================
	  Compute speed
	====================================================*/
	diffAngle = mot->posAndSpeed.position.rotor_angle_k -
			mot->posAndSpeed.position.rotor_angle_k1;
	
	if(diffAngle > PI)
	{
		diffAngle -= 2.0F*PI;
	}
	else if(diffAngle < -PI)
	{
		diffAngle += 2.0F*PI;
	}
	
	speedNonFilt = diffAngle * mot->posAndSpeed.speed.Fe;
	
	/*====================================================
	  Filter speed
	====================================================*/
	mot->posAndSpeed.speed.value = control_filterIIR(&mot->posAndSpeed.speed.filter, 
													speedNonFilt);
}


volatile int32_t pmsmMotors_isRes_null(int16_t adc_sin, int16_t adc_cos)
{
	int32_t returnVal = 0;
	
	if((-PMSM_TOL_NULL_RES  <= adc_sin && adc_sin <= PMSM_TOL_NULL_RES) &&
       (-PMSM_TOL_NULL_RES  <= adc_cos && adc_cos <= PMSM_TOL_NULL_RES)	)
		returnVal = 1;
	
	return returnVal;
}
