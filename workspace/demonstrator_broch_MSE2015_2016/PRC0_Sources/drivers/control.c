/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2016 Jan 11
 *	Last modification : 2016 Jan 11
 *      Author        : Nicolas Broch
 */



#include "drivers/control.h"

void control_initPI(Control_piControl_s *piData)
{
	/*====================================================
	  Initialise the internal values
	====================================================*/
	piData->data.integrator = 0.0F;
	piData->data.command    = 0.0F;
	piData->data.command_prime = 0.0F;
	
	/*====================================================
	  Pre-compute Te * Gi
	====================================================*/
	piData->data.teGi = piData->param.Gi * piData->param.Te;
}

float control_algoPI(Control_piControl_s *piData, const float error)
{
	float eMulKp;
	float error_prime;
	
	/*====================================================
	  Compute error
	====================================================*/
	piData->data.error = error;
	error_prime = piData->data.error - 
			     (piData->data.command_prime - piData->data.command) 
			     * piData->param.Gp_w;
	
	/*====================================================
	  Do the integral with limits
	====================================================*/
	eMulKp = error_prime * piData->data.teGi;
	
	if(eMulKp >= 0 && piData->data.integrator < CONTROL_MAX_INT ||
	   eMulKp < 0 && piData->data.integrator > CONTROL_MIN_INT)
	{
		piData->data.integrator += eMulKp;
	}
	// Limit the value in buffer - no integration
	else
	{
		
	}
	
	/*====================================================
	  Control algorithm PI
	====================================================*/
	piData->data.command_prime = piData->param.Kp * (piData->data.error 
			                                         + piData->data.integrator);
	if(piData->data.command_prime > piData->param.maxOutput)
	{
		piData->data.command = piData->param.maxOutput;
	}
	else if(piData->data.command_prime < piData->param.minOutput)
	{
		piData->data.command = piData->param.minOutput;
	}
	else
	{
		piData->data.command = piData->data.command_prime;
	}
	
	return piData->data.command;	 
}
