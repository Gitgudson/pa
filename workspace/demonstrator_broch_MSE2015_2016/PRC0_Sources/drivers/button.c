/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2016 Jan 25
 *	Last modification : 2016 Jan 25
 *      Author        : Nicolas Broch
 */



#include "drivers/button.h"

void button_init(Button_s *button, const Siul_pcr_e pad, const uint32_t offState)
{
	/*====================================================
	  initialise data structure
	====================================================*/
	button->pad = pad;
	
	if(offState == SIUL_LOW){
		button->state = 0;
		button->stateCounter = 0;
	}
	else
	{
		button->state = 1;
		button->stateCounter = BUTTON_FILT_ITE;
	}
	
	/*====================================================
	  initialise the gpio
	====================================================*/
	siul_config_pad(button->pad,HAL_NO, HAL_NO, 
					SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
}

uint32_t button_state(Button_s *button)
{
	int32_t stateSwitch = 0;
	
	stateSwitch = siul_read_pad(button->pad);
		
		/*====================================================
		  The switch on/off use a "counter" filter. This
		  counter increment each time the switch is on and
		  decrement each time the switch is off. When the
		  counter = 0, the state is switched off. When
		  the counter = BUTTON_FILT_ITE, the state is switched on.
		====================================================*/
		if(button->stateCounter < BUTTON_FILT_ITE && stateSwitch > 0)
		{
			button->stateCounter++;
			
			if(button->stateCounter >= BUTTON_FILT_ITE)
			{
				button->state = 1; // switch in on state
			}
			else{} // MISRA-C compliance
		}
		
		else if(button->stateCounter > 0 && stateSwitch == 0)
		{
			button->stateCounter--;
			
			if(button->stateCounter <= 0)
			{
				button->state = 0; // switch in off state
			}
			else{} // MISRA-C compliance
		}
		else {} // MISRA-C compliance
		
		return button->state;
}
