/******************************************************************************
*
* Master semester project of Jan Huber
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* 	Created on			: 2019 Apr 7
*	Last modification	: 2019 Apr 7
* 	Author				: Jan Huber
*/

#include "HAL/mpc5643l_pit.h"
#include "IntcInterrupts.h"
//#include "HAL/mpc5643l_mode.h"


//-----------------------------------------------------------------------------
// Definition of external variable for system timer
//-----------------------------------------------------------------------------
uint8_t systickFlag = 0;


void pit_init()
{
	/*====================================================
	  Initialise periodic interrupt timer for the scheduler
	 ====================================================*/
	//Periodic Interrupt Timer (PIT)
	//MPC5643LRM.pdf, Chapter 36, page 1149
	
	INTC_InstallINTCInterruptHandler(pit_IRQ_SYSTICK, PIT_IRQ_SOURCE, 10);
	
	PIT_RTI.PITMCR.R &= ~PIT_MCR_MDIS;	// Enable PIT timers
	PIT_RTI.PITMCR.R |= PIT_MCR_FRZ;	// Freeze PIT in debug mode
	PIT_RTI.LDVAL0.R  = PIT_LDVAL0;		// Timer setup for 500'000 cycles (499'999)
	PIT_RTI.TFLG0.R  |= PIT_TFLG_TIF;	// Clear interrupt if any present
	PIT_RTI.TCTRL0.R |= PIT_TCTRL_TIE;	// Enable interrupt
	PIT_RTI.TCTRL0.R |= PIT_TCTRL_TEN;	// Start timer
	
	systickFlag = 0;
}


void pit_IRQ_SYSTICK()
{
	PIT_RTI.TCTRL0.R |= PIT_TCTRL_TEN;	// Stop timer
	systickFlag = 1;					// Set systick flag
	PIT_RTI.TFLG0.R  |= PIT_TFLG_TIF;	// Clear interrupt
	PIT_RTI.TCTRL0.R |= PIT_TCTRL_TEN;	// Start timer
}


int32_t pit_SWTEST_REGCRC()
{
	return 0;
}
