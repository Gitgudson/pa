/*
/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Nov 02
 *  Last modification : 2015 Nov 02
 *  Author            : Nicolas Broch
 */

#include "HAL/mpc5643l_wakeup.h"
#include "MPC5643L.h"



void wakeup_init()
{
	/*====================================================
	 Configure the wakeup unit and lock the register
	====================================================*/
	WKPU.NCR.R = WAKEUP_NMI_NCR;
}

int32_t wakeup_SWTEST_REGCRC()
{
	
}
