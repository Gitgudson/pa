/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Oct 27
 *	Last modification : 2019 Apr 16
 *      Author        : Nicolas Broch
 *      Author		  : Jan Huber
 */

#include "HAL/mpc5643l_clock.h"
#include "HAL/mpc5643l_mode.h"


void clock_init()
{
	/*====================================================
	  Configure interrupt
	 ====================================================*/
	INTC_InstallINTCInterruptHandler(clock_IRQ_xoscExp, 57, 10);
	
	/*====================================================
	  Set duration of frequency measurement duration
	 ====================================================*/
	CMU0.MDR.R = CLOCK_CMU_MDR;
	
	/*====================================================
	  External oscillator
	 ====================================================*/
	OSC.CTL.R = CLOCK_OSC_CTL;
	
	/*====================================================
	 System Clock
	 ====================================================*/
	CGM.SC_DC0.R  = CLOCK_SC_DC0;
	
    /*====================================================
     External clock (Clockout) configuration
     ====================================================*/
	CGM.OC_EN.R   = CLOCK_OC_EN;
	CGM.OCDS_SC.R = CLOCK_OC_SC;
	
    /*====================================================
     AUX clock selector 0
    	- Motor control clock
     	- SWG clock
     ====================================================*/
	CGM.AC0_SC.R  = CLOCK_MCSWG_SC;
	CGM.AC0_DC0.R = CLOCK_MC_DC;
	CGM.AC0_DC1.R = CLOCK_SWG_DC;
	
	/*====================================================
	 AUX clock selector 1 
	 	 - FlexRay clock
	 ====================================================*/
	CGM.AC1_SC.R  = CLOCK_FR_SC;
	CGM.AC1_DC0.R = CLOCK_FR_DC0;
	
	/*====================================================
	 AUX clock selector 2 
	 	 - FlexCAN clock
	 ====================================================*/
	CGM.AC2_SC.R  = CLOCK_CAN_SC;
	CGM.AC2_DC0.R = CLOCK_CAN_DC0;
	
    /*====================================================
     PLL_0
     ====================================================*/
	CGM.FMPLL[0].CR.R = CLOCK_PLL0_CR;
	CGM.FMPLL[0].MR.R = CLOCK_PLL0_MR;
	CGM.AC3_SC.R      = CLOCK_PLL0_CGM_AUX3;
	
    /*====================================================
     PLL_1
     ====================================================*/
	CGM.FMPLL[1].CR.R = CLOCK_PLL1_CR;
	CGM.FMPLL[1].MR.R = CLOCK_PLL1_MR;
	CGM.AC4_SC.R      = CLOCK_PLL1_CGM_AUX4;
	
	return;
}


void clock_init_CMU()
{
    /*====================================================
     Check if current system clock is PLL0
     ====================================================*/
	
	
    /*====================================================
     Configure CMU_0
     ====================================================*/	
	CMU0.CSR.R      = CLOCK_CMU0_CSR;
	CMU0.HFREFR_A.R = CLOCK_CMU0_HFREFR;
	CMU0.LFREFR_A.R = CLOCK_CMU0_LFREFR;
	
    /*====================================================
     Configure CMU_1
     ====================================================*/
	CMU1.CSR.R      = CLOCK_CMU1_CSR;
	CMU1.HFREFR_A.R = CLOCK_CMU1_HFREFR;
	CMU1.LFREFR_A.R = CLOCK_CMU1_LFREFR;
	
	/*====================================================
	 Configure CMU_2
	 ====================================================*/
	CMU2.CSR.R      = CLOCK_CMU2_CSR;
	CMU2.HFREFR_A.R = CLOCK_CMU2_HFREFR;
	CMU2.LFREFR_A.R = CLOCK_CMU2_LFREFR;
}


void clock_freq_meter_start()
{
	/*====================================================
	  Start Frequency meter
	 ====================================================*/
	CMU0.CSR.R |= CLOCK_CMU_CSR_SFM;
}


int32_t clock_freq_meter_check()
{
	int32_t returnVal = 0;
	uint32_t mesIrcosc = 0;
	
	/*====================================================
	  Wait until Frequency meter measure is finished
	 ====================================================*/	
	while((CMU0.CSR.R & CLOCK_CMU_CSR_SFM)> 0){ }
	
	/*====================================================
	  Check if the result of frequency meter measure of 
	  IRCOSC is in the allowed frequency band
	 ====================================================*/
	mesIrcosc = CMU0.FDR.R;
	
	/*====================================================
	  Check if frequency measured is okay.
	 ====================================================*/
	if(mesIrcosc > CLOCK_FM_FIRCOS_MAX || mesIrcosc < CLOCK_FM_FIRCOS_MIN)
	{
		returnVal = -1;
	}
	/* IRCOSC works fine */
	else 
	{
	}
	
	return returnVal;
}

void clock_IRQ_xoscExp()
{
	/*====================================================
	  Interrupt must be disable. If called, it
	  must send an error !
	====================================================*/
	mode_sw_error();
}


int32_t clock_SWTEST_REGCRC()
{
	return 0;
}
