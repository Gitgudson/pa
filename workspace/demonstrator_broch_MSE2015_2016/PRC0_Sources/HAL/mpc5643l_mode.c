/******************************************************************************
* 
*	Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Nov 03
 *	Last modification : 2015 Nov 03
 *  Author            : Nicolas Broch
 */

#include "MPC5643L.h"
#include "IntcInterrupts.h"
#include "HAL/mpc5643l_mode.h"
#include "HAL/mpc5643l_hal_type.h"


/*******************************************************************************
* Global variable
*******************************************************************************/
static Siul_pcr_e mode_led_safe = 0; /* 
									  * Output where is connected the LED to inform 
									  * of the SoC in SAFE mode
									  */
static uint8_t    mode_led_safe_value = SIUL_LOW; /*
										    * State of the output when the system
										    * is in SAFE mode
										    */


/***************************************************************************//*!
@brief          Function to go into RUN0 mode

@return  		int32_t\n
				return -1 if an error occurred.

@details        Switch to RUN0 mode and wait until the change is done.
******************************************************************************/
static int32_t mode_switch_RUN0();

/***************************************************************************//*!
@brief          Function to go into DRUN mode

@return  		int32_t\n
				return -1 if an error occurred.

@details        Switch to DRUN mode and wait until the change is done.
******************************************************************************/
static int32_t mode_switch_DRUN();





void mode_init()
{
	int32_t returnFct      = 0;
	
	/*====================================================
	  If the SoC is in SAFE mode, switch into DRUN mode
	====================================================*/	
	if((ME.GS.R & MODE_ME_GS_MODEMASK) == MODE_SAFE_SWITCH)
	{
		returnFct = mode_switch_DRUN();
		
		/* 
		 * Transfer to DRUN was not correctly done or
		 * timeout of transfer.
		 * Critical error - claim sw error
		 */
		if( returnFct != 0)
		{
			mode_sw_error();
		}
	}

	/*====================================================
	  SoC modes Enable
	====================================================*/	
	ME.MEN.R = MODE_ME; // enable modes
		
	/*====================================================
	  Configuration of SoC modes
	 ====================================================*/	
	ME.TEST_MC.R  = MODE_TEST_MC;
	ME.DRUN_MC.R  = MODE_DRUN_MC;
	ME.RUN0_MC.R  = MODE_RUN0_MC;
	ME.RUN1_MC.R  = MODE_RUN1_MC;
	ME.RUN2_MC.R  = MODE_RUN2_MC;
	ME.RUN3_MC.R  = MODE_RUN3_MC;
	ME.HALT0_MC.R = MODE_HALT_MC;
	ME.STOP0_MC.R = MODE_STOP_MC;
	
	
	/*====================================================
	  Configure clock generation
	====================================================*/
	clock_init();
	
	/*====================================================
	  Allow interrupts and configure Interrupts' handlers
	 ====================================================*/
	/* 
	 * the interrupt for SAFE mode is still activated even
	 * if after starting safety mechanisms, switching to
	 * SAFE mode would cause a RESET of the system. This is
	 * for avoid an unknown behaviour.
	 */
	INTC_InstallINTCInterruptHandler(mode_reac_safe_mode,51, 15);        // Safe Mode Interrupt    - priority: highest
	INTC_InstallINTCInterruptHandler(mode_IRQ_invalid_mode_int,53, 13);  // Invalid Mode Interrupt - priority: high
	INTC_InstallINTCInterruptHandler(mode_IRQ_invalid_mode_conf,54, 13); // Invalid Mode Config    - priority: high
	
	ME.IS.R  = MODE_CLEAR_ALL_INT; // clear interrupts
	ME.IM.R  = MODE_IM;            // interrupt masks
	
	
	/*====================================================
	  Switch to RUN0 mode
	====================================================*/
	returnFct = mode_switch_RUN0();
	
	/* 
	 * Transfer to RUN0 was not correctly done or
	 * timeout of transfer.
	 * Critical error - claims sw error
	 */
	if( returnFct != 0)
	{
		mode_sw_error();
	}
	
	/*====================================================
	  Turn on XOSC supervision - after this step, it is
	  not possible anymore to switch in SAFE mode directly
	  because it will cause a RESET of the system by the
	  FCCU.
	====================================================*/
	clock_init_CMU();
	
	/*====================================================
	  Configure periodic interrupt timer
	====================================================*/
	pit_init();
	
	/*====================================================
	  Configure error correction status module
	====================================================*/
	ecsm_init();
	
	/*====================================================
	  Lock registers
	====================================================*/
	
	return;
}


void mode_init_safeMode(const Siul_pcr_e led_safe, const uint8_t led_safe_state)
{
	int32_t returnFct      = 0;
	
	
	/*====================================================
	  Configuration of SAFE mode
	 ====================================================*/	
	ME.SAFE_MC.R  = MODE_SAFE_MC;
	
	
	/*====================================================
	  Configuration of mode configuration proposals
	  It is done here to avoid undefined behaviour of 
	  peripheral in SAFE mode.
	 ====================================================*/
	ME.RUN_PC0.R = MODE_RUN_PC0;
	ME.RUN_PC1.R = MODE_RUN_PC1;
	ME.RUN_PC2.R = MODE_RUN_PC2;
	ME.RUN_PC3.R = MODE_RUN_PC3;
	ME.RUN_PC4.R = MODE_RUN_PC4;
	ME.RUN_PC5.R = MODE_RUN_PC5;
	ME.RUN_PC6.R = MODE_RUN_PC6;
	ME.RUN_PC7.R = MODE_RUN_PC7;
	
	ME.LP_PC0.R  = MODE_LP_PC0;
	ME.LP_PC1.R  = MODE_LP_PC1;
	ME.LP_PC2.R  = MODE_LP_PC2;
	ME.LP_PC3.R  = MODE_LP_PC3;
	ME.LP_PC4.R  = MODE_LP_PC4;
	ME.LP_PC5.R  = MODE_LP_PC5;
	ME.LP_PC6.R  = MODE_LP_PC6;
	ME.LP_PC7.R  = MODE_LP_PC7;
	
	
	/*====================================================
	  Configuration of peripherals
	 ====================================================*/
	ME.PCTL4.R  = MODE_PCTL4;
	ME.PCTL5.R  = MODE_PCTL5;
	ME.PCTL6.R  = MODE_PCTL6;
	ME.PCTL16.R = MODE_PCTL16;
	ME.PCTL17.R = MODE_PCTL17;
	ME.PCTL24.R = MODE_PCTL24;
	ME.PCTL32.R = MODE_PCTL32;
	ME.PCTL33.R = MODE_PCTL33;
	ME.PCTL35.R = MODE_PCTL35;
	ME.PCTL38.R = MODE_PCTL38;
	ME.PCTL39.R = MODE_PCTL39;
	ME.PCTL40.R = MODE_PCTL40;
	ME.PCTL41.R = MODE_PCTL41;
	ME.PCTL42.R = MODE_PCTL42;
	ME.PCTL48.R = MODE_PCTL48;
	ME.PCTL49.R = MODE_PCTL49;
	ME.PCTL58.R = MODE_PCTL58;
	ME.PCTL62.R = MODE_PCTL62;
	ME.PCTL92.R = MODE_PCTL92;
	
	/*====================================================
	  STet the pad to inform
	  external world that the SoC is in SAFE mode.
	====================================================*/
	mode_led_safe       = led_safe;        // save into global local variable which LED is for SAFE mode
	mode_led_safe_value = led_safe_state;  // save the value to write to the SAFE LED in SAFE SoC mode
	
	// initialise the LED
	siul_config_pad(mode_led_safe, HAL_YES, HAL_NO,
					SIUL_MODE_GPIO, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE, HAL_YES);
	/* 
	 * write value on the PAD, use siul_write_fast_pad() 
	 * because the watchdog is not activate at this stage.
	 * 
	 * Set the LED in non-SAFE mode state
	 */
	returnFct = siul_write_fast_pad(mode_led_safe, (uint8_t)!mode_led_safe_value);
	
	/* 
	 * Error to configure the SAFE mode LED, claim error.
	 */
	if(returnFct != 0)
	{
		mode_sw_error();
	}
	
	/*====================================================
	  Lock registers
	====================================================*/
}


int32_t mode_switch_low_power()
{
	uint32_t timerTrans = 0;
	int32_t  returnVal  = 0; // return 0 if no problem detected

	/*====================================================
	  Switch to HALT0 mode
	====================================================*/
	ME.MEN.R = MODE_ME_ME_HALT0; /*
	 	 	 	 	 	 	 	  * for unknown reason, if this bit is not set 
	                              * just before switching, it will be reset soon.
	                              */
	
	ME.MCTL.R = MODE_HALT0_SWITCH | MODE_KEY_CHANGE_1; // key 1 to switch
	ME.MCTL.R = MODE_HALT0_SWITCH | MODE_KEY_CHANGE_2; // key 2 to switch
	
	while((ME.GS.R & MODE_ME_GS_S_MTRANS) > 0 && 
		  timerTrans < MODE_TIMEOUT_TRANS)
	{
		timerTrans++;
	}

	/*====================================================
	  check if the loop is finished with time out. If it is 
	  the case, the system  was never in HALT0 mode. 
	  So it announces the error and go back into RUN0 mode
	  to avoid getting "late" to HALT0 mode.
	====================================================*/
	if(timerTrans >= MODE_TIMEOUT_TRANS)
	{
		/* 
		 * ignore error return of mode_g_RUN0() as the system
		 * is already in error.
		 */
		mode_switch_RUN0();
		
		/*
		 * signal the error
		 * No critical error - return an error code.
		 */ 
		returnVal = -1;
	}
	
	return returnVal;
}


static int32_t mode_switch_DRUN()
{
	int32_t returnVal = 0;
	uint32_t timerTrans = 0;
	
	
	/*====================================================
	  Enter 2 Keys to switch to DRUN mode
	====================================================*/
	
	ME.MCTL.R = MODE_DRUN_SWITCH | MODE_KEY_CHANGE_1; // key 1 to switch
	ME.MCTL.R = MODE_DRUN_SWITCH | MODE_KEY_CHANGE_2; // key 2 to switch
	
	/*====================================================
	  Wait until the mode transition is finished and all
	  clocks are stable and correctly configured
	====================================================*/
	timerTrans = 0;
	
	/* 
	 * wait until the system switch in RUN0 mode with all the clocks stable
	 * and the configuration are corrects.
	 */
		
	while((ME.GS.R & MODE_ME_GS_MODEMASK) != MODE_DRUN_SWITCH && timerTrans < MODE_TIMEOUT_TRANS)
	{
		timerTrans++;
	}
	
	/* 
	 * check if the loop is finished with time out or transition effective
	 */
	if(timerTrans >= MODE_TIMEOUT_TRANS)
	{
		/*
		 * signal the error
		 */ 
		returnVal = -1;
	}
	
	return returnVal;
}

static int32_t mode_switch_RUN0()
{
	int32_t returnVal = 0;
	uint32_t timerTrans = 0;
	
	/*====================================================
	  Enter 2 Keys to switch to RUN0 mode
	====================================================*/
	
	ME.MCTL.R = MODE_RUN0_SWITCH | MODE_KEY_CHANGE_1; // key 1 to switch
	ME.MCTL.R = MODE_RUN0_SWITCH | MODE_KEY_CHANGE_2; // key 2 to switch
	
	/*====================================================
	  Wait until the mode transition is finished and all
	  clocks are stable and correctly configured
	====================================================*/
	timerTrans = 0;
	
	/* 
	 * wait until the system switch in RUN0 mode with all the clocks stable
	 * and the configuration are corrects.
	 */
		
	while(ME.GS.R != MODE_ME_GS_RUN0_EXP && timerTrans < MODE_TIMEOUT_TRANS)
	{
		timerTrans++;
	}
	
	/* 
	 * check if the loop is finished with time out or transition effective
	 */
	if(timerTrans >= MODE_TIMEOUT_TRANS)
	{
		/*
		 * signal the error
		 */ 
		returnVal = -1;
	}
	/*====================================================
	 Check if every desired peripherals are on
	====================================================*/	
	timerTrans = 0;
	if(ME.PS0.R != MODE_ME_PSC0_RUN0 ||
	   ME.PS1.R != MODE_ME_PSC1_RUN0 ||
	   ME.PS2.R != MODE_ME_PSC2_RUN0)
	{
		/*
		 * signal the error
		 */ 
		returnVal = -1;
	}
	return returnVal;
}


void mode_switch_SAFE()
{
	uint32_t timerTrans = 0;
	
	/*====================================================
	  Switch to SAFE mode if it is not current SoC
	  mode
	====================================================*/
	if((ME.GS.R & MODE_ME_GS_MODEMASK) != MODE_ME_GS_SAFE)
	{
		/*====================================================
		  Switch to SAFE mode.
		====================================================*/
		ME.MCTL.R = MODE_SAFE_SWITCH | MODE_KEY_CHANGE_1; // key 1 to switch
		ME.MCTL.R = MODE_SAFE_SWITCH | MODE_KEY_CHANGE_2; // key 2 to switch
		
		/*====================================================
		  Wait until the mode has changed.
		====================================================*/
		timerTrans = 0;
		while((ME.GS.R & MODE_ME_GS_S_MTRANS) > 0 && 
			   timerTrans < MODE_TIMEOUT_TRANS)
		{
			timerTrans++;
		}
	}
	/* nothing to do, else here for MISRA compliance */
	else
	{
		
	}
	
	
	/*====================================================
	  	As the SAFE mode is called from the software, 
	  	the system doesn't generate an interrupt. So 
	  	the interrupt is manually called from here
	====================================================*/
	mode_reac_safe_mode();
	
	
	return;
}

void mode_sw_error()
{
	/*====================================================
	  Reset the system until the system is really reset.
	====================================================*/
	while(1)
	{
		ME.MCTL.R = MODE_RESET_SWITCH | MODE_KEY_CHANGE_1; // key 1 to switch
		ME.MCTL.R = MODE_RESET_SWITCH | MODE_KEY_CHANGE_2; // key 2 to switch
	}
	
}


void mode_reac_safe_mode()
{
	/*====================================================
	  At this stage, it is assumed that the SoC is in
	  SAFE mode or RESET mode and thus all GPIO are in a 
	  safe state and all peripherals are in a safe state.
	====================================================*/
	
	/*====================================================
	  System turns on the output to inform the user of the
	  state of the MCU by turning on the LED.
	====================================================*/
	siul_write_pad(mode_led_safe, mode_led_safe_value);
	
	/*====================================================
	  Clear the SAFE event
	====================================================*/
	ME.IMTS.R = 0x00000001;
	
	/*====================================================
	  Wait until RESET.
	====================================================*/
	while(1){}
}


void mode_SWTEST_REGCRC()
{
	/*
	 * Function need to be implemented
	 */
}


void mode_IRQ_invalid_mode_int()
{
	/*====================================================
	  RESET the SoC
	====================================================*/
	mode_sw_error();
}

void mode_IRQ_invalid_mode_conf()
{
	/*====================================================
	  RESET the SoC
	====================================================*/
	mode_sw_error();
}


