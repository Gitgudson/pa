/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Dec 26
 *	Last modification : 2015 Dec 26
 *      Author        : Nicolas Broch
 */

#include "HAL/mpc5643l_ctu.h"

void ctu_init()
{
	/*====================================================
	  Configure CTU registers
	 ====================================================*/
	CTU.TGSISR.R = CTU_TGSISR;
	CTU.TGSCR.R  = CTU_TGSCR;
	
	CTU.TCR[0].R = CTU_T0CR;
	CTU.TCR[1].R = CTU_T1CR;
	CTU.TCR[2].R = CTU_T2CR;
	CTU.TCR[3].R = CTU_T3CR;
	CTU.TCR[4].R = CTU_T4CR;
	CTU.TCR[5].R = CTU_T5CR;
	CTU.TCR[6].R = CTU_T6CR;
	CTU.TCR[7].R = CTU_T7CR;
	
	CTU.TGSCCR.R = CTU_TGSCCR;
	CTU.TGSCRR.R = CTU_TGSCRR;

	CTU.CLCR1.R   = CTU_CLCR1;
	CTU.CLCR2.R   = CTU_CLCR2;
	CTU.CLR[0].R  = CTU_CLR0;
	CTU.CLR[1].R  = CTU_CLR1;
	CTU.CLR[2].R  = CTU_CLR2;
	CTU.CLR[3].R  = CTU_CLR3;
	CTU.CLR[4].R  = CTU_CLR4;
	CTU.CLR[5].R  = CTU_CLR5;
	CTU.CLR[6].R  = CTU_CLR6;
//	CTU.CLR[7].R  = CTU_CLR7;
//	CTU.CLR[8].R  = CTU_CLR8;
//	CTU.CLR[9].R  = CTU_CLR9;
//	CTU.CLR[10].R = CTU_CLR10;
//	CTU.CLR[11].R = CTU_CLR11;
//	CTU.CLR[12].R = CTU_CLR12;
//	CTU.CLR[13].R = CTU_CLR13;
//	CTU.CLR[14].R = CTU_CLR14;
//	CTU.CLR[15].R = CTU_CLR15;
//	CTU.CLR[16].R = CTU_CLR16;
//	CTU.CLR[17].R = CTU_CLR17;
//	CTU.CLR[18].R = CTU_CLR18;
//	CTU.CLR[19].R = CTU_CLR19;
//	CTU.CLR[20].R = CTU_CLR20;
//	CTU.CLR[21].R = CTU_CLR21;
//	CTU.CLR[22].R = CTU_CLR22;
//	CTU.CLR[23].R = CTU_CLR23;

	CTU.THCR1.R  = CTU_THCR1;
	CTU.THCR2.R  = CTU_THCR2;
	
//	CTU.CTUIR.R  = CTU_CTUIR;
//	
//	CTU.EXPECTED_A.R = CTU_EXP_A;
//	CTU.EXPECTED_B.R = CTU_EXP_B;

	/*====================================================
	  Clearing errors and interrupts
	 ====================================================*/
	CTU.CTUEFR.R = CTU_CTUEFR;
	CTU.CTUIFR.R = CTU_CTUIFR;
	
	/*====================================================
	  Updating double-buffered registers
	 ====================================================*/
	CTU.CTUCR.R  = (CTU_CTUCR & (0xFFFB)); // mask to avoid to set MRS_SG
	CTU.CTUCR.B.GRE           = 1;    // CTU General Reload Enable
	CTU.CTUCR.R        = (CTU_CTUCR & (0x4)); // enable MRS_SG bit
}



void ctu_IRQ_adc_ack()
{
	/*====================================================
	  Acknowledge interrupt
	 ====================================================*/
	CTU.CTUIFR.R |= CTU_IRQ_ADC_ACK;
}
