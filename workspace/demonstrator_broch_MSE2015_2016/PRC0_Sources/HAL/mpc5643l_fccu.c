/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Nov 09
 *	Last modification : 2015 Nov 09
 *  Author            : Nicolas Broch
 */

#include "HAL/mpc5643l_fccu.h"
#include "HAL/mpc5643l_fccu_types.h"
#include "HAL/mpc5643l_mode.h"
#include "IntcInterrupts.h"
#include "MPC5643L.h"

/*******************************************************************************
* Global variables accessible from outside of library
*******************************************************************************/
Fccu_errors_s fccu_fails;


/***************************************************************************//*!
@brief          Send FCCU command through FCCU_CTRL


@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If an error occurred and the FCCU is not configured

@details       This functio switch the FCCU mode
******************************************************************************/
static int32_t fccu_ctrl_command(Fccu_ctrl_e command);



int32_t fccu_init()
{
	int32_t returnVal  = 0;
	int32_t returnCmd  = 0;
	uint32_t checkReg  = 0;
	uint32_t temp      = 0;
	
	/*====================================================
	  Configure the Interrupt of FCCU
	 ====================================================*/
	FCCU.STAT.R = FCCU_CLEAR_IRQ; /* Clear interrupts */
	INTC_InstallINTCInterruptHandler(fccu_IRQ_alarm, 250, 14);
	INTC_InstallINTCInterruptHandler(fccu_IRQ_configTimeOut, 251, 14);
	INTC_InstallINTCInterruptHandler(fccu_IRQ_rccx, 252, 14);
	INTC_InstallINTCInterruptHandler(fccu_IRQ_rccx, 253, 14);
	
	/*====================================================
	  Switch the FCCU in CONFIG mode
	 ====================================================*/
	returnCmd = fccu_ctrl_command(FCCU_SWITCH_CONFIG);
	
	/*====================================================
	  Check if the mode change was successful
	 ====================================================*/
	if(returnCmd == 0)
	{
		/*====================================================
		  Configure FCCU
		 ====================================================*/		
		FCCU.CFG.R      = FCCU_CFG;
		
		
		FCCU.CF_CFG0.R   = FCCU_CF_CFG0;
		FCCU.CF_CFG1.R   = FCCU_CF_CFG1;
		FCCU.CF_CFG2.R   = FCCU_CF_CFG2;
		FCCU.CF_CFG3.R   = FCCU_CF_CFG3;
		
		FCCU.NCF_CFG0.R  = FCCU_NCF_CFG0;
		FCCU.NCF_CFG1.R  = FCCU_NCF_CFG1;
		FCCU.NCF_CFG2.R  = FCCU_NCF_CFG2;
		FCCU.NCF_CFG3.R  = FCCU_NCF_CFG3;
		
		FCCU.CFS_CFG0.R  =  FCCU_CFS_CFG0;
		FCCU.CFS_CFG1.R  =  FCCU_CFS_CFG1;
		FCCU.CFS_CFG2.R  =  FCCU_CFS_CFG2;
		
		FCCU.NCFS_CFG0.R = FCCU_NCFS_CFG0;
		FCCU.NCFS_CFG1.R = FCCU_NCFS_CFG1;
		
		FCCU.NCF_E0.R    = FCCU_NCF_E0;
		
		FCCU.NCF_TOE0.R  = FCCU_NCF_TOE;
		FCCU.NCF_TO.R    = FCCU_NCF_TO;
		
		FCCU.IRQ_EN.R    = FCCU_IRQ_EN;
		
		
		/*====================================================
		  Switch the FCCU in NORMAL mode
		 ====================================================*/
		returnCmd = fccu_ctrl_command(FCCU_SWITCH_NORMAL);
		
		/*====================================================
		  Check if the mode change was successful and FCCU
		  is in NORMAL mode and no error has already occurred
		 ====================================================*/
		if(returnCmd != 0 && returnVal == 0)
		{
			returnVal = -1;
		}
		/*====================================================
		  In case of success, lock the registers
		 ====================================================*/
		else if(returnVal == 0)
		{
			/*====================================================
			  Don't lock the configuration because it doesn't
			  work
		    ====================================================*/
//			returnCmd = fccu_ctrl_command(FCCU_LOCKCONF);
//			
//			/*
//			 * Here add protection of fake fault injection!!!
//			 */
//			
//			/*====================================================
//			  Check if the command was successful
//			 ====================================================*/
//			if(returnCmd != 0)
//			{
//				returnVal = -2;
//			}
		}
	}
	
	/*====================================================
	  Impossible to switch in CONFIG mode
	 ====================================================*/
	else
	{
		returnVal = -1;
	}
	
	return returnVal;
}

int32_t fccu_read_fails()
{
	int32_t returnVal = 0;
	int32_t returnCmd  = 0;
	
	/*====================================================
	  Read critical faults
	 ====================================================*/
	returnCmd = fccu_ctrl_command(FCCU_READ_CF);
	
	/* check if failure of reading critical failures state */
	if(returnCmd != 0)
	{
		returnVal = -1;
	}
	/* else, read critical faults */
	else
	{
		fccu_fails.CF_S0 = FCCU.CF_S0.R;
		fccu_fails.CF_S1 = FCCU.CF_S1.R;
		fccu_fails.CF_S2 = FCCU.CF_S2.R;
		fccu_fails.CF_S3 = FCCU.CF_S3.R;
	}
	
	/*====================================================
	  Read non-critical faults
	 ====================================================*/
	returnCmd = fccu_ctrl_command(FCCU_READ_NCF);
	
	/* check if failure of reading non-critical failures state */
	if(returnCmd != 0)
	{
		returnVal = -1;
	}
	/* else, read non-critical faults */
	else
	{
		fccu_fails.NCF_S0 = FCCU.NCF_S0.R;
		fccu_fails.NCF_S1 = FCCU.NCF_S1.R;
		fccu_fails.NCF_S2 = FCCU.NCF_S2.R;
		fccu_fails.NCF_S3 = FCCU.NCF_S3.R;
	}
	
	return returnVal;
}

int32_t fccu_clear_fails()
{
	int32_t  returnVal  = 0; // init return value with no error
	uint32_t fccuOps   = 0;  // State of the FCCU command
	uint32_t regToClear = 0;
	int32_t returnCmd  = 0;
	
	/*====================================================
	  Clear critical faults
	 ====================================================*/
	for(regToClear = 0; regToClear < FCCU_NB_CF_REG; regToClear++)
	{
		FCCU.CFK.R   = FCCU_CFK_KEY;            /* Write key in FCCU_CFK to reset CF */
		FCCU.CF_S[regToClear].R = 0xFFFFFFFFUL; /* Reset CF flags */
		
		/*
		 * Wait while the command is in progress
		 */
		while((FCCU.CTRL.R & FCCU_MASK_STAT_CTRL) == FCCU_OPS_IN_PRO) {}
		
		/* Read if the clearing has been correctly done */
		fccuOps = FCCU.CTRL.R & FCCU_MASK_STAT_CTRL;
		
		/*
		 * Operation error, function return an error but continue
		 * to clear next registers
		 */
		if(fccuOps != FCCU_OPS_SUCCESS)
		{
			returnVal = -1;
		}
		else
		{
		}
	}
	
	
	/*====================================================
	  Clear non-critical faults
	 ====================================================*/
	for(regToClear = 0; regToClear < FCCU_NB_CF_REG; regToClear++)
	{
		FCCU.NCFK.R   = FCCU_NCFK_KEY;           /* Write key in FCCU_CFK to reset CF */
		FCCU.NCF_S[regToClear].R = 0xFFFFFFFFUL; /* Reset CF flags */
		
		/*
		 * Wait while the command is in progress
		 */
		while((FCCU.CTRL.R & FCCU_MASK_STAT_CTRL) == FCCU_OPS_IN_PRO) {}
		
		/* Read if the clearing has been correctly done */
		fccuOps = FCCU.CTRL.R & FCCU_MASK_STAT_CTRL;
		
		/*
		 * Operation error, function return an error but continue
		 * to clear next registers
		 */
		if(fccuOps != FCCU_OPS_SUCCESS)
		{
			returnVal = -1;
		}
		else
		{
		}
	}
	
	return returnVal;
}


int32_t fccu_SWTEST_REGCRC()
{
	return 0;
}


static int32_t fccu_ctrl_command(Fccu_ctrl_e command)
{
	int32_t  returnVal = 0; /* Value returned by the function */
	uint32_t fccuOps   = 0; /* State of the FCCU command */
	
	
	switch(command){
	case FCCU_SWITCH_CONFIG:
		
		/*====================================================
		  Switch the FCCU in CONFIG mode
		 ====================================================*/
		FCCU.CTRLK.R = FCCU_CTRL_KEY_OP1;
		FCCU.CTRL.R  = FCCU_SWITCH_CONFIG;		
		break;
		
		
	case FCCU_SWITCH_NORMAL:
		/*====================================================
		  Switch FCCU in NORMAL mode
		 ====================================================*/
		FCCU.CTRLK.R = FCCU_CTRL_KEY_OP2;
		FCCU.CTRL.R  = FCCU_SWITCH_NORMAL;
		break;
	
	case FCCU_READ_STATE:
		/*====================================================
		  Read FCCU state
		 ====================================================*/
		FCCU.CTRL.R  = FCCU_READ_STATE;
		break;
		
	case FCCU_LOCKCONF:
		/*====================================================
		  Lock the configuration of FCCU
		 ====================================================*/
		FCCU.CTRLK.R = FCCU_CTRL_KEY_OP16;
		FCCU.CTRL.R  = FCCU_LOCKCONF;		
		break;
		
	case FCCU_READ_CF:
		/*====================================================
		  Read state of critical failures
		 ====================================================*/
		FCCU.CTRL.R  = FCCU_READ_CF;		
		break;
		
	case FCCU_READ_NCF:
		/*====================================================
		  Read state of non-critical failures
		 ====================================================*/
		FCCU.CTRL.R  = FCCU_READ_NCF;		
		break;		
		
	default:
		/* If the command is unknown, return the error code */
			returnVal = -1;
		break;
	}
	
	/*====================================================
	  If no error is detected and a command has been sent to
	  FCCU, wait until it finishes.
	 ====================================================*/
	/*
	 * Wait while the command is in progress
	 */
	while((FCCU.CTRL.R & FCCU_MASK_STAT_CTRL) == FCCU_OPS_IN_PRO)
	{
	}
	
	fccuOps = FCCU.CTRL.R & FCCU_MASK_STAT_CTRL;
	
	/*====================================================
	  Check if the mode change was successful
	 ====================================================*/
	/*
	 * Operation error, function return an error
	 */
	if(fccuOps != FCCU_OPS_SUCCESS)
	{
		returnVal = -1;
	}
	/*
	 * Operation successful - FCCU in CONFIG mode
	 */
	else
	{
		returnVal = 0;
	}
	
	return returnVal;
}


void fccu_IRQ_alarm()
{
	/*====================================================
	  Does nothing, system will RESET itself
	====================================================*/

}


void fccu_IRQ_configTimeOut()
{
	/*====================================================
	  RESET the SoC
	====================================================*/
	mode_sw_error();
}



void fccu_IRQ_rccx()
{
	/*====================================================
	  RESET the SoC
	====================================================*/
	mode_sw_error();
}


