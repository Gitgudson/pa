/*
/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Nov 14
 *  Last modification : 2019 Apr 16
 *  Author            : Nicolas Broch
 *  Author			  : Jan Huber
 */

#pragma ipa off

#include "HAL/mpc5643l_power.h"
#include "HAL/mpc5643l_mode.h"
#include "HAL/mpc5643l_fccu.h"
#include "MPC5643l.h"
#include "IntcInterrupts.h"

int32_t power_init()
{
	int32_t returnVal = 0;
	int32_t retFct    = 0;
	
	/*====================================================
	  Configure PMU and interrupts
	 ====================================================*/
	PMUCTRL.MASKF.R = POWER_MASKF;
	PMUCTRL.IRQE.R  = POWER_IRQE;
	
	/* handle interrupt */
	INTC_InstallINTCInterruptHandler(power_IRQ_fails, 254, 14);
		
	
	/*====================================================
	  Check if external transistor is available
	 ====================================================*/
	if(PMUCTRL.STATUS.R != POWER_STATUS_EXP)
	{
		returnVal = -1;
	}
	/* nothing to do, else here for MISRA compliance */
	else
	{
		
	}
	
	/*====================================================
	  Do BIST
	 ====================================================*/
	retFct = power_bist_start();
	
	/*====================================================
	  Check if the BIST was successful. If the BIST shows
	  critical failures, the MCU puts itself in error. So
	  this code never execute.
	 ====================================================*/
	if(retFct != 0)
	{
		returnVal = -1;
	}
	/*====================================================
	  In case of success, check if failures occurred into
	  the FCCU
	 ====================================================*/
	else
	{
		/*====================================================
		  Clear fails into PMU
		 ====================================================*/
		power_clear_fails();
		
		/*====================================================
		  Read fails into the FCCU
		 ====================================================*/
		retFct = fccu_read_fails();
		
		/* If the failure reading failed, return error */
		if(retFct != 0)
		{
			returnVal = -1;
		}
		else
		{
			/*====================================================
			  Check if FCCU has set the flags of PMU BIST success
			 ====================================================*/
			if((fccu_fails.NCF_S0 & POWER_FCCU_MASK_NCFS0) 
					!= POWER_FCCU_BIST_SUC)
			{
				returnVal = -1;
			}
		}
		

	}
	
	/*====================================================
	  Lock registers
	 ====================================================*/
	SET_SOFTLOCK32(PMUCTRL.CTRL);
	SET_SOFTLOCK32(PMUCTRL.IRQE);
	SET_HARDLOCK(0xC3FE8080UL);
		
	return returnVal;
}

void power_clear_fails()
{
	/*====================================================
	  Clear all fails
	 ====================================================*/
	PMUCTRL.IRQS.R = POWER_CLEAR_IRQ;
}


int32_t power_bist_start()
{
	int32_t returnVal = 0;
	uint32_t timeOut  = 0;
	
	/*====================================================
	  Do LVD BIST
	 ====================================================*/
	timeOut  = 0;
	PMUCTRL.CTRL.R = 1;
	/*
	 * PMUCTRL.CTRL.R = 0 when BIST finished
	 */
	while(PMUCTRL.CTRL.R != 0 && timeOut < POWER_BIST_TO)
	{
		timeOut++;
	}

	/*====================================================
	  Check if the BIST has been done before time-out
	 ====================================================*/
	if(timeOut >= POWER_BIST_TO)
	{
		returnVal = -1;
	}
	else
	{
		/*====================================================
		  Do HVD BIST
		 ====================================================*/
		timeOut = 0;
		PMUCTRL.CTRL.R = 2;
		
		/*
		 * PMUCTRL.CTRL.R = 0 when BIST finished
		 */
		while(PMUCTRL.CTRL.R != 0 && timeOut < POWER_BIST_TO)
		{
			timeOut++;
		}
		
		/*====================================================
		  Check if the BIST has been done before time-out
		 ====================================================*/
		if(timeOut >= POWER_BIST_TO)
		{
			returnVal = -1;
		}
		else
		{
			/*====================================================
			  Check if BIST detected something
			 ====================================================*/
			if((PMUCTRL.FAULT.R & POWER_FAULT_MASK) != POWER_BIST_SUC)
			{
				returnVal = -1;
			}
			/* elsewhere, nothing to do */
			else
			{
			}
		}
	}
	
	return returnVal;
}

void power_IRQ_fails()
{
	mode_sw_error();
}
