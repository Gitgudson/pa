/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Nov 26
 *	Last modification : 2015 Nov 26
 *      Author        : Nicolas Broch
 */

#include "HAL/mpc5643l_uart.h"

/*******************************************************************************
* Global variable
*******************************************************************************/
static volatile Uart_RxBuffer bufferRxUART0;
static volatile Uart_RxBuffer bufferRxUART1;

int32_t uart_init(Uart_flexlin linModule)
{
	int32_t  returnVal = 0;
	volatile LINFLEX_tag *uart = 0; /* pt to FLEXLinD registers */
	uint32_t timerChangeMod = 0;    /* timer (in reading iteration) for module
									   to change of mode */
	
	/*====================================================
	  Copy the correct UART structure in uart
	 ====================================================*/
	if(linModule == UART_FLEXLIND0)
	{
		uart = &LINFLEX0;
	}
	else if(linModule == UART_FLEXLIND1)
	{
		uart = &LINFLEX1;
	}
	else
	{
		returnVal = -1;
	}
	
	/*====================================================
	  If the UART ID is unknown, doesn't pursue the
	  configuration
	 ====================================================*/
	if(returnVal == 0)
	{		
		/*====================================================
		  Initialisation of Rx circular buffer
		 ====================================================*/
		if(linModule == UART_FLEXLIND0)
		{
			bufferRxUART0.inIndex  = 0;
			bufferRxUART0.outIndex = 0;
		}
		else
		{
			bufferRxUART1.inIndex  = 0;
			bufferRxUART1.outIndex = 0;
		}
		
		/*====================================================
		  1. UART in INIT mode
		 ====================================================*/
		uart->LINCR1.R &= ~UART_SLEEP_BIT; /* go out of SLEEP mode - state machine should be 
											  in normal mode after this line*/
		uart->LINCR1.R |= UART_INIT_BIT;   /* go in INIT mode */	
		
		/*====================================================
		  2. Wait until module is in Init mode
		 ====================================================*/
		timerChangeMod = 0;
		while((uart->LINSR.R & UART_SR_MOD) != UART_INIT_SR && timerChangeMod < UART_TIMEOUT_MOD)
		{
			timerChangeMod++;
		}
		
		/* check whether the loop was finished by timeout */
		if(timerChangeMod >= UART_TIMEOUT_MOD)
		{
			returnVal = -2;
		}
		/* FlexLinD in Init mode */
		else
		{
			/*====================================================
			  3. Configure registers
			 ====================================================*/
			uart->LINCR1.R  = UART_LINCR1 | UART_INIT_BIT; /* add flag to stay in INIT mode */
			uart->LINIER.R  = UART_LINIER;
			uart->UARTCR.R  = UART_UARTCR_U; /* Write in UARTCR in 2 step because this register 
			                                    is read-only until UART is not set */
			uart->UARTCR.R  = UART_UARTCR;
			uart->LINFBRR.R = UART_LINFBRR;
			uart->LINIBRR.R = UART_LINIBRR;
			uart->GCR.R     = UART_GCR;
			uart->UARTPTO.R = UART_UARTPTO;
			
			/*====================================================
			  4. Clear errors
			 ====================================================*/
			uart->UARTSR.R = UART_ERR_CLEAR;
			
			/*====================================================
			  5. Configure interrupts
			 ====================================================*/
			if(linModule == UART_FLEXLIND0)
			{
				/* handle interrupt - Data received */
				INTC_InstallINTCInterruptHandler(uart_IRQ_uart0_rx, 79, 9);
				/* handle interrupt - Data transmitted */
				INTC_InstallINTCInterruptHandler(uart_IRQ_uart0_tx, 80, 9);
				/* handle interrupt - error */
				INTC_InstallINTCInterruptHandler(uart_IRQ_uart0_error, 81, 13);
			}
			else
			{
				/* handle interrupt - Data received */
				INTC_InstallINTCInterruptHandler(uart_IRQ_uart1_rx, 99, 9);
				/* handle interrupt - Data transmitted */
				INTC_InstallINTCInterruptHandler(uart_IRQ_uart1_tx, 100, 9);
				/* handle interrupt - error */
				INTC_InstallINTCInterruptHandler(uart_IRQ_uart1_error, 101, 13);
			}
			
			/*====================================================
			  6. Put UART in Idle mode (Data reception/transmission 
			     and SLEEP are also acceptable)
			 ====================================================*/
			uart->LINCR1.R &= ~UART_INIT_BIT;   /* go in INIT mode */
			
			timerChangeMod = 0;
			while((uart->LINSR.R & UART_SR_MOD) == UART_INIT_SR && 
				  timerChangeMod < UART_TIMEOUT_MOD)
			{
				timerChangeMod++;
			}
			
			/* check whether the loop was finished by timeout */
			if(timerChangeMod >= UART_TIMEOUT_MOD)
			{
				returnVal = -2;
			}
			else
			{				
				/*====================================================
				  Configure the Pads
				 ====================================================*/
				if(linModule == UART_FLEXLIND0)
				{
					/* configure pad Tx */
					siul_config_pad(PCR_B_2, HAL_NO, HAL_NO, SIUL_MODE_ALTER_1, 
									SIUL_INOUT_NONE, SIUL_FAST, SIUL_TYPE_NONE,
									HAL_YES);
					/* configure pad Rx */
					siul_config_pad(PCR_B_3, HAL_NO, HAL_NO, SIUL_MODE_ALTER_2, 
									SIUL_INOUT_NONE, SIUL_FAST, SIUL_TYPE_NONE,
									HAL_YES);
					siul_config_padSelection(31, 0);
				}
				else
				{
					/* configure pad Tx */
					siul_config_pad(PCR_F_14, HAL_NO, HAL_NO, SIUL_MODE_ALTER_1, 
									SIUL_INOUT_NONE, SIUL_FAST, SIUL_TYPE_NONE,
									HAL_YES);
					/* configure pad Rx */
					siul_config_pad(PCR_F_15, HAL_NO, HAL_NO, SIUL_MODE_ALTER_2, 
									SIUL_INOUT_NONE, SIUL_FAST, SIUL_TYPE_NONE,
									HAL_YES);
					siul_config_padSelection(32, 2);
				}
			}
		}
		
	}
	/* Error already declared - nothing to do */
	else
	{
	}
	
	return returnVal;	
}

int32_t uart_transmit(Uart_flexlin linModule, uint8_t *pData, uint32_t size)
{
	int32_t  returnVal = 0;
	volatile LINFLEX_tag *uart = 0; /* pt to FLEXLinD registers */
	uint32_t i = 0;                 /* Index of data to send */
	uint32_t timerSend = 0;         /* timer (in reading iteration) for module
									   to change of mode */
	
	/*====================================================
	  Copy the correct UART structure in uart
	 ====================================================*/
	if(linModule == UART_FLEXLIND0)
	{
		uart = &LINFLEX0;
	}
	else if(linModule == UART_FLEXLIND1)
	{
		uart = &LINFLEX1;
	}
	else
	{
		returnVal = -1;
	}
	
	/*====================================================
	  Acknowledge last sent bit (if it is useful)
	 ====================================================*/
	uart->UARTSR.R |= UART_TX_END; /* ACK last sent data */
	
	/*====================================================
	  Send byte by byte
	 ====================================================*/
	for(i = 0; i < size; i++)
	{
		/*====================================================
		  Send 1 byte
		 ====================================================*/
		uart->BDRL.R    = *(pData + i); /* With mask to avoid overflow */
		
		/*====================================================
		  Wait until data is sent with timeout
		 ====================================================*/
		timerSend = 0;
		while((uart->UARTSR.R & UART_TX_END) == 0 && timerSend < UART_TIMEOUT_SENT)
		{
			timerSend++;
		}
		
		/* check if timeout has expired */
		if(timerSend >= UART_TIMEOUT_SENT)
		{
			/* abort function */
			returnVal = -1;
			break;
		}
		else
		{
			/*====================================================
			  Acknowledge last sent bit.
			 ====================================================*/
			uart->UARTSR.R |= UART_TX_END; /* ACK last sent data */
		}
	}
	
	return returnVal;
}


int32_t uart_receive(Uart_flexlin linModule, uint8_t *pData, uint32_t size)
{
	int32_t returnVal = 0;
	volatile Uart_RxBuffer *buffer; /* pointer to Rx buffer */
	uint32_t i = 0;                 /* Index to pData */
	
	/*====================================================
	  Check linModule parameter
	 ====================================================*/
	if(linModule == UART_FLEXLIND0)
	{
		buffer = &bufferRxUART0;
	}
	else if(linModule == UART_FLEXLIND1)
	{
		buffer = &bufferRxUART1;
	}
	else /* Error */
	{
		returnVal = -1;
	}
	
	/*====================================================
	  If no error - copy read value into pData
	 ====================================================*/
	if(returnVal >= 0)
	{
		for(i = 0; i < size; i++)
		{
			/* Wait until data to read */
			while(buffer->inIndex == buffer->outIndex){}
			
			/*====================================================
			  Read data
			 ====================================================*/
			*(pData + i) = buffer->buffer[buffer->outIndex];
			buffer->outIndex = (buffer->outIndex + 1) % UART_SIZE_BUFFER;
		}
	}
	/* Error - nothing to do */
	else
	{
	}
	
	return returnVal;
}

int32_t uart_size_receive(Uart_flexlin linModule)
{
	int32_t size = 0;
	
	/*====================================================
	  Compute size of received and not-read datas in buffer
	 ====================================================*/
	if(linModule == UART_FLEXLIND0)
	{
		size = ((int32_t) bufferRxUART0.inIndex - (int32_t) bufferRxUART0.outIndex + 
				(int32_t) UART_SIZE_BUFFER) % (int32_t) UART_SIZE_BUFFER;
	}
	else if(linModule == UART_FLEXLIND1)
	{
		size = ((int32_t) bufferRxUART1.inIndex - (int32_t) bufferRxUART1.outIndex + 
				(int32_t) UART_SIZE_BUFFER) % (int32_t) UART_SIZE_BUFFER;
	}
	else /* Error */
	{
		size = -1;
	}
	
	return size;
}


void uart_IRQ_uart0_error()
{
	/* go straightly to error mode */
	mode_sw_error();
}

void uart_IRQ_uart0_rx()
{
	/*====================================================
	  check if there is really an interrupt
	 ====================================================*/
	if((LINFLEX0.UARTSR.R & UART_RX_END) > 0)
	{	
		/*====================================================
		  Save data
		 ====================================================*/
		bufferRxUART0.buffer[bufferRxUART0.inIndex] = (uint8_t) LINFLEX0.BDRM.R & 0xFF;
		
		/*====================================================
		  Update pointer to input index
		 ====================================================*/
		bufferRxUART0.inIndex = (bufferRxUART0.inIndex + 1) % UART_SIZE_BUFFER;
		
		/*====================================================
		  ACK of received data
		 ====================================================*/
		LINFLEX0.UARTSR.R |= UART_RX_END;
	}
	/* no interrupt - nothing to do */
	else
	{
		
	}
}

void uart_IRQ_uart0_tx()
{
	/* not used - go straightly to error mode */
	mode_sw_error();
}


void uart_IRQ_uart1_error()
{
	/* go straightly to error mode */
	mode_sw_error();
}

void uart_IRQ_uart1_rx()
{
	/*====================================================
	  check if there is really an interrupt
	 ====================================================*/
	if((LINFLEX1.UARTSR.R & UART_RX_END) > 0)
	{	
		/*====================================================
		  Save data
		 ====================================================*/
		bufferRxUART1.buffer[bufferRxUART1.inIndex] = (uint8_t) LINFLEX1.BDRM.R & 0xFF;
		
		/*====================================================
		  Update pointer to input index
		 ====================================================*/
		bufferRxUART1.inIndex = (bufferRxUART1.inIndex + 1) % UART_SIZE_BUFFER;
		
		/*====================================================
		  ACK of received data
		 ====================================================*/
		LINFLEX1.UARTSR.R |= UART_RX_END;
	}
	/* no interrupt - nothing to do */
	else
	{
		
	}
}

void uart_IRQ_uart1_tx()
{
	/* not used - go straightly to error mode */
	mode_sw_error();
}

