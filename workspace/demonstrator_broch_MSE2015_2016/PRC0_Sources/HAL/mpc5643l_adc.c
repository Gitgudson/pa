/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Dec 29
 *	Last modification : 2015 Dec 29
 *      Author        : Nicolas Broch
 */

#include "HAL/mpc5643l_adc.h"

/***************************************************************************//*!
@brief          ADC hw offset cancelation function

@param[in,out]  void
@param[in]      void

@return         void

@details        This function performs hardware offset cancellation. It is used 
                to cancel the offset built up on the ADC capacitor bank and
                hence to increase the accuracy of conversion basic start-up
                setting of the ADC. Internal voltage is sampled and result
                is captured in OFFWR register.

@note           

@warning        
******************************************************************************/
void adc_off_canc(volatile ADC_tag *pADC);



void adc_init()
{
	/*====================================================
	  Configure ADC0
	 ====================================================*/
    // initialization of peripheral base address
    ADC_0.MCR.R       = ADC0_MCR;
    ADC_0.CTR0.R      = ADC0_CTR0;
    
    adc_off_canc(&ADC0);
    
	/*====================================================
	  Configure ADC1
	 ====================================================*/   
    ADC_1.MCR.R       = ADC1_MCR;
    ADC_1.CTR0.R      = ADC1_CTR0;
    
    adc_off_canc(&ADC1);
}

void adc_off_canc(volatile ADC_tag *pADC)
{
    vuint32_t   ctuen;

    ctuen   = pADC->MCR.B.CTUEN;      // save current value of CTUEN bit
    pADC->MCR.B.CTUEN     = 0x0;      // disable CTU control mode

    while(pADC->MSR.B.CTUSTART){}     // wait until there is no conversion started by CTU

    pADC->MCR.B.PWDN      = 0x1;      // Analog module in power down mode
    pADC->MCR.B.OFFCANC   = 0x1;      // offset cancellation phase is enabled
    pADC->MCR.B.PWDN      = 0x0;      // Exit power down mode to enable offset cancellation

    while (pADC->MCR.B.OFFCANC){}     // wait until Offset cancellation is over

    pADC->MCR.B.CTUEN     = ctuen;    // restore CTUEN bit
}


void adc_reset_endOfConv()
{
	/*====================================================
	  Clear CEOCFR0 registers
	====================================================*/
	ADC0.CEOCFR0.R = ADC_CLR_CEOCFR0;
	ADC1.CEOCFR0.R = ADC_CLR_CEOCFR0;
}
