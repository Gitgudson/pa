/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Nov 23
 *	Last modification : 2015 Nov 23
 *      Author        : Nicolas Broch
 */

#include "MPC5643L.h"
#include "HAL/mpc5643l_swg.h"
#include "HAL/mpc5643l_siul.h"
#include "IntcInterrupts.h"
#include "HAL/mpc5643l_mode.h"

/*
 * Static global variable to save the 
 * configuration of SWG. Used to start
 * the SWG.
 */
static uint32_t swgCtrl = SWG_CTRL_DEF;



void swg_init(Hal_YN_e swg_int)
{	
	/*====================================================
	  Configure pad D[7]
	====================================================*/
	siul_config_pad(PCR_D_7, HAL_NO, HAL_NO, 
				    SIUL_MODE_ALTER_3, SIUL_INOUT_NONE,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Configure for SWG interrupts
	====================================================*/
	if(swg_int == HAL_YES)
	{
		swgCtrl |= SWG_INT_ERR_EN; // enable interrupt
	}
	/*
	 * else disable interrupt
	 */
	else
	{
		swgCtrl &= ~SWG_INT_ERR_EN; // disable interrupt
	}
	
	/* handle interrupt */
	INTC_InstallINTCInterruptHandler(swg_IRQ_error, 255, 14);
	
	/*====================================================
	  Clear errors
	====================================================*/
	swg_clear_error();
}

void swg_start()
{
	/*====================================================
	  Set LDOS to 0 to stop SWG
	====================================================*/
	SGENDIG.CTRL.R &= ~0x80000000UL;
	
	/*====================================================
	  Configure SWG_CTRL
	====================================================*/
	SGENDIG.CTRL.R = swgCtrl;
	
	/*====================================================
	  Set LDOS to 1 to start SWG
	====================================================*/
	SGENDIG.CTRL.R |= 0x80000000UL;
}

void swg_stop()
{
	/*====================================================
	  Set LDOS to 0 to stop SWG
	====================================================*/
	SGENDIG.CTRL.R &= ~0x80000000UL;
	
	/*====================================================
	  Configure SWG
	====================================================*/
	SGENDIG.CTRL.R |= 0xFFFF0000UL;
	
	/*====================================================
	  Set LDOS to 1 to start SWG
	====================================================*/
	SGENDIG.CTRL.R |= 0x80000000UL;
}


Hal_YN_e swg_is_error()
{
	Hal_YN_e returnVal = HAL_NO;
	
	/*====================================================
	   Check if in error
	====================================================*/
	if((SGENDIG.IRQE.R & SWG_CHECK_CLEAR_ERROR) > 0)
	{
		returnVal = HAL_YES;
	}
	/* Here for MISRA compliance */
	else
	{
	}
	
	return returnVal;
}



void swg_clear_error()
{
	/*====================================================
	   Clear error 
	====================================================*/
	SGENDIG.IRQE.R |= SWG_CHECK_CLEAR_ERROR;
}


void swg_IRQ_error()
{
	/*====================================================
	   Put SoC in error mode
	====================================================*/
	mode_sw_error();
}
