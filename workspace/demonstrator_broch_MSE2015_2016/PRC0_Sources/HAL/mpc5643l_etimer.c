/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Dec 04
 *	Last modification : 2016 Jan 02
 *      Author        : Nicolas Broch
 */


#include "HAL/mpc5643l_etimer.h"



void etimer_init()
{
	/*====================================================
	  Disable all eTimers and all channels for 
	  configuration
	 ====================================================*/
	ETIMER_0.ENBL.R = 0x0000;
	
	
	/*====================================================
	  Configure Timer 0 - channel 0
	 ====================================================*/
#if (ETIMER0_ENBL & 0x0001) > 0
	ETIMER_0.STS0.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_0.WDTOH.R   = ETIMER0_CH0_WDTH;
    ETIMER_0.WDTOL.R   = ETIMER0_CH0_WDTH;
	ETIMER_0.COMP10.R  = ETIMER0_CH0_COMP1;
	ETIMER_0.COMP20.R  = ETIMER0_CH0_COMP2;
	ETIMER_0.LOAD0.R   = ETIMER0_CH0_LOAD;
	ETIMER_0.CTRL10.R  = ETIMER0_CH0_CTRL1;
	ETIMER_0.CTRL20.R  = ETIMER0_CH0_CTRL2;
	ETIMER_0.CTRL30.R  = ETIMER0_CH0_CTRL3;
	ETIMER_0.INTDMA0.R = ETIMER0_CH0_INTDMA;
	ETIMER_0.CMPLD10.R = ETIMER0_CH0_CMPLD1;
	ETIMER_0.CMPLD20.R = ETIMER0_CH0_CMPLD2;
	ETIMER_0.CCCTRL0.R = ETIMER0_CH0_CCCTRL;
	ETIMER_0.FILT0.R   = ETIMER0_CH0_FILT;
#endif
	
	/*====================================================
	  Configure Timer 0 - channel 1
	 ====================================================*/
#if (ETIMER0_ENBL & 0x0002) > 0
	ETIMER_0.STS1.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_0.COMP11.R  = ETIMER0_CH1_COMP1;
	ETIMER_0.COMP21.R  = ETIMER0_CH1_COMP2;
	ETIMER_0.LOAD1.R   = ETIMER0_CH1_LOAD;
	ETIMER_0.CTRL11.R  = ETIMER0_CH1_CTRL1;
	ETIMER_0.CTRL21.R  = ETIMER0_CH1_CTRL2;
	ETIMER_0.CTRL31.R  = ETIMER0_CH1_CTRL3;
	ETIMER_0.INTDMA1.R = ETIMER0_CH1_INTDMA;
	ETIMER_0.CMPLD11.R = ETIMER0_CH1_CMPLD1;
	ETIMER_0.CMPLD21.R = ETIMER0_CH1_CMPLD2;
	ETIMER_0.CCCTRL1.R = ETIMER0_CH1_CCCTRL;
	ETIMER_0.FILT1.R   = ETIMER0_CH1_FILT;
#endif
	
	/*====================================================
	  Configure Timer 0 - channel 2
	 ====================================================*/
#if (ETIMER0_ENBL & 0x0004) > 0
	ETIMER_0.STS2.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_0.COMP12.R  = ETIMER0_CH2_COMP1;
	ETIMER_0.COMP22.R  = ETIMER0_CH2_COMP2;
	ETIMER_0.LOAD2.R   = ETIMER0_CH2_LOAD;
	ETIMER_0.CTRL12.R  = ETIMER0_CH2_CTRL1;
	ETIMER_0.CTRL22.R  = ETIMER0_CH2_CTRL2;
	ETIMER_0.CTRL32.R  = ETIMER0_CH2_CTRL3;
	ETIMER_0.INTDMA2.R = ETIMER0_CH2_INTDMA;
	ETIMER_0.CMPLD12.R = ETIMER0_CH2_CMPLD1;
	ETIMER_0.CMPLD22.R = ETIMER0_CH2_CMPLD2;
	ETIMER_0.CCCTRL2.R = ETIMER0_CH2_CCCTRL;
	ETIMER_0.FILT2.R   = ETIMER0_CH2_FILT;
#endif
	
	/*====================================================
	  Configure Timer 0 - channel 3
	 ====================================================*/
#if (ETIMER0_ENBL & 0x0008) > 0
	ETIMER_0.STS3.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_0.COMP13.R  = ETIMER0_CH3_COMP1;
	ETIMER_0.COMP23.R  = ETIMER0_CH3_COMP2;
	ETIMER_0.LOAD3.R   = ETIMER0_CH3_LOAD;
	ETIMER_0.CTRL13.R  = ETIMER0_CH3_CTRL1;
	ETIMER_0.CTRL23.R  = ETIMER0_CH3_CTRL2;
	ETIMER_0.CTRL33.R  = ETIMER0_CH3_CTRL3;
	ETIMER_0.INTDMA3.R = ETIMER0_CH3_INTDMA;
	ETIMER_0.CMPLD13.R = ETIMER0_CH3_CMPLD1;
	ETIMER_0.CMPLD23.R = ETIMER0_CH3_CMPLD2;
	ETIMER_0.CCCTRL3.R = ETIMER0_CH3_CCCTRL;
	ETIMER_0.FILT3.R   = ETIMER0_CH3_FILT;
#endif
	
	/*====================================================
	  Configure Timer 0 - channel 4
	 ====================================================*/
#if (ETIMER0_ENBL & 0x0010) > 0
	ETIMER_0.STS4.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_0.COMP14.R  = ETIMER0_CH4_COMP1;
	ETIMER_0.COMP24.R  = ETIMER0_CH4_COMP2;
	ETIMER_0.LOAD4.R   = ETIMER0_CH4_LOAD;
	ETIMER_0.CTRL14.R  = ETIMER0_CH4_CTRL1;
	ETIMER_0.CTRL24.R  = ETIMER0_CH4_CTRL2;
	ETIMER_0.CTRL34.R  = ETIMER0_CH4_CTRL3;
	ETIMER_0.INTDMA4.R = ETIMER0_CH4_INTDMA;
	ETIMER_0.CMPLD14.R = ETIMER0_CH4_CMPLD1;
	ETIMER_0.CMPLD24.R = ETIMER0_CH4_CMPLD2;
	ETIMER_0.CCCTRL4.R = ETIMER0_CH4_CCCTRL;
	ETIMER_0.FILT4.R   = ETIMER0_CH4_FILT;
#endif
	
	/*====================================================
	  Configure Timer 0 - channel 5
	 ====================================================*/
#if (ETIMER0_ENBL & 0x0020) > 0
	ETIMER_0.STS5.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_0.COMP15.R  = ETIMER0_CH5_COMP1;
	ETIMER_0.COMP25.R  = ETIMER0_CH5_COMP2;
	ETIMER_0.LOAD5.R   = ETIMER0_CH5_LOAD;
	ETIMER_0.CTRL15.R  = ETIMER0_CH5_CTRL1;
	ETIMER_0.CTRL25.R  = ETIMER0_CH5_CTRL2;
	ETIMER_0.CTRL35.R  = ETIMER0_CH5_CTRL3;
	ETIMER_0.INTDMA5.R = ETIMER0_CH5_INTDMA;
	ETIMER_0.CMPLD15.R = ETIMER0_CH5_CMPLD1;
	ETIMER_0.CMPLD25.R = ETIMER0_CH5_CMPLD2;
	ETIMER_0.CCCTRL5.R = ETIMER0_CH5_CCCTRL;
	ETIMER_0.FILT5.R   = ETIMER0_CH5_FILT;
#endif
	
	/*====================================================
	  Configure Timer 0 - general
	 ====================================================*/
	// configure DREQ only if at least a channel is enable
#if ETIMER0_ENBL > 0
	ETIMER_0.DREQ0.R = ETIMER0_DREQ0;
    ETIMER_0.DREQ1.R = ETIMER0_DREQ1;
#endif
    
	ETIMER_0.ENBL.R  = ETIMER0_ENBL;
	
	
	
	
	/*====================================================
	  Configure Timer 1 - channel 0
	 ====================================================*/
#if (ETIMER1_ENBL & 0x0001) > 0
	ETIMER_1.STS0.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_1.COMP10.R  = ETIMER1_CH0_COMP1;
	ETIMER_1.COMP20.R  = ETIMER1_CH0_COMP2;
	ETIMER_1.LOAD0.R   = ETIMER1_CH0_LOAD;
	ETIMER_1.CTRL10.R  = ETIMER1_CH0_CTRL1;
	ETIMER_1.CTRL20.R  = ETIMER1_CH0_CTRL2;
	ETIMER_1.CTRL30.R  = ETIMER1_CH0_CTRL3;
	ETIMER_1.INTDMA0.R = ETIMER1_CH0_INTDMA;
	ETIMER_1.CMPLD10.R = ETIMER1_CH0_CMPLD1;
	ETIMER_1.CMPLD20.R = ETIMER1_CH0_CMPLD2;
	ETIMER_1.CCCTRL0.R = ETIMER1_CH0_CCCTRL;
	ETIMER_1.FILT0.R   = ETIMER1_CH0_FILT;	
#endif
	
	/*====================================================
	  Configure Timer 1 - channel 1
	 ====================================================*/
#if (ETIMER1_ENBL & 0x0002) > 0
	ETIMER_1.STS1.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_1.COMP11.R  = ETIMER1_CH1_COMP1;
	ETIMER_1.COMP21.R  = ETIMER1_CH1_COMP2;
	ETIMER_1.LOAD1.R   = ETIMER1_CH1_LOAD;
	ETIMER_1.CTRL11.R  = ETIMER1_CH1_CTRL1;
	ETIMER_1.CTRL21.R  = ETIMER1_CH1_CTRL2;
	ETIMER_1.CTRL31.R  = ETIMER1_CH1_CTRL3;
	ETIMER_1.INTDMA1.R = ETIMER1_CH1_INTDMA;
	ETIMER_1.CMPLD11.R = ETIMER1_CH1_CMPLD1;
	ETIMER_1.CMPLD21.R = ETIMER1_CH1_CMPLD2;
	ETIMER_1.CCCTRL1.R = ETIMER1_CH1_CCCTRL;
	ETIMER_1.FILT1.R   = ETIMER1_CH1_FILT;
#endif
	
	/*====================================================
	  Configure Timer 1 - channel 2
	 ====================================================*/
#if (ETIMER1_ENBL & 0x0004) > 0
	ETIMER_1.STS2.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_1.COMP12.R  = ETIMER1_CH2_COMP1;
	ETIMER_1.COMP22.R  = ETIMER1_CH2_COMP2;
	ETIMER_1.LOAD2.R   = ETIMER1_CH2_LOAD;
	ETIMER_1.CTRL12.R  = ETIMER1_CH2_CTRL1;
	ETIMER_1.CTRL22.R  = ETIMER1_CH2_CTRL2;
	ETIMER_1.CTRL32.R  = ETIMER1_CH2_CTRL3;
	ETIMER_1.INTDMA2.R = ETIMER1_CH2_INTDMA;
	ETIMER_1.CMPLD12.R = ETIMER1_CH2_CMPLD1;
	ETIMER_1.CMPLD22.R = ETIMER1_CH2_CMPLD2;
	ETIMER_1.CCCTRL2.R = ETIMER1_CH2_CCCTRL;
	ETIMER_1.FILT2.R   = ETIMER1_CH2_FILT;
#endif
	
	/*====================================================
	  Configure Timer 1 - channel 3
	 ====================================================*/
#if (ETIMER1_ENBL & 0x0008) > 0
	ETIMER_1.STS3.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_1.COMP13.R  = ETIMER1_CH3_COMP1;
	ETIMER_1.COMP23.R  = ETIMER1_CH3_COMP2;
	ETIMER_1.LOAD3.R   = ETIMER1_CH3_LOAD;
	ETIMER_1.CTRL13.R  = ETIMER1_CH3_CTRL1;
	ETIMER_1.CTRL23.R  = ETIMER1_CH3_CTRL2;
	ETIMER_1.CTRL33.R  = ETIMER1_CH3_CTRL3;
	ETIMER_1.INTDMA3.R = ETIMER1_CH3_INTDMA;
	ETIMER_1.CMPLD13.R = ETIMER1_CH3_CMPLD1;
	ETIMER_1.CMPLD23.R = ETIMER1_CH3_CMPLD2;
	ETIMER_1.CCCTRL3.R = ETIMER1_CH3_CCCTRL;
	ETIMER_1.FILT3.R   = ETIMER1_CH3_FILT;
#endif
	
	/*====================================================
	  Configure Timer 1 - channel 4
	 ====================================================*/
#if (ETIMER1_ENBL & 0x0010) > 0
	ETIMER_1.STS4.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_1.COMP14.R  = ETIMER1_CH4_COMP1;
	ETIMER_1.COMP24.R  = ETIMER1_CH4_COMP2;
	ETIMER_1.LOAD4.R   = ETIMER1_CH4_LOAD;
	ETIMER_1.CTRL14.R  = ETIMER1_CH4_CTRL1;
	ETIMER_1.CTRL24.R  = ETIMER1_CH4_CTRL2;
	ETIMER_1.CTRL34.R  = ETIMER1_CH4_CTRL3;
	ETIMER_1.INTDMA4.R = ETIMER1_CH4_INTDMA;
	ETIMER_1.CMPLD14.R = ETIMER1_CH4_CMPLD1;
	ETIMER_1.CMPLD24.R = ETIMER1_CH4_CMPLD2;
	ETIMER_1.CCCTRL4.R = ETIMER1_CH4_CCCTRL;
	ETIMER_1.FILT4.R   = ETIMER1_CH4_FILT;
#endif
	
	/*====================================================
	  Configure Timer 1 - channel 5
	 ====================================================*/
#if (ETIMER1_ENBL & 0x0020) > 0
	ETIMER_1.STS5.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_1.COMP15.R  = ETIMER1_CH5_COMP1;
	ETIMER_1.COMP25.R  = ETIMER1_CH5_COMP2;
	ETIMER_1.LOAD5.R   = ETIMER1_CH5_LOAD;
	ETIMER_1.CTRL15.R  = ETIMER1_CH5_CTRL1;
	ETIMER_1.CTRL25.R  = ETIMER1_CH5_CTRL2;
	ETIMER_1.CTRL35.R  = ETIMER1_CH5_CTRL3;
	ETIMER_1.INTDMA5.R = ETIMER1_CH5_INTDMA;
	ETIMER_1.CMPLD15.R = ETIMER1_CH5_CMPLD1;
	ETIMER_1.CMPLD25.R = ETIMER1_CH5_CMPLD2;
	ETIMER_1.CCCTRL5.R = ETIMER1_CH5_CCCTRL;
	ETIMER_1.FILT5.R   = ETIMER1_CH5_FILT;	
#endif
	
	/*====================================================
	  Configure Timer 1 - general
	 ====================================================*/
	// configure DREQ only if at least a channel is enable
#if ETIMER1_ENBL > 0
	ETIMER_1.DREQ0.R = ETIMER1_DREQ0;
    ETIMER_1.DREQ1.R = ETIMER1_DREQ1;
#endif
    
	ETIMER_1.ENBL.R  = ETIMER1_ENBL;	
	
	/*====================================================
	  Configure Timer 2 - channel 0
	 ====================================================*/
#if (ETIMER2_ENBL & 0x0001) > 0
	ETIMER_2.STS0.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_2.COMP10.R  = ETIMER2_CH0_COMP1;
	ETIMER_2.COMP20.R  = ETIMER2_CH0_COMP2;
	ETIMER_2.LOAD0.R   = ETIMER2_CH0_LOAD;
	ETIMER_2.CTRL10.R  = ETIMER2_CH0_CTRL1;
	ETIMER_2.CTRL20.R  = ETIMER2_CH0_CTRL2;
	ETIMER_2.CTRL30.R  = ETIMER2_CH0_CTRL3;
	ETIMER_2.INTDMA0.R = ETIMER2_CH0_INTDMA;
	ETIMER_2.CMPLD10.R = ETIMER2_CH0_CMPLD1;
	ETIMER_2.CMPLD20.R = ETIMER2_CH0_CMPLD2;
	ETIMER_2.CCCTRL0.R = ETIMER2_CH0_CCCTRL;
	ETIMER_2.FILT0.R   = ETIMER2_CH0_FILT;		
#endif
	
	/*====================================================
	  Configure Timer 2 - channel 1
	 ====================================================*/
#if (ETIMER2_ENBL & 0x0002) > 0
	ETIMER_2.STS1.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_2.COMP11.R  = ETIMER2_CH1_COMP1;
	ETIMER_2.COMP21.R  = ETIMER2_CH1_COMP2;
	ETIMER_2.LOAD1.R   = ETIMER2_CH1_LOAD;
	ETIMER_2.CTRL11.R  = ETIMER2_CH1_CTRL1;
	ETIMER_2.CTRL21.R  = ETIMER2_CH1_CTRL2;
	ETIMER_2.CTRL31.R  = ETIMER2_CH1_CTRL3;
	ETIMER_2.INTDMA1.R = ETIMER2_CH1_INTDMA;
	ETIMER_2.CMPLD11.R = ETIMER2_CH1_CMPLD1;
	ETIMER_2.CMPLD21.R = ETIMER2_CH1_CMPLD2;
	ETIMER_2.CCCTRL1.R = ETIMER2_CH1_CCCTRL;
	ETIMER_2.FILT1.R   = ETIMER2_CH1_FILT;		
#endif
	
	/*====================================================
	  Configure Timer 2 - channel 2
	 ====================================================*/
#if (ETIMER2_ENBL & 0x0004) > 0
	ETIMER_2.STS2.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_2.COMP12.R  = ETIMER2_CH2_COMP1;
	ETIMER_2.COMP22.R  = ETIMER2_CH2_COMP2;
	ETIMER_2.LOAD2.R   = ETIMER2_CH2_LOAD;
	ETIMER_2.CTRL12.R  = ETIMER2_CH2_CTRL1;
	ETIMER_2.CTRL22.R  = ETIMER2_CH2_CTRL2;
	ETIMER_2.CTRL32.R  = ETIMER2_CH2_CTRL3;
	ETIMER_2.INTDMA2.R = ETIMER2_CH2_INTDMA;
	ETIMER_2.CMPLD12.R = ETIMER2_CH2_CMPLD1;
	ETIMER_2.CMPLD22.R = ETIMER2_CH2_CMPLD2;
	ETIMER_2.CCCTRL2.R = ETIMER2_CH2_CCCTRL;
	ETIMER_2.FILT2.R   = ETIMER2_CH2_FILT;	
#endif
	
	/*====================================================
	  Configure Timer 2 - channel 3
	 ====================================================*/
#if (ETIMER2_ENBL & 0x0008) > 0
	ETIMER_2.STS3.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_2.COMP13.R  = ETIMER2_CH3_COMP1;
	ETIMER_2.COMP23.R  = ETIMER2_CH3_COMP2;
	ETIMER_2.LOAD3.R   = ETIMER2_CH3_LOAD;
	ETIMER_2.CTRL13.R  = ETIMER2_CH3_CTRL1;
	ETIMER_2.CTRL23.R  = ETIMER2_CH3_CTRL2;
	ETIMER_2.CTRL33.R  = ETIMER2_CH3_CTRL3;
	ETIMER_2.INTDMA3.R = ETIMER2_CH3_INTDMA;
	ETIMER_2.CMPLD13.R = ETIMER2_CH3_CMPLD1;
	ETIMER_2.CMPLD23.R = ETIMER2_CH3_CMPLD2;
	ETIMER_2.CCCTRL3.R = ETIMER2_CH3_CCCTRL;
	ETIMER_2.FILT3.R   = ETIMER2_CH3_FILT;	
#endif
	
	/*====================================================
	  Configure Timer 2 - channel 4
	 ====================================================*/
#if (ETIMER2_ENBL & 0x0010) > 0
	ETIMER_2.STS34.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_2.COMP14.R  = ETIMER2_CH4_COMP1;
	ETIMER_2.COMP24.R  = ETIMER2_CH4_COMP2;
	ETIMER_2.LOAD4.R   = ETIMER2_CH4_LOAD;
	ETIMER_2.CTRL14.R  = ETIMER2_CH4_CTRL1;
	ETIMER_2.CTRL24.R  = ETIMER2_CH4_CTRL2;
	ETIMER_2.CTRL34.R  = ETIMER2_CH4_CTRL3;
	ETIMER_2.INTDMA4.R = ETIMER2_CH4_INTDMA;
	ETIMER_2.CMPLD14.R = ETIMER2_CH4_CMPLD1;
	ETIMER_2.CMPLD24.R = ETIMER2_CH4_CMPLD2;
	ETIMER_2.CCCTRL4.R = ETIMER2_CH4_CCCTRL;
	ETIMER_2.FILT4.R   = ETIMER2_CH4_FILT;		
#endif
	
	/*====================================================
	  Configure Timer 2 - channel 5
	 ====================================================*/
#if (ETIMER2_ENBL & 0x0020) > 0
	ETIMER_2.STS35.R    = ETIMER_CLR_STS; // clear flags
	ETIMER_2.COMP15.R  = ETIMER2_CH5_COMP1;
	ETIMER_2.COMP25.R  = ETIMER2_CH5_COMP2;
	ETIMER_2.LOAD5.R   = ETIMER2_CH5_LOAD;
	ETIMER_2.CTRL15.R  = ETIMER2_CH5_CTRL1;
	ETIMER_2.CTRL25.R  = ETIMER2_CH5_CTRL2;
	ETIMER_2.CTRL35.R  = ETIMER2_CH5_CTRL3;
	ETIMER_2.INTDMA5.R = ETIMER2_CH5_INTDMA;
	ETIMER_2.CMPLD15.R = ETIMER2_CH5_CMPLD1;
	ETIMER_2.CMPLD25.R = ETIMER2_CH5_CMPLD2;
	ETIMER_2.CCCTRL5.R = ETIMER2_CH5_CCCTRL;
	ETIMER_2.FILT5.R   = ETIMER2_CH5_FILT;	
#endif
	
	/*====================================================
	  Configure Timer 2 - general
	 ====================================================*/
	// configure DREQ only if at least a channel is enable
#if ETIMER2_ENBL > 0
	ETIMER_2.DREQ0.R = ETIMER2_DREQ0;
    ETIMER_2.DREQ1.R = ETIMER2_DREQ1;
#endif
    
	ETIMER_2.ENBL.R  = ETIMER2_ENBL;
    
    
	/*====================================================
	  Lock configurations
	 ====================================================*/
}
