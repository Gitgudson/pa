/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Nov 12
 *	Last modification : 2015 Nov 12
 *      Author        : Nicolas Broch
 */

#include "MPC5643L.h"
#include "HAL/mpc5643l_reset.h"
#include "HAL/mpc5643l_siul.h"


void reset_init()
{
	/*====================================================
	  Configure the MC_RGM
	 ====================================================*/
	RGM.FERD.R = RESET_RGM_FERD;
	RGM.FEAR.R = RESET_RGM_FEAR;
	RGM.FESS.R = RESET_RGM_FESS;
	RGM.FBRE.R =  RESET_RGM_FBRE;
}


int32_t reset_read_reset()
{
	int32_t  returnVal = 0;
	uint16_t fes = 0, des = 0;
	
	/*====================================================
	  Read reset flags
	 ====================================================*/
	fes = RGM.FES.R;
	des = RGM.DES.R;
	
	
	/*====================================================
	  Analyse the reset flags
	 ====================================================*/
	/*
	 * Power on Reset and self-test completed
	 */
	if((des & RESET_MASK_POR) != 0 && (fes & RESET_MASK_ST) != 0)
	{
		returnVal = 0;
	}
	/*
	 * Software RESET (functional reset)
	 */
	else if((fes & RESET_MASK_SWRES) != 0)
	{
		returnVal = -1;
	}
	/*
	 * Failure reset
	 */
	else
	{
		returnVal = -2;
	}
	
	return returnVal;
}


void reset_clear_reset()
{
	/*====================================================
	  Reset all reset flags
	 ====================================================*/
	RGM.FES.R  = 0xFFFF;
	RGM.DES.R  = 0xFFFF;
}
