/*
/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Oct 29
 *  Last modification : 2015 Oct 29
 *  Author            : Nicolas Broch
 */

#include "HAL/mpc5643l_siul.h"


/*******************************************************************************
* SIUL global variables
*******************************************************************************/


void siul_init()
{
	/*====================================================
	 Disable interrupts
	 	 - disable in interrupt request enable
	 	 - disable the rising-edge event
	 	 - disable the falling-edge event
	====================================================*/
	SIUL.IRER.R  = 0x00000000;
	SIUL.IREER.R = 0x00000000; 
	SIUL.IFEER.R = 0x00000000;	
}

void siul_config_pad(const Siul_pcr_e pad, 
					 const Hal_YN_e safeModeEN,
		             const Hal_YN_e analogPad, 
		             const Siul_gpio_mode_e mode, 
		             const Siul_inputOutput_e inout,
		             const Siul_slewRate_e slewRateControl,
		             const Siul_inputOutputType_e type,
		             const Hal_YN_e lockConfig)
{
	uint16_t pcrTmp = 0x0000;
	
	/*====================================================
	  Prepare PCR register
	====================================================*/
	
	/****** Safe Mode Control ******/
	if(safeModeEN == HAL_YES)
	{
		pcrTmp |= 0x4000;
	}
	else
	{
		pcrTmp &= ~0x4000;
	}
	
	/* Analog Pad Control */
	if(analogPad == HAL_YES)
	{
		pcrTmp |= 0x2000;
	}
	else // SIUL_NO or undefined case
	{
		pcrTmp &= ~0x2000;
	}
	
	/****** Pad Output Assignment ******/
	if(mode == SIUL_MODE_ALTER_1)
	{
		pcrTmp |= 0x0400;
	}
	else if(mode == SIUL_MODE_ALTER_2)
	{
		pcrTmp |= 0x0800;
	}
	else if(mode == SIUL_MODE_ALTER_3)
	{
		pcrTmp |= 0x0C00;
	}
	else // SIUL_MODE_GPIO or undefined case
	{
		pcrTmp &= ~0x0C00;
	}
	
	/****** Input/Output pad ******/
	if(inout == SIUL_INPUT)
	{
		pcrTmp |= 0x0100;
	}
	else if(inout == SIUL_OUTPUT)
	{
		pcrTmp |= 0x0300;
	}
	else // SIUL_INOUT_NONE or undefined case
	{
		pcrTmp |= 0x0300;
	}
	
	/****** Slew Rate Control ******/
	if(slewRateControl == SIUL_FAST)
	{
		pcrTmp |= 0x0004;
	}
	else // SIUL_SLOW or undefined case
	{
		pcrTmp &= ~0x0004;
	}
	
	/****** Open drain, pull-up, pull-down ******/
	if(type == SIUL_OUTPUT_OPEN_DRAIN && inout == SIUL_OUTPUT) // open drain
	{
		pcrTmp |= 0x0020;
	}
	else if(type == SIUL_INPUT_PULL_UP && inout == SIUL_INPUT) // pull-up resistor
	{
		pcrTmp |= 0x0003;
	}
	else if(type == SIUL_INPUT_PULL_DOWN && inout == SIUL_INPUT) // pull-down resistor
	{
		pcrTmp |= 0x0002;
	}
	else  // outputType == SIUL_TYPE_NONE or undetermined case
	{
		pcrTmp &= ~0x0023; 
	}
	
	/*====================================================
	  Write PCR register
	====================================================*/
	SIUL.PCR[pad].R = pcrTmp;
	
	/*====================================================
	  Lock register Pad
	====================================================*/
	if(lockConfig == HAL_YES)
	{
		
	}
	else // no lock protection, nothing to do
	{
		
	}
}

void siul_deconfig_pad(const Siul_pcr_e pad)
{
	/*====================================================
	  Reset register PCR of pad
	====================================================*/
	SIUL.PCR[pad].R = 0x0000;
}


void siul_config_padSelection(const uint8_t psmiVal, const uint8_t padsel_value)
{
	SIUL.PSMI[psmiVal].R = padsel_value;
}


int32_t siul_read_pad(const Siul_pcr_e pad)
{
	int32_t returnVal = 0; // value to return
	
	/*====================================================
	  Check if the pad input register is configured
	====================================================*/
	if((SIUL.PCR[pad].R & SIUL_PCR_IBE) > 0)
	{
		/*====================================================
		  Read the input value
		====================================================*/
		returnVal = (int8_t) SIUL.GPDI[pad].R;
	}
	else /* not configured as an input */
	{
		returnVal = -1;
	}
		
	return returnVal;
}

int32_t siul_write_pad(const Siul_pcr_e pad, const uint8_t value)
{
	int32_t returnVal = 0;
	
	/*====================================================
	  Check if the pad output and input registers are 
	  configured
	====================================================*/
	if((SIUL.PCR[pad].R & (SIUL_PCR_OBE | SIUL_PCR_IBE)) == (SIUL_PCR_OBE | SIUL_PCR_IBE))
	{
		/*====================================================
		  Check the number in Value, as value is an unsigned, 
		  only values > 1 are illegal
		====================================================*/
		if(value <= 1)
		{
			/*====================================================
			  Write the value on the pad
			====================================================*/
			SIUL.GPDO[pad].R = value;
			
			/*====================================================
			  Wait until the pad is written correctly
			====================================================*/
			/* Read back the value written into the pad */
			while(SIUL.GPDI[pad].R != value){}
		}

		/*====================================================
		  Illegal number in value
		====================================================*/
		else
		{
			returnVal = -2;
		}	
	}
	/*====================================================
	  Pad not configured as an output and input
	====================================================*/
	else
	{
		returnVal = -1;
	}
	
	return returnVal;
}


int32_t siul_write_fast_pad(const Siul_pcr_e pad, const uint8_t value)
{
	int32_t returnVal = 0;
	
	/*====================================================
	  Check if the pad output register is configured
	====================================================*/
	if((SIUL.PCR[pad].R & SIUL_PCR_OBE) > 0)
	{
		/*====================================================
		  Check the number in Value, as value is an unsigned, 
		  only values > 1 are illegal
		====================================================*/
		if(value <= 1)
		{
			/*====================================================
			  Write the value on the pad
			====================================================*/
			SIUL.GPDO[pad].R = value;
		}

		/*====================================================
		  Illegal number in value
		====================================================*/
		else
		{
			returnVal = -2;
		}	
	}
	/*====================================================
	  Pad not configured as an output
	====================================================*/
	else
	{
		returnVal = -1;
	}
	
	return returnVal;	
}

int32_t siul_toggle_pad(const Siul_pcr_e pad)
{
	int32_t returnVal = 0;
	
	/*====================================================
	  Check if the pad output and input registers are
	  configured
	====================================================*/
	if((SIUL.PCR[pad].R & (SIUL_PCR_OBE|SIUL_PCR_IBE)) == (SIUL_PCR_OBE|SIUL_PCR_IBE))
	{
		SIUL.GPDO[pad].R = (vuint8_t) ~SIUL.GPDI[pad].R;
	}
	/*====================================================
	  Pad not configured as an output and input
	====================================================*/
	else
	{
		returnVal = -1;
	}
	
	return returnVal;
}


int32_t siul_SWTEST_REGCRC()
{
	
}

