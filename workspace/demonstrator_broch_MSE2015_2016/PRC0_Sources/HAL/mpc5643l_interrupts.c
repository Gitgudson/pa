/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Dec 23
 *	Last modification : 2015 Dec 23
 *      Author        : Nicolas Broch
 */


#include "HAL/mpc5643l_interrupts.h"


void interrupts_init()
{
	uint16_t i = 0;
	
	/*====================================================
	 Initialise interrupts in software vector mode
	====================================================*/
	INTC_InitINTCInterrupts();
	
	/*====================================================
	 By default, disable all interrupts
	====================================================*/
	interrupts_disable();
	
	/*====================================================
	 Configure all default interrupt vectors to error vector
	 with highest priority as it is an error.
	====================================================*/
	for(i=0; i < INTERRUPTS_NBR; i++)
	{
		INTC_InstallINTCInterruptHandler(interrupts_IRQ_error, 
				                         i, 15);
	}
}

void interrupts_enable()
{
	/*====================================================
	 Put interrupt mask to lowest priority to enable
	 all interrupts
	====================================================*/
	INTC.CPR_PRC0.R = INTERRUPTS_LOW_PRIO;
}

void interrupts_disable()
{
	/*====================================================
	 Put interrupt mask to highest priority to disable
	 all interrupts
	====================================================*/
	INTC.CPR_PRC0.R = INTERRUPTS_HIGH_PRIO;
}

void interrupts_IRQ_error()
{
	/*====================================================
	  Error, reset the MCU
	====================================================*/
	mode_sw_error();
}
