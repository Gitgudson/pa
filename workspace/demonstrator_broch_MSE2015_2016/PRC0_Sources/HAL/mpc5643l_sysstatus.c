/*
/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Nov 16
 *  Last modification : 2015 Nov 16
 *  Author            : Nicolas Broch
 */

#include "MPC5643l.h"
#include "HAL/mpc5643l_sysstatus.h"


int32_t sysstatus_check()
{
	int32_t returnVal = 0;
	
	/*====================================================
	  Configure SoC error configuration
	 ====================================================*/
	SSCM.ERROR.R = SYSSTATUS_ERROR;
	
	/*====================================================
	  Check the system Status
	 ====================================================*/
	if(SIUL.MIDR1.R != SYSSTATUS_MIDR1 ||
	   SIUL.MIDR2.R != SYSSTATUS_MIDR2 ||
	   (SSCM.STATUS.R & SYSSTATUS_STATUS_MASK) != SYSSTATUS_STATUS ||
	   (SSCM.MEMCONFIG.R & SYSSTATUS_MEMCONFIG_MASK) != SYSSTATUS_MEMCONFIG)
	{
		returnVal = -1;
	}
	else
	{			
	}
	
	
	/*====================================================
	  Lock registers
	 ====================================================*/
	
	return returnVal;
}
