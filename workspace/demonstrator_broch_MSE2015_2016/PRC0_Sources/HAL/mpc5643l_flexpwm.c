/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Dec 16
 *	Last modification : 2015 Dec 16
 *      Author        : Nicolas Broch
 */


#include "HAL/mpc5643l_flexpwm.h"




void flexpwm_init()
{
	/*====================================================
	  FlexPWM0 and FlexPWM1 - Stop FlexPWM0 in case it was running
	 ====================================================*/
	FLEXPWM_0.MCTRL.R = 0x0000U;
	FLEXPWM_1.MCTRL.R = 0x0000U;
	
	/*====================================================
	  FlexPWM0 - Configuration of FAULT input pads
	 ====================================================*/
	//FAULT[0]
	siul_config_pad(PCR_G_8,HAL_NO, HAL_NO, 
				    SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	siul_config_padSelection(16, 2);
	//FAULT[1]
	siul_config_pad(PCR_G_9,HAL_NO, HAL_NO, 
				    SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	siul_config_padSelection(17, 2);
	//FAULT[2]
	siul_config_pad(PCR_G_10,HAL_NO, HAL_NO, 
				    SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	siul_config_padSelection(18, 1);
	//FAULT[3]
	siul_config_pad(PCR_G_11,HAL_NO, HAL_NO, 
				    SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	siul_config_padSelection(19, 2);
	
	/*====================================================
	  FlexPWM1 - Configuration of FAULT input pads
	 ====================================================*/
	//FAULT[0]
	siul_config_pad(PCR_I_0,HAL_NO, HAL_NO, 
				    SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	//FAULT[1]
	siul_config_pad(PCR_I_1,HAL_NO, HAL_NO, 
				    SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	//FAULT[2]
	siul_config_pad(PCR_I_2,HAL_NO, HAL_NO, 
				    SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	//FAULT[3]
	siul_config_pad(PCR_I_3,HAL_NO, HAL_NO, 
				    SIUL_MODE_GPIO, SIUL_INPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);

	/*====================================================
	  Configure FlexPWM0 - submodule 0
	 ====================================================*/
#if (FLEXPWM0_MCTRL & 0x0100) > 0	
	/*====================================================
	  Configure Pads of submodule 0
	 ====================================================*/
	// PWMA
	siul_config_pad(PCR_D_10,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	//PWMB
	siul_config_pad(PCR_D_11,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Configure register of FlexPWM0-sub0
	 ====================================================*/
	FLEXPWM_0.SUB[0].INIT.R      = FLEXPWM0_INIT_0;
	FLEXPWM_0.SUB[0].CTRL2.R     = FLEXPWM0_CTRL2_0;
	FLEXPWM_0.SUB[0].CTRL1.R     = FLEXPWM0_CTRL1_0;
	FLEXPWM_0.SUB[0].VAL_0.R     = FLEXPWM0_VAL0_0;
	FLEXPWM_0.SUB[0].VAL_1.R     = FLEXPWM0_VAL1_0;
	FLEXPWM_0.SUB[0].VAL_2.R     = FLEXPWM0_VAL2_0;
	FLEXPWM_0.SUB[0].VAL_3.R     = FLEXPWM0_VAL3_0;
	FLEXPWM_0.SUB[0].VAL_4.R     = FLEXPWM0_VAL4_0;
	FLEXPWM_0.SUB[0].VAL_5.R     = FLEXPWM0_VAL5_0;
	FLEXPWM_0.SUB[0].OCTRL.R     = FLEXPWM0_OCTRL_0;
	FLEXPWM_0.SUB[0].INTEN.R     = FLEXPWM0_INTEN_0;
	FLEXPWM_0.SUB[0].DMAEN.R     = FLEXPWM0_DMAEN_0;
	FLEXPWM_0.SUB[0].TCTRL.R     = FLEXPWM0_TCTRL_0;
	FLEXPWM_0.SUB[0].DISMAP.R    = FLEXPWM0_DISMAP_0;
	FLEXPWM_0.SUB[0].DTCNT0.R    = FLEXPWM0_DTCNT0_0;
	FLEXPWM_0.SUB[0].DTCNT1.R    = FLEXPWM0_DTCNT1_0;
	FLEXPWM_0.SUB[0].CAPTCMPX.R  = FLEXPWM0_CAPTCMPX_0;
	FLEXPWM_0.SUB[0].CAPTCTRLX.R = FLEXPWM0_CAPTCTRLX_0;
#endif

	
	/*====================================================
	  Configure FlexPWM0 - submodule 1
	 ====================================================*/
#if (FLEXPWM0_MCTRL & 0x0200) > 0
	/*====================================================
	  Configure Pads of submodule 1
	 ====================================================*/
	// PWMA
	siul_config_pad(PCR_F_0,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	//PWMB
	siul_config_pad(PCR_D_14,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Configure register of FlexPWM0-sub0
	 ====================================================*/
	FLEXPWM_0.SUB[1].INIT.R      = FLEXPWM0_INIT_1;
	FLEXPWM_0.SUB[1].CTRL2.R     = FLEXPWM0_CTRL2_1;
	FLEXPWM_0.SUB[1].CTRL1.R     = FLEXPWM0_CTRL1_1;
	FLEXPWM_0.SUB[1].VAL_0.R     = FLEXPWM0_VAL0_1;
	FLEXPWM_0.SUB[1].VAL_1.R     = FLEXPWM0_VAL1_1;
	FLEXPWM_0.SUB[1].VAL_2.R     = FLEXPWM0_VAL2_1;
	FLEXPWM_0.SUB[1].VAL_3.R     = FLEXPWM0_VAL3_1;
	FLEXPWM_0.SUB[1].VAL_4.R     = FLEXPWM0_VAL4_1;
	FLEXPWM_0.SUB[1].VAL_5.R     = FLEXPWM0_VAL5_1;
	FLEXPWM_0.SUB[1].OCTRL.R     = FLEXPWM0_OCTRL_1;
	FLEXPWM_0.SUB[1].INTEN.R     = FLEXPWM0_INTEN_1;
	FLEXPWM_0.SUB[1].DMAEN.R     = FLEXPWM0_DMAEN_1;
	FLEXPWM_0.SUB[1].TCTRL.R     = FLEXPWM0_TCTRL_1;
	FLEXPWM_0.SUB[1].DISMAP.R    = FLEXPWM0_DISMAP_1;
	FLEXPWM_0.SUB[1].DTCNT0.R    = FLEXPWM0_DTCNT0_1;
	FLEXPWM_0.SUB[1].DTCNT1.R    = FLEXPWM0_DTCNT1_1;
	FLEXPWM_0.SUB[1].CAPTCMPX.R  = FLEXPWM0_CAPTCMPX_1;
	FLEXPWM_0.SUB[1].CAPTCTRLX.R = FLEXPWM0_CAPTCTRLX_1;
#endif
	
	/*====================================================
	  Configure FlexPWM0 - submodule 2
	 ====================================================*/
#if (FLEXPWM0_MCTRL & 0x0400) > 0
	/*====================================================
	  Configure Pads of submodule 2
	 ====================================================*/
	// PWMA
	siul_config_pad(PCR_G_3,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	//PWMB
	siul_config_pad(PCR_G_4,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Configure register of FlexPWM0-sub0
	 ====================================================*/
	FLEXPWM_0.SUB[2].INIT.R      = FLEXPWM0_INIT_2;
	FLEXPWM_0.SUB[2].CTRL2.R     = FLEXPWM0_CTRL2_2;
	FLEXPWM_0.SUB[2].CTRL1.R     = FLEXPWM0_CTRL1_2;
	FLEXPWM_0.SUB[2].VAL_0.R     = FLEXPWM0_VAL0_2;
	FLEXPWM_0.SUB[2].VAL_1.R     = FLEXPWM0_VAL1_2;
	FLEXPWM_0.SUB[2].VAL_2.R     = FLEXPWM0_VAL2_2;
	FLEXPWM_0.SUB[2].VAL_3.R     = FLEXPWM0_VAL3_2;
	FLEXPWM_0.SUB[2].VAL_4.R     = FLEXPWM0_VAL4_2;
	FLEXPWM_0.SUB[2].VAL_5.R     = FLEXPWM0_VAL5_2;
	FLEXPWM_0.SUB[2].OCTRL.R     = FLEXPWM0_OCTRL_2;
	FLEXPWM_0.SUB[2].INTEN.R     = FLEXPWM0_INTEN_2;
	FLEXPWM_0.SUB[2].DMAEN.R     = FLEXPWM0_DMAEN_2;
	FLEXPWM_0.SUB[2].TCTRL.R     = FLEXPWM0_TCTRL_2;
	FLEXPWM_0.SUB[2].DISMAP.R    = FLEXPWM0_DISMAP_2;
	FLEXPWM_0.SUB[2].DTCNT0.R    = FLEXPWM0_DTCNT0_2;
	FLEXPWM_0.SUB[2].DTCNT1.R    = FLEXPWM0_DTCNT1_2;
	FLEXPWM_0.SUB[2].CAPTCMPX.R  = FLEXPWM0_CAPTCMPX_2;
	FLEXPWM_0.SUB[2].CAPTCTRLX.R = FLEXPWM0_CAPTCTRLX_2;
#endif
	
	/*====================================================
	  Configure FlexPWM0 - submodule 3
	 ====================================================*/
#if (FLEXPWM0_MCTRL & 0x0800) > 0
	/*====================================================
	  Configure Pads of submodule 3
	 ====================================================*/
	// PWMA
	siul_config_pad(PCR_G_6,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	//PWMB
	siul_config_pad(PCR_G_7,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Configure register of FlexPWM0-sub0
	 ====================================================*/
	FLEXPWM_0.SUB[3].INIT.R      = FLEXPWM0_INIT_3;
	FLEXPWM_0.SUB[3].CTRL2.R     = FLEXPWM0_CTRL2_3;
	FLEXPWM_0.SUB[3].CTRL1.R     = FLEXPWM0_CTRL1_3;
	FLEXPWM_0.SUB[3].VAL_0.R     = FLEXPWM0_VAL0_3;
	FLEXPWM_0.SUB[3].VAL_1.R     = FLEXPWM0_VAL1_3;
	FLEXPWM_0.SUB[3].VAL_2.R     = FLEXPWM0_VAL2_3;
	FLEXPWM_0.SUB[3].VAL_3.R     = FLEXPWM0_VAL3_3;
	FLEXPWM_0.SUB[3].VAL_4.R     = FLEXPWM0_VAL4_3;
	FLEXPWM_0.SUB[3].VAL_5.R     = FLEXPWM0_VAL5_3;
	FLEXPWM_0.SUB[3].OCTRL.R     = FLEXPWM0_OCTRL_3;
	FLEXPWM_0.SUB[3].INTEN.R     = FLEXPWM0_INTEN_3;
	FLEXPWM_0.SUB[3].DMAEN.R     = FLEXPWM0_DMAEN_3;
	FLEXPWM_0.SUB[3].TCTRL.R     = FLEXPWM0_TCTRL_3;
	FLEXPWM_0.SUB[3].DISMAP.R    = FLEXPWM0_DISMAP_3;
	FLEXPWM_0.SUB[3].DTCNT0.R    = FLEXPWM0_DTCNT0_3;
	FLEXPWM_0.SUB[3].DTCNT1.R    = FLEXPWM0_DTCNT1_3;
	FLEXPWM_0.SUB[3].CAPTCMPX.R  = FLEXPWM0_CAPTCMPX_3;
	FLEXPWM_0.SUB[3].CAPTCTRLX.R = FLEXPWM0_CAPTCTRLX_3;
#endif
	
	/*====================================================
	  Configure FlexPWM1 - submodule 0
	 ====================================================*/
#if (FLEXPWM1_MCTRL & 0x0100) > 0	
	/*====================================================
	  Configure Pads of submodule 0
	 ====================================================*/
	// PWMA
	siul_config_pad(PCR_H_5,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	//PWMB
	siul_config_pad(PCR_H_6,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Configure register of FlexPWM0-sub0
	 ====================================================*/
	FLEXPWM_1.SUB[0].INIT.R      = FLEXPWM1_INIT_0;
	FLEXPWM_1.SUB[0].CTRL2.R     = FLEXPWM1_CTRL2_0;
	FLEXPWM_1.SUB[0].CTRL1.R     = FLEXPWM1_CTRL1_0;
	FLEXPWM_1.SUB[0].VAL_0.R     = FLEXPWM1_VAL0_0;
	FLEXPWM_1.SUB[0].VAL_1.R     = FLEXPWM1_VAL1_0;
	FLEXPWM_1.SUB[0].VAL_2.R     = FLEXPWM1_VAL2_0;
	FLEXPWM_1.SUB[0].VAL_3.R     = FLEXPWM1_VAL3_0;
	FLEXPWM_1.SUB[0].VAL_4.R     = FLEXPWM1_VAL4_0;
	FLEXPWM_1.SUB[0].VAL_5.R     = FLEXPWM1_VAL5_0;
	FLEXPWM_1.SUB[0].OCTRL.R     = FLEXPWM1_OCTRL_0;
	FLEXPWM_1.SUB[0].INTEN.R     = FLEXPWM1_INTEN_0;
	FLEXPWM_1.SUB[0].DMAEN.R     = FLEXPWM1_DMAEN_0;
	FLEXPWM_1.SUB[0].TCTRL.R     = FLEXPWM1_TCTRL_0;
	FLEXPWM_1.SUB[0].DISMAP.R    = FLEXPWM1_DISMAP_0;
	FLEXPWM_1.SUB[0].DTCNT0.R    = FLEXPWM1_DTCNT0_0;
	FLEXPWM_1.SUB[0].DTCNT1.R    = FLEXPWM1_DTCNT1_0;
	FLEXPWM_1.SUB[0].CAPTCMPX.R  = FLEXPWM1_CAPTCMPX_0;
	FLEXPWM_1.SUB[0].CAPTCTRLX.R = FLEXPWM1_CAPTCTRLX_0;
#endif
	
	/*====================================================
	  Configure FlexPWM1 - submodule 1
	 ====================================================*/
#if (FLEXPWM1_MCTRL & 0x0200) > 0
	/*====================================================
	  Configure Pads of submodule 1
	 ====================================================*/
	// PWMA
	siul_config_pad(PCR_H_8,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	//PWMB
	siul_config_pad(PCR_H_9,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Configure register of FlexPWM0-sub0
	 ====================================================*/
	FLEXPWM_1.SUB[1].INIT.R      = FLEXPWM1_INIT_1;
	FLEXPWM_1.SUB[1].CTRL2.R     = FLEXPWM1_CTRL2_1;
	FLEXPWM_1.SUB[1].CTRL1.R     = FLEXPWM1_CTRL1_1;
	FLEXPWM_1.SUB[1].VAL_0.R     = FLEXPWM1_VAL0_1;
	FLEXPWM_1.SUB[1].VAL_1.R     = FLEXPWM1_VAL1_1;
	FLEXPWM_1.SUB[1].VAL_2.R     = FLEXPWM1_VAL2_1;
	FLEXPWM_1.SUB[1].VAL_3.R     = FLEXPWM1_VAL3_1;
	FLEXPWM_1.SUB[1].VAL_4.R     = FLEXPWM1_VAL4_1;
	FLEXPWM_1.SUB[1].VAL_5.R     = FLEXPWM1_VAL5_1;
	FLEXPWM_1.SUB[1].OCTRL.R     = FLEXPWM1_OCTRL_1;
	FLEXPWM_1.SUB[1].INTEN.R     = FLEXPWM1_INTEN_1;
	FLEXPWM_1.SUB[1].DMAEN.R     = FLEXPWM1_DMAEN_1;
	FLEXPWM_1.SUB[1].TCTRL.R     = FLEXPWM1_TCTRL_1;
	FLEXPWM_1.SUB[1].DISMAP.R    = FLEXPWM1_DISMAP_1;
	FLEXPWM_1.SUB[1].DTCNT0.R    = FLEXPWM1_DTCNT0_1;
	FLEXPWM_1.SUB[1].DTCNT1.R    = FLEXPWM1_DTCNT1_1;
	FLEXPWM_1.SUB[1].CAPTCMPX.R  = FLEXPWM1_CAPTCMPX_1;
	FLEXPWM_1.SUB[1].CAPTCTRLX.R = FLEXPWM1_CAPTCTRLX_1;
#endif
	
	/*====================================================
	  Configure FlexPWM1 - submodule 2
	 ====================================================*/
#if (FLEXPWM1_MCTRL & 0x0400) > 0
	/*====================================================
	  Configure Pads of submodule 2
	 ====================================================*/
	// PWMA
	siul_config_pad(PCR_H_11,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	//PWMB
	siul_config_pad(PCR_H_12,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Configure register of FlexPWM0-sub0
	 ====================================================*/
	FLEXPWM_1.SUB[2].INIT.R      = FLEXPWM1_INIT_2;
	FLEXPWM_1.SUB[2].CTRL2.R     = FLEXPWM1_CTRL2_2;
	FLEXPWM_1.SUB[2].CTRL1.R     = FLEXPWM1_CTRL1_2;
	FLEXPWM_1.SUB[2].VAL_0.R     = FLEXPWM1_VAL0_2;
	FLEXPWM_1.SUB[2].VAL_1.R     = FLEXPWM1_VAL1_2;
	FLEXPWM_1.SUB[2].VAL_2.R     = FLEXPWM1_VAL2_2;
	FLEXPWM_1.SUB[2].VAL_3.R     = FLEXPWM1_VAL3_2;
	FLEXPWM_1.SUB[2].VAL_4.R     = FLEXPWM1_VAL4_2;
	FLEXPWM_1.SUB[2].VAL_5.R     = FLEXPWM1_VAL5_2;
	FLEXPWM_1.SUB[2].OCTRL.R     = FLEXPWM1_OCTRL_2;
	FLEXPWM_1.SUB[2].INTEN.R     = FLEXPWM1_INTEN_2;
	FLEXPWM_1.SUB[2].DMAEN.R     = FLEXPWM1_DMAEN_2;
	FLEXPWM_1.SUB[2].TCTRL.R     = FLEXPWM1_TCTRL_2;
	FLEXPWM_1.SUB[2].DISMAP.R    = FLEXPWM1_DISMAP_2;
	FLEXPWM_1.SUB[2].DTCNT0.R    = FLEXPWM1_DTCNT0_2;
	FLEXPWM_1.SUB[2].DTCNT1.R    = FLEXPWM1_DTCNT1_2;
	FLEXPWM_1.SUB[2].CAPTCMPX.R  = FLEXPWM1_CAPTCMPX_2;
	FLEXPWM_1.SUB[2].CAPTCTRLX.R = FLEXPWM1_CAPTCTRLX_2;
#endif
	
	/*====================================================
	  Configure FlexPWM1 - submodule 3
	 ====================================================*/
#if (FLEXPWM1_MCTRL & 0x0800) > 0
	/*====================================================
	  Configure Pads of submodule 3
	 ====================================================*/
	// PWMA
	siul_config_pad(PCR_H_14,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	//PWMB
	siul_config_pad(PCR_H_15,HAL_NO, HAL_NO, 
					SIUL_MODE_ALTER_1, SIUL_OUTPUT,
					SIUL_SLOW, SIUL_TYPE_NONE,
					HAL_YES);
	
	/*====================================================
	  Configure register of FlexPWM0-sub0
	 ====================================================*/
	FLEXPWM_1.SUB[3].INIT.R      = FLEXPWM1_INIT_3;
	FLEXPWM_1.SUB[3].CTRL2.R     = FLEXPWM1_CTRL2_3;
	FLEXPWM_1.SUB[3].CTRL1.R     = FLEXPWM1_CTRL1_3;
	FLEXPWM_1.SUB[3].VAL_0.R     = FLEXPWM1_VAL0_3;
	FLEXPWM_1.SUB[3].VAL_1.R     = FLEXPWM1_VAL1_3;
	FLEXPWM_1.SUB[3].VAL_2.R     = FLEXPWM1_VAL2_3;
	FLEXPWM_1.SUB[3].VAL_3.R     = FLEXPWM1_VAL3_3;
	FLEXPWM_1.SUB[3].VAL_4.R     = FLEXPWM1_VAL4_3;
	FLEXPWM_1.SUB[3].VAL_5.R     = FLEXPWM1_VAL5_3;
	FLEXPWM_1.SUB[3].OCTRL.R     = FLEXPWM1_OCTRL_3;
	FLEXPWM_1.SUB[3].INTEN.R     = FLEXPWM1_INTEN_3;
	FLEXPWM_1.SUB[3].DMAEN.R     = FLEXPWM1_DMAEN_3;
	FLEXPWM_1.SUB[3].TCTRL.R     = FLEXPWM1_TCTRL_3;
	FLEXPWM_1.SUB[3].DISMAP.R    = FLEXPWM1_DISMAP_3;
	FLEXPWM_1.SUB[3].DTCNT0.R    = FLEXPWM1_DTCNT0_3;
	FLEXPWM_1.SUB[3].DTCNT1.R    = FLEXPWM1_DTCNT1_3;
	FLEXPWM_1.SUB[3].CAPTCMPX.R  = FLEXPWM1_CAPTCMPX_3;
	FLEXPWM_1.SUB[3].CAPTCTRLX.R = FLEXPWM1_CAPTCTRLX_3;
#endif

	
	/*====================================================
	  FlexPWM0 - General configuration
	 ====================================================*/
	FLEXPWM_0.OUTEN.R    = FLEXPWM0_OUTEN;
	FLEXPWM_0.MASK.R     = FLEXPWM0_MASK;
	FLEXPWM_0.SWCOUT.R   = FLEXPWM0_SWCOUT;
	FLEXPWM_0.DTSRCSEL.R = FLEXPWM0_DTSRCSEL;
	FLEXPWM_0.FSTS.R     = FLEXPWM0_FSTS;
	FLEXPWM_0.FCTRL.R    = FLEXPWM0_FCTRL;
	FLEXPWM_0.FFILT.R    = FLEXPWM0_FFILT;
	
	/*====================================================
	  FlexPWM1 - General configuration
	 ====================================================*/
	FLEXPWM_1.OUTEN.R    = FLEXPWM1_OUTEN;
	FLEXPWM_1.MASK.R     = FLEXPWM1_MASK;
	FLEXPWM_1.SWCOUT.R   = FLEXPWM1_SWCOUT;
	FLEXPWM_1.DTSRCSEL.R = FLEXPWM1_DTSRCSEL;
	FLEXPWM_1.FSTS.R     = FLEXPWM1_FSTS;
	FLEXPWM_1.FCTRL.R    = FLEXPWM1_FCTRL;
	FLEXPWM_1.FFILT.R    = FLEXPWM1_FFILT;
	
	
	/*====================================================
	  Allow to update PWM values in both FlexPWM modules
	 ====================================================*/
	FLEXPWM_0.MCTRL.R |= FLEXPWM0_MCTRL | FLEXPWM_LDOK;
	FLEXPWM_1.MCTRL.R |= FLEXPWM1_MCTRL | FLEXPWM_LDOK;
}



int32_t flexpwm_run(Flexpwm_module module)
{
	int32_t  returnVal  = 0;
	
	/*====================================================
	  Activate FlexPWM0, FlexPWM1 or all modules regarding
	  of value of modNum. If modNum == 255, turn on all
	  FlexPWM modules. Other values leads to system error.
	 ====================================================*/
	if(module == FLEXPWM_MOD0){
		FLEXPWM_0.MASK.R = 0x0000U;
	}
	else if(module == FLEXPWM_MOD1){
		FLEXPWM_1.MASK.R = 0x0000U;
	}
	else if(module == FLEXPWM_ALL){
		FLEXPWM_0.MASK.R = 0x0000U;
		FLEXPWM_1.MASK.R = 0x0000U;
	}
	else{
		returnVal = -1;
	}
	
	return returnVal;
}


int32_t flexpwm_stop(Flexpwm_module module)
{
	int32_t  returnVal  = 0;
	
	/*====================================================
	  Disable FlexPWM0, FlexPWM1 or all modules regarding
	  of value of modNum. If modNum == 255, turn off all
	  FlexPWM modules. Other values leads to system error.
	 ====================================================*/
	if(module == FLEXPWM_MOD0){
		FLEXPWM_0.MASK.R = FLEXPWM_MASK_OFF;
	}
	else if(module == FLEXPWM_MOD1){
		FLEXPWM_1.MASK.R = FLEXPWM_MASK_OFF;
	}
	else if(module == FLEXPWM_ALL){
		FLEXPWM_0.MASK.R = FLEXPWM_MASK_OFF;
		FLEXPWM_1.MASK.R = FLEXPWM_MASK_OFF;
	}
	else{
		returnVal = -1;
	}
	
	return returnVal;
}


int32_t flexpwm_change_dutyCycle(Flexpwm_module module, const Flexpwm_dutyCycle_s *dutyCycle)
{
	int32_t  returnVal  = 0; // return code of this function
	uint32_t sub        = 0; // number of submodule to configure
	int16_t  tval       = 0; // duty cycle in FlexPWM scale
	
	volatile mcPWM_tag *FlexPWMMod = 0; // structure of data of FlexPWM module to configure
	
	/*====================================================
	  Check number of FlexPWM module value
	 ====================================================*/
	if(module == FLEXPWM_MOD0){
		FlexPWMMod = &FLEXPWM_0;
	}
	else if(module == FLEXPWM_MOD1){
		FlexPWMMod = &FLEXPWM_1;
	}
	else{
		returnVal = -1;
	}
	
	
	if(returnVal >= 0){
		/*====================================================
		 For each submodule, compute a tval value between
		 0 (-100%) and 2* FLEXPWM_DUTY_RANGE (100%)
		 ====================================================*/
		for(sub = 0; sub < FLEXPWM_NUMB_SUB; sub++)
		{			
			/* FLEXPWM0_DTCNT0_0 is the dead time between the 2 signal High
			 * and low. It is added here to avoid error in the H-bridge drivers
			 */
			if(dutyCycle->sub[sub] > ((int16_t)FLEXPWM_DUTY_RANGE - (int16_t)FLEXPWM0_DTCNT0_0))
			{
				tval = FLEXPWM_DUTY_RANGE << 1 - FLEXPWM0_DTCNT0_0; // 2 * FLEXPWM_DUTY_RANGE
			}
			else if(dutyCycle->sub[sub] <  -((int16_t)FLEXPWM_DUTY_RANGE - (int16_t)FLEXPWM0_DTCNT0_0))
			{
				tval = FLEXPWM0_DTCNT0_0;
			}
			else
			{
				tval = dutyCycle->sub[sub] + FLEXPWM_DUTY_RANGE;
			}
			
			FlexPWMMod->SUB[sub].VAL_3.R  = (uint16_t) (tval);
			FlexPWMMod->SUB[sub].VAL_2.R  = (uint16_t) (-tval);
		}
		
		/*====================================================
		  Valid PWM values
		 ====================================================*/
		FlexPWMMod->MCTRL.R |= FLEXPWM_LDOK;
	}
	
	return returnVal;
}

