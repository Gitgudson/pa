/******************************************************************************
*
* Master semester project of Jan Huber
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* 	Created on			: 2019 Apr 30
*	Last modification	: 2019 May 5
* 	Author				: Jan Huber
*/

#include "MPC5643l.h"
#include "HAL/mpc5643l_mode.h"
#include "HAL/mpc5643l_ecsm.h"
#include "IntcInterrupts.h"


//-----------------------------------------------------------------------------
// Local variables to store register states
//-----------------------------------------------------------------------------
static uint8_t  ecsm_esr = 0;

// RAM
static uint32_t ecsm_prear  = 0;
static uint8_t  ecsm_presr  = 0;
static uint8_t  ecsm_premr  = 0;
static uint8_t  ecsm_preat  = 0;
static uint32_t ecsm_predrl = 0;
static uint32_t ecsm_predrh = 0;

// Flash
static uint32_t ecsm_pfear  = 0;
static uint8_t  ecsm_pfemr  = 0;
static uint8_t  ecsm_pfeat  = 0;
static uint32_t ecsm_pfedrl = 0;
static uint32_t ecsm_pfedrh = 0;


static vuint32_t test[2] __attribute__ ((aligned (8))) = 
{
    0xBABADEDA,
    0xABBAABBA
};

static vuint32_t test_read = 0;


void ecsm_init()
{
	/*====================================================
	  Initialise error correction status module
	 ====================================================*/
	
	INTC_InstallINTCInterruptHandler(ecsm_error_handler, ECSM_IRQ_NCE_SOURCE, 10);
	INTC_InstallINTCInterruptHandler(ecsm_error_handler, ECSM_IRQ_1BC_SOURCE, 10);
	
//	SPP_MCM.ECR.R |= ECSM_ECR_EPRNCR |	// Enable Platform RAM Non-Correctable Reporting
//					 ECSM_ECR_EPR1BR;	// Enable Platform RAM 1-bit Error Reporting	
	SPP_MCM.ECR.R |= ECSM_ECR_EPR1BR;	// Enable Platform RAM 1-bit Error Reporting
}


void ecsm_error_handler()
{
    for (;;)
    {
        /* 1. Read the ECSM_ESR and save it */
        ecsm_esr = SPP_MCM.ESR.R;
        
        /* 2. Read and save all the address and attribute reporting registers */        
        /* Read only RAM registers if error is caused by RAM ECC error */
        if( (ecsm_esr & ECSM_ESR_PR1BC) || (ecsm_esr & ECSM_ESR_PRNCE) )
        {
        	ecsm_prear  = SPP_MCM.PREAR.R;
        	ecsm_presr  = SPP_MCM.PRESR.R;
        	ecsm_premr  = SPP_MCM.PREMR.R;
        	ecsm_preat  = SPP_MCM.PREAT.R;
        	ecsm_predrl = SPP_MCM.PREDRL.R;
        	ecsm_predrh = SPP_MCM.PREDRH.R;
        }
        
        /* Read only FLASH registers if error is caused by FLASH ECC error */
        if( (ecsm_esr & ECSM_ESR_PF1BC) || (ecsm_esr & ECSM_ESR_PFNCE) )
        {       	
        	ecsm_pfear  = SPP_MCM.PFEAR.R;
        	ecsm_pfemr  = SPP_MCM.PFEMR.R;
        	ecsm_pfeat  = SPP_MCM.PFEAT.R;
			ecsm_pfedrl = SPP_MCM.PFEDRL.R;
			ecsm_pfedrh = SPP_MCM.PFEDRH.R;
        }
       	 
        /* 3. Re-read the ECSM_ESR and verify the current contents matches 
              the original contents. If the two values are different, go back 
              to step 1 and repeat */
        if (ecsm_esr == SPP_MCM.ESR.R)
        {
            break;
        }
    }
        
    /* 4. When the values are identical, write a 1 to the asserted ECSM_ESR flag 
          to negate the interrupt request - we actually negate all flags */
    SPP_MCM.ESR.R &= ~ecsm_esr;
    
    // For testing purposes
	//mode_sw_error();
}

void ecsm_nce_gen()
{    
    /* Force internal SRAM one non-correctable data error */
    SPP_MCM.EEGR.R = (ECSM_EEGR_FR1NCI | 32);
    
    /* write to internal SRAM - this generates the wrong data */
    test[0] = 0xCAFEBEEF;
    
    /* ECC is checked and non-correctable error found */
	test_read = test[0];
}

void ecsm_obe_gen()
{    
    /* Force internal SRAM one 1-bit correctable data error */
    SPP_MCM.EEGR.R = (ECSM_EEGR_FR11BI | 32);
    
    /* write to internal SRAM - this generates the wrong data */
    test[0] = 0xCAFEBEEF;
    
    /* ECC is checked and 1-bit error found */
	test_read = test[0]; // error caused by read
	test_read = test[1];
}

int32_t ecsm_SWTEST_REGCRC()
{
	return 0;
}



/*******************************************************************************
Function Name : Generate_noncorrectable_ECC_error
Engineer      : b05111
Date          : Dec-08-2015
Parameters    : 
Modifies      :  
Returns       : 
Notes         : generates 2-bit ECC error in RAM using the ECSM_EEGR register
Issues        : RAM[0] of the odd bank means bit 0, RAM[0] of the even bank 
                means bit 32
*******************************************************************************/


