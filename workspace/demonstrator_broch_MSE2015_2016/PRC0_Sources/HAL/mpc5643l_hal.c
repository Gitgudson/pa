/*
/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Nov 12
 *  Last modification : 2015 Nov 12
 *  Author            : Nicolas Broch
 */

#include "HAL/mpc5643l_hal.h"
#include "IntcInterrupts.h"


/***************************************************************************//*!
@brief         Clear CF and NCF into the SoC

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If an error occurred

@details      This function clears faults into the SoC in such way that it
 	 	 	  can switch back to DRUN/RUN0 mode after it was called.
******************************************************************************/
int32_t hal_clear_faults();


void hal_init_sys(Siul_pcr_e led_safe_mode)
{
	int32_t resetSrc = 0;
	int32_t retFct   = 0;
	
	/*====================================================
	 Initialise interrupts
	====================================================*/
	interrupts_init();
	
	/*====================================================
	 Read the reason of reset
	====================================================*/
	resetSrc = reset_read_reset();
	
	/*====================================================
	 Initialise SIUL
	====================================================*/
	siul_init();
	
	/*====================================================
	 Initialise the mode driver
	====================================================*/
	mode_init_safeMode(led_safe_mode, SIUL_HIGH);

	//DEBUG
	//resetSrc = 0;
	
	/*====================================================
	 Reset after a POR
	====================================================*/
	 /*********** To be corrected ******************/
	if(resetSrc == 0) 
	{				
		/*====================================================
		 Initialise the power supervision - must be done
		 before hal_clear_faults()
		====================================================*/
		retFct = power_init();

		/*
		 * In case of error of power supervision init - announce the
		 * error
		 */
		if(retFct != 0)
		{
			mode_sw_error();
		}
		
		/*====================================================
		 Clear failures flags
		 ====================================================*/
		retFct = hal_clear_faults();
		
		/*
		 * If all the failures haven't been cleared, claims
		 * the error.
		 */
		if(retFct != 0)
		{
			mode_sw_error();
		}
		
		/*====================================================
		 Check the system status
		 Do not activate this code if it runs in ram.
		====================================================*/
		//retFct = sysstatus_check();
		
		/*
		 * If the system status is not the one expected, set
		 * the SoC in error mode
		 */
		//if(retFct != 0)
		//{
		//	mode_sw_error();
		//}
		
		/*====================================================
		 Initialise the FCCU.
		====================================================*/
		retFct = fccu_init();
		
		/*
		 * In case of error of FCCU init - announce the
		 * error
		 */
		if(retFct != 0)
		{
			mode_sw_error();
		}
				
		
		/*====================================================
		 Initialise the Reset driver.
		====================================================*/
		reset_init();
		
		/*====================================================
		 Initialise the mode HAL driver (including clock 
		 generation).
		====================================================*/
		mode_init();
		
		/*====================================================
		 Initialise the wakeup module HAL.
		====================================================*/
		wakeup_init();
	}
	/*====================================================
	 Reset after an FCCU error detection or 
	 a SW reset (SW error mechanisms) or a debug/jtag reset
	====================================================*/
	else
	{
		/*====================================================
		 Stay in RESET mode (SM_703) and if an FCCU error
		 has occurred, it is not cleared. So the FCCU_F[0:1]
		 pads still inform about the error (SM_086).
		 Just activate the mode_reac_safe_mode() to inform
		 outside of an internal error
		====================================================*/
		mode_reac_safe_mode();
	}

}


int32_t hal_clear_faults()
{
	int32_t returnVal = 0;
	int32_t retFct    = 0;
	
	/*====================================================
	 Clear the failures of power supervision
	====================================================*/
	power_clear_fails();
	
	/*====================================================
	 Clear the FCCU failures.
	====================================================*/
	retFct = fccu_clear_fails();
	
	/*
	 * In case of error of Reset FCCU errors - announce the
	 * error
	 */
	if(retFct != 0)
	{
		returnVal = -1;
	}
	
	/*====================================================
	 Clear the source of reset
	====================================================*/
	reset_clear_reset();
	
	
	return returnVal;
}
