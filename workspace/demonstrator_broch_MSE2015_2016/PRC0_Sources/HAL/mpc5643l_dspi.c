/******************************************************************************
* 
*	Semester project PS5 of Olivier Progin
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Nov 11
 *	Last modification : 2015 Dec 06
 *  Author            : Olivier Progin
 */


#include "HAL/mpc5643l_dspi.h"


void dspi_init(const Dspi_nbr_e dspinb, const Dspi_pcs_e* Pads, const uint32_t nbpad)
{
	Siul_pcr_e Port_ID;
	Siul_gpio_mode_e Alter;
	uint32_t i;
	
	switch(dspinb)
	{
		case DSPI0 :		//!< Configuration of the DSPI0
		{
			/*====================================================
				SCK, Sout, Sin pads configuration
			====================================================*/	
			siul_config_pad(PCR_C_5,HAL_YES,HAL_NO,SIUL_MODE_ALTER_1, 
							SIUL_OUTPUT,SIUL_FAST,SIUL_TYPE_NONE,
							HAL_YES); 									//!< set the port C5 as DSPI0 SCK
			
			siul_config_pad(PCR_C_6,HAL_YES,HAL_NO,SIUL_MODE_ALTER_1, 
							SIUL_OUTPUT,SIUL_FAST,SIUL_TYPE_NONE,
							HAL_YES); 									//!< set the port C6 as DSPI0 SOUT
			
			siul_config_pad(PCR_C_7,HAL_YES,HAL_NO,SIUL_MODE_GPIO, 
							SIUL_INPUT,SIUL_FAST,SIUL_TYPE_NONE,
							HAL_YES); 									//!< set the port C7 as DSPI0 SIN
			/*====================================================
				SS pads configuration
			====================================================*/	
			for (i=0;i<nbpad;i++)
			{
				switch(*(Pads+i))				// switch to configure the correct PCS
				{
					case PCS0 :
					{
						Port_ID=PCR_C_4;
						Alter=SIUL_MODE_ALTER_1;
					}
					break;
					
					case PCS1 :
					{
						Port_ID=PCR_E_15;
						Alter=SIUL_MODE_ALTER_1;
					}
					break;
					
					case PCS2 :
					{
						Port_ID=PCR_D_6;
						Alter=SIUL_MODE_ALTER_1;
					}
					break;
					
					case PCS3 :
					{
						Port_ID=PCR_D_5;
						Alter=SIUL_MODE_ALTER_1;
					}
					break;
					
					case PCS4 :
					{
						Port_ID=PCR_D_7;
						Alter=SIUL_MODE_ALTER_3;
					}
					break;
					
					case PCS5 :
					{
						Port_ID=PCR_D_8;
						Alter=SIUL_MODE_ALTER_3;
					}
					break;
					case PCS6 :
					{
						Port_ID=PCR_F_3;
						Alter=SIUL_MODE_ALTER_1;
					}
					break;
					case PCS7 :
					{
						Port_ID=PCR_H_9;
						Alter=SIUL_MODE_ALTER_3;
					}
					break;
				}
				siul_config_pad(Port_ID,HAL_NO, HAL_NO, Alter, 
						        SIUL_OUTPUT, SIUL_FAST, SIUL_TYPE_NONE,
						        HAL_YES);
			}
			/*====================================================
				Configuration of the MCR registers for DSPI0
			====================================================*/
			DSPI_A.MCR.R = DSPI0_MCR;
			
			/*====================================================
				Configuration of the CTAR registers for DSPI0
			====================================================*/
			DSPI_A.CTAR0.R = DSPI0_CTAR0;
			DSPI_A.CTAR1.R = DSPI0_CTAR1;
			DSPI_A.CTAR2.R = DSPI0_CTAR2;
			DSPI_A.CTAR3.R = DSPI0_CTAR3;
			
			/*====================================================
				Configuration of the RSER registers for DPSI0
			====================================================*/
			DSPI_A.RSER.R = DSPI0_RSER;	
			
			/*====================================================
				Installation of interrupt handler
			====================================================*/
			INTC_InstallINTCInterruptHandler((*dspi0_interrupt),74,0);
			INTC_InstallINTCInterruptHandler((*dspi0_interrupt),75,5);
			INTC_InstallINTCInterruptHandler((*dspi0_interrupt),76,5);
			INTC_InstallINTCInterruptHandler((*dspi0_interrupt),77,5);
			INTC_InstallINTCInterruptHandler((*dspi0_interrupt),78,5);
		}
		break;
		
		
		case DSPI1 :		//!< Configuration of the DSPI1
		{
			/*====================================================
				SCK, Sout, Sin pads configuration
			====================================================*/	
			siul_config_pad(PCR_A_6,HAL_NO,HAL_NO,SIUL_MODE_ALTER_1, 
							SIUL_OUTPUT,SIUL_FAST,SIUL_TYPE_NONE,
							HAL_YES); //!< set the port A6 as DSPI0 SCK
			
			siul_config_pad(PCR_A_7,HAL_NO,HAL_NO,SIUL_MODE_ALTER_1, 
							SIUL_OUTPUT,SIUL_FAST,SIUL_TYPE_NONE,
							HAL_YES); //!< set the port A7 as DSPI0 SOUT	
			
			siul_config_pad(PCR_A_8,HAL_NO,HAL_NO,SIUL_MODE_ALTER_1, 
							SIUL_INPUT,SIUL_FAST,SIUL_TYPE_NONE,
							HAL_YES); //!< set the port A8 as DSPI0 SIN
			
			/*====================================================
				SS pads configuration
			====================================================*/
			for (i=0;i<nbpad;i++)
			{
				switch(*(Pads+i))					// switch to configure the correct PCS
				{
					case PCS0 :
					{
						Port_ID=PCR_A_5;
						Alter=SIUL_MODE_ALTER_1;
					}
					break;
					
					case PCS1 :
					{
						Port_ID=PCR_G_2;
						Alter=SIUL_MODE_ALTER_2;
					}
					break;
					
					case PCS2 :
					{
						Port_ID=PCR_D_8;
						Alter=SIUL_MODE_ALTER_1;
					}
					break;
					
					case PCS3 :
					{
						Port_ID=PCR_D_7;
						Alter=SIUL_MODE_ALTER_1;
					}
					break;
				}
				siul_config_pad(Port_ID,HAL_NO, HAL_NO, Alter, 
						        SIUL_OUTPUT, SIUL_FAST, SIUL_TYPE_NONE,
						        HAL_YES);
			}
			/*====================================================
				Configuration of the MCR registers for DSPI1
			====================================================*/
			DSPI_B.MCR.R = DSPI1_MCR;
			
			/*====================================================
				Configuration of the CTAR registers for DSPI1
			====================================================*/
			DSPI_B.CTAR0.R = DSPI1_CTAR0;
			DSPI_B.CTAR1.R = DSPI1_CTAR1;
			DSPI_B.CTAR2.R = DSPI1_CTAR2;
			DSPI_B.CTAR3.R = DSPI1_CTAR3;
			
			/*====================================================
				Configuration of the RSER registers for DPSI1
			====================================================*/
			DSPI_B.RSER.R = DSPI1_RSER;	
			
			/*====================================================
				Installation of interrupt handler
			====================================================*/
			INTC_InstallINTCInterruptHandler((*dspi1_interrupt),94,0);
			INTC_InstallINTCInterruptHandler((*dspi1_interrupt),95,5);
			INTC_InstallINTCInterruptHandler((*dspi1_interrupt),96,5);
			INTC_InstallINTCInterruptHandler((*dspi1_interrupt),97,5);
			INTC_InstallINTCInterruptHandler((*dspi1_interrupt),98,5);
		}
		break;
		
		
		case DSPI2 :	//!< Configuration of the DSPI2
		{
			/*====================================================
				SCK, Sout, Sin pads configuration
			====================================================*/	
			siul_config_pad(PCR_A_11,HAL_NO,HAL_NO,SIUL_MODE_ALTER_1, 
							SIUL_OUTPUT,SIUL_FAST,SIUL_TYPE_NONE,
							HAL_YES); //!< set the port A as DSPI2 SCK
			siul_config_padSelection(1,1);
			
			siul_config_pad(PCR_A_12,HAL_NO,HAL_NO,SIUL_MODE_ALTER_1, 
							SIUL_OUTPUT,SIUL_FAST,SIUL_TYPE_NONE,
							HAL_YES); //!< set the port A as DSPI2 SOUT
			
			siul_config_pad(PCR_A_13,HAL_NO,HAL_NO,SIUL_MODE_ALTER_1, 
							SIUL_INPUT,SIUL_FAST,SIUL_TYPE_NONE,
							HAL_YES); //!< set the port A as DSPI2 SIN
			siul_config_padSelection(2,1);
			/*====================================================
				SS pads configuration
			====================================================*/
			for (i=0;i<nbpad;i++)
			{
				switch(*(Pads+i))					// switch to configure the correct PCS
				{
					case PCS0 :
					{
						Port_ID=PCR_A_3;
						Alter=SIUL_MODE_ALTER_2;
						siul_config_padSelection(3,0);
					}
					break;
					
					case PCS1 :
					{
						Port_ID=PCR_A_4;
						Alter=SIUL_MODE_ALTER_2;
					}
					break;
					
					case PCS2 :
					{
						Port_ID=PCR_C_10;
						Alter=SIUL_MODE_ALTER_1;
					}
					break;
					
					case PCS3 :
					{
						Port_ID=PCR_E_13;
						Alter=SIUL_MODE_ALTER_2;
					}
					break;
				}
				siul_config_pad(Port_ID,HAL_NO, HAL_NO, Alter, 
						        SIUL_OUTPUT, SIUL_FAST, SIUL_TYPE_NONE,
						        HAL_YES);
			}
			/*====================================================
				Configuration of the MCR registers for DSPI2
			====================================================*/
			DSPI_C.MCR.R = DSPI2_MCR;
			
			/*====================================================
				Configuration of the CTAR registers for DSPI2
			====================================================*/
			DSPI_C.CTAR0.R = DSPI2_CTAR0;
			DSPI_C.CTAR1.R = DSPI2_CTAR1;
			DSPI_C.CTAR2.R = DSPI2_CTAR2;
			DSPI_C.CTAR3.R = DSPI2_CTAR3;
			
			/*====================================================
				Configuration of the RSER registers for DPSI2
			====================================================*/
			DSPI_C.RSER.R = DSPI2_RSER;			
			
			/*====================================================
				Installation of interrupt handler
			====================================================*/
			INTC_InstallINTCInterruptHandler((*dspi2_interrupt),114,0);
			INTC_InstallINTCInterruptHandler((*dspi2_interrupt),115,5);
			INTC_InstallINTCInterruptHandler((*dspi2_interrupt),116,5);
			INTC_InstallINTCInterruptHandler((*dspi2_interrupt),117,5);
			INTC_InstallINTCInterruptHandler((*dspi2_interrupt),118,5);
		}
		break;
	}
}


void dspi_send(const Dspi_nbr_e dspinb, uint16_t* data, uint32_t nbr_data, 
		       const Dspi_pcs_e PCS_nbr, const Dspi_ctar_e CTAR)
{
	uint32_t i;
	uint32_t Pushr= 0x00000000;
	
	/*====================================================
		Clear TX FIFO
	====================================================*/
	DSPI_0.MCR.B.CLR_TXF = 1;
	DSPI_1.MCR.B.CLR_TXF = 1;
	//DSPI_2.MCR.B.CLR_TXF = 1;

	
	for(i=0;i<nbr_data;i++)
	{
		/*====================================================
			Preparing data, PCS and CTAR
		====================================================*/	
		Pushr= 0x00000000;
		Pushr= (uint32_t)*(data+i);
		Pushr= PCS_nbr | Pushr;
		Pushr= CTAR | Pushr;
		if (i==(nbr_data-1))
		{
			Pushr=Pushr | 0x08000000;
		}
		
		switch (dspinb)
		{
			case DSPI0:
			{
				/*====================================================
					Write data in TX FIFO
				====================================================*/	
				Pushr= DSPI0_PUSHR | Pushr;
				DSPI_A.PUSHR.R = Pushr;

				/*====================================================
					Activate the transmission
				====================================================*/
				DSPI_A.MCR.B.HALT = 0;
				while(DSPI_A.SR.B.TCF!=1)
				{
				}
				DSPI_A.MCR.B.HALT = 1;
				DSPI_A.SR.B.EOQF = 1;
			}
			break;
			
			case DSPI1:
			{
				/*====================================================
					Write data in TX FIFO
				====================================================*/	
				Pushr= DSPI1_PUSHR | Pushr;
				DSPI_B.PUSHR.R = Pushr;

				/*====================================================
					Activate the transmission
				====================================================*/
				DSPI_B.MCR.B.HALT = 0;
				while(DSPI_B.SR.B.TCF!=1)
				{
				}
				DSPI_B.MCR.B.HALT = 1;
				DSPI_B.SR.B.EOQF = 1;
			}
			break;
			
			case DSPI2:
			{
				/*====================================================
					Write data in TX FIFO
				====================================================*/	
				Pushr= DSPI2_PUSHR | Pushr;
				DSPI_C.PUSHR.R = Pushr;

				/*====================================================
					Activate the transmission
				====================================================*/
				DSPI_C.MCR.B.HALT = 0;
				while(DSPI_C.SR.B.TCF!=1)
				{
				}
				DSPI_C.MCR.B.HALT = 1;
				DSPI_C.SR.B.EOQF = 1;
			}
			break;
		}
	}
}

void dspi_send_receive(const Dspi_nbr_e dspinb, uint16_t* data_s, uint32_t nbr_data, const Dspi_pcs_e PCS_nbr, 
		               const Dspi_ctar_e CTAR, uint32_t* data_r)
{
	uint32_t i;
	/*====================================================
		Clear TX FIFO
	====================================================*/
	DSPI_0.MCR.B.CLR_TXF = 1;
	DSPI_1.MCR.B.CLR_TXF = 1;
//	DSPI_2.MCR.B.CLR_TXF = 1;
	DSPI_0.MCR.B.CLR_RXF = 1;
	DSPI_1.MCR.B.CLR_RXF = 1;
//	DSPI_2.MCR.B.CLR_RXF = 1;

	
	for(i=0;i<nbr_data;i++)
	{
		/*====================================================
			Preparing data, PCS and CTAR
		====================================================*/	
		uint32_t Pushr= 0x00000000;
		Pushr= *(data_s+i);
		Pushr= PCS_nbr | Pushr;
		Pushr= CTAR | Pushr;
		if (i==(nbr_data-1))
		{
			Pushr=Pushr | 0x08000000;
		}
		
		switch (dspinb)
		{
			case DSPI0:
			{
				/*====================================================
					Write data in TX FIFO
				====================================================*/	
				Pushr= DSPI0_PUSHR | Pushr;
				DSPI_A.PUSHR.R = Pushr;

				/*====================================================
					Activate the transmission
				====================================================*/
				DSPI_A.MCR.B.HALT = 0;
				while(DSPI_A.SR.B.TCF!=1)
				{
				}
				/*====================================================
					Read the received data
				====================================================*/
				*(data_r+i)=DSPI_A.POPR.R;
				
				DSPI_A.MCR.B.HALT = 1;
				DSPI_A.SR.B.EOQF = 1;
				
			}
			break;
			
			case DSPI1:
			{
				/*====================================================
					Write data in TX FIFO
				====================================================*/	
				Pushr= DSPI1_PUSHR | Pushr;
				DSPI_B.PUSHR.R = Pushr;

				/*====================================================
					Activate the transmission
				====================================================*/
				DSPI_B.MCR.B.HALT = 0;
				while(DSPI_B.SR.B.TCF!=1)
				{
				}
				/*====================================================
					Read the received data
				====================================================*/
				*(data_r+i)=DSPI_B.POPR.R;
				
				DSPI_B.MCR.B.HALT = 1;
				DSPI_B.SR.B.EOQF = 1;

				
			}
			break;
			
			case DSPI2:
			{
				/*====================================================
					Write data in TX FIFO
				====================================================*/	
				Pushr= DSPI2_PUSHR | Pushr;
				DSPI_C.PUSHR.R = Pushr;

				/*====================================================
					Activate the transmission
				====================================================*/
				DSPI_C.MCR.B.HALT = 0;
				while(DSPI_C.SR.B.TCF!=1)
				{
				}
				/*====================================================
					Read the received data
				====================================================*/
				*(data_r+i)=DSPI_C.POPR.R;
				
				DSPI_C.MCR.B.HALT = 1;
				DSPI_C.SR.B.EOQF = 1;
				
			}
			break;
		}
	}
}

void dspi0_interrupt()
{
	mode_switch_SAFE();
}

void dspi1_interrupt()
{
	mode_switch_SAFE();
}

void dspi2_interrupt()
{
	mode_switch_SAFE();
}
