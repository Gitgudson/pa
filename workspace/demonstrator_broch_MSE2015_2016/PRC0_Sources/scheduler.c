/******************************************************************************
*
* Master semester project of Jan Huber
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* 	Created on			: 2019 Apr 2
*	Last modification	: 2019 Apr 7
* 	Author				: Jan Huber
*/

#include <typedefs.h>
#include "HAL/mpc5643l_pit.h"
#include "scheduler.h"

#include "HAL/mpc5643l_siul.h"

//----------------------------------------------------------------------------
// Local definitions
//----------------------------------------------------------------------------

// Task numbers
static uint32_t nbrOfTasks;
static uint32_t maxNbrOfTasks;

// Tasks
static TaskStruct_t* pTasks;


void schedulerInit(TaskStruct_t *pTasksStruct, uint32_t nbTasks)
{
	uint32_t i;
	
    // Init pointer on structure pTasks and maxNbrOfTasks
    pTasks = pTasksStruct;
    maxNbrOfTasks = nbTasks;
    
    // Reset every task in task table and reset nbrOfTasks
    for(i = 0; i < maxNbrOfTasks; i++)
    {
        pTasks[i].running     = 0;
        pTasks[i].run         = 0;
        pTasks[i].elapsedTime = 0;
        pTasks[i].period      = 0;
        pTasks[i].pFunction   = 0;
        //strncpy(pTasks[i].taskName, "Not initialised", eMaxTaskNameLength);
        pTasks[i].pFunctionParam = NULL_PTR;
    }
    
    nbrOfTasks = 0;
}

void schedulerAddTask(Task_t pFunction, uint32_t period, /*char name[],*/ void *FunctionParam)
{
    // Add a task in task table pointed by pTasks and increment nbrOfTasks
	pTasks[nbrOfTasks].running     = 0;
    pTasks[nbrOfTasks].run         = 1;
    pTasks[nbrOfTasks].period      = period;
    pTasks[nbrOfTasks].pFunction   = pFunction;

    pTasks[nbrOfTasks].elapsedTime = 0;

    //strncpy(pTasks[nbrOfTasks].taskName, name, eMaxTaskNameLength);
    pTasks[nbrOfTasks].pFunctionParam = FunctionParam;
    nbrOfTasks++;
}


void schedulerRemoveTask(Task_t pFunction)
{
	uint32_t i, j;
	
    // Find task to be removed
    for(i = 0; (pFunction != pTasks[i].pFunction) && (i < nbrOfTasks); i++)
    {
    	
    }

    // No task found
    if(pFunction != pTasks[i].pFunction)
    {
        return;
    }
    else{} // MISRA-C compliance

    // Copy tasks after task to be removed in cell before
    for(j = i+1; j < nbrOfTasks; j++)
    {
        pTasks[j-1].running     = pTasks[j].running;
        pTasks[j-1].run         = pTasks[j].run;
        pTasks[j-1].elapsedTime = pTasks[j].elapsedTime;
        pTasks[j-1].period      = pTasks[j].period;
        pTasks[j-1].pFunction   = pTasks[j].pFunction;
        pTasks[j-1].pFunctionParam = pTasks[j].pFunctionParam;
        //strncpy(pTasks[j-1].taskName, pTasks[j].taskName, eMaxTaskNameLength);
    }

    // Reinitialise last task
    pTasks[nbrOfTasks].running     = 0;
    pTasks[nbrOfTasks].run         = 0;
    pTasks[nbrOfTasks].elapsedTime = 0;
    pTasks[nbrOfTasks].period      = 0;
    pTasks[nbrOfTasks].pFunction   = 0;
    //strncpy(pTasks[nbrOfTasks].taskName, "Not initialised", eMaxTaskNameLength);
    pTasks[nbrOfTasks].pFunctionParam = NULL_PTR;
    nbrOfTasks--;
}

void schedulerPauseTask(Task_t pFunction)
{
	uint32_t i;
	
    // Find task to pause
    for(i = 0; (pFunction != pTasks[i].pFunction) && (i < nbrOfTasks); i++)
    {
    	
    }

    // No task found
    if(pFunction != pTasks[i].pFunction)
    {
        return;
    }
    else{} // MISRA-C compliance

    pTasks[i].run = 0;
}

void schedulerResumeTask(Task_t pFunction)
{
	uint32_t i;
	
    // Find task to resume
    for(i = 0; (pFunction != pTasks[i].pFunction) && (i < nbrOfTasks); i++)
    {
    	
    }

    // No task found
    if(pFunction != pTasks[i].pFunction)
    {
        return;
    }
    else{} // MISRA-C compliance

    pTasks[i].run = 1;
}

uint8_t schedulerIsTaskRunning()
{
	uint32_t i;
	
    //  Check nbrOfTasks is not equal to 0
    if(!nbrOfTasks)
    {
        return 0;
    }
    else{} // MISRA-C compliance
    
    //    Parsing in the task table every tasks registered
    for(i = 0; i < nbrOfTasks; i++)
    {
        //      Check if a task running
        //        return true
        if(pTasks[i].running)
        {
            return 1;
        }
    }

    return 0;
}

void schedulerRun()
{
	uint32_t i;
	
    //  Check sysTick flag
    if(1 == systickFlag)
    {
    	//siul_write_fast_pad(PCR_A_12, SIUL_HIGH);
    	
        //    Parsing in the task table every registered task
        for(i = 0; i < nbrOfTasks; i++)
        {
            // Update elapsed time if not paused
            if(1 == pTasks[i].run)
            {
                pTasks[i].elapsedTime++;
            }
            else{} // MISRA-C compliance
            
            // Check counter elapsedTime of task if equal or more than period
            if(pTasks[i].elapsedTime >= pTasks[i].period)
            {
                // Check if task function pointer is not null and task is not running
                if((NULL_PTR != pTasks[i].pFunction) && (!schedulerIsTaskRunning()))
                {
                    // set running as true
                    pTasks[i].running = 0;
                    // call task
                    pTasks[i].pFunction(pTasks[i].pFunctionParam);
                    // reset counter elapsedTime of task
                    pTasks[i].elapsedTime = 0;
                    // set running as false
                    pTasks[i].running = 0;
                }
                else{} // MISRA-C compliance
                
            }
            else{} // MISRA-C compliance
            
        }        
        // Clear flag to check tasks execution time
        // External variable declared in "mpc5643l_pit.h" and defined in "mpc5643l_pit.c"
        systickFlag = 0;
        
        //siul_write_fast_pad(PCR_A_12, SIUL_LOW);
    }
    else{} // MISRA-C compliance
    
}
