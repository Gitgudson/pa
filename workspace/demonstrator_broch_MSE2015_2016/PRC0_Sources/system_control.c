/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2016 Jan 25
 *	Last modification : 2019 Apr 16
 *      Author        : Nicolas Broch
 *      Author		  : Jan Huber
 */


#include "system_control.h"

//-----------------------------------------------------------------------------
// Global variables for speed and torque commands
//-----------------------------------------------------------------------------
float w_motor0 = 0; //!< Speed command of motor 0
float w_motor1 = 0; //!< Speed command of motor 1
float t_motor0 = 0; //!< Torque command of motor 0
float t_motor1 = 0; //!< Torque command of motor 1
float id_motor0 = 0; //!< Id command of motor 0
float id_motor1 = 0; //!< Id command of motor 1

int32_t critFaultM0 = 0; //!< = 1 if a critical fault has occurred in motor 0
int32_t critFaultM1 = 0; //!< = 1 if a critical fault has occurred in motor 1

Button_s switchOnOff;
Button_s buttonUp;
Button_s buttonDown;


void system_control_init()
{
	/*====================================================
	  Initialisation of the pad to measure interrupt timing
	====================================================*/
	siul_config_pad(OUTPUT_MES_IRQ,HAL_NO, HAL_NO, 
					SIUL_MODE_GPIO, SIUL_OUTPUT,
					SIUL_FAST, SIUL_TYPE_NONE,
					HAL_YES);
	/*====================================================
	  Initialisation of the switch and the buttons
	====================================================*/
	button_init(&switchOnOff, SWITCH_ON_OFF, SIUL_HIGH);
	button_init(&buttonUp, BUTTON_UP, SIUL_HIGH);
	button_init(&buttonDown, BUTTON_DOWN, SIUL_HIGH);
	
	
	/*====================================================
	  Interrupt for control loop
	====================================================*/
	INTC_InstallINTCInterruptHandler(control_IRQ, 206, 10);
}


int32_t readSensors(error_code_e errorCodes[10])
{
	int32_t returnVal = 0; // return variable of the function
	int32_t returnFct = 0; // variable for return of functions
	
	/*====================================================
	  Read motors' position
	====================================================*/
	pmsmMotors_read2Enc(&motorPMSM0.meas.pos, &motorPMSM1.meas.pos);
	returnFct = pmsmMotors_readResolv(&motorPMSM0.meas.pos);
	if(returnFct != 0) // return error if exists
	{
		errorCodes[returnVal] = ERROR_RESOL_MOTOR_0;
		critFaultM0++;
		returnVal++;
	}
	// MISRA-C compliance
	else
	{
	}
	
	returnFct = pmsmMotors_readResolv(&motorPMSM1.meas.pos);
	if(returnFct != 0) // return error if exists
	{
		errorCodes[returnVal] = ERROR_RESOL_MOTOR_1;
		critFaultM1++;
		returnVal++;
	}
	// MISRA-C compliance
	else
	{
	}
	
	/*====================================================
	  Decide which position is the position of the control
	  system
	====================================================*/
	returnFct =  pmsmMotors_setPositionSpeed(&motorPMSM0.meas.pos);
	if(returnFct != 0) // return error if exists
	{
		errorCodes[returnVal] = ERROR_POS_MOTOR_0;
		critFaultM0++;
		returnVal++;
	}
	// MISRA-C compliance
	else
	{
	}
	
	returnFct =  pmsmMotors_setPositionSpeed(&motorPMSM1.meas.pos);
	if(returnFct != 0) // return error if exists
	{
		errorCodes[returnVal] = ERROR_POS_MOTOR_1;
		critFaultM1++;
		returnVal++;
	}
	// MISRA-C compliance
	else
	{
	}
	
	/*====================================================
	  Wait until end of conversion of currents and voltages
	====================================================*/
	while(!pmsmMotors_endOfConv()){}
	
	/*====================================================
	  Read and convert measures
	====================================================*/
	returnFct = pmsmMotors_readVoltage(&motorPMSM0, &motorPMSM1);
	if(returnFct != 0) // return error if exists
	{
		errorCodes[returnVal] = ERROR_READ_VOLTAGE;
		critFaultM0++;
		critFaultM1++;
		returnVal++;
	}
	// MISRA-C compliance
	else
	{
	}
	
	returnFct = pmsmMotors_readCurrents(&motorPMSM0, &motorPMSM1);
	if(returnFct != 0) // return error if exists
	{
		errorCodes[returnVal] = ERROR_READ_CURRENT;
		critFaultM0++;
		critFaultM1++;
		returnVal++;
	}
	// MISRA-C compliance
	else
	{
	}
	
	
	return returnVal;
}

int32_t brakingResistor()
{
	int32_t returnVal = 0; // return variable of the function
	int32_t returnFct = 0; // variable for return of functions
	
	/*====================================================
	  Check if DC-Bus voltage is too high
	====================================================*/
	returnFct =  pmsmMotors_checkDCbusVoltage(&motorPMSM0);
	returnFct +=  pmsmMotors_checkDCbusVoltage(&motorPMSM1);
	
	/*====================================================
	  Check if a braking resistor is activated
	====================================================*/
	if(returnFct != 0)
	{
		returnVal = 1;
	}
	// MISRA-C compliance
	else
	{
	}
	
	return returnVal;
}


int32_t speedControl(error_code_e errorCodes[10], uint16_t nIter)
{
	int32_t returnVal = 0; // return variable of the function
	int32_t returnFct = 0; // variable for return of functions
	static countLoopsW = 1; // variable to count iterations
	
	if(countLoopsW >= nIter)
	{
		returnFct =  pmsmMotors_speedControl(&motorPMSM0, w_motor0);
		if(returnFct != 0) // return error if exists
		{
			errorCodes[returnVal] = ERROR_STATE_SPEED_CTRL_M0;
			critFaultM0++;
			returnVal++;
		}
		// MISRA-C compliance
		else
		{
		}
		
		returnFct =  pmsmMotors_speedControl(&motorPMSM1, w_motor1);
		if(returnFct != 0) // return error if exists
		{
			errorCodes[returnVal] = ERROR_STATE_SPEED_CTRL_M1;
			critFaultM1++;
			returnVal++;
		}
		// MISRA-C compliance
		else
		{
		}
		
		countLoopsW = 1;
	}
	else{
		countLoopsW++;
	}
	
	return returnVal;
}

int32_t torqueControl(error_code_e errorCodes[10])
{
	int32_t returnVal = 0; // return variable of the function
	int32_t returnFct = 0; // variable for return of functions
	
	/*====================================================
	  Torque controller of motor 0
	====================================================*/
	returnFct = pmsmMotors_torqueControl(&motorPMSM0, 
										 t_motor0, 
			           	   	   	   	   	 id_motor0);
	if(returnFct != 0) // return error if exists
	{
		errorCodes[returnVal] = ERROR_STATE_CURR_CTRL_M0;
		critFaultM0++;
		returnVal++;
	}
	// MISRA-C compliance
	else
	{
	}
	
	/*====================================================
	  Torque controller of motor 1
	====================================================*/
	returnFct = pmsmMotors_torqueControl(&motorPMSM1, 
										 t_motor1, 
										 id_motor1);
	if(returnFct != 0) // return error if exists
	{
		errorCodes[returnVal] = ERROR_STATE_CURR_CTRL_M1;
		critFaultM1++;
		returnVal++;
	}
	// MISRA-C compliance
	else
	{
	}
	
	/*====================================================
	  Apply output of control system on PWM
	====================================================*/
	pmsmMotors_setVoltage(&motorPMSM0, &motorPMSM1);
}


void control_IRQ()
{	
	static int32_t returnFct = 0;
	error_code_e errorCodes[10]; // list of error detected
	
	SIU.GPDO[OUTPUT_MES_IRQ].R = 1;
	
	/*====================================================
	  Read the motor sensors
	====================================================*/
	returnFct =  readSensors(errorCodes);
		
	/*====================================================
	  Check and activate the braking resistors
	====================================================*/
	returnFct =  brakingResistor();
	
	/*====================================================
	  Speed control algorithm
	====================================================*/
	returnFct =  speedControl(errorCodes, PMSM_TE_IT_W);
	
	/*====================================================
	  If the speed controller are activated, send
	  torque command to torque controllers.
	====================================================*/
	if(motorPMSM0.ctrlSpeed.state == CTRL_ON)
	{
		t_motor0  = motorPMSM0.ctrlSpeed.outputTorque;
	}
	// Send t_motor0 torque command
	else{
		// Redundant assignment to itself
		//t_motor0 = t_motor0;
	}
	
	if(motorPMSM1.ctrlSpeed.state == CTRL_ON)
	{
		t_motor1  = motorPMSM1.ctrlSpeed.outputTorque;
	}
	// Send t_motor1 torque command
	else{
		// Redundant assignment to itself
		//t_motor1 = t_motor1;
	}
	
	/*====================================================
	  Current control algorithm
	====================================================*/
	returnFct =  torqueControl(errorCodes);
	
	/*====================================================
	  ACK interrupt
	====================================================*/
	pmsmMotors_endIRQ();
	
	SIU.GPDO[OUTPUT_MES_IRQ].R = 0;
}
