/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     system_control.h
*
* @author   Nicolas Broch
* @author	Jan Huber
* 
* @version  0.1
* 
* @date     2019 Apr 16
* 
* @brief    System library to control the motor
* 
* @copyright (C) Nicolas Broch
*
*
* This library offers functions to control the motor at the system level
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* 
* 
* 
*
*/
#ifndef SYSTEM_CONTROL_H_
#define SYSTEM_CONTROL_H_

#include "drivers/pmsm_motors.h"
#include "drivers/button.h"
#include "HAL/mpc5643l_hal.h"
#include "MPC5643L.h"
#include <typedefs.h> 
#include "error_codes.h"

//-----------------------------------------------------------------------------
// Global variables for speed and torque commands
//-----------------------------------------------------------------------------
extern float w_motor0;  //!< Speed command of motor 0
extern float w_motor1;  //!< Speed command of motor 1
extern float t_motor0;  //!< Torque command of motor 0
extern float t_motor1;  //!< Torque command of motor 1
extern float id_motor0; //!< Id command of motor 0
extern float id_motor1; //!< Id command of motor 1

extern Button_s switchOnOff; //!< Structure of the on/off switch
extern Button_s buttonUp;    //!< Structure of the up pushbutton
extern Button_s buttonDown;   //!< Structure of the down pushbutton

extern int32_t critFaultM0; //!< number of critical faults detected in motor 0
extern int32_t critFaultM1; //!< number of critical faults detected in motor 1


//-----------------------------------------------------------------------------
// GPIO configuration
//-----------------------------------------------------------------------------
#define OUTPUT_MES_IRQ PCR_F_3   //!< Output on which the IRQ duration is measured
#define SWITCH_ON_OFF  PCR_G_14  //!< ON/OFF switch input
#define BUTTON_UP      PCR_G_12  //!< pushbutton up
#define BUTTON_DOWN    PCR_G_13  //!< pushbutton down

/***************************************************************************//*!
@brief         Initialisation of system_control

@return  	   void
				
@details       Initialisation of system_control. Must be called before other
			   functions of this library.
******************************************************************************/
void system_control_init();

/***************************************************************************//*!
@brief         Function to read motor sensors

@param 			errorCodes	List of detected errors.

@return  		int32_t\n
				0 if no problem detected
				otherwise, number of error detected
				
@details       This function reads the values of the 2 resolvers and
			   save the result into the motor sensors structure given.
******************************************************************************/
int32_t readSensors(error_code_e errorCodes[10]);

/***************************************************************************//*!
@brief          Activation of braking resistor

@return  		int32_t\n
				0 if the braking resistor is not triggered
				1 if a braking resistor is activated
								
@details        Check the DC bus voltage and activate the braking resistor
			    if necessary.
******************************************************************************/
int32_t brakingResistor();


/***************************************************************************//*!
@brief         Function to control the speed of the 2 motors

@param 			errorCodes	List of detected errors.
@param			nIter		Number of iterations between 2 executions of the
							speed control algorithm

@return  		int32_t\n
				0 if no problem detected
				otherwise, number of error detected
				
@details       This function calls the speed controllers each nIter call.
******************************************************************************/
int32_t speedControl(error_code_e errorCodes[10], uint16_t nIter);

/***************************************************************************//*!
@brief         Function to control the torque of the 2 motors

@param 			errorCodes	List of detected errors.

@return  		int32_t\n
				0 if no problem detected
				otherwise, number of error detected
				
@details       This function calls the torqueControl controllers of each motor 
******************************************************************************/
int32_t torqueControl(error_code_e errorCodes[10]);


/***************************************************************************//*!
@brief         Interrupt called for at each torque control iteration

@return  	   void
				
@details       This function measures the sensors and carries out the 
			   torque and speed controllers.
******************************************************************************/
void control_IRQ();

/***************************************************************************//*!
@brief         Return the state of the on/off switch.

@return  	   0 if the switch is in off state
			   1 if the switch is in on state
				
@details       Return the state of the on/off switch. This function must
			   be called periodically as it contains a numerical filter. It
			   can be done by calling it in the state machine
******************************************************************************/
int32_t switch_onOff_state();


#endif /* SYSTEM_CONTROL_H_ */
