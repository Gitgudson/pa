/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     error_codes.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2016 Jan 25
* 
* @brief    System library to control the motor
* 
* @copyright (C) Nicolas Broch
*
*
* This library offers functions to control the motor at the system level
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* 
* 
* 
*
*/
#ifndef ERROR_CODES_H_
#define ERROR_CODES_H_

#include <typedefs.h> 


/***************************************************************************//*!
@brief          List of error codes
******************************************************************************/
typedef enum{
	NO_ERROR,			 //!< No error
	ERROR_RESOL_MOTOR_0, //!< error to read the resolver of motor 0
	ERROR_RESOL_MOTOR_1, //!< error to read the resolver of motor 1
	ERROR_POS_MOTOR_0,   //!< error with position sensors of motor 0
	ERROR_POS_MOTOR_1,   //!< error with position sensors of motor 1
	ERROR_READ_VOLTAGE,  //!< error during bus voltage reading
	ERROR_READ_CURRENT,	 //!< error during current reading
	ERROR_STATE_SPEED_CTRL_M0, //!< unknow state of the speed controller of motor 0
	ERROR_STATE_SPEED_CTRL_M1, //!< unknow state of the speed controller of motor 1
	ERROR_STATE_CURR_CTRL_M0,  //!< unknow state of the current controller of motor 0
	ERROR_STATE_CURR_CTRL_M1   //!< unknow state of the current controller of motor 1
}error_code_e;

#endif /* ERROR_CODES_H_ */
