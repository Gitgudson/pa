/******************************************************************************
*
* Master semester project of Jan Huber
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     task.h
*
* @author   Jan Huber
* 
* @version  0.2
* 
* @date     2019 May 5
* 
* @brief    Regrouping of tasks to be added to the scheduler
* 
* @copyright (C) Jan Huber
*
*
* This library regroups the functions that should be added to the scheduler
* to be executed as tasks.
* 
* How to use it
* -------------
* 
* 
*/

#ifndef TASK_H_
#define TASK_H_

#include <typedefs.h>


/***************************************************************************//*!
@brief			Measurement and verification of the clock

@param			void*

@return  		void

@details        Measure the frequency of IRCOSC and make sure that it is in
				the allowed range
******************************************************************************/
void freq_meas_task(void*);

/***************************************************************************//*!
@brief			Injection of a 2-bit non correctable error into SRAM

@param			void*

@return  		void

@details        Inject a 2-bit non correctable error into SRAM for testing
				purposes
				
@warning		Detection is not activated because correct handling is not
				implemented
******************************************************************************/
void nce_injection_task(void*);

/***************************************************************************//*!
@brief			Injection of a 1-bit correctable error into SRAM

@param			void*

@return  		void

@details        Inject a 1-bit correctable error into SRAM for testing
				purposes
******************************************************************************/
void obe_injection_task(void*);


#endif /* TASK_H_ */

