/******************************************************************************
*
* Master semester project of Jan Huber
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     scheduler.h
*
* @author   Jan Huber
* 
* @version  0.1
* 
* @date     2019 Apr 16
* 
* @brief    Scheduler and task manager of the motor control demonstrator
* 
* @copyright (C) Jan Huber
*
*
* This library offers the scheduler and task manager definition for the demonstrator
* 
* How to use it
* -------------
* 
* 
*/

#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include <typedefs.h>

/*******************************************************************************
* Macro definitions
*******************************************************************************/
#define NULL_PTR	0
//#define eMaxTaskNameLength	(16)


/***************************************************************************//*!
* Type definitions
******************************************************************************/
typedef void(*Task_t)(void*);

/***************************************************************************//*!
@brief          Structure of a task
******************************************************************************/
typedef struct {
	uint8_t		running;						//!< Flag if the task is running
	uint8_t		run;							//!< Flag if the task should be run
    uint32_t	period;							//!< Period of the task
    uint32_t	elapsedTime;					//!< Elapsed time since last execution
    Task_t		pFunction;						//!< Function pointer of the task
    //char		taskName[eMaxTaskNameLength+1];	//!< Name of the task
    void		*pFunctionParam;				//!< Parameter to be given to the task
} TaskStruct_t;

/***************************************************************************//*!
@brief          Enumeration of the task periods
******************************************************************************/
enum PERIOD_LIST {
    TASK_PERIOD_1ms =   1, // 1ms based...
    TASK_PERIOD_2ms =   2,
    TASK_PERIOD_5ms =   5,
    TASK_PERIOD_10ms =   10,
    TASK_PERIOD_20ms =   20,
    TASK_PERIOD_50ms =   50,
    TASK_PERIOD_100ms =   100,
    TASK_PERIOD_200ms =   200,
    TASK_PERIOD_500ms =   500,
    TASK_PERIOD_1s =   1000
};


/***************************************************************************//*!
@brief          Scheduler initialisation

@param			pTasksStruct	Pointer to the tasks structure
@param			nbTasks			Number of tasks
@param			usSysTick		SysTick period in [us]

@return  		void

@details        Initialisation of the scheduler. The tick is given by the
				system timer
******************************************************************************/
void schedulerInit(TaskStruct_t *pTasksStruct, uint32_t nbTasks);

/***************************************************************************//*!
@brief          Add a new task

@param			pFunction		Function pointer of task to register
@param			period			Period of function calls
@param			FunctionParam	Parameter to transmit to the task

@return  		void

@details        Add a new task to the scheduler to be executed periodically
******************************************************************************/
void schedulerAddTask(Task_t pFunction, uint32_t period, void *FunctionParam);

/***************************************************************************//*!
@brief          Remove a task

@param			pFunction	Function pointer of task to remove

@return  		void

@details        Remove a task from the scheduler
******************************************************************************/
void schedulerRemoveTask(Task_t pFunction);

/***************************************************************************//*!
@brief          Pause a task

@param			pFunction	Function pointer of task to pause

@return  		void

@details        Pause a task in the scheduler
******************************************************************************/
void schedulerPauseTask(Task_t pFunction);

/***************************************************************************//*!
@brief          Resume a task

@param			pFunction	Function pointer of task to resume

@return  		void

@details        Resume a task in the scheduler
******************************************************************************/
void schedulerResumeTask(Task_t pFunction);

/***************************************************************************//*!
@brief          Check if a task is running

@return  		uint8_t\n
				1 if at least one task is running
				0 otherwise

@details        Parse the table of task to check if one is running
******************************************************************************/
uint8_t schedulerIsTaskRunning();

/***************************************************************************//*!
@brief          Run the scheduler

@return  		void

@details        Check the table of tasks every tick of the system timer. Then
				update or execute the tasks.
******************************************************************************/
void schedulerRun();


#endif /* SCHEDULER_H_ */
