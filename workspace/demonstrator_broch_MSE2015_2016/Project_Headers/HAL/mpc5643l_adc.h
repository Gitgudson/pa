/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_adc.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Dec 29
* 
* @brief    Hardware abstraction layer for the ADC when it is commanded by the CTU
* 
* @copyright (C) Nicolas Broch
*
*
* The CTU coordinates the FlexPWM, ADC and eTimers. This HAL library configures the
* ADC to work correctly with CTU triggers and DMA.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
*
*/


#ifndef MPC5643L_ADC_H_
#define MPC5643L_ADC_H_

#include "MPC5643L.h"
#include <typedefs.h> 



/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define ADC_MAX_VAL      0x00000FFFUL //!< Maximal value of the ADC data, MPC5643LRM 8.3.13
#define ADC_BI_MAX_VAL	 0x000007FFUL //!< Maximal value of the ADC data in bilateral measurement, MPC5643LRM 8.3.13
#define ADC_VALID        0x00080000UL //!< Mask to check if data in a CDRx register is valid, MPC5643LRM 8.3.13
#define ADC_CDATA_MASK   0x00000FFFUL //!< Mask to read 12 bits of data in CDRx register, MPC5643LRM 8.3.13
#define ADC_CLR_CEOCFR0  0x0000FFFFUL //!< Mask to clear the flags of the register CEOCFR0 12 bits of data in CDRx register, MPC5643LRM 8.3.2

//-----------------------------------------------------------------------------
// ADC0 configuration
//-----------------------------------------------------------------------------
#define ADC0_MCR         0x80020001   //!< MCR register\n user defined in file adc.xlsx, MPC5643LRM 8.3.2.1
#define ADC0_CTR0        0x0000060C   //!< CTR0 register\n user defined in file adc.xlsx, MPC5643LRM 8.3.10.1

//-----------------------------------------------------------------------------
// ADC1 configuration
//-----------------------------------------------------------------------------
#define ADC1_MCR         0x80020001   //!< MCR register\n user defined in file adc.xlsx, MPC5643LRM 8.3.2.1
#define ADC1_CTR0        0x0000060C   //!< CTR0 register\n user defined in file adc.xlsx, MPC5643LRM 8.3.10.1



/*******************************************************************************
* Functions
*******************************************************************************/

/***************************************************************************//*!
@brief         Configure the ADC

@return  	   void

@details       Configure the ADC to work with CTU.   

@warning	   Not developed following safety development process
******************************************************************************/
void adc_init();

/***************************************************************************//*!
@brief         Reset "end of conversion register" of the 2 ADC.

@return  	   void

@details       Reset "end of conversion register" of the 2 ADC.   

@warning	   Not developed following safety development process
******************************************************************************/
void adc_reset_endOfConv();



#endif /* MPC5643L_ADC_H_ */
