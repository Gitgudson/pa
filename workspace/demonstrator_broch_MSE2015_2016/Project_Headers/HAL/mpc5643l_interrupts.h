/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_interrupts.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Dec 04
* 
* @brief    Hardware abstraction layer for interrupts
* 
* @copyright (C) Nicolas Broch
* 
*
*
* This hardware abstraction layer configure a default interrupt vector for
* each interrupt line. Thus, by default, if an interrupt is generated, it
* will execute the error function interrupts_IRQ_error() and put the MCu
* in a safe state.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
* How to use it
* -------------
* 
* The function interrupts_init() must be called before configuring in other
* functions the interrupt vectors. This way, if a vector is enabled, the
* second configuration of the interrupt function will overwrite the 
* error interrupt vector.
* 
* The functions interrupts_enable() and interrupts_disable() allows respectively
* to enable and disable all the interrupts.
*
*
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
* 
*/

#ifndef MPC5643L_INTERRUPTS_H_
#define MPC5643L_INTERRUPTS_H_

#include "MPC5643L.h"
#include <typedefs.h>

#include "IntcInterrupts.h"
#include "HAL/mpc5643l_mode.h"


/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define INTERRUPTS_NBR         256 //!< Number of interrupt vectors, MPC5643LRM 28.7
#define INTERRUPTS_HIGH_PRIO   15U //!< Highest priority of interrupts, MPC5643LRM 28.7
#define INTERRUPTS_LOW_PRIO     0U //!< Lowest priority of interrupts, MPC5643LRM 28.7




/*******************************************************************************
* Functions prototypes
*******************************************************************************/


/***************************************************************************//*!
@brief         Configure the interrupts functions

@return  	   void

@details       Configure the interrupts functions and intialize the software
			   vector mode.
******************************************************************************/
void interrupts_init();

/***************************************************************************//*!
@brief         Allow interrupts

@return  	   void

@details       Disable interrupt mask for all interrupts.
******************************************************************************/
void interrupts_enable();

/***************************************************************************//*!
@brief         Disable interrupts

@return  	   void

@details       Enable interrupt mask for all interrupts.
******************************************************************************/
void interrupts_disable();


/***************************************************************************//*!
@brief         Error function for interrupts

@return  	   void

@details       If this function is executed, a false interrupt has been generated.
			   In consequence, this function switches the MCU in safe mode.
******************************************************************************/
void interrupts_IRQ_error();






#endif /* MPC5643L_INTERRUPTS_H_ */
