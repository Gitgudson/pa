/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_ctu.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Dec 26
* 
* @brief    Hardware abstraction layer for the Cross-triggering unit (CTU)
* 
* @copyright (C) Nicolas Broch
*
*
* The CTU coordinates the FlexPWM, ADC and eTimers. This HAL library allows
* to configure it.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* The function ctu_init() has to be called to initialise the CTU. Then,
* if an interrupt is generated, to clear is, use function ctu_IRQ_adc_ack()
* at the end of the interupt vector.
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
*/


#ifndef MPC5643L_CTU_H_
#define MPC5643L_CTU_H_

#include "MPC5643L.h"
#include <typedefs.h> 


/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define CTU_CTUEFR            0x1FFFU       //!< CTUEFR, clear error flags\n user defined in file ctu.xlsx, MPC5643LRM 13.10.10
#define CTU_CTUIFR            0x1FFFU       //!< CTUIFR clear interrupts\n user defined in file ctu.xlsx, MPC5643LRM 13.10.11
#define CTU_IRQ_ADC_ACK       0x0200U       //!< ACK ADC interrupt\n user defined in file ctu.xlsx, MPC5643LRM 13.10.11

//-----------------------------------------------------------------------------
// Trigger unit
//-----------------------------------------------------------------------------
#define CTU_TGSISR            0x00000001UL  //!< TGSISR\n user defined in file ctu.xlsx, MPC5643LRM 13.10.1
#define CTU_TGSCR             0x0000        //!< TGSCR\n user defined in file ctu.xlsx, MPC5643LRM 13.10.2

#define CTU_T0CR              0              //!< CTU[0]\n user defined in file ctu.xlsx, MPC5643LRM 13.10.3
#define CTU_T1CR              6000           //!< CTU[1]\n user defined in file ctu.xlsx, MPC5643LRM 13.10.3
#define CTU_T2CR              0              //!< CTU[2]\n user defined in file ctu.xlsx, MPC5643LRM 13.10.3
#define CTU_T3CR              0              //!< CTU[3]\n user defined in file ctu.xlsx, MPC5643LRM 13.10.3
#define CTU_T4CR              0              //!< CTU[4]\n user defined in file ctu.xlsx, MPC5643LRM 13.10.3
#define CTU_T5CR              0              //!< CTU[5]\n user defined in file ctu.xlsx, MPC5643LRM 13.10.3
#define CTU_T6CR              0              //!< CTU[6]\n user defined in file ctu.xlsx, MPC5643LRM 13.10.3
#define CTU_T7CR              0              //!< CTU[7]\n user defined in file ctu.xlsx, MPC5643LRM 13.10.3

#define CTU_TGSCCR            0x2328U        //!< TGSCCR\n user defined in file ctu.xlsx, MPC5643LRM 13.10.4
#define CTU_TGSCRR            0x0000U        //!< TGSCR\n user defined in file ctu.xlsx, MPC5643LRM 13.10.5


//-----------------------------------------------------------------------------
// Scheduler subunit (SU)
//-----------------------------------------------------------------------------
#define CTU_THCR1              0x00004445UL  //!< THCR1\n user defined in file ctu.xlsx, MPC5643LRM 13.10.8
#define CTU_THCR2              0x00000000UL  //!< THCR2\n user defined in file ctu.xlsx, MPC5643LRM 13.10.8


//-----------------------------------------------------------------------------
// General
//-----------------------------------------------------------------------------
#define CTU_CTUCR              0x0003U        //!< CTUCR\n user defined in file ctu.xlsx, MPC5643LRM 13.10.14

#define CTU_CLCR1              0x000000000UL
#define CTU_CLCR2              0x000000000UL


#define CTU_CLR0                       0x2000	  // dual conversion
												  // B_0 (Resolver motor 0 cos), A_0 (Resolver motor 0 sin)
												  // interrupt
#define CTU_CLR1                       0xA0C6	  // dual conversion
												  // B_6 (Resolver motor 1 cos), A_6 (Resolver motor 1 sin)
#define CTU_CLR2                       0x2162	  // dual conversion
												  // B_11 (motor 0 - Ia), A_2 (motor 0 - Ic)
#define CTU_CLR3                       0x2181     // dual conversion
												  // B_12 (motor 0 - Ib), A_1 (DCBUS motor 0)
#define CTU_CLR4                       0x204D	  // dual conversion
												  // B_2 (motor 1 - Ic), A_13 (motor 1 - Ia)
#define CTU_CLR5                       0x21C3	  // dual conversion
											      // B_14 (motor 1 - Ib), A_3 (DCBUS motor 1)
#define CTU_CLR6                       0x4000	  // last command bit


/*******************************************************************************
* Functions
*******************************************************************************/

/***************************************************************************//*!
@brief         Configure the CTU

@return  	   void

@details       Configure the CTU to coordinate FlexPWMs, eTimers and ADCs.   

@warning	   Lack of lock of registers
******************************************************************************/
void ctu_init();

/***************************************************************************//*!
@brief         Acknowledge ADC interrupt

@return  	   void

@details       Must be called at the end of the vector function 206 for
 	 	 	   ADC interrupts.
******************************************************************************/
void ctu_IRQ_adc_ack();


#endif /* MPC5643L_CTU_H_ */
