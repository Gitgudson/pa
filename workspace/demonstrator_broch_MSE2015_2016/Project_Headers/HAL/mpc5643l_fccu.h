/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_fccu.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Nov 09
* 
* @brief    Hardware abstraction layer for the Fault Collection and Control Unit (FCCU)
* 
* @copyright (C) Nicolas Broch
*
*
* This library is the hardware abstraction layer for the Fault Collection and Control Unit (FCCU).
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* 
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
*
*/


#ifndef MPC5643L_FCCU_H_
#define MPC5643L_FCCU_H_

#include <typedefs.h> 
#include "HAL/mpc5643l_fccu_types.h"


/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define FCCU_MASK_STAT_CTRL 0x000000C0UL  //!< Mask for FCCU_CTRL to read the mode of the system, MPC5643LRM 22.6.1
#define FCCU_OPS_IDLE		0x00000000UL  //!< Value "idle" in FCCU_CTRL[OPS], MPC5643LRM 22.6.1 
#define FCCU_OPS_IN_PRO     0x00000040UL  //!< Value "in progress" in FCCU_CTRL[OPS], MPC5643LRM 22.6.1 
#define FCCU_OPS_ABORT      0x00000080UL  //!< Value "aborted" in FCCU_CTRL[OPS], MPC5643LRM 22.6.1 
#define FCCU_OPS_SUCCESS    0x000000C0UL  //!< Value "successful" in FCCU_CTRL[OPS], MPC5643LRM 22.6.1 

#define FCCU_CFK_KEY        0x618B7A50UL  //!< Key to write into FCCU_FCK to ACK critical faults, MPC5643LRM 22.6.9
#define FCCU_NCFK_KEY       0xAB3498FEUL  //!< Key to write into FCCU_FCK to ACK non-critical faults, MPC5643LRM 22.6.11
#define FCCU_NB_CF_REG      4             //!< Number of critical faults registers, MPC5643LRM 22.6
#define FCCU_NB_NCF_REG     4             //!< Number of non-critical faults registers, MPC5643LRM 22.6

#define FCCU_CTRL_KEY_OP1   0x913756AFUL  //!< Value to write into FCCU_CTRLK to be allowed to switch the FCCU in CONFIG mode, MPC5643LRM 22.6.2
#define FCCU_CTRL_KEY_OP2   0x825A132BUL  //!< Value to write into FCCU_CTRLK to be allowed to switch the FCCU in NORMAL mode, MPC5643LRM 22.6.2
#define FCCU_CTRL_KEY_OP16  0x825A132BUL  //!< Value to write into FCCU_CTRLK to be allowed to lock the FCCU configuration, MPC5643LRM 22.6.2

#define FCCU_CLEAR_IRQ      0x00000001UL   //!< Value to write into FCCU_STAT to clear configuration time-out interrupt, MPC5643LRM 22.6.21

//-----------------------------------------------------------------------------
// Control Registers
//-----------------------------------------------------------------------------
#define FCCU_CFG            0x003F0844UL  //!< FCCU_CFG configuration\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.3
 


//-----------------------------------------------------------------------------
// Configuration of faults
//-----------------------------------------------------------------------------
#define FCCU_CF_CFG0        0xF9EFCFFFUL  //!< FCCU_CFG0 configuration HW/SW recovery for critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.4
#define FCCU_CF_CFG1        0x00000020UL  //!< FCCU_CFG1 configuration HW/SW recovery for critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.4
#define FCCU_CF_CFG2        0x00000000UL  //!< FCCU_CFG2 configuration HW/SW recovery for critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.4
#define FCCU_CF_CFG3        0x00000000UL  //!< FCCU_CFG3 configuration HW/SW recovery for critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.4

#define FCCU_NCF_CFG0       0x01FB9FFFUL  //!< FCCU_NCFG0 configuration HW/SW recovery for non-critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.5
#define FCCU_NCF_CFG1       0x00000000UL  //!< FCCU_NCFG1 configuration HW/SW recovery for non-critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.5
#define FCCU_NCF_CFG2       0x00000000UL  //!< FCCU_NCFG2 configuration HW/SW recovery for non-critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.5
#define FCCU_NCF_CFG3       0x00000000UL  //!< FCCU_NCFG3 configuration HW/SW recovery for non-critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.5


#define FCCU_CFS_CFG0        0xA0AAAAAAUL  //!< FCCU_CFS_CFG0 fault reaction configuration for critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.6
#define FCCU_CFS_CFG1        0xAA82A8AAUL  //!< FCCU_CFS_CFG1 fault reaction configuration for critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.6
#define FCCU_CFS_CFG2        0x00000800UL  //!< FCCU_CFS_CFG2 fault reaction configuration for critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.6

#define FCCU_NCFS_CFG0       0x82A0AAAAUL  //!< FCCU_NCFS_CFG0 fault reaction configuration for non-critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.7
#define FCCU_NCFS_CFG1       0x0002AA8AUL  //!< FCCU_NCFS_CFG1 fault reaction configuration for non-critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.7


#define FCCU_NCF_E0          0x01F39CFFUL  //!< FCCU_NCF_E0 enable fault reaction  for non-critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.12

#define FCCU_NCF_TOE         0x00000000UL  //!< FCCU_NCF_TOE0 fault reaction  for non-critical faults - goes to ALARM state or straightly to FAULT\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.13

#define FCCU_NCF_TO          0x0000FFFFUL  //!< FCCU_NCF_TO timeout to switch from ALARM to FAULT for non-critical faults\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.14
#define FCCU_CFG_TO          0x00000006UL  //!< FCCU_CFG_TO timeout to switch from CONFIG to NORMAL\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.15

#define FCCU_IRQ_EN          0x00000000UL  //!< FCCU_IRQ_EN enable interrupt for CONFIG time-out\n user defined in file fccu_.xlsx, MPC5643LRM 22.6.22


/*******************************************************************************
* Global variables accessible from outside of library
*******************************************************************************/
extern Fccu_errors_s fccu_fails;


/*******************************************************************************
* Functions
*******************************************************************************/

/***************************************************************************//*!
@brief          Initialisation the FCCU. This function must be called at startup
				of the microcontroller before safety mechanisms are running.

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If a FCCU mode switch was not successful
			   -2 If the configuration is not locked

@details        This function initialise the FCCU.   

@warning		Lack of lock of registers of fault injection 
******************************************************************************/
int32_t fccu_init();

/***************************************************************************//*!
@brief         Read FCCU detected fails 

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If an error has occured

@details       This function reads current flags of fails in FCCU and save the
			   result in the global variable fccu_fails.\n
			   This function must be called before clearing errors.
******************************************************************************/
int32_t fccu_read_fails();

/***************************************************************************//*!
@brief         Clear FCCU detected fails 

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If an error occured

@details        This function must be called only when all the FCCU errors can
  	  	  	  	be cleared and before fccu_init().
******************************************************************************/
int32_t fccu_clear_fails();

/***************************************************************************//*!
@brief         Compute the CRC for the register of this library.

@return  	   void

@details        This function computes the CRC of register and compare it
				with expected value.
                
@warning		Not implemented yet
******************************************************************************/
int32_t fccu_SWTEST_REGCRC();


/***************************************************************************//*!
@brief          Interrupt in case of non-critical interrupt.

@return  	    void

@details        This function is called in case of an interrupt occurred for a
				non-critical failure. This function does nothing as the system
				is configured to automatically switch the SoC in SAFE mode.
******************************************************************************/
void fccu_IRQ_alarm();


/***************************************************************************//*!
@brief          Interrupt in case of configuration time-out.

@return  	    void

@details        This function is called in case of a time-out during configuration
				of FCCU. It switches the SoC in SAFE mode.
******************************************************************************/
void fccu_IRQ_configTimeOut();


/***************************************************************************//*!
@brief          Interrupt in case of failure of RCCx self-check

@return  	    void

@details        This function is called if the RCCx detect a failure during
				a self-check. This function switches the SoC in SAFE mode.
******************************************************************************/
void fccu_IRQ_rccx();


#endif /* MPC5643L_FCCU_H_ */
