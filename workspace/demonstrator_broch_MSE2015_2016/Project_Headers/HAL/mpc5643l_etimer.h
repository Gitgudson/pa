/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_etimer.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Dec 04
* 
* @brief    Hardware abstraction layer to configure the 3 eTimers.
* 
* @copyright (C) Nicolas Broch
* 
*
*
* This library is the hardware abstraction layer for the 3 eTimers modules.
* This library doesn't integrate all functions to change the behaviour of the eTimer
* or to read values in run-time as they are many different possibilities to 
* use them.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
* How to use it
* -------------
* 
* The function eTimer_init() configures the 3 eTimers with the parameters given through defines.
* Before calling this function, the Pads used as input or output of eTimers have to be
* configured outside of the function. This library doesn't integrate function(s) to check that
* the timers are correctly configured. If necessary, it must be integrated in the system.\n
* 
* In this library, the interrupts are not used. Nevertheless, if the interrupts would be used, please, 
* comment the configuration of the interrupt signal in the function eTimer_init() and integrate 
* it in another library.
*
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
* 
*/

#ifndef MPC5643L_ETIMER_H_
#define MPC5643L_ETIMER_H_

#include "MPC5643L.h"
#include <typedefs.h>

#include "IntcInterrupts.h"
#include "HAL/mpc5643l_mode.h"


/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define ETIMER_CLR_STS    0x03FFU //!< Mask to clear all flags in STS register, MPC5643LRM 20.4.3.11


//-----------------------------------------------------------------------------
//eTimer 0
//-----------------------------------------------------------------------------
#define ETIMER0_ENBL       0x0001U      //!< eTimer0 - ENBL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.5.1
#define ETIMER0_DREQ0      0x0000U      //!< eTimer0 - DREQ0\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.5.2
#define ETIMER0_DREQ1      0x0000U      //!< eTimer0 - DREQ1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.5.2

//-----------------------------------------------------------------------------
//eTimer 0 - channel 0
//-----------------------------------------------------------------------------
#define ETIMER0_CH0_WDTH   0x0000U      //!< eTimer0 - channel 0 - WDTOH\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.4.1
#define ETIMER0_CH0_WDTL   0x0000U      //!< eTimer0 - channel 0 - WDTOL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.4.1
#define ETIMER0_CH0_COMP1  0x07FFU      //!< eTimer0 - channel 0 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER0_CH0_COMP2  0xF800U      //!< eTimer0 - channel 0 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER0_CH0_CMPLD1 0x07FFU      //!< eTimer0 - channel 0 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER0_CH0_CMPLD2 0xF800U      //!< eTimer0 - channel 0 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER0_CH0_LOAD   0x0000U      //!< eTimer0 - channel 0 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER0_CH0_CTRL1  0x8041U      //!< eTimer0 - channel 0 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER0_CH0_CTRL2  0x8003U      //!< eTimer0 - channel 0 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER0_CH0_CTRL3  0x0000U      //!< eTimer0 - channel 0 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER0_CH0_INTDMA 0x0000U      //!< eTimer0 - channel 0 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER0_CH0_CCCTRL 0xDE00U      //!< eTimer0 - channel 0 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER0_CH0_FILT   0x0000U      //!< eTimer0 - channel 0 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 0 - channel 1
//-----------------------------------------------------------------------------
#define ETIMER0_CH1_COMP1  0x0000U      //!< eTimer0 - channel 1 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER0_CH1_COMP2  0x0000U      //!< eTimer0 - channel 1 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER0_CH1_CMPLD1 0x0000U      //!< eTimer0 - channel 1 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER0_CH1_CMPLD2 0x0000U      //!< eTimer0 - channel 1 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER0_CH1_LOAD   0x0000U      //!< eTimer0 - channel 1 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER0_CH1_CTRL1  0x0000U      //!< eTimer0 - channel 1 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER0_CH1_CTRL2  0x0000U      //!< eTimer0 - channel 1 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER0_CH1_CTRL3  0x0000U      //!< eTimer0 - channel 1 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER0_CH1_INTDMA 0x0000U      //!< eTimer0 - channel 1 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER0_CH1_CCCTRL 0x0000U      //!< eTimer0 - channel 1 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER0_CH1_FILT   0x0000U      //!< eTimer0 - channel 1 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 0 - channel 2
//-----------------------------------------------------------------------------
#define ETIMER0_CH2_COMP1  0x0000U      //!< eTimer0 - channel 2 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER0_CH2_COMP2  0x0000U      //!< eTimer0 - channel 2 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER0_CH2_CMPLD1 0x0000U      //!< eTimer0 - channel 2 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER0_CH2_CMPLD2 0x0000U      //!< eTimer0 - channel 2 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER0_CH2_LOAD   0x0000U      //!< eTimer0 - channel 2 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER0_CH2_CTRL1  0x0000U      //!< eTimer0 - channel 2 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER0_CH2_CTRL2  0x0000U      //!< eTimer0 - channel 2 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER0_CH2_CTRL3  0x0000U      //!< eTimer0 - channel 2 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER0_CH2_INTDMA 0x0000U      //!< eTimer0 - channel 2 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER0_CH2_CCCTRL 0x0000U      //!< eTimer0 - channel 2 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER0_CH2_FILT   0x0000U      //!< eTimer0 - channel 2 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 0 - channel 3
//-----------------------------------------------------------------------------
#define ETIMER0_CH3_COMP1  0x0000U      //!< eTimer0 - channel 3 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER0_CH3_COMP2  0x0000U      //!< eTimer0 - channel 3 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER0_CH3_CMPLD1 0x0000U      //!< eTimer0 - channel 3 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER0_CH3_CMPLD2 0x0000U      //!< eTimer0 - channel 3 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER0_CH3_LOAD   0x0000U      //!< eTimer0 - channel 3 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER0_CH3_CTRL1  0x0000U      //!< eTimer0 - channel 3 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER0_CH3_CTRL2  0x0000U      //!< eTimer0 - channel 3 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER0_CH3_CTRL3  0x0000U      //!< eTimer0 - channel 3 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER0_CH3_INTDMA 0x0000U      //!< eTimer0 - channel 3 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER0_CH3_CCCTRL 0x0000U      //!< eTimer0 - channel 3 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER0_CH3_FILT   0x0000U      //!< eTimer0 - channel 3 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 0 - channel 4
//-----------------------------------------------------------------------------
#define ETIMER0_CH4_COMP1  0x0000U      //!< eTimer0 - channel 4 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER0_CH4_COMP2  0x0000U      //!< eTimer0 - channel 4 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER0_CH4_CMPLD1 0x0000U      //!< eTimer0 - channel 4 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER0_CH4_CMPLD2 0x0000U      //!< eTimer0 - channel 4 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER0_CH4_LOAD   0x0000U      //!< eTimer0 - channel 4 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER0_CH4_CTRL1  0x0000U      //!< eTimer0 - channel 4 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER0_CH4_CTRL2  0x0000U      //!< eTimer0 - channel 4 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER0_CH4_CTRL3  0x0000U      //!< eTimer0 - channel 4 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER0_CH4_INTDMA 0x0000U      //!< eTimer0 - channel 4 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER0_CH4_CCCTRL 0x0000U      //!< eTimer0 - channel 4 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER0_CH4_FILT   0x0000U      //!< eTimer0 - channel 4 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 0 - channel 5
//-----------------------------------------------------------------------------
#define ETIMER0_CH5_COMP1  0x0000U      //!< eTimer0 - channel 5 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER0_CH5_COMP2  0x0000U      //!< eTimer0 - channel 5 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER0_CH5_CMPLD1 0x0000U      //!< eTimer0 - channel 5 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER0_CH5_CMPLD2 0x0000U      //!< eTimer0 - channel 5 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER0_CH5_LOAD   0x0000U      //!< eTimer0 - channel 5 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER0_CH5_CTRL1  0x0000U      //!< eTimer0 - channel 5 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER0_CH5_CTRL2  0x0000U      //!< eTimer0 - channel 5 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER0_CH5_CTRL3  0x0000U      //!< eTimer0 - channel 5 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER0_CH5_INTDMA 0x0000U      //!< eTimer0 - channel 5 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER0_CH5_CCCTRL 0x0000U      //!< eTimer0 - channel 5 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER0_CH5_FILT   0x0000U      //!< eTimer0 - channel 5 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16




//-----------------------------------------------------------------------------
//eTimer 1
//-----------------------------------------------------------------------------
#define ETIMER1_ENBL       0x0031U      //!< eTimer1 - ENBL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.5.1
#define ETIMER1_DREQ0      0x0000U      //!< eTimer1 - DREQ0\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.5.2
#define ETIMER1_DREQ1      0x0000U      //!< eTimer1 - DREQ1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.5.2


//-----------------------------------------------------------------------------
//eTimer 1 - channel 0
//-----------------------------------------------------------------------------
#define ETIMER1_CH0_COMP1  0x07FFU      //!< eTimer1 - channel 0 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER1_CH0_COMP2  0xF800U      //!< eTimer1 - channel 0 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER1_CH0_CMPLD1 0x07FFU      //!< eTimer1 - channel 0 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER1_CH0_CMPLD2 0xF800U      //!< eTimer1 - channel 0 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER1_CH0_LOAD   0x0000U      //!< eTimer1 - channel 0 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER1_CH0_CTRL1  0x8142U      //!< eTimer1 - channel 0 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER1_CH0_CTRL2  0x8003U      //!< eTimer1 - channel 0 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER1_CH0_CTRL3  0x0000U      //!< eTimer1 - channel 0 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER1_CH0_INTDMA 0x0000U      //!< eTimer1 - channel 0 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER1_CH0_CCCTRL 0xDE00U      //!< eTimer1 - channel 0 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER1_CH0_FILT   0x0000U      //!< eTimer1 - channel 0 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 1 - channel 1
//-----------------------------------------------------------------------------
#define ETIMER1_CH1_COMP1  0x0000U      //!< eTimer1 - channel 1 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER1_CH1_COMP2  0x0000U      //!< eTimer1 - channel 1 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER1_CH1_CMPLD1 0x0000U      //!< eTimer1 - channel 1 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER1_CH1_CMPLD2 0x0000U      //!< eTimer1 - channel 1 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER1_CH1_LOAD   0x0000U      //!< eTimer1 - channel 1 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER1_CH1_CTRL1  0x0000U      //!< eTimer1 - channel 1 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER1_CH1_CTRL2  0x0000U      //!< eTimer1 - channel 1 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER1_CH1_CTRL3  0x0000U      //!< eTimer1 - channel 1 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER1_CH1_INTDMA 0x0000U      //!< eTimer1 - channel 1 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER1_CH1_CCCTRL 0x0000U      //!< eTimer1 - channel 1 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER1_CH1_FILT   0x0000U      //!< eTimer1 - channel 1 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 1 - channel 2
//-----------------------------------------------------------------------------
#define ETIMER1_CH2_COMP1  0x0000U      //!< eTimer1 - channel 2 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER1_CH2_COMP2  0x0000U      //!< eTimer1 - channel 2 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER1_CH2_CMPLD1 0x0000U      //!< eTimer1 - channel 2 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER1_CH2_CMPLD2 0x0000U      //!< eTimer1 - channel 2 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER1_CH2_LOAD   0x0000U      //!< eTimer1 - channel 2 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER1_CH2_CTRL1  0x0000U      //!< eTimer1 - channel 2 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER1_CH2_CTRL2  0x0000U      //!< eTimer1 - channel 2 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER1_CH2_CTRL3  0x0000U      //!< eTimer1 - channel 2 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER1_CH2_INTDMA 0x0000U      //!< eTimer1 - channel 2 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER1_CH2_CCCTRL 0x0000U      //!< eTimer1 - channel 2 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER1_CH2_FILT   0x0000U      //!< eTimer1 - channel 2 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 1 - channel 3
//-----------------------------------------------------------------------------
#define ETIMER1_CH3_COMP1  0x0000U      //!< eTimer1 - channel 3 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER1_CH3_COMP2  0x0000U      //!< eTimer1 - channel 3 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER1_CH3_CMPLD1 0x0000U      //!< eTimer1 - channel 3 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER1_CH3_CMPLD2 0x0000U      //!< eTimer1 - channel 3 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER1_CH3_LOAD   0x0000U      //!< eTimer1 - channel 3 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER1_CH3_CTRL1  0x0000U      //!< eTimer1 - channel 3 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER1_CH3_CTRL2  0x0000U      //!< eTimer1 - channel 3 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER1_CH3_CTRL3  0x0000U      //!< eTimer1 - channel 3 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER1_CH3_INTDMA 0x0000U      //!< eTimer1 - channel 3 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER1_CH3_CCCTRL 0x0000U      //!< eTimer1 - channel 3 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER1_CH3_FILT   0x0000U      //!< eTimer1 - channel 3 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 1 - channel 4
//-----------------------------------------------------------------------------
#define ETIMER1_CH4_COMP1  0x0000U      //!< eTimer1 - channel 4 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER1_CH4_COMP2  0x0000U      //!< eTimer1 - channel 4 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER1_CH4_CMPLD1 0x0000U      //!< eTimer1 - channel 4 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER1_CH4_CMPLD2 0x0000U      //!< eTimer1 - channel 4 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER1_CH4_LOAD   0x0000U      //!< eTimer1 - channel 4 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER1_CH4_CTRL1  0xD848U      //!< eTimer1 - channel 4 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER1_CH4_CTRL2  0x8083U      //!< eTimer1 - channel 4 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER1_CH4_CTRL3  0x0000U      //!< eTimer1 - channel 4 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER1_CH4_INTDMA 0x0000U      //!< eTimer1 - channel 4 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER1_CH4_CCCTRL 0x0000U      //!< eTimer1 - channel 4 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER1_CH4_FILT   0x0000U      //!< eTimer1 - channel 4 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 1 - channel 5
//-----------------------------------------------------------------------------
#define ETIMER1_CH5_COMP1  0x0000U      //!< eTimer1 - channel 5 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER1_CH5_COMP2  0x0000U      //!< eTimer1 - channel 5 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER1_CH5_CMPLD1 0x0000U      //!< eTimer1 - channel 5 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER1_CH5_CMPLD2 0x0000U      //!< eTimer1 - channel 5 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER1_CH5_LOAD   0x0000U      //!< eTimer1 - channel 5 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER1_CH5_CTRL1  0xD848U      //!< eTimer1 - channel 5 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER1_CH5_CTRL2  0x8083U      //!< eTimer1 - channel 5 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER1_CH5_CTRL3  0x0000U      //!< eTimer1 - channel 5 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER1_CH5_INTDMA 0x0000U      //!< eTimer1 - channel 5 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER1_CH5_CCCTRL 0x0000U      //!< eTimer1 - channel 5 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER1_CH5_FILT   0x0000U      //!< eTimer1 - channel 5 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16




//-----------------------------------------------------------------------------
//eTimer 2
//-----------------------------------------------------------------------------
#define ETIMER2_ENBL       0x0000U      //!< eTimer2 - ENBL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.5.1
#define ETIMER2_DREQ0      0x0000U      //!< eTimer2 - DREQ0\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.5.2
#define ETIMER2_DREQ1      0x0000U      //!< eTimer2 - DREQ1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.5.2


//-----------------------------------------------------------------------------
//eTimer 2 - channel 0
//-----------------------------------------------------------------------------
#define ETIMER2_CH0_COMP1  0x0000U      //!< eTimer2 - channel 0 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER2_CH0_COMP2  0x0000U      //!< eTimer2 - channel 0 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER2_CH0_CMPLD1 0x0000U      //!< eTimer2 - channel 0 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER2_CH0_CMPLD2 0x0000U      //!< eTimer2 - channel 0 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER2_CH0_LOAD   0x0000U      //!< eTimer2 - channel 0 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER2_CH0_CTRL1  0x0000U      //!< eTimer2 - channel 0 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER2_CH0_CTRL2  0x0000U      //!< eTimer2 - channel 0 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER2_CH0_CTRL3  0x0000U      //!< eTimer2 - channel 0 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER2_CH0_INTDMA 0x0000U      //!< eTimer2 - channel 0 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER2_CH0_CCCTRL 0x0000U      //!< eTimer2 - channel 0 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER2_CH0_FILT   0x0000U      //!< eTimer2 - channel 0 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 2 - channel 1
//-----------------------------------------------------------------------------
#define ETIMER2_CH1_COMP1  0x0000U      //!< eTimer2 - channel 1 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER2_CH1_COMP2  0x0000U      //!< eTimer2 - channel 1 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER2_CH1_CMPLD1 0x0000U      //!< eTimer2 - channel 1 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER2_CH1_CMPLD2 0x0000U      //!< eTimer2 - channel 1 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER2_CH1_LOAD   0x0000U      //!< eTimer2 - channel 1 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER2_CH1_CTRL1  0x0000U      //!< eTimer2 - channel 1 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER2_CH1_CTRL2  0x0000U      //!< eTimer2 - channel 1 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER2_CH1_CTRL3  0x0000U      //!< eTimer2 - channel 1 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER2_CH1_INTDMA 0x0000U      //!< eTimer2 - channel 1 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER2_CH1_CCCTRL 0x0000U      //!< eTimer2 - channel 1 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER2_CH1_FILT   0x0000U      //!< eTimer2 - channel 1 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 2 - channel 2
//-----------------------------------------------------------------------------
#define ETIMER2_CH2_COMP1  0x0000U      //!< eTimer2 - channel 2 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER2_CH2_COMP2  0x0000U      //!< eTimer2 - channel 2 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER2_CH2_CMPLD1 0x0000U      //!< eTimer2 - channel 2 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER2_CH2_CMPLD2 0x0000U      //!< eTimer2 - channel 2 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER2_CH2_LOAD   0x0000U      //!< eTimer2 - channel 2 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER2_CH2_CTRL1  0x0000U      //!< eTimer2 - channel 2 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER2_CH2_CTRL2  0x0000U      //!< eTimer2 - channel 2 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER2_CH2_CTRL3  0x0000U      //!< eTimer2 - channel 2 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER2_CH2_INTDMA 0x0000U      //!< eTimer2 - channel 2 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER2_CH2_CCCTRL 0x0000U      //!< eTimer2 - channel 2 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER2_CH2_FILT   0x0000U      //!< eTimer2 - channel 2 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 2 - channel 3
//-----------------------------------------------------------------------------
#define ETIMER2_CH3_COMP1  0x0000U      //!< eTimer2 - channel 3 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER2_CH3_COMP2  0x0000U      //!< eTimer2 - channel 3 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER2_CH3_CMPLD1 0x0000U      //!< eTimer2 - channel 3 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER2_CH3_CMPLD2 0x0000U      //!< eTimer2 - channel 3 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER2_CH3_LOAD   0x0000U      //!< eTimer2 - channel 3 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER2_CH3_CTRL1  0x0000U      //!< eTimer2 - channel 3 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER2_CH3_CTRL2  0x0000U      //!< eTimer2 - channel 3 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER2_CH3_CTRL3  0x0000U      //!< eTimer2 - channel 3 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER2_CH3_INTDMA 0x0000U      //!< eTimer2 - channel 3 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER2_CH3_CCCTRL 0x0000U      //!< eTimer2 - channel 3 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER2_CH3_FILT   0x0000U      //!< eTimer2 - channel 3 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 2 - channel 4
//-----------------------------------------------------------------------------
#define ETIMER2_CH4_COMP1  0x0000U      //!< eTimer2 - channel 4 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER2_CH4_COMP2  0x0000U      //!< eTimer2 - channel 4 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER2_CH4_CMPLD1 0x0000U      //!< eTimer2 - channel 4 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER2_CH4_CMPLD2 0x0000U      //!< eTimer2 - channel 4 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER2_CH4_LOAD   0x0000U      //!< eTimer2 - channel 4 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER2_CH4_CTRL1  0x0000U      //!< eTimer2 - channel 4 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER2_CH4_CTRL2  0x0000U      //!< eTimer2 - channel 4 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER2_CH4_CTRL3  0x0000U      //!< eTimer2 - channel 4 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER2_CH4_INTDMA 0x0000U      //!< eTimer2 - channel 4 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER2_CH4_CCCTRL 0x0000U      //!< eTimer2 - channel 4 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER2_CH4_FILT   0x0000U      //!< eTimer2 - channel 4 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16

//-----------------------------------------------------------------------------
//eTimer 2 - channel 5
//-----------------------------------------------------------------------------
#define ETIMER2_CH5_COMP1  0x0000U      //!< eTimer2 - channel 5 - COMP1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.1
#define ETIMER2_CH5_COMP2  0x0000U      //!< eTimer2 - channel 5 - COMP2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.2
#define ETIMER2_CH5_CMPLD1 0x0000U      //!< eTimer2 - channel 5 - CMPL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.13
#define ETIMER2_CH5_CMPLD2 0x0000U      //!< eTimer2 - channel 5 - CMPL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.14
#define ETIMER2_CH5_LOAD   0x0000U      //!< eTimer2 - channel 5 - LOAD\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.5
#define ETIMER2_CH5_CTRL1  0x0000U      //!< eTimer2 - channel 5 - CTRL1\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.8
#define ETIMER2_CH5_CTRL2  0x0000U      //!< eTimer2 - channel 5 - CTRL2\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.9
#define ETIMER2_CH5_CTRL3  0x0000U      //!< eTimer2 - channel 5 - CTRL3\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.10
#define ETIMER2_CH5_INTDMA 0x0000U      //!< eTimer2 - channel 5 - INTDMA\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.12
#define ETIMER2_CH5_CCCTRL 0x0000U      //!< eTimer2 - channel 5 - CCCTRL\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.15
#define ETIMER2_CH5_FILT   0x0000U      //!< eTimer2 - channel 5 - FILT\n user defined in file etimer_.xlsx, MPC5643LRM 20.4.3.16


/*******************************************************************************
* Functions prototypes
*******************************************************************************/


/***************************************************************************//*!
@brief         Configure the 3 eTimers

@return  	   void

@details       This function configures the 3 eTimers. 

@warning	   Miss of register lock against writing
******************************************************************************/
void etimer_init();


/***************************************************************************//*!
@brief          Interrupt vector for each eTimer

@return  		void

@details        Function called by interrupts of eTimer0. By default, all
				interrupts are disabled. So if this function is called, an
				error has occurred and it is declared to the SoC.
******************************************************************************/
void etimer_IRQ_eTimers();



#endif /* MPC5643L_ETIMER_H_ */
