/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_uart_type.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Nov 26
* 
* @brief    Types defined for the UART HAL driver
* 
* @copyright (C) Nicolas Broch
*
*
* This file defines the types used for configure or handle the UART
* in the HAL mpc5643l_uart.h
* 
* This file is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
*
*/

#ifndef MPC5643L_UART_TYPES_H_
#define MPC5643L_UART_TYPES_H_

#include <typedefs.h>


/***************************************************************************//*!
@brief          Enumeration to define which FLEXLinD controller is used
******************************************************************************/
typedef enum{
	UART_FLEXLIND0, //!< Value for FLEXLinD 0
	UART_FLEXLIND1  //!< Value for FLEXLinD 1
}Uart_flexlin;


/***************************************************************************//*!
@brief          Buffer structure for received datas
******************************************************************************/
#define UART_SIZE_BUFFER 16 //!< Size of buffer for Rx datas

typedef struct{
	uint8_t buffer[16]; //!< Circular buffer for input data
	uint8_t inIndex;    //!< Index for next data to write
	uint8_t outIndex;   //!< Index for next data to read
}Uart_RxBuffer;


#endif /* MPC5643L_UART_TYPES_H_ */
