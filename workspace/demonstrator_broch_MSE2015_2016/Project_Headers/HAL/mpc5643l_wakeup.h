/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_wakeup.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Oct 02
* 
* @brief    Hardware abstraction layer to desactivate the wakeup unit of the 
* 			microcontroller
* 
* @copyright (C) Nicolas Broch
*
*
* This library is the hardware abstraction layer for the wakeup unit of the MPC5643L
* 
* This HAL is only compatible with the Freescale MPC5643L MCU.
* 
* How to use it
* -------------
* 
* The function 'wakeup_init()' must be called during the initialisation.
* 
* The functions 'wakeup_SWTEST_REGCRC()' must be called
* one per FTTI.
*
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*/


#ifndef MPC5643L_WAKEUP_H_
#define MPC5643L_WAKEUP_H_

#include "typedefs.h"


/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// NMI_NCR register
//-----------------------------------------------------------------------------
#define WAKEUP_NMI_NCR   0xE0000000UL //!< NMI_NCR, MPC5643LRM 51.3.2.2

/***************************************************************************//*!
@brief          Configure the wakeup unit such that it cannot generate 
				interrupts.

@return  		void

@details        Configure the wakeup unit to not generate interrupts and lock
     	 	 	the configuration register.
******************************************************************************/
void wakeup_init();


/***************************************************************************//*!
@brief          Check the registers of wakeup_ by calculating a CRC of them.

@return  		int32_t\n
				0 if no problem detected
				-1 if the CRC is not the result expected

@details        This function computes the CRC of register and compare it
				with expected value.
                
@warning		Not implemented yet
******************************************************************************/
int32_t wakeup_SWTEST_REGCRC();

#endif /* MPC5643L_WAKEUP_H_ */
