/******************************************************************************
* 
*	Semester project PS5 of Olivier Progin
*
***************************************************************************//*!
*
* @file     mpc5643l_dspi_types.h
*
* @author   Olivier Progin
* 
* @version  0.1
* 
* @date     2015 Nov 11
* 
* @brief    Hardware abstraction layer to handle the SoC modes.
* 
* @copyright (C) Olivier Progin
*
*
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
* 
* How to use it
* -------------
*
*
*/

#ifndef MPC5643L_DSPI_TYPES_H_
#define MPC5643L_DSPI_TYPES_H_

/***************************************************************************//*!
@brief          Enumeration of the different DSPI
******************************************************************************/

typedef enum {
	DSPI0=0,	//!< select the DSPI0
	DSPI1=1,	//!< select the DSPI0
	DSPI2=2 	//!< select the DSPI0	
}Dspi_nbr_e;


/***************************************************************************//*!
@brief          Enumeration of the PCSx
******************************************************************************/
typedef enum {
	PCS0=0x00010000,	//!< select the PCS0
	PCS1=0x00020000,	//!< select the PCS1
	PCS2=0x00040000,	//!< select the PCS2
	PCS3=0x00080000,	//!< select the PCS3
	PCS4=0x00100000,	//!< select the PCS4
	PCS5=0x00200000,	//!< select the PCS5
	PCS6=0x00400000,	//!< select the PCS6
	PCS7=0x00800000		//!< select the PCS7
}Dspi_pcs_e;

/***************************************************************************//*!
@brief          Enumeration of the CTAR available
******************************************************************************/
typedef enum {
	CTAR0=0x00000000,	//!< select the CTAR0
	CTAR1=0x10000000,	//!< select the CTAR0
	CTAR2=0x20000000,	//!< select the CTAR0
	CTAR3=0x30000000	//!< select the CTAR0
}Dspi_ctar_e;

#endif /* MPC5643L_DSPI_TYPES_H_ */
