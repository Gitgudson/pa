/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_swg.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Nov 23
* 
* @brief    Hardware abstraction layer for the swg
* 
* @copyright (C) Nicolas Broch
*
*
* This library is the hardware abstraction layer for the sine wave generator (SWG).
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* The function swg_init() configure the SWG and the pad D[7] which is the output pad
* for the SWG signal. Then, the SWG works alone.
* 
* The interrupt SWG ERROR can be generated when an error occur or the swg_is_error() can
* be used to poll the state of SWG.
* 
* 
* Pad used
* --------
* 
* |Pad       | Function                  | When is it configure?
* |:---------|:--------------------------|:------------------------
* | D[7]     | Analog sinus output       | If swg_init() is called
*
*
*/
#ifndef MPC5643L_SWG_H_
#define MPC5643L_SWG_H_

#include "MPC5643L.h"
#include <typedefs.h> 
#include "HAL/mpc5643l_hal_type.h"


/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define SWG_CHECK_CLEAR_ERROR 0x00800000UL //!< Mask to read if SWG is in error and clear it, MPC5643LRM 44.3.2
#define SWG_INT_ERR_EN        0x00800000UL //!< Mask to enable/disable the SWG interrupt for errors, MPC5643LRM 44.3.1


//-----------------------------------------------------------------------------
// SWG configuration
//-----------------------------------------------------------------------------
#define SWG_CTRL_DEF 0x7C02020DUL //!< SWG_CTRL\n user defined in file swg_.xlsx, MPC5643LRM 44.3.1




/*******************************************************************************
* Functions prototypes
*******************************************************************************/

/***************************************************************************//*!
@brief          Configure the SWG

@return  		void
@param			swg_int		If equal to Hal_YN_e, an interrupt is generated when
							an error is detected in SWG. Otherwise, the polling
							function swg_isError() has to be used to detect errors
							in SWG.

@details        The function setups the SWG and the pad D[7]. The parameter swg_int 
				allows to choose if the error interrupt is generated or not.\n
				If the SWG is already started, calling this function will turn
				it off.\n
				This function can be called during init.
******************************************************************************/
void swg_init(Hal_YN_e swg_int);


/***************************************************************************//*!
@brief          start the SWG

@return  		void

@details        Start the SWG sinus generation.
******************************************************************************/
void swg_start();

/***************************************************************************//*!
@brief          stop the SWG

@return  		void

@details        Stop the SWG sinus generation. After a stop, the pad D[7]
				has an undefined DC value.
******************************************************************************/
void swg_stop();


/***************************************************************************//*!
@brief          Check if the SWG is in error

@return 		return HAL_YES if in error or HAL_NO otherwise

@details        This function check if the SWG module is in error
******************************************************************************/
Hal_YN_e swg_is_error();

/***************************************************************************//*!
@brief          Clear SWG error

@return 		void

@details        Clear SWG error. After a clearing
******************************************************************************/
void swg_clear_error();

/***************************************************************************//*!
@brief          Interrupt for SWG error

@return  	    void

@details        This function is called when an error occurs in SWG. If it is
				executed, this function called mode_sw_error() to put SoC
				in error mode
******************************************************************************/
void swg_IRQ_error();



#endif /* MPC5643L_SWG_H_ */
