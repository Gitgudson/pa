/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_siul.h
*
* @author   Nicolas Broch
* 
* @version  0.2
* 
* @date     2015 Oct 29
* 
* @brief    Hardware abstraction layer for the GPIO controller
* 
* @copyright (C) Nicolas Broch
*
*
* This library is the hardware abstraction layer for the "System Integration Unit
* Lite" (SIUL) which controls the GPIOs and their configuration.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* The function 'siul_init()' must be called after initialising the FCCU and the clock, but
* before initialising the peripherals of the microcontroller.
* 
* This HAL doesn't handle interrupt from GPIO. If needed, please integrate it in an
* other level.
* 
*
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*/

#ifndef SIUL_H_
#define SIUL_H_

#include "MPC5643L.h"
#include "IntcInterrupts.h"
#include <typedefs.h> 
#include "HAL/mpc5643l_hal_type.h"
#include "HAL/mpc5643l_siul_types.h"


/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
// SIUL Pad Configuration Registers masks
//-----------------------------------------------------------------------------
#define SIUL_PCR_OBE 0x0200 //!< SIUL_PCR[OBE] mask - pad in output mode, MPC5643LRM 47.5.2.8
#define SIUL_PCR_IBE 0x0100 //!< SIUL_PCR[IBE] mask - pad in input mode, MPC5643LRM 47.5.2.8



/*******************************************************************************
* Functions
*******************************************************************************/


/***************************************************************************//*!
@brief          Initialisation of SIUL controller.

@return  		void

@details        The function inhibits all the interrupts of the GPIO in the SIUL

@warning		Miss of register lock
******************************************************************************/
void siul_init();

/***************************************************************************//*!
@brief          Configure a GPIO Pad.

@param         pad         number of the GPIO pad.
@param         safeModeEN  if equal to SIUL_YES, in SoC SAFE mode, the output buffer remains functional.
						   Otherwise, the output buffer of the pad is disabled in SoC SAFE mode.
@param         analogPad   set to SIUL_YES if this pad is used as an analog input.
@param         mode		   SIUL_MODE_GPIO : pad configured in GPIO mode\n
						   SIUL_MODE_ALTER_x : pad configured in alternative mode x
@param		   inout	   Set up the Pad as input or output. When configured as an
						   output, the SoC internal mechanism to read back the output is set. So the function
						   siul_read_pad() can be use on an input or output pad.
@param		   slewRateControl Set if the Pad is configured as fast or slow mode.
@param		   type		   if the pad is configured as an input, SIUL_INPUT_PULL_UP or SIUL_INPUT_PULL_DOWN set
                           the pull-up, respectively pull-down resistor of the pad. If the pad is configured as
                           output, 'SIUL_OUTPUT_OPEN_DRAIN' set the output in open-drain mode.
@param		   lockConfig  if equal to SIUL_YES, the register to configuration this pad is hard locked. It will not
						   be possible anymore to change the configuration of this pad until reset.

@return  		void

@details        The function configure a Pad, except the interrupt 
				configuration. For more information about configuration of 
				a pad, please refer to the reference manual, chapter 47.5.2.8

@warning		Miss of register lock
******************************************************************************/
void siul_config_pad(const Siul_pcr_e pad, 
					 const Hal_YN_e safeModeEN,
		             const Hal_YN_e analogPad, 
		             const Siul_gpio_mode_e mode, 
		             const Siul_inputOutput_e inout,
		             const Siul_slewRate_e slewRateControl,
		             const Siul_inputOutputType_e type,
		             const Hal_YN_e lockConfig);

/***************************************************************************//*!
@brief         Set in reset state a GPIO Pad.

@param         pad         number of the GPIO pad.


@return  	   void

@details        The function sets up a pad in its reset state if it is not
                locked. Pad with special reset configuration are also configured
                with the standard reset configuration. For more information, 
                please refer to the reference manual, chapter 47.5.2.8.
******************************************************************************/
void siul_deconfig_pad(const Siul_pcr_e pad);


/***************************************************************************//*!
@brief         Set up the PSMI register for a pad

@param         psmiVal        PSMI value, refer to reference manual, chapter 3.4
			   padsel_value	  PADSEL value, refer to reference manual, chapter 3.4


@return  	   void

@details        The function sets up a PSMI register to configure the pad
                selection for a multiplexed input, reference manual chapter 47.5.2.9.\n
******************************************************************************/
void siul_config_padSelection(const uint8_t psmiVal, const uint8_t padsel_value);




/***************************************************************************//*!
@brief         Read a GPIO input or output.

@param         pad         number of the GPIO pad.

@return  	   int32_t\n
			   If the Pad is configured as a GPIO input or output, return the value of GPIO [0, 1]
			   If the Pad is not configured as a GPIO input or output, return -1.\n 
			   Please use the SIUL_LOW and SIUL_HIGH definition to interpret the 
			   returned value if >= 0.

@details       Read the value of an input GPIO. Check also if the Pad given is
			   configured as an Input or not.\n
			   This function can be use on a pad configured as input or output.
******************************************************************************/
int32_t siul_read_pad(const Siul_pcr_e pad);



/***************************************************************************//*!
@brief         Write a GPIO.

@param         pad         number of the GPIO pad.
@param		   value	   [0, 1] as output value. Please use the SIUL_LOW and
						   SIUL_HIGH to write parameter.

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If the Pad is not configured as a GPIO input and output.\n
			   -2 If the number in value is not 0 or 1.

@details       Write the value of an input GPIO. Check also if the Pad given is
			   configured as an output or not and check the number in value.
			   This function also wait until the value is effectively written
			   into the pad. A timeout must be set through a watchdog.\n
			   This function can be only use on a pad configured as output.
			   
@warning	   This function waits until the value written on the pad is read
			   through the read-back mechanisms. It is assumed that a watchdog
			   would be triggered in case of failure of the pad. If it is not
			   the case, please use function siul_write_fast_pad() instead.
******************************************************************************/
int32_t siul_write_pad(const Siul_pcr_e pad, const uint8_t value);


/***************************************************************************//*!
@brief         Write a GPIO.

@param         pad         number of the GPIO pad.
@param		   value	   [0, 1] as output value. Please use the SIUL_LOW and
						   SIUL_HIGH to write parameter.

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If the Pad is not configured as a GPIO output.\n
			   -2 If the number in value is not 0 or 1.

@details       Write the value of an input GPIO. Check also if the Pad given is
			   configured as an output or not and check the number in value. This
			   function doesn't wait that the value is effectively written into
			   the pad to return.\n
			   This function can be only use on a pad configured as output.
******************************************************************************/
int32_t siul_write_fast_pad(const Siul_pcr_e pad, const uint8_t value);

/***************************************************************************//*!
@brief         Toggle the state of an output GPIO.

@param         pad         number of the GPIO pad.

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If the Pad is not configured as a GPIO input and output.\n

@details       Toggle the state of an output GPIO if the pad is configured as
			   an output GPIO.\n
			   This function can be only use on a pad configured as output.
			   
@warning	   This function uses the value read on the pad to toggle it. In
			   case of failure of the pad, this function can have unexpected
			   behaviour. So this function should only be used in non-safety
			   code.
******************************************************************************/
int32_t siul_toggle_pad(const Siul_pcr_e pad);


/***************************************************************************//*!
@brief          Check the registers of siul_ by calculating a CRC of them.

@return  		int32_t\n
				0 if no problem detected
				-1 if the CRC is not the result expected

@details        This function computes the CRC of register and compare it
				with expected value.
                
@warning		Not implemented yet
******************************************************************************/
int32_t siul_SWTEST_REGCRC();

#endif /* SIUL_H_ */
