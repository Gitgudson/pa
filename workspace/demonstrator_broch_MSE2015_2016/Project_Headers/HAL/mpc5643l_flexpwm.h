/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_flexpwm.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Dec 16
* 
* @brief    Hardware abstraction layer for the 2 FlexPWM modules.
* 
* @copyright (C) Nicolas Broch
* 
*
*
* This library is the hardware abstraction layer for the 2 FlexPWM modules which are
* configured as PWM generators.
* It proposes functions to initialise the modules and change the duty cycle of each
* channel independently.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
* How to use it
* -------------
* 
* The function flexpwm_init() configures the modules and must be called before the
* other functions.
* 
* The interrupts FFLAG of FlexPWM_0 and FlexPWM_1 must be implemented at a system
* level.
* 
* 
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* | G[8]     | FlexPWM0 fault 0 input     | If flexpwm_init() is called
* | G[9]     | FlexPWM0 fault 1 input     | If flexpwm_init() is called
* | G[10]    | FlexPWM0 fault 2 input     | If flexpwm_init() is called
* | G[11]    | FlexPWM0 fault 3 input     | If flexpwm_init() is called
* | I[0]     | FlexPWM1 fault 0 input     | If flexpwm_init() is called
* | I[1]     | FlexPWM1 fault 1 input     | If flexpwm_init() is called
* | I[2]     | FlexPWM1 fault 2 input     | If flexpwm_init() is called
* | I[3]     | FlexPWM1 fault 3 input     | If flexpwm_init() is called
* | D[10]    | FlexPWM0_sub0 PWMA output  | If FlexPWM0 - submodule 0 is enabled
* | D[11]    | FlexPWM0_sub0 PWMB output  | If FlexPWM0 - submodule 0 is enabled
* | F[0]     | FlexPWM0_sub1 PWMA output  | If FlexPWM0 - submodule 1 is enabled
* | D[14]    | FlexPWM0_sub1 PWMB output  | If FlexPWM0 - submodule 1 is enabled
* | G[3]     | FlexPWM0_sub2 PWMA output  | If FlexPWM0 - submodule 2 is enabled
* | G[4]     | FlexPWM0_sub2 PWMB output  | If FlexPWM0 - submodule 2 is enabled
* | G[6]     | FlexPWM0_sub3 PWMA output  | If FlexPWM0 - submodule 3 is enabled
* | G[7]     | FlexPWM0_sub3 PWMB output  | If FlexPWM0 - submodule 3 is enabled
* | H[5]     | FlexPWM1_sub0 PWMA output  | If FlexPWM1 - submodule 0 is enabled
* | H[6]     | FlexPWM1_sub0 PWMB output  | If FlexPWM1 - submodule 0 is enabled
* | H[8]     | FlexPWM1_sub1 PWMA output  | If FlexPWM1 - submodule 1 is enabled
* | H[9]     | FlexPWM1_sub1 PWMB output  | If FlexPWM1 - submodule 1 is enabled
* | H[11]    | FlexPWM1_sub2 PWMA output  | If FlexPWM1 - submodule 2 is enabled
* | H[12]    | FlexPWM1_sub2 PWMB output  | If FlexPWM1 - submodule 2 is enabled
* | H[14]    | FlexPWM1_sub3 PWMA output  | If FlexPWM1 - submodule 3 is enabled
* | H[15]    | FlexPWM1_sub3 PWMB output  | If FlexPWM1 - submodule 3 is enabled
* 
* 
* Updates
* -------------
*
* - 2015.01.02
* 		+ Change the parameter numMod of functions flexpwm_run(), flexpwm_stop() and 
* 		  flexpwm_change_dutyCycle() to use an enumeration
*/

#ifndef MPC5643L_FLEXPWM_H_
#define MPC5643L_FLEXPWM_H_

#include "MPC5643L.h"
#include <typedefs.h>

#include "IntcInterrupts.h"
#include "HAL/mpc5643l_siul.h"
#include "HAL/mpc5643l_mode.h"
#include "HAL/mpc5643l_flexpwm_types.h"


/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define FLEXPWM_LDOK       0x000FU      //!< Mask for MCTRL register to allow update of VALx registers, MPC5643LRM 25.4.4.5
#define FLEXPWM_MASK_OFF   0x0770U      //!< Mask for MCTRL register to allow update of VALx registers, MPC5643LRM 25.4.4.5
#define FLEXPWM_DUTY_RANGE 1500L		//!< Value in FlexPWM module for 100% duty cycle
#define FLEXPWM_MAX_DUTY   0x8FFF		//!< Value for 100% duty cycle in function flexpwm_change_dutyCycle()
#define FLEXPWM_MIN_DUTY   0x8000		//!< Value for -100% duty cycle in function flexpwm_change_dutyCycle()


//-----------------------------------------------------------------------------
//FlexPWM0 - general
//-----------------------------------------------------------------------------
#define FLEXPWM0_OUTEN     0x0770U      //!< FlexPWM0 - OUTEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.4.1
#define FLEXPWM0_MASK      0x0770U      //!< FlexPWM0 - MASK\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.4.2
#define FLEXPWM0_SWCOUT    0x0000U      //!< FlexPWM0 - SWOUT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.4.3
#define FLEXPWM0_DTSRCSEL  0x0000U      //!< FlexPWM0 - DTSRCSEL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.4.4
#define FLEXPWM0_MCTRL     0x0700U      //!< FlexPWM0 - MCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.4.5
#define FLEXPWM0_FCTRL     0xF0FFU      //!< FlexPWM0 - FCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.5.1
#define FLEXPWM0_FSTS      0x000FU      //!< FlexPWM0 - FSTS\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.5.2
#define FLEXPWM0_FFILT     0x0002U      //!< FlexPWM0 - FFILT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.5.3


//-----------------------------------------------------------------------------
//FlexPWM0 - submodule 0 - configuration
//-----------------------------------------------------------------------------
#define FLEXPWM0_INIT_0      0xF448U      //!< FlexPWM0 - INIT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.2
#define FLEXPWM0_CTRL2_0     0x0010U      //!< FlexPWM0 - CTRL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.3
#define FLEXPWM0_CTRL1_0     0x1400U      //!< FlexPWM0 - CTRL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.4
#define FLEXPWM0_VAL0_0      0x0000U      //!< FlexPWM0 - VAL0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.5
#define FLEXPWM0_VAL1_0      0x0BB8U      //!< FlexPWM0 - VAL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.6
#define FLEXPWM0_VAL2_0      0xFA24U      //!< FlexPWM0 - VAL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.7
#define FLEXPWM0_VAL3_0      0x05DCU      //!< FlexPWM0 - VAL3\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.8
#define FLEXPWM0_VAL4_0      0xFA24U      //!< FlexPWM0 - VAL4\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.9
#define FLEXPWM0_VAL5_0      0x05DCU      //!< FlexPWM0 - VAL5\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.10
#define FLEXPWM0_OCTRL_0     0x0410U      //!< FlexPWM0 - OCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.11
#define FLEXPWM0_INTEN_0     0x0000U      //!< FlexPWM0 - INTEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.13
#define FLEXPWM0_DMAEN_0     0x0000U      //!< FlexPWM0 - DMAEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.14
#define FLEXPWM0_TCTRL_0     0x0001U      //!< FlexPWM0 - TCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.15
#define FLEXPWM0_DISMAP_0    0x0FFFU      //!< FlexPWM0 - DISMAP\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.16
#define FLEXPWM0_DTCNT0_0    0x001EU      //!< FlexPWM0 - DTCNT0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM0_DTCNT1_0    0x001EU      //!< FlexPWM0 - DTCNT1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM0_CAPTCTRLX_0 0x0000U      //!< FlexPWM0 - CAPTCTRLX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.18
#define FLEXPWM0_CAPTCMPX_0  0x0000U      //!< FlexPWM0 - CAPTCMPX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.19

//-----------------------------------------------------------------------------
//FlexPWM0 - submodule 1 - configuration
//-----------------------------------------------------------------------------
#define FLEXPWM0_INIT_1      0xF448U      //!< FlexPWM0 - INIT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.2
#define FLEXPWM0_CTRL2_1     0x0214U      //!< FlexPWM0 - CTRL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.3
#define FLEXPWM0_CTRL1_1     0x1400U      //!< FlexPWM0 - CTRL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.4
#define FLEXPWM0_VAL0_1      0x0000U      //!< FlexPWM0 - VAL0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.5
#define FLEXPWM0_VAL1_1      0x0BB8U      //!< FlexPWM0 - VAL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.6
#define FLEXPWM0_VAL2_1      0xFA24U      //!< FlexPWM0 - VAL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.7
#define FLEXPWM0_VAL3_1      0x05DCU      //!< FlexPWM0 - VAL3\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.8
#define FLEXPWM0_VAL4_1      0xFA24U      //!< FlexPWM0 - VAL4\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.9
#define FLEXPWM0_VAL5_1      0x05DCU      //!< FlexPWM0 - VAL5\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.10
#define FLEXPWM0_OCTRL_1     0x0410U      //!< FlexPWM0 - OCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.11
#define FLEXPWM0_INTEN_1     0x0000U      //!< FlexPWM0 - INTEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.13
#define FLEXPWM0_DMAEN_1     0x0000U      //!< FlexPWM0 - DMAEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.14
#define FLEXPWM0_TCTRL_1     0x0000U      //!< FlexPWM0 - TCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.15
#define FLEXPWM0_DISMAP_1    0x0FFFU      //!< FlexPWM0 - DISMAP\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.16
#define FLEXPWM0_DTCNT0_1    0x001EU      //!< FlexPWM0 - DTCNT0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM0_DTCNT1_1    0x001EU      //!< FlexPWM0 - DTCNT1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM0_CAPTCTRLX_1 0x0000U      //!< FlexPWM0 - CAPTCTRLX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.18
#define FLEXPWM0_CAPTCMPX_1  0x0000U      //!< FlexPWM0 - CAPTCMPX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.19

//-----------------------------------------------------------------------------
//FlexPWM0 - submodule 2 - configuration
//-----------------------------------------------------------------------------
#define FLEXPWM0_INIT_2      0xF448U      //!< FlexPWM0 - INIT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.2
#define FLEXPWM0_CTRL2_2     0x0214U      //!< FlexPWM0 - CTRL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.3
#define FLEXPWM0_CTRL1_2     0x1400U      //!< FlexPWM0 - CTRL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.4
#define FLEXPWM0_VAL0_2      0x0000U      //!< FlexPWM0 - VAL0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.5
#define FLEXPWM0_VAL1_2      0x0BB8U      //!< FlexPWM0 - VAL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.6
#define FLEXPWM0_VAL2_2      0xFA24U      //!< FlexPWM0 - VAL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.7
#define FLEXPWM0_VAL3_2      0x05DCU      //!< FlexPWM0 - VAL3\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.8
#define FLEXPWM0_VAL4_2      0xFA24U      //!< FlexPWM0 - VAL4\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.9
#define FLEXPWM0_VAL5_2      0x05DCU      //!< FlexPWM0 - VAL5\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.10
#define FLEXPWM0_OCTRL_2     0x0410U      //!< FlexPWM0 - OCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.11
#define FLEXPWM0_INTEN_2     0x0000U      //!< FlexPWM0 - INTEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.13
#define FLEXPWM0_DMAEN_2     0x0000U      //!< FlexPWM0 - DMAEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.14
#define FLEXPWM0_TCTRL_2     0x0000U      //!< FlexPWM0 - TCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.15
#define FLEXPWM0_DISMAP_2    0x0FFFU      //!< FlexPWM0 - DISMAP\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.16
#define FLEXPWM0_DTCNT0_2    0x001EU      //!< FlexPWM0 - DTCNT0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM0_DTCNT1_2    0x001EU      //!< FlexPWM0 - DTCNT1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM0_CAPTCTRLX_2 0x0000U      //!< FlexPWM0 - CAPTCTRLX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.18
#define FLEXPWM0_CAPTCMPX_2  0x0000U      //!< FlexPWM0 - CAPTCMPX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.19

//-----------------------------------------------------------------------------
//FlexPWM0 - submodule 3 - configuration
//-----------------------------------------------------------------------------
#define FLEXPWM0_INIT_3      0x0000U      //!< FlexPWM0 - INIT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.2
#define FLEXPWM0_CTRL2_3     0x0000U      //!< FlexPWM0 - CTRL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.3
#define FLEXPWM0_CTRL1_3     0x0000U      //!< FlexPWM0 - CTRL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.4
#define FLEXPWM0_VAL0_3      0x0000U      //!< FlexPWM0 - VAL0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.5
#define FLEXPWM0_VAL1_3      0x0000U      //!< FlexPWM0 - VAL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.6
#define FLEXPWM0_VAL2_3      0x0000U      //!< FlexPWM0 - VAL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.7
#define FLEXPWM0_VAL3_3      0x0000U      //!< FlexPWM0 - VAL3\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.8
#define FLEXPWM0_VAL4_3      0x0000U      //!< FlexPWM0 - VAL4\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.9
#define FLEXPWM0_VAL5_3      0x0000U      //!< FlexPWM0 - VAL5\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.10
#define FLEXPWM0_OCTRL_3     0x0000U      //!< FlexPWM0 - OCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.11
#define FLEXPWM0_INTEN_3     0x0000U      //!< FlexPWM0 - INTEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.13
#define FLEXPWM0_DMAEN_3     0x0000U      //!< FlexPWM0 - DMAEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.14
#define FLEXPWM0_TCTRL_3     0x0000U      //!< FlexPWM0 - TCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.15
#define FLEXPWM0_DISMAP_3    0x0000U      //!< FlexPWM0 - DISMAP\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.16
#define FLEXPWM0_DTCNT0_3    0x0000U      //!< FlexPWM0 - DTCNT0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM0_DTCNT1_3    0x0000U      //!< FlexPWM0 - DTCNT1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM0_CAPTCTRLX_3 0x0000U      //!< FlexPWM0 - CAPTCTRLX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.18
#define FLEXPWM0_CAPTCMPX_3  0x0000U      //!< FlexPWM0 - CAPTCMPX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.19

//-----------------------------------------------------------------------------
//FlexPWM1 - general
//-----------------------------------------------------------------------------
#define FLEXPWM1_OUTEN     0x0770U      //!< FlexPWM1 - OUTEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.4.1
#define FLEXPWM1_MASK      0x0770U      //!< FlexPWM1 - MASK\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.4.2
#define FLEXPWM1_SWCOUT    0x0000U      //!< FlexPWM1 - SWOUT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.4.3
#define FLEXPWM1_DTSRCSEL  0x0000U      //!< FlexPWM1 - DTSRCSEL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.4.4
#define FLEXPWM1_MCTRL     0x0700U      //!< FlexPWM1 - MCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.4.5
#define FLEXPWM1_FCTRL     0xF0FFU      //!< FlexPWM1 - FCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.5.1
#define FLEXPWM1_FSTS      0x000FU      //!< FlexPWM1 - FSTS\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.5.2
#define FLEXPWM1_FFILT     0x0002U      //!< FlexPWM1 - FFILT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.5.3

//-----------------------------------------------------------------------------
//FlexPWM1 - submodule 0 - configuration
//-----------------------------------------------------------------------------
#define FLEXPWM1_INIT_0      0xF448U      //!< FlexPWM1 - INIT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.2
#define FLEXPWM1_CTRL2_0     0x03A0U      //!< FlexPWM1 - CTRL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.3
#define FLEXPWM1_CTRL1_0     0x0400U      //!< FlexPWM1 - CTRL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.4
#define FLEXPWM1_VAL0_0      0x0000U      //!< FlexPWM1 - VAL0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.5
#define FLEXPWM1_VAL1_0      0x0BB8U      //!< FlexPWM1 - VAL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.6
#define FLEXPWM1_VAL2_0      0xFA24U      //!< FlexPWM1 - VAL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.7
#define FLEXPWM1_VAL3_0      0x05DCU      //!< FlexPWM1 - VAL3\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.8
#define FLEXPWM1_VAL4_0      0xFA24U      //!< FlexPWM1 - VAL4\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.9
#define FLEXPWM1_VAL5_0      0x05DCU      //!< FlexPWM1 - VAL5\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.10
#define FLEXPWM1_OCTRL_0     0x0410U      //!< FlexPWM1 - OCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.11
#define FLEXPWM1_INTEN_0     0x0000U      //!< FlexPWM1 - INTEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.13
#define FLEXPWM1_DMAEN_0     0x0000U      //!< FlexPWM1 - DMAEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.14
#define FLEXPWM1_TCTRL_0     0x0000U      //!< FlexPWM1 - TCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.15
#define FLEXPWM1_DISMAP_0    0x0FFFU      //!< FlexPWM1 - DISMAP\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.16
#define FLEXPWM1_DTCNT0_0    0x001EU      //!< FlexPWM1 - DTCNT0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM1_DTCNT1_0    0x001EU      //!< FlexPWM1 - DTCNT1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM1_CAPTCTRLX_0 0x0000U      //!< FlexPWM1 - CAPTCTRLX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.18
#define FLEXPWM1_CAPTCMPX_0  0x0000U      //!< FlexPWM1 - CAPTCMPX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.19

//-----------------------------------------------------------------------------
//FlexPWM1 - submodule 1 - configuration
//-----------------------------------------------------------------------------
#define FLEXPWM1_INIT_1      0xF448U      //!< FlexPWM1 - INIT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.2
#define FLEXPWM1_CTRL2_1     0x03ACU      //!< FlexPWM1 - CTRL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.3
#define FLEXPWM1_CTRL1_1     0x0400U      //!< FlexPWM1 - CTRL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.4
#define FLEXPWM1_VAL0_1      0x0000U      //!< FlexPWM1 - VAL0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.5
#define FLEXPWM1_VAL1_1      0x0BB8U      //!< FlexPWM1 - VAL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.6
#define FLEXPWM1_VAL2_1      0xFA24U      //!< FlexPWM1 - VAL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.7
#define FLEXPWM1_VAL3_1      0x05DCU      //!< FlexPWM1 - VAL3\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.8
#define FLEXPWM1_VAL4_1      0xFA24U      //!< FlexPWM1 - VAL4\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.9
#define FLEXPWM1_VAL5_1      0x05DCU      //!< FlexPWM1 - VAL5\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.10
#define FLEXPWM1_OCTRL_1     0x0410U      //!< FlexPWM1 - OCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.11
#define FLEXPWM1_INTEN_1     0x0000U      //!< FlexPWM1 - INTEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.13
#define FLEXPWM1_DMAEN_1     0x0000U      //!< FlexPWM1 - DMAEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.14
#define FLEXPWM1_TCTRL_1     0x0000U      //!< FlexPWM1 - TCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.15
#define FLEXPWM1_DISMAP_1    0x0FFFU      //!< FlexPWM1 - DISMAP\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.16
#define FLEXPWM1_DTCNT0_1    0x001EU      //!< FlexPWM1 - DTCNT0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM1_DTCNT1_1    0x001EU      //!< FlexPWM1 - DTCNT1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM1_CAPTCTRLX_1 0x0000U      //!< FlexPWM1 - CAPTCTRLX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.18
#define FLEXPWM1_CAPTCMPX_1  0x0000U      //!< FlexPWM1 - CAPTCMPX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.19

//-----------------------------------------------------------------------------
//FlexPWM1 - submodule 2 - configuration
//-----------------------------------------------------------------------------
#define FLEXPWM1_INIT_2      0xF448U      //!< FlexPWM1 - INIT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.2
#define FLEXPWM1_CTRL2_2     0x03ACU      //!< FlexPWM1 - CTRL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.3
#define FLEXPWM1_CTRL1_2     0x0400U      //!< FlexPWM1 - CTRL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.4
#define FLEXPWM1_VAL0_2      0x0000U      //!< FlexPWM1 - VAL0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.5
#define FLEXPWM1_VAL1_2      0x0BB8U      //!< FlexPWM1 - VAL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.6
#define FLEXPWM1_VAL2_2      0xFA24U      //!< FlexPWM1 - VAL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.7
#define FLEXPWM1_VAL3_2      0x05DCU      //!< FlexPWM1 - VAL3\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.8
#define FLEXPWM1_VAL4_2      0xFA24U      //!< FlexPWM1 - VAL4\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.9
#define FLEXPWM1_VAL5_2      0x05DCU      //!< FlexPWM1 - VAL5\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.10
#define FLEXPWM1_OCTRL_2     0x0410U      //!< FlexPWM1 - OCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.11
#define FLEXPWM1_INTEN_2     0x0000U      //!< FlexPWM1 - INTEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.13
#define FLEXPWM1_DMAEN_2     0x0000U      //!< FlexPWM1 - DMAEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.14
#define FLEXPWM1_TCTRL_2     0x0000U      //!< FlexPWM1 - TCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.15
#define FLEXPWM1_DISMAP_2    0x0FFFU      //!< FlexPWM1 - DISMAP\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.16
#define FLEXPWM1_DTCNT0_2    0x001EU      //!< FlexPWM1 - DTCNT0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM1_DTCNT1_2    0x001EU      //!< FlexPWM1 - DTCNT1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM1_CAPTCTRLX_2 0x0000U      //!< FlexPWM1 - CAPTCTRLX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.18
#define FLEXPWM1_CAPTCMPX_2  0x0000U      //!< FlexPWM1 - CAPTCMPX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.19

//-----------------------------------------------------------------------------
//FlexPWM1 - submodule 3 - configuration
//-----------------------------------------------------------------------------
#define FLEXPWM1_INIT_3      0x0000U      //!< FlexPWM1 - INIT\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.2
#define FLEXPWM1_CTRL2_3     0x0000U      //!< FlexPWM1 - CTRL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.3
#define FLEXPWM1_CTRL1_3     0x0000U      //!< FlexPWM1 - CTRL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.4
#define FLEXPWM1_VAL0_3      0x0000U      //!< FlexPWM1 - VAL0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.5
#define FLEXPWM1_VAL1_3      0x0000U      //!< FlexPWM1 - VAL1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.6
#define FLEXPWM1_VAL2_3      0x0000U      //!< FlexPWM1 - VAL2\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.7
#define FLEXPWM1_VAL3_3      0x0000U      //!< FlexPWM1 - VAL3\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.8
#define FLEXPWM1_VAL4_3      0x0000U      //!< FlexPWM1 - VAL4\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.9
#define FLEXPWM1_VAL5_3      0x0000U      //!< FlexPWM1 - VAL5\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.10
#define FLEXPWM1_OCTRL_3     0x0000U      //!< FlexPWM1 - OCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.11
#define FLEXPWM1_INTEN_3     0x0000U      //!< FlexPWM1 - INTEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.13
#define FLEXPWM1_DMAEN_3     0x0000U      //!< FlexPWM1 - DMAEN\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.14
#define FLEXPWM1_TCTRL_3     0x0000U      //!< FlexPWM1 - TCTRL\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.15
#define FLEXPWM1_DISMAP_3    0x0000U      //!< FlexPWM1 - DISMAP\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.16
#define FLEXPWM1_DTCNT0_3    0x0000U      //!< FlexPWM1 - DTCNT0\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM1_DTCNT1_3    0x0000U      //!< FlexPWM1 - DTCNT1\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.17
#define FLEXPWM1_CAPTCTRLX_3 0x0000U      //!< FlexPWM1 - CAPTCTRLX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.18
#define FLEXPWM1_CAPTCMPX_3  0x0000U      //!< FlexPWM1 - CAPTCMPX\n user defined in file flexpwm.xlsx, MPC5643LRM 25.4.3.19

/*******************************************************************************
* Functions prototypes
*******************************************************************************/


/***************************************************************************//*!
@brief         Configure the 2 FlexPWM

@return  	   void

@details       This function configures the 3 FlexPWM, all submodules and
			   configure also the output PAD for PWM signals if used.
			   
			   This module configures the 3 times, their outputs pad. But
			   after this function the output signals are masked to avoid
			   to start to commute the output without control algorithm running.

@warning	   Miss of register lock against writing
******************************************************************************/
void flexpwm_init();


/***************************************************************************//*!
@brief         Start the 2 FlexPWM generation on output pads

@return  		int32_t\n
				0 if no problem detected
				-1 if number of modNum is invalid

@param		   modNum		number of module to turn on (only affect the pads,
							not the timers). 0 for FlexPWM0 and
							1 for FlexPWM1. If the value is 255, all FlexPWM
							modules are turned on. Other values put the MCU
							in a safe mode.

@details       This function removes the mask on pads where a PWM signal is
			   generated.
******************************************************************************/
int32_t flexpwm_run(Flexpwm_module module);


/***************************************************************************//*!
@brief         Stop the 2 FlexPWM generation on output pads

@return  		int32_t\n
				0 if no problem detected
				-1 if number of modNum is invalid

@param		   module		number of module to turn on (only affect the pads,
							not the timers). 0 for FlexPWM0 and
							1 for FlexPWM1.

@details       This function puts the mask on pads where a PWM signal is
			   generated so the output.
******************************************************************************/
int32_t flexpwm_stop(Flexpwm_module module);


/***************************************************************************//*!
@brief         Change duty cycle of each channels of 1 FlexPWM module

@return  		int32_t\n
				0 if no problem detected
				-1 if number of modNum is invalid

@param		   module		number of module to turn on (only affect the pads,
							not the timers). 0 for FlexPWM0 and
							1 for FlexPWM1. FLEXPWM_ALL is not applicable for
							this function and will return and error
@param		   dutyCycle	list of duty cycle for the given FlexPWM module. The
							value must be between FLEXPWM_DUTY_RANGE and -FLEXPWM_DUTY_RANGE

@details       This function changes the duty cycle of the 4 PWM submodules.
			   For performance issues, it only updates the PWM23 registers, not
			   the PWM45.
******************************************************************************/
int32_t flexpwm_change_dutyCycle(Flexpwm_module module, const Flexpwm_dutyCycle_s *dutyCycle);


#endif /* MPC5643L_FLEXPWM_H_ */
