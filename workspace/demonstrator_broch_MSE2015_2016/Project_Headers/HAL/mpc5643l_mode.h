/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_mode.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Oct 03
* 
* @brief    Hardware abstraction layer to handle the SoC modes.
* 
* @copyright (C) Nicolas Broch
*
*
* This library is the hardware abstraction layer to handle the MC_ME which is
* in charge of the different SoC modes
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
*
* The function mode_init() must be called during the initialisation. This function
* initialise the SoC mode, the clock generation and the SIUL driver.
* 
* Then, the function mode_switch_low_power() can be called to turn off the processors
* and safe energy. 
* 
* The function mode_switch_SAFE() can be called when an issue has occurred and the
* SoC has to be set into a safe state.
* 
* The function mode_IRQ_invalid_mode_int() and mode_IRQ_invalid_mode_conf() are called
* through interrupt if an error occurs during mode switching. They switch the SoC
* into SAFE mode as reaction.
* 
* The function mode_IRQ_safe_mode() is called by an interrupt when the SoC switch
* to SAFE mode. It implements the SoC reaction.
* 
* The functions mode_SWTEST_REGCRC() must be called one per FTTI.
*
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
* 
*/

#ifndef MPC5643L_MODE_H_
#define MPC5643L_MODE_H_

#include "HAL/mpc5643l_clock.h"
#include "HAL/mpc5643l_pit.h"
#include "HAL/mpc5643l_ecsm.h"
#include "HAL/mpc5643l_siul.h"
#include "HAL/mpc5643l_siul_types.h"

/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define MODE_KEY_CHANGE_1  0x5AF0UL       //!< First key to write into ME_MCTL to change mode, MPC5643LRM 32.3.2.2
#define MODE_KEY_CHANGE_2  0xA50FUL       //!< Second key to write into ME_MCTL to change mode, MPC5643LRM 32.3.2.2

#define MODE_CLEAR_ALL_INT 0x0000001FUL   //!< ME_IS clear all interrupts, MPC5643LRM 32.3.2.4
#define MODE_CLEAR_INT_MODCONF 0x18UL     //!< ME_IS clear interrupts I_ICONF_CU and I_ICONF, MPC5643LRM 32.3.2.4
#define MODE_CLEAR_INT_MOD 0x04UL         //!< ME_IS clear interrupts I_IMODE, MPC5643LRM 32.3.2.4

#define MODE_SAFE_SWITCH   0x20000000UL   //!< Value to write into ME_MCTL to switch to SAFE mode, MPC5643LRM 32.3.2.2
#define MODE_DRUN_SWITCH   0x30000000UL   //!< Value to write into ME_MCTL to switch to DRUN mode, MPC5643LRM 32.3.2.2
#define MODE_RUN0_SWITCH   0x40000000UL   //!< Value to write into ME_MCTL to switch to RUN0 mode, MPC5643LRM 32.3.2.2
#define MODE_HALT0_SWITCH  0x80000000UL   //!< Value to write into ME_MCTL to switch to HALT0 mode, MPC5643LRM 32.3.2.2
#define MODE_RESET_SWITCH  0x00000000UL   //!< Value to write into ME_MCTL to switch to do an hard reset, MPC5643LRM 32.3.2.2

#define MODE_ME_GS_RUN0_EXP  0x441F0074UL //!< Value expected into ME_GS when SoC is in RUN0 mode, MPC5643LRM 32.3.2.1
#define MODE_ME_GS_S_MTRANS  0x08000000UL //!< Mask to read S_MTRANS of ME_GS and known when mode transition is finished, MPC5643LRM 32.3.2.1
#define MODE_ME_GS_MODEMASK  0xF0000000UL //!< Mask to read the SoC mode in ME_GS, MPC5643LRM 32.3.2.1
#define MODE_ME_GS_SAFE      0x20000000UL //!< Value of ME_GS if the system is really in SAFE mode, MPC5643LRM 32.3.2.1

#define MODE_ME_ME_HALT0     0x00000100UL //!< Mask to allow the SoC to switch in HALT0 mode, MPC5643LRM 32.3.2.3

#define MODE_TIMEOUT_TRANS   8000         //!< Timeout to wait until the mode transition is finished, in reading iteration, MPC5643LRM 32.4.7


#define MODE_ME_PSC0_RUN0    0x00000000UL /*!< @brief Value expected in ME_PS0 when in RUN0 mode, MPC5643LRM 32.3.2.15
										   	   @warning The DSPI flag are never set! Problem to fix! */
#define MODE_ME_PSC1_RUN0    0x440107CBUL //!< Value expected in ME_PS1 when in RUN0 mode, MPC5643LRM 32.3.2.16
#define MODE_ME_PSC2_RUN0    0x10000000UL //!< Value expected in ME_PS2 when in RUN0 mode, MPC5643LRM 32.3.2.17



//-----------------------------------------------------------------------------
// Global parameters
//-----------------------------------------------------------------------------
#define MODE_ME 0x0000811DUL //!< ME_ME\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.3
#define MODE_IM 0x0000001EUL //!< ME_IM\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.5


//-----------------------------------------------------------------------------
// Mode configuration
//-----------------------------------------------------------------------------
#define MODE_TEST_MC  0x001F0010UL //!< ME_TEST_MC\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.9
#define MODE_SAFE_MC  0x001F0010UL //!< ME_SAFE_MC\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.10
#define MODE_DRUN_MC  0x001F0010UL //!< ME_DRUN_MC\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.11
#define MODE_RUN0_MC  0x001F0074UL //!< ME_RUN0_MC\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.12
#define MODE_RUN1_MC  0x001F0010UL //!< ME_RUN1_MC\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.12
#define MODE_RUN2_MC  0x001F0010UL //!< ME_RUN2_MC\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.12
#define MODE_RUN3_MC  0x001F0010UL //!< ME_RUN3_MC\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.12
#define MODE_HALT_MC  0x001A0074UL //!< ME_HALT0_MC\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.13
#define MODE_STOP_MC  0x00150010UL //!< ME_STOP0_MC\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.14


//-----------------------------------------------------------------------------
// RUN peripheral configurations
//-----------------------------------------------------------------------------
#define MODE_RUN_PC0 0x00000014UL //!< ME_RUN_PC0\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.18
#define MODE_RUN_PC1 0x00000000UL //!< ME_RUN_PC1\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.18
#define MODE_RUN_PC2 0x00000010UL //!< ME_RUN_PC2\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.18
#define MODE_RUN_PC3 0x00000000UL //!< ME_RUN_PC3\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.18
#define MODE_RUN_PC4 0x00000000UL //!< ME_RUN_PC4\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.18
#define MODE_RUN_PC5 0x00000000UL //!< ME_RUN_PC5\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.18
#define MODE_RUN_PC6 0x00000000UL //!< ME_RUN_PC6\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.18
#define MODE_RUN_PC7 0x00000000UL //!< ME_RUN_PC7\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.18

#define MODE_LP_PC0 0x00000100UL //!< ME_LP_PC0\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.19
#define MODE_LP_PC1 0x00000000UL //!< ME_LP_PC1\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.19
#define MODE_LP_PC2 0x00000000UL //!< ME_LP_PC2\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.19
#define MODE_LP_PC3 0x00000000UL //!< ME_LP_PC3\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.19
#define MODE_LP_PC4 0x00000000UL //!< ME_LP_PC4\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.19
#define MODE_LP_PC5 0x00000000UL //!< ME_LP_PC5\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.19
#define MODE_LP_PC6 0x00000000UL //!< ME_LP_PC6\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.19
#define MODE_LP_PC7 0x00000000UL //!< ME_LP_PC7\n user defined in file mode.xlsx, MPC5643LRM 32.3.2.19


//-----------------------------------------------------------------------------
// Peripheral mode configuration
//-----------------------------------------------------------------------------
#define MODE_PCTL4  0x02 //!< ME_PCTL4 - DSPI0 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL5  0x02 //!< ME_PCTL5 - DSPI1 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL6  0x49 //!< ME_PCTL6 - DSPI2 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL16 0x49 //!< ME_PCTL16 - FlexCAN0 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL17 0x49 //!< ME_PCTL17 - FlexCAN1 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL24 0x49 //!< ME_PCTL24 - FlexRay Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL32 0x02 //!< ME_PCTL32 - ADC0 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL33 0x02 //!< ME_PCTL33 - ADC1 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL35 0x02 //!< ME_PCTL35 - CTU Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL38 0x02 //!< ME_PCTL38 - eTimer0 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL39 0x02 //!< ME_PCTL39 - eTimer1 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL40 0x02 //!< ME_PCTL40 - eTimer2 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL41 0x02 //!< ME_PCTL41 - FlexPWM0 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL42 0x02 //!< ME_PCTL42 - FlexPWM1 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL48 0x02 //!< ME_PCTL48 - LIN_FLEX0 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL49 0x49 //!< ME_PCTL49 - LIN_FLEX1 Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL58 0x02 //!< ME_PCTL58 - CRC Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL62 0x02 //!< ME_PCTL62 - SWG Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20
#define MODE_PCTL92 0x02 //!< ME_PCTL92 - PIT Control \n user defined in file mode.xlsx, MPC5643LRM 32.2.3.2.20



/***************************************************************************//*!
@brief          Initialisation of MC_ME to handle SoC mode. 

@return  		void
								

@details        The function run in DRUN mode, configure the different modes, 
				initialise the clock generation and then switch to RUN0 mode.
				Before calling this function, the system must intialise the 
				SAFE mode with the function mode_init_safeMode().

@warning		Miss of register lock\n
******************************************************************************/
void mode_init();


/***************************************************************************//*!
@brief          Initialisation of MC_ME to handle the SAFE SoC mode. 

@return  		void

@param			led_safe		PAD where is located the LED (or something else)
								to communicate to outside world that the SoC is
								in SAFE mode.
								
@param			led_safe_state	Boolean value to set the LED in SAFE mode state.

@details        This function must be called before mode_init() to initialise
				the SAFE mode.  If the system has to be switch to SAFE mode,
				the function mode_switch_SAFE() has to be called after this
				function.

@warning		Miss of register lock\n
******************************************************************************/
void mode_init_safeMode(const Siul_pcr_e led_safe, const uint8_t led_safe_state);


/***************************************************************************//*!
@brief          Put the SoC in low power mode.

@return  		int32_t\n
				0 if no problem detected
				-1 if it was not possible to switch into low power mode

@details        The function switch the SoC in HALT0 mode to shutdown the 
 	 	 	 	processor. The mode of peripherals and the configuration
 	 	 	 	of clocks can be set up through the function mode_init(). 
 	 	 	 	The SoC goes out of this mode when an interrupt occurs.\n
 	 	 	 	
 	 	 	 	If the function wasn't able to switch into low power mode,
 	 	 	 	it does a request to go into RUN0 mode and return -1. In this
 	 	 	 	case, the user has to decide how to handle the error.
 	 	 	 	
@remark	 	 	For debug purpose, in low power mode, the JTAG probe cannot 
				control the processor.
******************************************************************************/
int32_t mode_switch_low_power();



/***************************************************************************//*!
@brief          Go in SAFE mode following a recoverable error.

@return  		void

@details        This function is called to switch the SoC mode in SAFE mode to
				have a SAFE system when an error is detected.\n
				Before calling this function, the GPIO should be in a safe mode
				or configured to switch to a safe mode when the SoC switches
				in SAFE mode.
				
@warning		This function can be only called before the start of system
				safety mechanisms after call of mode_init_safeMode(). 
				Otherwise, using this function can create a RESET of the system.\n

******************************************************************************/
void mode_switch_SAFE();


/***************************************************************************//*!
@brief          SW ERROR: reset the SoC to turn the system in a SAFE mode.

@return  		void

@details        If an recoverable fault is detected by SW, this function has
				to be used to switch into the SAFE SoC mode. This function can
				be called anytime.
******************************************************************************/
void mode_sw_error();



/***************************************************************************//*!
@brief          Interrupt vector for Safe Mode Interrupt

@return  		void

@details        Function executed when the interrupt "safe mode interrupt" 
				occurs. Signal to user that the SoC is in SAFE mode, all hw
				reactions configured to SAFE mode are set and the SoC turn
				on a LED chosen when function mode_init_safeMode() is called.
******************************************************************************/
void mode_reac_safe_mode();


/***************************************************************************//*!
@brief          Compute the CRC for the register of this library

@return  		void

@details        This function computes the CRC of register and compare it
				with expected value.
                
@warning		Not implemented yet
******************************************************************************/
void mode_SWTEST_REGCRC();


/*******************************************************************************
* Interrupt functions
*******************************************************************************/
/***************************************************************************//*!
@brief          Interrupt vector for invalid transition

@return  		void

@details        Function executed when an invalid transition is asked. Switch
				to SAFE mode.
******************************************************************************/
void mode_IRQ_invalid_mode_int();


/***************************************************************************//*!
@brief          Interrupt vector for invalid mode configuration

@return  		void

@details        Function executed when an invalid mode configuration is set. 
				Switch to SAFE mode.
******************************************************************************/
void mode_IRQ_invalid_mode_conf();




#endif /* MPC5643L_MODE_H_ */
