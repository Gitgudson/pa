/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_uart.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Nov 26
* 
* @brief    Hardware abstraction layer to configure a FLEXLinD module for UART
* 			communication.
* 
* @copyright (C) Nicolas Broch
* 
*
*
* This library is the hardware abstraction layer for uart communication. It 
* configures FLEXLinD 0 or/and 1 to communicate in UART mode and allows through
* functions to send and receive messages.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
* How to use it
* -------------
* 
* The function uart_init() configures a FlexLinD peripheral to communicate
* with UART. The interrupts are not used.
* 
* As UART cannot be used for real-time task. The function uart_init() has a protection
* to avoid to be blocked in it. Otherwise, functions have no protections, so adding a
* watchdog would be useful.
* 
* When a bit is received, it is stocked into a circular buffer. No security has
* been added to avoid buffer overflow. So the protocol used should be a 
* send-receive protocol or the buffer must be emptied fast enough to avoid
* a buffer overflow with loss of data.
* 
* 
* The UART communication has the following parameters:
* 
* |Parameter        | Value   
* |:----------------|-----------------------------
* | Baud rate       | 115200 Bauds
* | Parity          | even
* | Frame size      | 8 bits + parity
* | Order transmit  | LSB first
* | Stop bis		| 1 stop bit
* 
*
* Pad used
* --------
* 
* |Pad       | Function                  | When is it configure?
* |:---------|:--------------------------|:------------------------------------------------------
* | B[2]     | UART0 - Tx                | If uart_init() is called with parameter UART_FLEXLIND0
* | B[3]	 | UART0 - Rx				 | If uart_init() is called with parameter UART_FLEXLIND0
* | F[14]    | UART1 - Tx				 | If uart_init() is called with parameter UART_FLEXLIND1
* | F[15]	 | UART1 - Rx				 | If uart_init() is called with parameter UART_FLEXLIND1
* 
*/

#ifndef MPC5643L_UART_H_
#define MPC5643L_UART_H_

#include "MPC5643L.h"
#include <typedefs.h>

#include "HAL/mpc5643l_uart_type.h"
#include "HAL/mpc5643l_mode.h"
#include "HAL/mpc5643l_siul.h"

#include "IntcInterrupts.h"


/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define UART_SLEEP_BIT 0x00000002UL //!< Mask for LINCR1 to put the FLEXLinD in sleep mode, MPC5643LRM 30.10.1
#define UART_INIT_BIT  0x00000001UL //!< Mask for LINCR1 to put the FLEXLinD in init mode, MPC5643LRM 30.10.1

#define UART_ERR_CLEAR 0x0000FFAFUL //!< Value to write in UARTSR to clear errors, MPC5643LRM 30.10.6

#define UART_SR_MOD    0x0000F000UL //!< Mask to read mode of module in LINSR, MPC5643LRM 30.10.3
#define UART_INIT_SR   0x00001000UL //!< Value to check in LINSR if FLEXLinD is in init mode, MPC5643LRM 30.10.3

#define UART_TX_END    0x00000002UL //!< Mask to check and clear in UARTSR if the data have been sent, MPC5643LRM 30.10.6
#define UART_RX_END    0x00000004UL //!< Mask to check and clear in UARTSR if the data have been received, MPC5643LRM 30.10.6

#define UART_TIMEOUT_MOD  2000      //!< Timeout in iteration of reading of the mode changing of UART module
#define UART_TIMEOUT_SENT 10000     //!< Timeout in nteration to send data on UART

//-----------------------------------------------------------------------------
// UART Configuration
//-----------------------------------------------------------------------------
#define UART_LINCR1   0x00000000UL  //!< LINCR1\n user defined in file uart_.xlsx, MPC5643LRM 30.10.1
#define UART_LINIER   0x00000004UL  //!< LINIER\n user defined in file uart_.xlsx, MPC5643LRM 30.10.2
#define UART_UARTCR   0x00000037UL  //!< UARTCR\n user defined in file uart_.xlsx, MPC5643LRM 30.10.5
#define UART_UARTCR_U 0x00000001UL  //!< UARTCR to put device in UART mode\n user defined in file uart_.xlsx, MPC5643LRM 30.10.5
#define UART_LINFBRR  0x00000005UL  //!< LINFBRR\n user defined in file uart_.xlsx, MPC5643LRM 30.10.10
#define UART_LINIBRR  0x0000001DUL  //!< LINIBRR\n user defined in file uart_.xlsx, MPC5643LRM 30.10.11
#define UART_GCR      0x00000000UL  //!< GCR\n user defined in file uart_.xlsx, MPC5643LRM 30.10.21
#define UART_UARTPTO  0x00000FFFUL  //!< UARTPTO\n user defined in file uart_.xlsx, MPC5643LRM 30.10.22



/*******************************************************************************
* Functions prototypes
*******************************************************************************/

/***************************************************************************//*!
@brief          Configure a FlexLinD into UART

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If there is an error in function parameters
			   -2 If an error occured during execution of the function
			   
@param			uart		The ID of which FlexLinD has to be configured for
							uart communication.

@details        This function must be called before using the other functions of
				this library
******************************************************************************/
int32_t uart_init(Uart_flexlin linModule);


/***************************************************************************//*!
@brief         Send data with UART

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If an error occured during execution of the function
			   
@param			uart		The ID of which FlexLinD has to be configured for
							uart communication.
@param			pData		Data to send
@param			size		size of data to send in bytes

@details        This function send data from pData through UART.
******************************************************************************/
int32_t uart_transmit(Uart_flexlin linModule, uint8_t *pData, uint32_t size);



/***************************************************************************//*!
@brief         Read 1 byte of data data with UART

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If an error occured during execution of the function
			   
@param			uart		The ID of which FlexLinD has to be configured for
							uart communication.
@param			pData		Data to be received
@param			size		size of data to be received in bytes

@details        This function receives data from pData through UART. As it is
				supposed that this function is watched by a watchdog, no internal
				protection is included to avoid beeing stuck into it.
******************************************************************************/
int32_t uart_receive(Uart_flexlin linModule, uint8_t *pData, uint32_t size);


/***************************************************************************//*!
@brief         Give size of received data

@return  	   uint32_t\n
			   Number of bytes received. If return is under 0, an error has
			   Occurred.
			   
@param			uart		The ID of which FlexLinD has to be configured for
							uart communication.

@details        This function returns the size, in bytes of received datas which
				have not been read.
******************************************************************************/
int32_t uart_size_receive(Uart_flexlin linModule);


/***************************************************************************//*!
@brief          Interrupt for FlexLinD0 errors

@return  	    void

@details        This function is called when errors in FlexLinD0 occur
******************************************************************************/
void uart_IRQ_uart0_error();

/***************************************************************************//*!
@brief          Interrupt for FlexLinD0 data reception

@return  	    void

@details        This function is called when UART0 receives data. The data is
				store into a buffer.
******************************************************************************/
void uart_IRQ_uart0_rx();

/***************************************************************************//*!
@brief          Interrupt for FlexLinD0 data transmission complete

@return  	    void

@details        This function is called when UART0 has sent datas
******************************************************************************/
void uart_IRQ_uart0_tx();

/***************************************************************************//*!
@brief          Interrupt for FlexLinD1 errors

@return  	    void

@details        This function is called when errors in FlexLinD1 occur
******************************************************************************/
void uart_IRQ_uart1_error();

/***************************************************************************//*!
@brief          Interrupt for FlexLinD1 data reception

@return  	    void

@details        This function is called when UART0 receives data. The data is
				store into a buffer.
******************************************************************************/
void uart_IRQ_uart1_rx();

/***************************************************************************//*!
@brief          Interrupt for FlexLinD1 data transmission complete

@return  	    void

@details        This function is called when UART1 has sent datas
******************************************************************************/
void uart_IRQ_uart1_tx();


#endif /* MPC5643L_UART_H_ */
