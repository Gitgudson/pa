/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_hal.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Nov 12
* 
* @brief    General functions of HAL for MPC5643L
* 
* @copyright (C) Nicolas Broch
*
*
* This library regroups all the general functions for the HAL of MPC5643L
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
* How to use it
* -------------
* 
* The function hal_init() must be called at startup of the microcontroller.
* 
*
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
*/


#include "MPC5643L.h"
#include <typedefs.h> 

#include "HAL/mpc5643l_fccu.h"
#include "HAL/mpc5643l_mode.h"
#include "HAL/mpc5643l_power.h"
#include "HAL/mpc5643l_reset.h"
#include "HAL/mpc5643l_siul.h"
#include "HAL/mpc5643l_wakeup.h"
#include "HAL/mpc5643l_sysstatus.h"
#include "HAL/mpc5643l_interrupts.h"



/***************************************************************************//*!
@brief          Initialisation of the microcontroller

@param		  	led_safe_mode		ID of the LEDs to indicate that the system
									is in safe mode

@return  		void

@details        The function initialises the basic functions of thmicrocontroller. 
				It doesn't initialise the peripheral which are not necessary to
				run the minimal system. If the function
				returns, the microcontroller is in a safe state with all
				the safety mechanisms started.

******************************************************************************/
void hal_init_sys(Siul_pcr_e led_safe_mode);
