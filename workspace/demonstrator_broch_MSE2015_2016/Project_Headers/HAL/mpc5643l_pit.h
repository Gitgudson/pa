/******************************************************************************
*
* Master semester project of Jan Huber
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_pit.h
*
* @author   Jan Huber
* 
* @version  0.1
* 
* @date     2019 Apr 16
* 
* @brief    Hardware abstraction layer for the system tick generation
* 
* @copyright (C) Jan Huber
*
*
* This library is the hardware abstraction layer for the periodic timer interrupt (PIT).
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* The function pit_init() must be called after initialising the clock.
* 
*/


#ifndef PIT_H_
#define PIT_H_

#include "MPC5643L.h"
#include <typedefs.h> 


/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// PIT_0
//-----------------------------------------------------------------------------
#define PIT_MCR_MDIS		0x00000002UL //!< bit MDIS of PIT_MCR register, MPC5643LRM 36.3.2.1
#define PIT_MCR_FRZ			0x00000001UL //!< bit FRZ of PIT_MCR register, MPC5643LRM 36.3.2.1
#define PIT_TCTRL_TIE		0x00000002UL //!< bit TIE of PIT_MCR register, MPC5643LRM 36.3.2.4
#define PIT_TCTRL_TEN		0x00000001UL //!< bit TEN of PIT_MCR register, MPC5643LRM 36.3.2.4
#define PIT_TFLG_TIF		0x00000001UL //!< bit TIE of PIT_MCR register, MPC5643LRM 36.3.2.5
// 1 ms
#define PIT_LDVAL0			0x0000EA5FUL //!< PIT_0 cycle count down value, MPC5643LRM 36.3.2.2

#define PIT_IRQ_SOURCE		59		//!< PIT_0 interrupt vector number, MPC5643LRM 28.7

//-----------------------------------------------------------------------------
// Declaration of external variable for system timer
//-----------------------------------------------------------------------------
extern uint8_t systickFlag;


/***************************************************************************//*!
@brief          Initialisation of the PIT module.

@return  		void

@details        The function initialises the periodic interrupt timer (PIT)     

@warning		Miss of register lock
******************************************************************************/
void pit_init();


/***************************************************************************//*!
@brief          Interrupt handler of the PIT

@return  		void

@details        When the PIT has reached zero an interrupt is generated and this
 	 	 	 	function is called. This function resets the interrupt, sets the
 	 	 	 	systick flag and restarts the PIT.

@warning		Miss of register lock
******************************************************************************/
void pit_IRQ_SYSTICK();


/***************************************************************************//*!
@brief          Check the registers of pit_ by calculating a CRC of them.

@return  		int32_t\n
				0 if no problem detected
				-1 if the CRC is not the result expected

@details        This function computes the CRC of register and compare it
				with expected value.
                
@warning		Not implemented yet
******************************************************************************/
int32_t pit_SWTEST_REGCRC();


#endif /* PIT_H_ */
