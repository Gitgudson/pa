/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_hal_type.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Nov 01
* 
* @brief    General type definition for HAL
* 
* @copyright (C) Nicolas Broch
*
*
* This file defines the types used for configure or handle HAL drivers
* 
* This file is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
*
*/

#ifndef HAL_TYPE_H_
#define HAL_TYPE_H_


/***************************************************************************//*!
@brief          Enumeration for yes/no parameters
******************************************************************************/
typedef enum{
	HAL_NO  = 0,   //!< Answer no to a SIUL parameter
	HAL_YES = 1    //!< Answer yes to a SIUL parameter
}Hal_YN_e;

#endif /* HAL_TYPE_H_ */
