/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_reset.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Nov 12
* 
* @brief    Hardware abstraction layer for the reset module MC_RGM
* 
* @copyright (C) Nicolas Broch
*
*
* This library is the hardware abstraction layer for the reset MC_RGM.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* The function  reset_init() must be called during startup to initialise
* the reset module. If it is desired to know the reset source, the function
* reset_read_reset() must be called and then, the reset sources are
* cleared with the function reset_clear_reset().
* 
* 
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
*/
#ifndef MPC5643L_RESET_H_
#define MPC5643L_RESET_H_

#include <typedefs.h> 

/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define RESET_MASK_POR   0x8000U //!< Mask for RGM_DES to read if the reset was caused by a power on reset,  MPC5643LRM 41.3.1.2
#define RESET_MASK_JTAG  0x0001U //!< Mask for RGM_FES to read if the reset was called by JTAG,  MPC5643LRM 41.3.1.1
#define RESET_MASK_SWRES 0x0004U //!< Mask for RGM_FES to read if the reset was called by SW,  MPC5643LRM 41.3.1.1
#define RESET_MASK_ST    0x1000U //!< Mask for RGM_FES to read if the Self-test was completed,  MPC5643LRM 41.3.1.1



//-----------------------------------------------------------------------------
// Control Registers
//-----------------------------------------------------------------------------
#define RESET_RGM_FERD 0x0080U  //!< RGM_FERD configuration\n user defined in file reset_.xlsx, MPC5643LRM 41.3.1.3
#define RESET_RGM_FEAR 0x0000U  //!< RGM_FEAR configuration\n user defined in file reset_.xlsx, MPC5643LRM 41.3.1.5
#define RESET_RGM_FESS 0x2000U  //!< RGM_FESS configuration\n user defined in file reset_.xlsx, MPC5643LRM 41.3.1.6
#define RESET_RGM_FBRE 0x6F7FU  //!< RGM_FBRE configuration\n user defined in file reset_.xlsx, MPC5643LRM 41.3.1.7


/***************************************************************************//*!
@brief         Initialisation the RESET module MC_RGM.

@return  	   void

@details       This function configure the RESET module MC_RGM regarding
   	   	   	   the behaviour in case of Critical fault detected in the system.

@warning		Lack of lock of registers
******************************************************************************/
void reset_init();


/***************************************************************************//*!
@brief         Read the reason of last reset

@return  	   int32_t\n
			   0  if Power On Reset and Self-test completed
			   -1 if SW reset
			   -2 if following an FCCU error

@details       This function reads the reason of last reset and return a
			   simplified value to define it. Must be called before reset_init().
			   It is a destructive reading. Calling this function acknowledge
			   all the reset causes.

@warning	   Lack of lock of registers
******************************************************************************/
int32_t reset_read_reset();


/***************************************************************************//*!
@brief         Clear the reset event

@return  	   void

@details       This function clears the reset source. Must be called before
			   switching in DRUN or RUN0 mode.
******************************************************************************/
void reset_clear_reset();


#endif /* MPC5643L_RESET_H_ */
