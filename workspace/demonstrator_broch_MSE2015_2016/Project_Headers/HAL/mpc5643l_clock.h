/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_clock.h
*
* @author   Nicolas Broch
* @author	Jan Huber
* 
* @version  0.3
* 
* @date     2019 Apr 16
* 
* @brief    Hardware abstraction layer for the clock generation
* 
* @copyright (C) Nicolas Broch
*
*
* This library is the hardware abstraction layer for the clock generation module (CGM), 
* including the PLL, and clock monitor unit (CMU).
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* The function clock_init() must be called after initialising the FCCU, but
* before initialising the peripheral devices of the microcontroller.
*
* The function clock_init_CMU() must be called after 'clock_init()' and once
* the system CLK is set to the FMPLL0 and the SoC mode is "RUN0". Otherwise,
* the CMUs will generate an error because the right clocks or PLL will not be
* running before starting CMUs.
* 
* The functions clock_freq_meter_start() and clock_freq_meter_check() must  
* be called one per FTTI to measure the frequency of IRCOSC. The first function 
* starts the measure. The measure takes a long time (in microsecond, defined 
* by CLOCK_CMU_MDR). The function clock_freq_meter_check() waits until end of 
* the measure and then check if the frequency measured if really close to 
* 16 [MHz].
*
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
*
*/

#ifndef CLOCK_H_
#define CLOCK_H_

#include "MPC5643L.h"
#include <typedefs.h> 


/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define CLOCK_FM_FIRCOS_MIN 758	//!< Parameter F_min of IRCOSC\n user defined in file clock_.xlsx, MPC5643LRM 12.4.1.4
#define CLOCK_FM_FIRCOS_MAX 847	//!< Parameter F_max of IRCOSC\n user defined in file clock_.xlsx, MPC5643LRM 12.4.1.4

//-----------------------------------------------------------------------------
// Frequency meter
//-----------------------------------------------------------------------------
#define CLOCK_CMU_MDR       0x00000640UL //!< CMU_MDR - frequency meter test duration, MPC5643LRM 12.3.6
#define CLOCK_CMU_CSR_SFM   0x00800000UL //!< bit SFM of CMU_CSR register, MPC5643LRM 12.3.1

//-----------------------------------------------------------------------------
// XOSC - Frequency of the external oscillator and configuration register
//-----------------------------------------------------------------------------
#define CLOCK_OSC_CTL       0x00640000UL //!< OSC_CTL\n user defined in file clock_.xlsx, MPC5643LRM 35.2.3

//-----------------------------------------------------------------------------
// System clock - clock source is defined by MC_ME
//-----------------------------------------------------------------------------
#define CLOCK_SC_DC0        0x81U        //!< CGM_SC_DC0\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.4

//-----------------------------------------------------------------------------
// Output clock
//-----------------------------------------------------------------------------
#define CLOCK_OC_EN        0x00000000UL //!< CGM_OC_EN\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.1
#define CLOCK_OC_SC        0x00U        //!< CGM_OCDS_SC\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.2

//-----------------------------------------------------------------------------
// Motor control and SWG clock
//-----------------------------------------------------------------------------
#define CLOCK_MCSWG_SC    0x04000000UL  //!< CGM_AC0_SC\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.5
#define CLOCK_MC_DC       0x80U         //!< CGM_AC0_DC0\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.6
#define CLOCK_SWG_DC      0x85U         //!< CGM_AC0_DC1\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.6

//-----------------------------------------------------------------------------
// FlexRay clock
//-----------------------------------------------------------------------------
#define CLOCK_FR_SC      0x04000000U   //!< CGM_A1_SC\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.7
#define CLOCK_FR_DC0     0x00U         //!< CGM_AC1_DC0\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.8

//-----------------------------------------------------------------------------
// FlexCAN clock
//-----------------------------------------------------------------------------
#define CLOCK_CAN_SC    0x04000000UL  //!< CGM_AC2_SC\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.9
#define CLOCK_CAN_DC0   0x00U         //!< CGM_AC2_DC0\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.10

//-----------------------------------------------------------------------------
// PLL0
//-----------------------------------------------------------------------------
#define CLOCK_PLL0_CR       0x013C0100UL //!< PLL0_CR\n user defined in file clock_.xlsx, MPC5643LRM 27.5.1\n - For Motor-board : 0x013C0100\n - For USB-Board : 0x0D300100 
#define CLOCK_PLL0_MR       0x00000000UL //!< PLL0_MR\n user defined in file clock_.xlsx, MPC5643LRM 27.5.2
#define CLOCK_PLL0_CGM_AUX3 0x01000000UL //!< CGM_AC3_SC\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.11


//-----------------------------------------------------------------------------
// PLL1
//-----------------------------------------------------------------------------
#define CLOCK_PLL1_CR       0x05500000UL //!< PLL1_CR\n user defined in file clock_.xlsx, MPC5643LRM 27.5.1\n - For Motor-board : 0x05500000\n - For USB-Board : 0x1D400000
#define CLOCK_PLL1_MR       0x00000000UL //!< PLL1_MR\n user defined in file clock_.xlsx, MPC5643LRM 27.5.2
#define CLOCK_PLL1_CGM_AUX4 0x01000000UL //!< CGM_AC4_SC\n user defined in file clock_.xlsx, MPC5643LRM 11.3.1.12


//-----------------------------------------------------------------------------
// CMU_0
//-----------------------------------------------------------------------------
#define CLOCK_CMU0_CSR      0x00000005UL //!< CMU_CSR\n user defined in file clock_.xlsx, MPC5643LRM 12.3.1
#define CLOCK_CMU0_HFREFR   0x000001F4UL //!< CMU_HFREFR_A\n user defined in file clock_.xlsx, MPC5643LRM 12.3.3
#define CLOCK_CMU0_LFREFR   0x000001CCUL //!< CMU_LFREFR_A\n user defined in file clock_.xlsx, MPC5643LRM 12.3.4

//-----------------------------------------------------------------------------
// CMU_1
//-----------------------------------------------------------------------------
#define CLOCK_CMU1_CSR    0x00000001UL //!< CMU_CSR\n user defined in file clock_.xlsx, MPC5643LRM 12.3.1
#define CLOCK_CMU1_HFREFR 0x000001F4UL //!< CMU_HFREFR_A\n user defined in file clock_.xlsx, MPC5643LRM 12.3.3
#define CLOCK_CMU1_LFREFR 0x000001CCUL //!< CMU_LFREFR_A\n user defined in file clock_.xlsx, MPC5643LRM 12.3.4

//-----------------------------------------------------------------------------
// CMU_2
//-----------------------------------------------------------------------------
#define CLOCK_CMU2_CSR    0x00000000UL //!< CMU_CSR\n user defined in file clock_.xlsx, MPC5643LRM 12.3.1
#define CLOCK_CMU2_HFREFR 0x00000000UL //!< CMU_HFREFR_A\n user defined in file clock_.xlsx, MPC5643LRM 12.3.3
#define CLOCK_CMU2_LFREFR 0x00000000UL //!< CMU_LFREFR_A\n user defined in file clock_.xlsx, MPC5643LRM 12.3.4


/***************************************************************************//*!
@brief          Initialisation of clock generation. To have an effect, then
                the MC_ME must start the PLL0 and defines the system clock as
                XOSC.

@return  		void

@details        The function setups the clock generation, which includes clock
                generation module (CGM), clock monitor unit (CMU).     

@warning		Miss of register lock
******************************************************************************/
void clock_init();


/***************************************************************************//*!
@brief          Initialisation of clock generation and set up of safety 
				mechanisms to check the clocks.

@return  		void

@details        The function setup the clock generation, which includes clock
                generation module (CGM), clock monitor unit (CMU).     

@warning		Miss of register lock
******************************************************************************/
void clock_init_CMU();


/***************************************************************************//*!
@brief          Start computation of a frequency meter of the IRCOSC.

@return  		void

@details        The function starts the measurement of the frequency of IRCOSC in 
				function of the frequency of XOSC.
******************************************************************************/
void clock_freq_meter_start();


/***************************************************************************//*!
@brief          Check computation of a frequency meter of the IRCOSC.

@return  		int32_t\n
				0 if no problem detected
				-1 if the frequency of the IRCOSC is not 16 MHz

@details        Before using this function, please, call clock_freq_meter_start() to 
				start the measurement of IRCOSC.The function waits until the measurement 
				of the frequency of IRCOSC is finished. Then check the measure. 
				The valid range of frequencies which are considered as valid is set by 
				the 2 macro-parameters CLOCK_FM_FIRCOS_MIN and CLOCK_FM_FIRCOS_MAX.
******************************************************************************/
int32_t clock_freq_meter_check();


/***************************************************************************//*!
@brief          Interrupt in case for XOSC counter expired

@return  	    void

@details        This function is called when the counter of XOSC during startup
				expired. That means that the XOSC is available and stable.
******************************************************************************/
void clock_IRQ_xoscExp();


/***************************************************************************//*!
@brief          Check the registers of clock_ by calculating a CRC of them.

@return  		int32_t\n
				0 if no problem detected
				-1 if the CRC is not the result expected

@details        This function computes the CRC of register and compare it
				with expected value.
                
@warning		Not implemented yet
******************************************************************************/
int32_t clock_SWTEST_REGCRC();


#endif /* CLOCK_H_ */
