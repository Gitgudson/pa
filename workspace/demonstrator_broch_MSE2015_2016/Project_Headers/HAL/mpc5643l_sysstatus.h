/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_sysstatus.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Nov 16
* 
* @brief    Hardware abstraction layer for the system status
* 
* @copyright (C) Nicolas Broch
*
*
* This library is the hardware abstraction layer for the system status. It checks
* if the system is correct or not.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
* How to use it
* -------------
* 
* During initialisation of microcontroller, the function sysstatus_check() must
* be called to check that the SoC corresponds to the expected SoC. If this 
* function doesn't return 0, the software cannot be executed.
* 
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
*/

#ifndef MPC5643L_SYSSTATUS_H_
#define MPC5643L_SYSSTATUS_H_

/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define SYSSTATUS_STATUS_MASK    0xC2F0U     //!< Mask for STATUS to read the status of SoC, MPC5643LRM 48.3.1.1
#define SYSSTATUS_MEMCONFIG_MASK 0x003EU     //!< Mask for MEMCONFIG to read the status of SoC, MPC5643LRM 48.3.1.2

//-----------------------------------------------------------------------------
// ID of SoC
//-----------------------------------------------------------------------------
#define SYSSTATUS_MIDR1       0x56432012UL   //!< MIDR1 status\n user defined in file sysstatus_.xlsx, MPC5643LRM 47.5.2.1
#define SYSSTATUS_MIDR2       0x30004C01UL   //!< MIDR2 status\n user defined in file sysstatus_.xlsx, MPC5643LRM 47.5.2.2

#define SYSSTATUS_STATUS      0x8070U        //!< STATUS register\n user defined in file sysstatus_.xlsx, MPC5643LRM 48.3.1.1
#define SYSSTATUS_MEMCONFIG   0x0024U        //!< MEMCONFIG register\n user defined in file sysstatus_.xlsx, MPC5643LRM 48.3.1.2

#define SYSSTATUS_ERROR       0x0003U        //!< ERROR register\n user defined in file sysstatus_.xlsx, MPC5643LRM 48.3.1.3


/*******************************************************************************
* Functions
*******************************************************************************/
/***************************************************************************//*!
@brief         Test of the system status

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If the system has an error

@details       This function check the status of the system and return -1 if
			   the system doesn't have the correct status. This function must
			   be called at startup.

@warning		Lack of lock of registers
******************************************************************************/
int32_t sysstatus_check();

#endif /* MPC5643L_SYSSTATUS_H_ */
