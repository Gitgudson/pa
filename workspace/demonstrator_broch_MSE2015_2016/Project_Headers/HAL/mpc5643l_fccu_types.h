/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_fccu_types.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Nov 09
* 
* @brief    Types defined for the FCCU HAL driver
* 
* @copyright (C) Nicolas Broch
*
*
* This file defines the types used for configure or handle the FCCU
* in the HAL mpc5643l_fccu.h
* 
* This file is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
*
*/

#ifndef MPC5643L_FCCU_TYPES_H_
#define MPC5643L_FCCU_TYPES_H_

#include <typedefs.h>


/***************************************************************************//*!
@brief          Enumeration to send command to the FCCU with the function
				fccu_ctrl_command()
******************************************************************************/
typedef enum{
	FCCU_SWITCH_CONFIG = 0x00000001UL, //!< Value to write into FCCU_CTRL to switch the FCCU in CONFIG mode, MPC5643LRM 22.6.1
	FCCU_SWITCH_NORMAL = 0x00000002UL, //!< Value to write into FCCU_CTRL to switch the FCCU in NORMAL mode, MPC5643LRM 22.6.1
	FCCU_READ_STATE    = 0x00000003UL, /*!< @brief Value to write into FCCU_CTRL to read state mode into FCCU_STAT, MPC5643LRM 22.6.1
										    @warning For debug/test only, remove after */
	FCCU_LOCKCONF      = 0x00000010UL, //!< Value to write into FCCU_CTRL to lock configuration of FCCU, MPC5643LRM 22.6.1
	FCCU_READ_CF       = 0x00000009UL, //!< Value to write into FCCU_CTRL to read critical failure state, MPC5643LRM 22.6.1
	FCCU_READ_NCF      = 0x0000000AUL  //!< Value to write into FCCU_CTRL to read non-critical failure state, MPC5643LRM 22.6.1
}Fccu_ctrl_e;


/***************************************************************************//*!
@brief          Structure of errors datas
******************************************************************************/
typedef struct{
	uint32_t CF_S0;
	uint32_t CF_S1;
	uint32_t CF_S2;
	uint32_t CF_S3;
	uint32_t NCF_S0;
	uint32_t NCF_S1;
	uint32_t NCF_S2;
	uint32_t NCF_S3;
}Fccu_errors_s;

#endif /* MPC5643L_FCCU_TYPES_H_ */
