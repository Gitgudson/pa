/******************************************************************************
*
* Master semester project of Jan Huber
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_ecsm.h
*
* @author   Jan Huber
* 
* @version  0.2
* 
* @date     2019 May 5
* 
* @brief    Hardware abstraction layer for the error correction
* 
* @copyright (C) Jan Huber
*
*
* This library is the hardware abstraction layer for the error correction status module (ECSM).
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* The function pit_init() must be called after initialising the clock.
* 
*/


#ifndef ECSM_H_
#define ECSM_H_

#include "MPC5643L.h"
#include <typedefs.h> 


/*******************************************************************************
* Macro definitions
*******************************************************************************/
#define ECSM_ECR_EPR1BR	   0x20 //! bit EPR1BR of SPP_MCM_ECR, MPC5643LRM 21.4.2.9
#define ECSM_ECR_EPF1BR    0x10 //! bit EPF1BR of SPP_MCM_ECR, MPC5643LRM 21.4.2.9
#define ECSM_ECR_EPRNCR    0x02 //! bit EPRNCR of SPP_MCM_ECR, MPC5643LRM 21.4.2.9
#define ECSM_ECR_EPFNCR    0x01 //! bit EPFNCR of SPP_MCM_ECR, MPC5643LRM 21.4.2.9

#define ECSM_ESR_PR1BC     0x20 //! bit PR1BC of SPP_MCM_ESR, MPC5643LRM 21.4.2.10
#define ECSM_ESR_PF1BC     0x10 //! bit PF1BC of SPP_MCM_ESR, MPC5643LRM 21.4.2.10
#define ECSM_ESR_PRNCE     0x02 //! bit PRNCE of SPP_MCM_ESR, MPC5643LRM 21.4.2.10
#define ECSM_ESR_PFNCE     0x01 //! bit PFNCE of SPP_MCM_ESR, MPC5643LRM 21.4.2.10

#define ECSM_EEGR_FRCAP   0x8000 //! bit FRCAP  of SPP_MCM_EEGR, MPC5643LRM 21.4.2.11
#define ECSM_EEGR_FRC1BI  0x2000 //! bit FRC1BI of SPP_MCM_EEGR, MPC5643LRM 21.4.2.11
#define ECSM_EEGR_FR11BI  0x1000 //! bit FR11BI of SPP_MCM_EEGR, MPC5643LRM 21.4.2.11
#define ECSM_EEGR_FRCNCI  0x0200 //! bit FRCNCI of SPP_MCM_EEGR, MPC5643LRM 21.4.2.11
#define ECSM_EEGR_FR1NCI  0x0100 //! bit FR1NCI of SPP_MCM_EEGR, MPC5643LRM 21.4.2.11
#define ECSM_EEGR_ERRBIT  0x007F //! bit ERRBIT of SPP_MCM_EEGR, MPC5643LRM 21.4.2.11

#define ECSM_IRQ_FLASH_BANK_SOURCE	9	 //!< ECSM interrupt vector number for Flash Bank, MPC5643LRM 28.7
#define ECSM_IRQ_NCE_SOURCE			35	 //!< ECSM interrupt vector number for non-correctable error, MPC5643LRM 28.7
#define ECSM_IRQ_1BC_SOURCE			36	 //!< ECSM interrupt vector number for 1-bit correction, MPC5643LRM 28.7

#define RGM_FES_F_FL_ECC_RCC	0x0400


/***************************************************************************//*!
@brief          Initialisation of the ECSM module.

@return  		void

@details        The function initialises the error correction status module (ECSM)     

@warning		Miss of register lock
				2-bit error reporting not implemented
******************************************************************************/
void ecsm_init();

/***************************************************************************//*!
@brief          ECC error handling

@return  		void

@details        This function is called as soon as an error in SRAM is detected
    			and handles it accordingly

@warning		Only 1-bit error is handled
******************************************************************************/
void ecsm_error_handler();

/***************************************************************************//*!
@brief          Generate a 2-bit non correctable error

@return  		void

@details        Generate and inject a 2-bit non correctable error into SRAM

@warning		Error reporting not implemented   
******************************************************************************/
void ecsm_nce_gen();

/***************************************************************************//*!
@brief          Generate a 1-bit correctable error

@return  		void

@details        Generate and inject a 1-bit correctable error into SRAM     
******************************************************************************/
void ecsm_obe_gen();

/***************************************************************************//*!
@brief          Check the registers of ecsm_ by calculating a CRC of them.

@return  		int32_t\n
				0 if no problem detected
				-1 if the CRC is not the result expected

@details        This function computes the CRC of register and compare it
				with expected value.
                
@warning		Not implemented yet
******************************************************************************/
int32_t ecsm_SWTEST_REGCRC();


#endif /* ECSM_H_ */
