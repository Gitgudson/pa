/******************************************************************************
* 
*	Semester project PS5 of Olivier Progin
*
***************************************************************************//*!
*
* @file     mpc5643l_dspi.h
*
* @author   Olivier Progin
* 
* @version  0.1
* 
* @date     2015 Nov 11
* 
* @brief    Hardware abstraction layer to handle the SoC Dspi.
* 
* @copyright (C) Olivier Progin
*
*
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* 
* -------------
* The function dspi_init must be called before using any other dspi function. The init function
* is used to configure correctly the SIN,SOUT,SCLK and all the CS pads used for the selected 
* Dspi module. The function also put the correct configuration into all the register of the module
* based on the data given in the mpc5643l_dspi.h file. The Excel file dspi_.xlsx can be use to 
* do the correct setup for each register with an automatic computation.
* 
* The function dspi_send is used to send a succession of data to a select slave. The speed, size,active
* low or high configuration is done in one of the CTAR available register.
* 
* The function dspi_send_receive is used to send and receive data at the same time. This function work
* exactly like the dspi_send function except that the data received during the transfer is moved into
* the table selected by the user.
* 
* The function dspix_interrupt is used if an interrupt is detected and the interrupt are activate.
*
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
*/


#ifndef MPC5643L_DSPI_H_
#define MPC5643L_DSPI_H_

#include "MPC5643L.h"
#include "HAL/mpc5643l_dspi_types.h"
#include "HAL/mpc5643l_siul.h"
#include "IntcInterrupts.h"
#include <typedefs.h>
#include "HAL/mpc5643l_mode.h"

//-----------------------------------------------------------------------------
// DSPIx_MCR configuration
//-----------------------------------------------------------------------------
#define DSPI0_MCR  0x80030C01UL //!< DSPI0_MCR\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.1
#define DSPI1_MCR  0x80010001UL //!< DSPI1_MCR\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.1
#define DSPI2_MCR  0x00000000UL //!< DSPI2_MCR\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.1


//-----------------------------------------------------------------------------
// DSPIx_CTAR0 to CTAR3 configuration
//-----------------------------------------------------------------------------
#define DSPI0_CTAR0  0x3A002277UL //!< DSPI0_CTAR0\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4
#define DSPI0_CTAR1  0x00000000UL //!< DSPI0_CTAR1\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4
#define DSPI0_CTAR2  0x00000000UL //!< DSPI0_CTAR2\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4
#define DSPI0_CTAR3  0x00000000UL //!< DSPI0_CTAR3\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4

#define DSPI1_CTAR0  0x7A552274UL //!< DSPI1_CTAR0\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4
#define DSPI1_CTAR1  0x00000000UL //!< DSPI1_CTAR1\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4
#define DSPI1_CTAR2  0x00000000UL //!< DSPI1_CTAR2\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4
#define DSPI1_CTAR3  0x00000000UL //!< DSPI1_CTAR3\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4

#define DSPI2_CTAR0  0x00000000UL //!< DSPI2_CTAR0\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4
#define DSPI2_CTAR1  0x00000000UL //!< DSPI2_CTAR1\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4
#define DSPI2_CTAR2  0x00000000UL //!< DSPI2_CTAR2\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4
#define DSPI2_CTAR3  0x00000000UL //!< DSPI2_CTAR3\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.4

//-----------------------------------------------------------------------------
// DSPIx_RSER configuration
//-----------------------------------------------------------------------------
#define DSPI0_RSER 0x00000000UL //!< DSPI0_RSER\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.6
#define DSPI1_RSER 0x00000000UL //!< DSPI0_RSER\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.6
#define DSPI2_RSER 0x00000000UL //!< DSPI0_RSER\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.6

//-----------------------------------------------------------------------------
// DSPIx_PUSHR configuration
//-----------------------------------------------------------------------------
#define DSPI0_PUSHR 0x00000000UL //!< DSPI0_PUSHR\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.7
#define DSPI1_PUSHR 0x00000000UL //!< DSPI0_PUSHR\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.7
#define DSPI2_PUSHR 0x00000000UL //!< DSPI0_PUSHR\n user defined in file dspi_.xlsx, MPC5643LRM 16.3.2.7


/***************************************************************************//*!
@brief          Initialisation of DSPI 0 to 2

@param			Pads 	pointer to a static table of type Siul_pcr_e created by the user with all the pads to initiate for the SPI
@param			Alter	pointer to a table of type Siul_gpio_mode_e created by the user with all the type corresponding to each pad to configure


@return  		void
								

@details        This function configure the 3 DSPI available on the chip


@warning		Miss of register lock\n
******************************************************************************/
void dspi_init(const Dspi_nbr_e dspinb, const Dspi_pcs_e* Pads, const uint32_t nbpad);


/***************************************************************************//*!
@brief          Function used to send data by SPI

@param			dspinb		select the Dspi module to use

@param			data		pointer to a table containing the data to send
@param			nbr_data	number of bytes to send

@param			PCS_nbr		select which chip select to activate to send the data

@param			CTAR		select which configuration to use to send the data		

@return  		void
								

@details        This function use the selected SPI to send the data passed in a
				to the slave selected.


@warning		Miss of register lock\n
******************************************************************************/
void dspi_send(const Dspi_nbr_e dspinb, uint16_t* data, uint32_t nbr_data, 
		       const Dspi_pcs_e PCS_nbr, const Dspi_ctar_e CTAR);

/***************************************************************************//*!
@brief          Function used to send and receive data by SPI

@param			dspinb		select the Dspi module to use

@param			data_s		pointer to a table containing the data to send
@param			nbr_data	number of data to send

@param			PCS_nbr		select which chip select to activate to send the data

@param			CTAR		select which configuration to use to send the data

@param			data_r		pointer to a table used to save the data received

@return  		void
								

@details        This function use the selected SPI to send the data passed in parameter
				to the slave selected and write the received data to table given 
				in parameter.


@warning		Miss of register lock\n
******************************************************************************/
void dspi_send_receive(const Dspi_nbr_e dspinb, uint16_t* data_s, uint32_t nbr_data, const Dspi_pcs_e PCS_nbr, 
		               const Dspi_ctar_e CTAR, uint32_t* data_r);

/***************************************************************************//*!
@brief          Interrupt function for DSPI0


@return  		void
								

@details        This the function to call when there is an interruption referring 
				to the DSIP0.


@warning		Miss of register lock\n
******************************************************************************/
void dspi0_interrupt();

/***************************************************************************//*!
@brief          Interrupt function for DSPI1

@return  		void
								

@details        This the function to call when there is an interruption referring 
				to the DSIP1.


@warning		Miss of register lock\n
******************************************************************************/
void dspi1_interrupt();


/***************************************************************************//*!
@brief          Interrupt function for DSPI2

@return  		void
								

@details        This the function to call when there is an interruption referring 
				to the DSIP2.


@warning		Miss of register lock\n
******************************************************************************/
void dspi2_interrupt();


#endif /* MPC5643L_DSPI_H_ */
