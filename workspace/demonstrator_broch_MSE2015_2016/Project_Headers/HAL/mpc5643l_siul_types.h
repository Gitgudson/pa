/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_siul_types.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Nov 01
* 
* @brief    Types defined for the siul HAL driver
* 
* @copyright (C) Nicolas Broch
*
*
* This file defines the types used for configure or handle the GPIO
* in the HAL mpc5643l_siul.h
* 
* This file is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
*
*/

#ifndef SIUL_PCR_DEFINE_H_
#define SIUL_PCR_DEFINE_H_

/***************************************************************************//*!
@brief          Enumeration to name GPIOs by their port ID. Valid for the 
				257 MAPBGA package.
******************************************************************************/
typedef enum{
	PCR_A_0  = 0,   //!< Convert A[0] into PCR number, MPC5643LRM 3.4
	PCR_A_1  = 1,   //!< Convert A[1] into PCR number, MPC5643LRM 3.4
	PCR_A_2  = 2,   //!< Convert A[2] into PCR number, MPC5643LRM 3.4
	PCR_A_3  = 3,   //!< Convert A[3] into PCR number, MPC5643LRM 3.4
	PCR_A_4  = 4,   //!< Convert A[4] into PCR number, MPC5643LRM 3.4
	PCR_A_5  = 5,   //!< Convert A[5] into PCR number, MPC5643LRM 3.4
	PCR_A_6  = 6,   //!< Convert A[6] into PCR number, MPC5643LRM 3.4
	PCR_A_7  = 7,   //!< Convert A[7] into PCR number, MPC5643LRM 3.4
	PCR_A_8  = 8,   //!< Convert A[8] into PCR number, MPC5643LRM 3.4
	PCR_A_9  = 9,   //!< Convert A[9] into PCR number, MPC5643LRM 3.4
	PCR_A_10 = 10,  //!< Convert A[10] into PCR number, MPC5643LRM 3.4
	PCR_A_11 = 11,  //!< Convert A[11] into PCR number, MPC5643LRM 3.4
	PCR_A_12 = 12,  //!< Convert A[12] into PCR number, MPC5643LRM 3.4
	PCR_A_13 = 13,  //!< Convert A[13] into PCR number, MPC5643LRM 3.4
	PCR_A_14 = 14,  //!< Convert A[14] into PCR number, MPC5643LRM 3.4
	PCR_A_15 = 15,  //!< Convert A[15] into PCR number, MPC5643LRM 3.4
	
	PCR_B_0  = 16,  //!< Convert B[0] into PCR number, MPC5643LRM 3.4
	PCR_B_1  = 17,  //!< Convert B[1] into PCR number, MPC5643LRM 3.4
	PCR_B_2  = 18,  //!< Convert B[2] into PCR number, MPC5643LRM 3.4
	PCR_B_3  = 19,  //!< Convert B[3] into PCR number, MPC5643LRM 3.4
	PCR_B_4  = 20,  //!< Convert B[4] into PCR number, MPC5643LRM 3.4
	PCR_B_5  = 21,  //!< Convert B[5] into PCR number, MPC5643LRM 3.4
	PCR_B_6  = 22,  //!< Convert B[6] into PCR number, MPC5643LRM 3.4
	PCR_B_7  = 23,  //!< Convert B[7] into PCR number, MPC5643LRM 3.4
	PCR_B_8  = 24,  //!< Convert B[8] into PCR number, MPC5643LRM 3.4
	PCR_B_9  = 25,  //!< Convert B[9] into PCR number, MPC5643LRM 3.4
	PCR_B_10 = 26,  //!< Convert B[10] into PCR number, MPC5643LRM 3.4
	PCR_B_11 = 27,  //!< Convert B[11] into PCR number, MPC5643LRM 3.4
	PCR_B_12 = 28,  //!< Convert B[12] into PCR number, MPC5643LRM 3.4
	PCR_B_13 = 29,  //!< Convert B[13] into PCR number, MPC5643LRM 3.4
	PCR_B_14 = 30,  //!< Convert B[14] into PCR number, MPC5643LRM 3.4
	PCR_B_15 = 31,  //!< Convert B[15] into PCR number, MPC5643LRM 3.4
	
	PCR_C_0  = 32,  //!< Convert C[0] into PCR number, MPC5643LRM 3.4
	PCR_C_1  = 33,  //!< Convert C[1] into PCR number, MPC5643LRM 3.4
	PCR_C_2  = 34,  //!< Convert C[2] into PCR number, MPC5643LRM 3.4
	PCR_C_3  = 35,  //!< Convert C[3] into PCR number, MPC5643LRM 3.4
	PCR_C_4  = 36,  //!< Convert C[4] into PCR number, MPC5643LRM 3.4
	PCR_C_5  = 37,  //!< Convert C[5] into PCR number, MPC5643LRM 3.4
	PCR_C_6  = 38,  //!< Convert C[6] into PCR number, MPC5643LRM 3.4
	PCR_C_7  = 39,  //!< Convert C[7] into PCR number, MPC5643LRM 3.4
	PCR_C_10 = 42,  //!< Convert C[10] into PCR number, MPC5643LRM 3.4
	PCR_C_11 = 43,  //!< Convert C[11] into PCR number, MPC5643LRM 3.4
	PCR_C_12 = 44,  //!< Convert C[12] into PCR number, MPC5643LRM 3.4
	PCR_C_13 = 45,  //!< Convert C[13] into PCR number, MPC5643LRM 3.4
	PCR_C_14 = 46,  //!< Convert C[14] into PCR number, MPC5643LRM 3.4
	PCR_C_15 = 47,  //!< Convert C[15] into PCR number, MPC5643LRM 3.4
	
	PCR_D_0  = 48,  //!< Convert D[0] into PCR number, MPC5643LRM 3.4
	PCR_D_1  = 49,  //!< Convert D[1] into PCR number, MPC5643LRM 3.4
	PCR_D_2  = 50,  //!< Convert D[2] into PCR number, MPC5643LRM 3.4
	PCR_D_3  = 51,  //!< Convert D[3] into PCR number, MPC5643LRM 3.4
	PCR_D_4  = 52,  //!< Convert D[4] into PCR number, MPC5643LRM 3.4
	PCR_D_5  = 53,  //!< Convert D[5] into PCR number, MPC5643LRM 3.4
	PCR_D_6  = 54,  //!< Convert D[6] into PCR number, MPC5643LRM 3.4
	PCR_D_7  = 55,  //!< Convert D[7] into PCR number, MPC5643LRM 3.4
	PCR_D_8  = 56,  //!< Convert D[8] into PCR number, MPC5643LRM 3.4
	PCR_D_9  = 57,  //!< Convert D[9] into PCR number, MPC5643LRM 3.4
	PCR_D_10 = 58,  //!< Convert D[10] into PCR number, MPC5643LRM 3.4
	PCR_D_11 = 59,  //!< Convert D[11] into PCR number, MPC5643LRM 3.4
	PCR_D_12 = 60,  //!< Convert D[12] into PCR number, MPC5643LRM 3.4
	PCR_D_14 = 62,  //!< Convert D[14] into PCR number, MPC5643LRM 3.4
	
	PCR_E_0  = 64,  //!< Convert E[0] into PCR number, MPC5643LRM 3.4
	PCR_E_2  = 66,  //!< Convert E[2] into PCR number, MPC5643LRM 3.4
	PCR_E_4  = 68,  //!< Convert E[4] into PCR number, MPC5643LRM 3.4
	PCR_E_5  = 69,  //!< Convert E[5] into PCR number, MPC5643LRM 3.4
	PCR_E_6  = 70,  //!< Convert E[6] into PCR number, MPC5643LRM 3.4
	PCR_E_7  = 71,  //!< Convert E[7] into PCR number, MPC5643LRM 3.4
	PCR_E_9  = 73,  //!< Convert E[9] into PCR number, MPC5643LRM 3.4
	PCR_E_10 = 74,  //!< Convert E[10] into PCR number, MPC5643LRM 3.4
	PCR_E_11 = 75,  //!< Convert E[11] into PCR number, MPC5643LRM 3.4
	PCR_E_12 = 76,  //!< Convert E[12] into PCR number, MPC5643LRM 3.4
	PCR_E_13 = 77,  //!< Convert E[13] into PCR number, MPC5643LRM 3.4
	PCR_E_14 = 78,  //!< Convert E[14] into PCR number, MPC5643LRM 3.4
	PCR_E_15 = 79,  //!< Convert E[15] into PCR number, MPC5643LRM 3.4
	
	PCR_F_0  = 80,  //!< Convert F[0] into PCR number, MPC5643LRM 3.4
	PCR_F_3  = 83,  //!< Convert F[3] into PCR number, MPC5643LRM 3.4
	PCR_F_4  = 84,  //!< Convert F[4] into PCR number, MPC5643LRM 3.4
	PCR_F_5  = 85,  //!< Convert F[5] into PCR number, MPC5643LRM 3.4
	PCR_F_6  = 86,  //!< Convert F[6] into PCR number, MPC5643LRM 3.4
	PCR_F_7  = 87,  //!< Convert F[7] into PCR number, MPC5643LRM 3.4
	PCR_F_8  = 88,  //!< Convert F[8] into PCR number, MPC5643LRM 3.4
	PCR_F_9  = 89,  //!< Convert F[9] into PCR number, MPC5643LRM 3.4
	PCR_F_10 = 90,  //!< Convert F[10] into PCR number, MPC5643LRM 3.4
	PCR_F_11 = 91,  //!< Convert F[11] into PCR number, MPC5643LRM 3.4
	PCR_F_12 = 92,  //!< Convert F[12] into PCR number, MPC5643LRM 3.4
	PCR_F_13 = 93,  //!< Convert F[13] into PCR number, MPC5643LRM 3.4
	PCR_F_14 = 94,  //!< Convert F[14] into PCR number, MPC5643LRM 3.4
	PCR_F_15 = 95,  //!< Convert F[15] into PCR number, MPC5643LRM 3.4
	
	PCR_G_2  = 98,   //!< Convert G[2] into PCR number, MPC5643LRM 3.4
	PCR_G_3  = 99,   //!< Convert G[3] into PCR number, MPC5643LRM 3.4
	PCR_G_4  = 100,  //!< Convert G[4] into PCR number, MPC5643LRM 3.4
	PCR_G_5  = 101,  //!< Convert G[5] into PCR number, MPC5643LRM 3.4
	PCR_G_6  = 102,  //!< Convert G[6] into PCR number, MPC5643LRM 3.4
	PCR_G_7  = 103,  //!< Convert G[7] into PCR number, MPC5643LRM 3.4
	PCR_G_8  = 104,  //!< Convert G[8] into PCR number, MPC5643LRM 3.4
	PCR_G_9  = 105,  //!< Convert G[9] into PCR number, MPC5643LRM 3.4
	PCR_G_10 = 106,  //!< Convert G[10] into PCR number, MPC5643LRM 3.4
	PCR_G_11 = 107,  //!< Convert G[11] into PCR number, MPC5643LRM 3.4
	PCR_G_12 = 108,  //!< Convert G[12] into PCR number, MPC5643LRM 3.4
	PCR_G_13 = 109,  //!< Convert G[13] into PCR number, MPC5643LRM 3.4
	PCR_G_14 = 110,  //!< Convert G[14] into PCR number, MPC5643LRM 3.4
	PCR_G_15 = 111,  //!< Convert G[15] into PCR number, MPC5643LRM 3.4
	
	PCR_H_0  = 112,  //!< Convert H[0] into PCR number, MPC5643LRM 3.4
	PCR_H_1  = 113,  //!< Convert H[1] into PCR number, MPC5643LRM 3.4
	PCR_H_2  = 114,  //!< Convert H[2] into PCR number, MPC5643LRM 3.4
	PCR_H_3  = 115,  //!< Convert H[3] into PCR number, MPC5643LRM 3.4
	PCR_H_4  = 116,  //!< Convert H[4] into PCR number, MPC5643LRM 3.4
	PCR_H_5  = 117,  //!< Convert H[5] into PCR number, MPC5643LRM 3.4
	PCR_H_6  = 118,  //!< Convert H[6] into PCR number, MPC5643LRM 3.4
	PCR_H_7  = 119,  //!< Convert H[7] into PCR number, MPC5643LRM 3.4
	PCR_H_8  = 120,  //!< Convert H[8] into PCR number, MPC5643LRM 3.4
	PCR_H_9  = 121,  //!< Convert H[9] into PCR number, MPC5643LRM 3.4
	PCR_H_10 = 122,  //!< Convert H[10] into PCR number, MPC5643LRM 3.4
	PCR_H_11 = 123,  //!< Convert H[11] into PCR number, MPC5643LRM 3.4
	PCR_H_12 = 124,  //!< Convert H[12] into PCR number, MPC5643LRM 3.4
	PCR_H_13 = 125,  //!< Convert H[13] into PCR number, MPC5643LRM 3.4
	PCR_H_14 = 126,  //!< Convert H[14] into PCR number, MPC5643LRM 3.4
	PCR_H_15 = 127,  //!< Convert H[15] into PCR number, MPC5643LRM 3.4
	
	PCR_I_0  = 128,  //!< Convert I[0] into PCR number, MPC5643LRM 3.4
	PCR_I_1  = 129,  //!< Convert I[1] into PCR number, MPC5643LRM 3.4
	PCR_I_2  = 130,  //!< Convert I[2] into PCR number, MPC5643LRM 3.4
	PCR_I_3  = 131,  //!< Convert I[3] into PCR number, MPC5643LRM 3.4
	
	PCR_RDY  = 132   //!< Convert RDY into PCR number, MPC5643LRM 3.4
}Siul_pcr_e;



/***************************************************************************//*!
@brief          Enumeration for for the mode of the Pad
******************************************************************************/
typedef enum{
	SIUL_MODE_GPIO,    //!< PCR.PA[1:0] configured in GPIO, MPC5643LRM 47.5.2.8
	SIUL_MODE_ALTER_1, //!< PCR.PA[1:0] configured in alternative mode 1, MPC5643LRM 47.5.2.8
	SIUL_MODE_ALTER_2, //!< PCR.PA[1:0] configured in alternative mode 2, MPC5643LRM 47.5.2.8
	SIUL_MODE_ALTER_3  //!< PCR.PA[1:0] configured in alternative mode 3, MPC5643LRM 47.5.2.8
}Siul_gpio_mode_e;


/***************************************************************************//*!
@brief          Enumeration to define if the port is an inputor output
******************************************************************************/
typedef enum{
	SIUL_INPUT,     //!< Configure a GPIO in input mode, MPC5643LRM 47.5.2.8
	SIUL_OUTPUT,    //!< Configure a GPIO in output mode and activate the internal read back of the output, MPC5643LRM 47.5.2.8
	SIUL_INOUT_NONE //!< For alternative modes, not configured as input or output, MPC5643LRM 47.5.2.8
}Siul_inputOutput_e;

/***************************************************************************//*!
@brief          Enumeration to define the slew rate of a PIN
******************************************************************************/
typedef enum{
	SIUL_SLOW,   //!< Configure a GPIO in slow rate configuration, MPC5643LRM 47.5.2.8
	SIUL_FAST    //!< Configure a GPIO in fast rate configuration, MPC5643LRM 47.5.2.8
}Siul_slewRate_e;


/***************************************************************************//*!
@brief          Enumeration to define if an output is open-drain, set a
				pull-up resistor or a pull-down resistor
******************************************************************************/
typedef enum
{
	SIUL_TYPE_NONE,         //!< No need to configure in open-drain or resistor, MPC5643LRM 47.5.2.8
	SIUL_OUTPUT_OPEN_DRAIN, //!< Configure an output in open drain mode if enable, MPC5643LRM 47.5.2.8
	SIUL_INPUT_PULL_UP,     //!< Configure an output in pull-up if enable, MPC5643LRM 47.5.2.8
	SIUL_INPUT_PULL_DOWN    //!< Configure an output in pull-down if enable, MPC5643LRM 47.5.2.8
}Siul_inputOutputType_e;

/***************************************************************************//*!
@brief          Enumeration to define if the interrupt is on rising or
				falling edge.
******************************************************************************/
typedef enum
{
	SIUL_RISING_EDGE,       //!< Configure an interrupt on rising-edge of the signal, MPC5643LRM 47.5.2.5
	SIUL_FALLING_EDGE       //!< Configure an interrupt on falling-edge of the signal, MPC5643LRM 47.5.2.6
}Siul_interruptType_e;


/***************************************************************************//*!
@brief          Declaration of SIUL_LOW and SIUL_HIGH to simplify the 
				code
******************************************************************************/
#define SIUL_LOW  ((uint8_t) 0)
#define SIUL_HIGH ((uint8_t) 1)

#endif /* SIUL_PCR_DEFINE_H_ */
