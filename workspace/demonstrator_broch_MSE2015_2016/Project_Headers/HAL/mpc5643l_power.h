/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_power.h
*
* @author   Nicolas Broch
* @author	Jan Huber
* 
* @version  0.1
* 
* @date     2019 Apr 16
* 
* @brief    Hardware abstraction layer for the supervision of power supply (PMU and MC_PCU)
* 
* @copyright (C) Nicolas Broch
*
*
* This library is the hardware abstraction layer for the supervision of power supply. It performs
* the BIST tests at startup and check the voltage of the SoC.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
* How to use it
* -------------
* 
* The function power_init() must be called during system init. If necessary,
* it is possible to perform a bist of the power units during in runtime with
* the function power_bist_start().
*
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
* 
*/

#ifndef MPC5643L_POWER_H_
#define MPC5643L_POWER_H_


#include <typedefs.h> 

/*******************************************************************************
* Macro definitions
*******************************************************************************/

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define POWER_STATUS_EXP  0xE0008006UL   //!< Value expected into PMUCTRL_STATUS at startup, user defined in file power_.xlsx, MPC5643LRM 39.7.1

#define POWER_CLEAR_IRQ   0xFFFFFFFFUL	//!< Value to write into PMUCTRL_IRQS to clear faults, MPC5643LRM 39.7.5

#define POWER_BIST_TO     1000           //!< Timeout for BIST in iteration, user defined in file power_.xlsx, MPC5643LRM 39.6

#define POWER_BIST_SUC    0x00000003UL   //!< Value to read into PMUCTRL_FAULT if BISTs haven't detected failures, MPC5643LRM 39.7.5
#define POWER_FAULT_MASK  0x00070007UL   //!< Mask to read into PMUCTRL_FAULT to know if BISTs haven't detected failures, MPC5643LRM 39.7.5


#define POWER_FCCU_MASK_NCFS0 0x00006000UL   //!< Mask to read in FCCU_NCF_S0 the NCF flags 13 and 14, MPC5643LRM 22.6.10
#define POWER_FCCU_BIST_SUC   0x00006000UL   //!< Value to read into FCCU_NCF_S0 if the BISTs of PMU were successful, MPC5643LRM 22.6.10

//-----------------------------------------------------------------------------
// Error registers
//-----------------------------------------------------------------------------
#define POWER_MASKF       0x00000000UL   //!< PMUCTRL_MASKF\n user defined in file power_.xlsx, MPC5643LRM 39.7.3
#define POWER_IRQE        0x00000000UL   //!< PMUCTRL_IRQE\n user defined in file power_.xlsx, MPC5643LRM 39.7.6



/*******************************************************************************
* Functions
*******************************************************************************/
/***************************************************************************//*!
@brief          Initialisation the power supply supervision

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If the external transistor is not working
			   -2 If the BIST tests wasn't performed before timeout

@details        This function initialises the power supply supervision, checks
				if the external transistor used to generate processor voltage
				works, performs BIST and clear errors.\n
				
				This function has to be executed before clearing failures flags.

@warning		Lack of lock of registers
******************************************************************************/
int32_t power_init();

/***************************************************************************//*!
@brief         Perform BIST tests of voltage supervisors

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 In case of failure

@details       This function tests the voltage supervisors by BIST. If the function
			   return -1, the execution of BIST has failed or the BIST
			   discovered a failure in the power supervision, otherwise, if the 
			   BIST are successful, the non-critical faults 13 and 14 has to be
			   set. Read FCCU_NCF_S0[13:14] to know it worked.
@warning	   Not implemented!!
******************************************************************************/
int32_t power_bist_start();

/***************************************************************************//*!
@brief         Reset failures detected in PMU

@return  	   void

@details       This function resets the failures detected by the PMU. If a
			   failure has been detected, this function is called by power_init().
			   So no need to call it again. 
@warning	   Not implemented!!
******************************************************************************/
void power_clear_fails();


/***************************************************************************//*!
@brief          Interrupt for PMU failure

@return  	    void

@details        This function is called if the PMU called an interrupt. 
				Normally, all interrupts are disable, so this function declares
				a failure if it is called.
******************************************************************************/
void power_IRQ_fails();



#endif /* MPC5643L_POWER_H_ */
