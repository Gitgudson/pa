/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     mpc5643l_flexpwm_types.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Dec 21
* 
* @brief    Types defined for the FlexPWM HAL driver
* 
* @copyright (C) Nicolas Broch
*
*
* This file defines the types used for configure or handle the FlexPWM
* in the HAL mpc5643l_fccu.h
* 
* This file is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
*
*/

#ifndef MPC5643L_FLEXPWM_TYPES_H_
#define MPC5643L_FLEXPWM_TYPES_H_

#include <typedefs.h>
#include "HAL/mpc5643l_flexpwm.h"

//-----------------------------------------------------------------------------
// HAL parameters
//-----------------------------------------------------------------------------
#define FLEXPWM_NUMB_SUB   4            //!< Number of submodule in a FlexPWM module, MPC5643LRM 25



/***************************************************************************//*!
@brief          Duty cycles of the 4 submodules of a FlexPWM module.
				-32768 means -100% and 32767 means 100% of duty cycle.
				sub[0] = submodule 0
				sub[1] = submodule 1, ...
******************************************************************************/
typedef struct{
	int16_t sub[FLEXPWM_NUMB_SUB];
}Flexpwm_dutyCycle_s;

/***************************************************************************//*!
@brief          Enumeration to define on which FlexPWM module the function is
				applied
******************************************************************************/
typedef enum{
	FLEXPWM_MOD0,    //!< FlexPWM0 module
	FLEXPWM_MOD1,	 //!< FlexPWM1 module
	FLEXPWM_ALL		 //!< FlexPWM0 and FlexPWM1 modules
}Flexpwm_module;


#endif /* MPC5643L_FLEXPWM_TYPES_H_ */
