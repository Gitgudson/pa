/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     state_machine.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2016 Jan 25
* 
* @brief    State machine of the motor control demonstrator
* 
* @copyright (C) Nicolas Broch
*
*
* This library offers the state machine definition for the demonstrator
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* 
* 
* 
*
*/
#ifndef STATE_MACHINE_H_
#define STATE_MACHINE_H_

#include <typedefs.h> 



/***************************************************************************//*!
@brief          States enumeration
******************************************************************************/
typedef enum{
	STATE_INIT,       //!< initialisation of the system
	STATE_ALIGN,      //!< state to align the motor magnets
	STATE_CTRL_2_MOT, //!< the controller for the 2 motors work
	STATE_ERROR       //!< the 2 motors are in error mode
}state_machine_e;

#endif /* STATE_MACHINE_H_ */
