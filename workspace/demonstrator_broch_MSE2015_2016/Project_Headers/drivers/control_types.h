/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     control_types.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Dec 30
* 
* @brief    Types defined for control algorithms
* 
* @copyright (C) Nicolas Broch
*
*
* This file defines the types used for control algorithms
* 
* This file is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
*
*/


#ifndef _CONTROL_TYPES_H_
#define _CONTROL_TYPES_H_

#include <typedefs.h> 

/***************************************************************************//*!
@brief          PI controller parameter structure
******************************************************************************/
typedef struct{
	float Kp;        //!< Kp parameter of PI controller
	float Gi;        //!< Gi = 1/Ti parameter of PI controller
	float Gp_w;      //!< Anti-windup gain
	float Te;	     //!< Sample time (= 1 / Fe)
	float maxOutput; //!< Max output value of the control system
	float minOutput; //!< Min output value of the control system
}Control_piControl_param_s;

/***************************************************************************//*!
@brief          PI controller parameter structure
******************************************************************************/
typedef struct{
	float command_prime; //!< Command before saturation of last iteration
	float command;		 //!< Command after saturation of this iteration
	float teGi;          //!< Pre-computed Te * Gi value
	float integrator;    //!< Buffer to stock integrate value - do not modify
	float error;         //!< Error computed
}Control_piControl_data_s;

/***************************************************************************//*!
@brief          PI controller structure
******************************************************************************/
typedef struct{
	Control_piControl_param_s param;     //!< Parameters of control system - should be set manually at system level
	Control_piControl_data_s  data;		 //!< Internal static data for the control algorithm
}Control_piControl_s;




#endif /* _CONTROL_TYPES_H_ */
