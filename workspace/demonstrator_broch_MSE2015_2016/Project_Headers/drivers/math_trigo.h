/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     math_trigo.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Dec 30
* 
* @brief    This library includes functions to compute trigonometry and mathematics functions.
* 
* @copyright (C) Nicolas Broch
*
*
* This library offers time-optimised mathematical functions.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
*
*/


#ifndef MATH_GEOMETRY_H_
#define MATH_GEOMETRY_H_

#include <typedefs.h> 

#define PI   3.1416F  //!< Value of pi
#define PI_2 1.5708F  //!< Value of pi/2

/*******************************************************************************
* Functions
*******************************************************************************/

/***************************************************************************//*!
@brief         Atan 4-quadrants

@param		   Y		First value of the calculation, see details
@param		   X		Second value of the calculation, see details

@return  	   angle in radian

@details       This function computes, from 2 signed integers the 4-quadrant
			   atan function.\n
			   Perform the operation out = atan4(Y/X)
******************************************************************************/
float atan4(int32_t Y, int32_t X);

/***************************************************************************//*!
@brief         Sine function

@param		   angle	angle in radian

@return  	   value of sin(angle)

@details       This function computes the sine of the angle.
******************************************************************************/
float sin_opt(float angle);

/***************************************************************************//*!
@brief         Cosine function

@param		   angle	angle in radian

@return  	   value of cos(angle)

@details       This function computes the cosine of the angle.
******************************************************************************/
float cos_opt(float angle);

/***************************************************************************//*!
@brief         Return cosine and sine function of an angle

@param		   angle	angle in radian
@param		   cos		cosine of the angle
@param		   sin		sine of the angle

@return  	   void

@details       This function computes the cosine and the sin of the angle.
******************************************************************************/
void sin_cos_opt(const float angle, float *sin, float *cos);



/***************************************************************************//*!
@brief         Modulo of float values

@param		   val		Value
@param		   mod		Value for modulo computation

@return  	   return the modulo of val per mod

@details       This function returns the result of the division of val per mod.
******************************************************************************/
float floatmod(float val, float mod);

/***************************************************************************//*!
@brief         Fast algorithm to compute sqare root in floating point

@param		   val		Value

@return  	   return the sqrt(val)

@details       This function returns the sqrt(val). The precision of this algorithm
			   is limited.
			   Source: https://en.wikipedia.org/wiki/Methods_of_computing_square_roots
******************************************************************************/
float fastsqrt(float val);






#endif /* MATH_GEOMETRY_H_ */
