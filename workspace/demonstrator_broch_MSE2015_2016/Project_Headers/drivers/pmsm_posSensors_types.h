/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     pmsm_posSensors_types.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2016 Jan 16
* 
* @brief    Types defined for the position sensors
* 
* @copyright (C) Nicolas Broch
*
*
* This file defines the types used to read the position sensors
* 
* This file is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
*
*/

#ifndef PMSM_POSSENSORS_TYPES_H_
#define PMSM_POSSENSORS_TYPES_H_

#include <typedefs.h> 

#include "HAL/mpc5643l_siul_types.h"
#include "HAL/mpc5643l_dspi_types.h"
#include "drivers/control_types.h"
#include "drivers/filter_types.h"

/***************************************************************************//*!
@brief          Enumeration to select which sensor select the position
******************************************************************************/
typedef enum{
	PMSM_POS_RESOLVER = 0, //!< reference position is resolver sensor
	PMSM_POS_ENCODER  = 1, //!< reference position is encoder sensor
	PSMS_POS_FIX   = 2     //!< position not updated from sensor
}Pmsm_selectSensPos_e;

/***************************************************************************//*!
@brief          Data structure for Speed filter
******************************************************************************/
typedef struct{
	Control_IIRfilter_s filter;    	//!< Filter data structure
	float value;      				//!< Speed in [rad/s]
	float Fe;						//!< Frequency of measure of position
}Pmsm_speed_s;

/***************************************************************************//*!
@brief          Data structure for datas from encoder
******************************************************************************/
typedef struct{
	int16_t pos_inc;    //!< Position of the encoder in increments, values between -2048 and 2047.\n -2048 = -180�\n 2047 = 179.91. Increment steps = 0.087�C
	float pos_rad;      //!< Position of the encoder in radian
}Pmsm_enc_s;

/***************************************************************************//*!
@brief          Data structure for datas from resolver
******************************************************************************/
typedef struct{
	int16_t adc_sin;        //!< Raw value of the SIN signal from ADC
	int16_t adc_cos;        //!< Raw value of the COS signal from ADC
	float   pos_rad_raw;    //!< Position of the resolver in radian in the sensor referential
	float   pos_rad;        //!< Position of the resolver in radian in the system referential
}Pmsm_res_s;

/***************************************************************************//*!
@brief          Data structure for system positions
******************************************************************************/
typedef struct{
	float   rotor_angle_k;    //!< Position in rad of the rotor of current iteration.
	float   rotor_angle_k1;   //!< Position in rad of the rotor of last iteration.
}Pmsm_pos_s;

/***************************************************************************//*!
@brief          Data structure for system positions and speed
******************************************************************************/
typedef struct{
	Pmsm_pos_s position;		 	//!< angular position structure
	Pmsm_speed_s speed;				//!< angular speed structure
	Pmsm_selectSensPos_e state;		//!< state machine of position and speed management
}Pmsm_pos_speed_s;

/***************************************************************************//*!
@brief          Data structure for encoder configuration
******************************************************************************/
typedef struct{
	vuint16_t *eTimerReg;			//!< Pointer to the eTimer Reg which contains the position
	float maxValueEnc;				//!< The encoder register has values between -maxValueEnc and maxValueEnc
}Pmsm_encoder_param_s;

/***************************************************************************//*!
@brief          Data structure for resolver configuration
******************************************************************************/
typedef struct{
	vuint32_t *ADCReg_sin;			//!< Pointer to the ADC register which contains SIN measure
	vuint32_t *ADCReg_cos;			//!< Pointer to the ADC register which contains COS measure
	float   angle0;         		//!< Correction value to transform sensor referential into system referential
}Pmsm_resolv_param_s;


/***************************************************************************//*!
@brief          Data structure for encoder
******************************************************************************/
typedef struct{
	Pmsm_encoder_param_s param;		//!< Parameters of encoder
	Pmsm_enc_s meas;				//!< Measures of encoder
}Pmsm_encoder_s;

/***************************************************************************//*!
@brief          Data structure for SIN/COS resolver
******************************************************************************/
typedef struct{
	Pmsm_resolv_param_s	param;		//!< Parameters of resolver
	Pmsm_res_s meas;				//!< Measures of resolver
}Pmsm_resolver_s;


/***************************************************************************//*!
@brief          Main data structure for this library
******************************************************************************/
typedef struct{
	Pmsm_pos_speed_s posAndSpeed;  	//!< Structure that contains the measured position and speed
	Pmsm_encoder_s encoder; 		//!< Structure of encoder parameters and measures
	Pmsm_resolver_s resolver; 		//!< Structure of resolver parameters and measures
}Pmsm_sensorsPosition_s;


#endif /* PMSM_POSSENSORS_TYPES_H_ */
