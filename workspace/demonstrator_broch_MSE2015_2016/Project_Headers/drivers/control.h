/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     control.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2016 Jan 11
* 
* @brief    Control algorithms
* 
* @copyright (C) Nicolas Broch
*
*
* This driver provides the functions for control algorithms
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 

* 
* 
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
*/


#ifndef _CONTROL_H_
#define _CONTROL_H_

#include "drivers/pmsm_motors_types.h"
#include "drivers/control_types.h"
#include <typedefs.h> 


//-----------------------------------------------------------------------------
// Floating point limits
//-----------------------------------------------------------------------------
#define CONTROL_MAX_INT 1.0e9F			//!< Maximal value of the integrator buffer
#define CONTROL_MIN_INT -1.0e9F			//!< Minimal value of the integrator buffer

/*******************************************************************************
* Functions
*******************************************************************************/

/***************************************************************************//*!
@brief        Init PI control structure

@param		  piData	data of the control algorithm

@return  	  void

@details      This function initialise the data structure for PI algorithm.
			  It must be called after setting the parameters of the control
			  system in the sub-structure param.
******************************************************************************/
void control_initPI(Control_piControl_s *piData);

/***************************************************************************//*!
@brief        PI control algorithm

@param		  piData	data of the control algorithm
@param		  error		error between command and signal

@return  	  float		output of the control algorithm

@details      This function computes a PI algorithm with anti-windup.
******************************************************************************/
float control_algoPI(Control_piControl_s *piData, const float error);





#endif /* _CONTROL_H_ */
