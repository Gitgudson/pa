/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     math_transforms.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Dec 30
* 
* @brief    This library includes functions to compute the system transforms for a PMSM motor.
* 
* @copyright (C) Nicolas Broch
*
*
* This library proposes reference transforms useful for PMSM motor vector control.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
*
*
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
*
*/


#ifndef MATH_TRANSFORMS_H_
#define MATH_TRANSFORMS_H_

#include <typedefs.h> 

#include "drivers/pmsm_motors_types.h"
#include "drivers/math_trigo.h"


/*******************************************************************************
* Functions
*******************************************************************************/



/***************************************************************************//*!
@brief         Do the transform from 3-phase currents space to 2 dimensions space

@param		   abc			Currents in 3-phases space, input
@param		   alphaBeta	Currents in 2-phases space, output

@return  	   void

@details       This function does the transform from Iabc to Ialphabeta.
******************************************************************************/
void trans_clarke(const Pmsm_abc_float_s *abc, Pmsm_alphaBeta_s *alphaBeta);

/***************************************************************************//*!
@brief         Do the transform from 2 dimensions currents space to 3-phases 
			   current space

@param		   abc			Currents in 3-phases space, output
@param		   alphaBeta	Currents in 2-phases space, input

@return  	   void

@details       This function does the transform from Ialphabeta to Iabc.
******************************************************************************/
void trans_clarke_backward(const Pmsm_alphaBeta_s *alphaBeta, Pmsm_abc_float_s *abc);

/***************************************************************************//*!
@brief         Do the transform from 3-phase currents space to 2 dimension space

@param		   alphaBeta	Currents in 2-phases space, input
@param		   angle		Angle between system state and dq state
@param		   dq			Currents in dq space, output


@return  	   void

@details       This function does the transform from Iabc to Ialphabeta.
******************************************************************************/
void trans_park(const Pmsm_alphaBeta_s *alphaBeta, const float angle, Pmsm_dq_s *dq);

/***************************************************************************//*!
@brief         Do the transform from 2 dimension space to 3-phase currents space

@param		   alphaBeta	Currents in 2-phases space, output
@param		   angle		Angle between system state and dq state
@param		   dq			Currents in dq space, input


@return  	   void

@details       This function does the transform from Ialphabeta to Iabc.
******************************************************************************/
void trans_park_backward(const Pmsm_dq_s *dq, const float angle, Pmsm_alphaBeta_s *alphaBeta);




#endif /* MATH_TRANSFORMS_H_ */
