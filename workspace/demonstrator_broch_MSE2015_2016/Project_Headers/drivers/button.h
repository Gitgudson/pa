/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     button.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2016 Jan 25
* 
* @brief    Button management
* 
* @copyright (C) Nicolas Broch
*
*
* This library offers functions to access to the button state with a
* numerical filter
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 

* 
* 
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
*/


#ifndef _BUTTONS_H_
#define _BUTTONS_H_

#include "HAL/mpc5643l_siul.h"
#include <typedefs.h> 


#define BUTTON_FILT_ITE 10 //!< Number of stable state iteration to switch the button state

/*******************************************************************************
* Structure for buttons
*******************************************************************************/
typedef struct{
	Siul_pcr_e pad;        //!< GPIO on which the button in connected
	uint32_t stateCounter; //!< Counter for numerical filter
	uint32_t state;        //!< State of the output of the numerical filter
}Button_s;


/*******************************************************************************
* Functions
*******************************************************************************/

/***************************************************************************//*!
@brief        Init button structure

@param		  button	data of the button
@param		  pad		pad where is connected the button
@param		  offState	state 0 or 1 in which the button is in off state

@return  	  void

@details      This function initialise the data structure buttons and
			  the gpio
******************************************************************************/
void button_init(Button_s *button, const Siul_pcr_e pad, const uint32_t offState);

/***************************************************************************//*!
@brief        Return the state of the button

@param		  button	data of the button

@return  	  int32_t\n
			   1 if button pushed
			   0 if button unpushed

@details      Return the state of the button. This function must be called
			  periodically to activate the filter.
******************************************************************************/
uint32_t button_state(Button_s *button);






#endif /* _BUTTONS_H_ */
