/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     filter.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2016 Jan 14
* 
* @brief    Numerical filter functions
* 
* @copyright (C) Nicolas Broch
*
*
* This library offers numerical filters functions
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* This library contains functions for numerical filters
* 
* 
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
* 
*/


#ifndef _FILTER_H_
#define _FILTER_H_

#include "drivers/filter_types.h"
#include <typedefs.h> 



/*******************************************************************************
* Functions
*******************************************************************************/

/***************************************************************************//*!
@brief        Init IIR filter of second order

@param		  filter	data of the filter

@return  	  void

@details      This function initialise the data structure for IIR filter with
			  the given parameters
******************************************************************************/
void control_initFilterIIR(Control_IIRfilter_s *filter);

/***************************************************************************//*!
@brief        IIR filter algorithm

@param		  filter	data of the filter
@param		  command	command of the PI control system
@param		  measure	measured signal

@return  	  float		output of the filter

@details      This function computes a PI algorithm with anti-windup.\n
			  y[k]/x[k] = (b1*z + b0)/(a1*z + a0)
******************************************************************************/
float control_filterIIR(Control_IIRfilter_s *filter, const float u_k);





#endif /* _FILTER_H_ */
