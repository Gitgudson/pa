/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     filter_types.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2016 Jan 14
* 
* @brief    Types defined for numerical filters
* 
* @copyright (C) Nicolas Broch
*
*
* This file defines the types used for numerical filters
* 
* This file is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
*
*/


#ifndef _FILTER_TYPES_H_
#define _FILTER_TYPES_H_

#include <typedefs.h> 

/***************************************************************************//*!
@brief          IIR filter parameter structure
******************************************************************************/
typedef struct{
	float y_k;   	//!< Current output value
	float y_k1;   	//!< Last iteration output value
	float x_k;   	//!< Current input value
	float x_k1;   	//!< Last iteration inpu value
	float b0a1;		//!< b1/a1 pre-computed
	float b1a1;		//!< b2/a1 pre-computed
	float a0a1;		//!< a2/a1 pre-computed
}Control_IIRfilter_data_s;

/***************************************************************************//*!
@brief          IIR filter parameter structure
******************************************************************************/
typedef struct{
	float a0;   //!< Parameter a1 of filter
	float a1;   //!< Parameter a2 of filter
	float b0;   //!< Parameter b1 of filter
	float b1;   //!< Parameter b2 of filter
}Control_IIRfilter_param_s;

/***************************************************************************//*!
@brief          IIR filter  structure
@details      This function computes a PI algorithm with anti-windup.\n
			  \f[
			  H(z) = \frac{Y(z)}{X(z)} = \frac{b1 + b2 \cdot z^{-1}}{a1 + a2 \cdot z^{-1}}
			  \f]
******************************************************************************/
typedef struct{
	Control_IIRfilter_param_s param;     //!< Parameters of IIR filter algorithm
	Control_IIRfilter_data_s  data;		 //!< Internal static data for the filter algorithm
}Control_IIRfilter_s;




#endif /* _FILTER_TYPES_H_ */
