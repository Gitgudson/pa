/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     MC33937A.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2016 Jan 25
* 
* @brief    Driver to use the H-bridge driver 33939A
* 
* @copyright (C) Nicolas Broch
*
*
* This driver offers functions to use the H-bridge driver 33939A.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 

* 
* 
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
*/


#ifndef _3937A_H_
#define _3937A_H_

#include "MPC5643L.h"
#include <typedefs.h> 

#include "drivers/pmsm_motors_types.h"
#include "drivers/pmsm_motors.h"
#include "HAL/mpc5643l_siul.h"
#include "HAL/mpc5643l_dspi.h"

//-----------------------------------------------------------------------------
// Mask of REG of MC33937A 
//-----------------------------------------------------------------------------
#define MOT_33937A_FULLON 0x02U	   //!< Mask of REG1 to check the state of deadtime control


//-----------------------------------------------------------------------------
// Pads for  H-bridges of the 2 motors
//-----------------------------------------------------------------------------
#define MOT01_33937A_RST PCR_A_10   //!< Pad of MCU which is connected to Reset pad of 33937A for motor 0

//-----------------------------------------------------------------------------
// Pads for Motor 0 H-bridge
//-----------------------------------------------------------------------------
#define MOT0_33937A_EN  PCR_A_11   //!< Pad of MCU which is connected to Enable pad of 33937A for motor 0

#define MOT0_33937A_PA_LS PCR_D_11 //!< Pad of MCU for PA_LS of motor 0
#define MOT0_33937A_PA_HS PCR_D_10 //!< Pad of MCU for PA_HS of motor 0
#define MOT0_33937A_PB_LS PCR_D_14 //!< Pad of MCU for PB_LS of motor 0
#define MOT0_33937A_PB_HS PCR_F_0  //!< Pad of MCU for PB_HS of motor 0
#define MOT0_33937A_PC_LS PCR_G_4  //!< Pad of MCU for PC_LS of motor 0
#define MOT0_33937A_PC_HS PCR_G_3  //!< Pad of MCU for PC_HS of motor 0


//-----------------------------------------------------------------------------
// Pads for Motor 1 H-bridge
//-----------------------------------------------------------------------------
#define MOT1_33937A_EN  PCR_A_9    //!< Pad of MCU which is connected to Enable pad of 33937A for motor 0

#define MOT1_33937A_PA_LS PCR_H_6  //!< Pad of MCU for PA_LS of motor 1
#define MOT1_33937A_PA_HS PCR_H_5  //!< Pad of MCU for PA_HS of motor 1
#define MOT1_33937A_PB_LS PCR_H_9  //!< Pad of MCU for PB_LS of motor 1
#define MOT1_33937A_PB_HS PCR_H_8  //!< Pad of MCU for PB_HS of motor 1
#define MOT1_33937A_PC_LS PCR_H_12 //!< Pad of MCU for PC_LS of motor 1
#define MOT1_33937A_PC_HS PCR_H_11 //!< Pad of MCU for PC_HS of motor 1

//-----------------------------------------------------------------------------
// SPI commands
//-----------------------------------------------------------------------------
#define MOT_33937A_CINT0 0x6FU     //!< Clear interrupts 0:0 to 0:3
#define MOT_33937A_CINT1 0x7FU     //!< Clear interrupts 1:0 to 1:3
#define MOT_33937A_DTOFF 0x42U	   //!< Command to desactivate the deatime
#define MOT_33937A_REG0R 0x00U	   //!< Command to reaad the status register 0
#define MOT_33937A_REG1R 0x01U	   //!< Command to reaad the status register 1

/*******************************************************************************
* Functions
*******************************************************************************/

/***************************************************************************//*!
@brief         Initialise the 2 33937A H-bridge drivers

@param		  motor		data structure of the motor

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If an error has occurred during pad writing.\n
			   -2 If an error has occurred during configuring deadtime.\n

@details       This function must be called before calling any other
   	   	   	   function of this library. This function must be called
	  	  	   before flexpwm_init() for normal initialisation.
	  	  	   This function configures the enable pad, but not the reset
	  	  	   pad because it can be common to different chips. But it
	  	  	   remove the reset signal on the pad.
******************************************************************************/
int32_t MC33937A_init(const Pmsm_motor_s *motor);

/***************************************************************************//*!
@brief         Enable the selected H-bridge 

@param		  motor		data structure of the motor

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If an error has occurred during pad writing.\n

@details       This function enables the selected motor's H-Bridge
******************************************************************************/
int32_t MC33937A_enable(const Pmsm_motor_s *motor);

/***************************************************************************//*!
@brief         Disable the selected H-bridge 

@param		  motor		data structure of the motor

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If an error has occurred during pad writing.\n

@details       This function disables the selected motor's H-Bridge
******************************************************************************/
int32_t MC33937A_disable(const Pmsm_motor_s *motor);

/***************************************************************************//*!
@brief        Clear faults from MC33937

@param		  motor		data structure of the motor

@return  	   void

@details      This function clears the faults from the MC33937 chip of the
			  selected board
******************************************************************************/
void MC33937A_clearFaults(const Pmsm_motor_s *motor);

/***************************************************************************//*!
@brief        Configure the deadtime of the given MC33937A

@param		  motor		data structure of the motor

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If the deadtime control disabled hasn't worked

@details      This function configures the deadtime of the given MC33937A chip
			  to be null
******************************************************************************/
int32_t MC33937A_setDeadTimeNull(const Pmsm_motor_s *motor);

/***************************************************************************//*!
@brief        Check faults MC33937A

@param		  motor0		data structure of the motor 0
@param		  motor1		data structure of the motor 1

@return  	   int32_t\n
			   0  If the chip doesn't have fault.\n
			   -1 If the chip 0 has a fault
			   -2 If the chip 1 has a fault
			   -3 If the chip 0 and 1 has a fault

@details      This function checks whether the chip MC33937A has a fault.
			  As this 
******************************************************************************/
int32_t MC33937A_checkFault(const Pmsm_motor_s *motor0, const Pmsm_motor_s *motor1);




#endif /* _3937A_H_ */
