/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     pmsm_currentVoltage.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2016 Jan 02
* 
* @brief    Library to measure currents and command voltage
* 
* @copyright (C) Nicolas Broch
*
*
* This library is part of the pmsmMotors library and allows to measure currents and command voltage.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* 
* 
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
* 
*
*/


#ifndef MPC5643L_PMSM_CURRENTMES_H_
#define MPC5643L_PMSM_CURRENTMES_H_

#include "MPC5643L.h"
#include <typedefs.h> 

#include "drivers/pmsm_motors.h"
#include "drivers/pmsm_motors_types.h"
#include "drivers/math_trigo.h"
#include "drivers/math_transforms.h"
#include "HAL/mpc5643l_etimer.h"
#include "HAL/mpc5643l_adc.h"




//-----------------------------------------------------------------------------
// Driver parameters
//-----------------------------------------------------------------------------


/*******************************************************************************
* Macros
*******************************************************************************/
#define PMSM_CURRENT_MID        0x07FF   //!< Value of ADC for 0.0A in resolver input

#define PMSM_CORR_OFFSET_M0_IA  0x0005U  //!< Value to add to Ia of motor 0 to remove the offset measure
#define PMSM_CORR_OFFSET_M0_IB  0x0009U  //!< Value to add to Ib of motor 0 to remove the offset measure
#define PMSM_CORR_OFFSET_M0_IC  0x0009U  //!< Value to add to Ic of motor 0 to remove the offset measure

#define PMSM_CORR_OFFSET_M1_IA  0x0011U  //!< Value to add to Ia of motor 1 to remove the offset measure
#define PMSM_CORR_OFFSET_M1_IB  0x0013U  //!< Value to add to Ib of motor 1 to remove the offset measure
#define PMSM_CORR_OFFSET_M1_IC  0x000CU  //!< Value to add to Ic of motor 1 to remove the offset measure



/*******************************************************************************
* Functions
*******************************************************************************/


/***************************************************************************//*!
@brief         Read the 3 phase-currents of each motor and convert them in
			   the different references

@param		   mot0		Pointer to the structure of the first motor data

@param		   mot1		Pointer to the structure of the second motor data


@return  		int32_t\n
				0 if no problem detected
				-1 if the data read in the ADC registers are not valid

@details       This function reads the values of the 3 phase-currents and
			   then converts them from abc reference to alpha-beta and then
			   in dq references. To convert in dq referential, the position
			   given by the Pmsm_motor_s structure is used.
******************************************************************************/
int32_t pmsmMotors_readCurrents(Pmsm_motor_s* mot0, Pmsm_motor_s* mot1);

/***************************************************************************//*!
@brief          Read voltage value in ADC and convert value into voltage value 

@param		   mot0		Pointer to the structure of the first motor data

@param		   mot1		Pointer to the structure of the second motor data

@return  	   int32_t\n
			   0 if no problem detected
			   -1 if the data read in the ADC registers are not valid

@details        This function reads the DCBUS measure of ADC and convert the 
				values into voltage values
******************************************************************************/
int32_t pmsmMotors_readVoltage(Pmsm_motor_s* mot0, Pmsm_motor_s* mot1);

/***************************************************************************//*!
@brief         Set up the 3-phases currents in function of the dq currents.

@param		   motor0		Pointer to the structure of the first motor data
@param		   motor1		Pointer to the structure of the second motor data

@return  	   void

@details       This function reads the values of the 3 phase-currents and
			   then converts them from abc reference to alpha-beta and then
			   in dq references. To convert in dq referential, the position
			   given by the Pmsm_motor_s structure is used.
******************************************************************************/
void pmsmMotors_setVoltage(Pmsm_motor_s *motor0, Pmsm_motor_s *motor1);


#endif /* MPC5643L_PMSM_CURRENTMES_H_ */
