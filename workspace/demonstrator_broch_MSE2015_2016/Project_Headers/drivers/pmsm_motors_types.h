/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     pmsm_motors_types.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2015 Dec 30
* 
* @brief    Types defined for the command of motors
* 
* @copyright (C) Nicolas Broch
*
*
* This file defines the types used to control a PMSM motor
* 
* This file is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* 
*
*/

#ifndef PMSM_MOTORS_TYPES_H_
#define PMSM_MOTORS_TYPES_H_

#include <typedefs.h> 

#include "HAL/mpc5643l_siul_types.h"
#include "HAL/mpc5643l_dspi_types.h"
#include "drivers/control_types.h"
#include "drivers/filter_types.h"
#include "drivers/pmsm_posSensors_types.h"


/***************************************************************************//*!
@brief          Enumeration to select the state of the current controller
******************************************************************************/
typedef enum{
	CTRL_OFF = 0, //!< disable control current system
	CTRL_ON  = 1  //!< enable control system
}Pmsm_ctrlState_e;

/***************************************************************************//*!
@brief          Enumeration to select a motor
******************************************************************************/
typedef enum{
	PMSM_motor0, //!< Select motor 0
	PMSM_motor1  //!< Select motor 1
}Pmsm_select_motor_e;

/***************************************************************************//*!
@brief          Data structure to config communication with MC33937A chip
******************************************************************************/
typedef struct{
	Dspi_nbr_e dspiCtrl;   //!< DSPI controller used to communicate
	Dspi_pcs_e dspiCS;     //!< Chip select for this MC33937A chip
	Siul_pcr_e padEnable;  //!< Pad to enable the chip
	Siul_pcr_e padReset;   //!< Configure the Pad Reset, by default, it's the same pad for each motor
	Siul_pcr_e padInterr;  //!< Interrupt pad
}Pmsm_mc33937a_s;


/***************************************************************************//*!
@brief          Data structure for configuration of the motor
******************************************************************************/
typedef struct{
	Siul_pcr_e padBrake;           //!< Pad on which the brake pad is connected
	Pmsm_mc33937a_s mc33937A;      //!< Configuration of MC33937A chip communication
}Pmsm_config_s;


/***************************************************************************//*!
@brief          Data structure for DCBUS voltage measure
******************************************************************************/
typedef struct{
	uint16_t vbus_adc;       //!< Raw value of voltage measure from ADC
	float    vbus_volt;      //!< Voltage of DCBus in [V]
	float    maxV_dq;		 //!< Maximal value for dq commands [V]
}Pmsm_vBus_s;

/***************************************************************************//*!
@brief          Data structure for currents/voltages in abc space
******************************************************************************/
typedef struct{
	int16_t a; //!< Raw current measure of phase A
	int16_t b; //!< Raw current measure of phase B
	int16_t c; //!< Raw current measure of phase B
}Pmsm_abc_raw_s;

/***************************************************************************//*!
@brief          Data structure for currents/voltages in abc space
******************************************************************************/
typedef struct{
	float a;       //!< Current of phase A [A] or [V]
	float b;       //!< Current of phase B [A] or [V]
	float c;       //!< Current of phase C [A] or [V]
}Pmsm_abc_float_s;

/***************************************************************************//*!
@brief          Data structure for currents/voltages in alpha-beta space
******************************************************************************/
typedef struct{
	float alpha;  //!< Current alpha [A] or [V]
	float beta;   //!< Current beta  [A] or [V]
}Pmsm_alphaBeta_s;

/***************************************************************************//*!
@brief          Data structure for currents/voltages in dq space
******************************************************************************/
typedef struct{
	float d;   //!< Current d [A] or [V]
	float q;   //!< Current q [A] or [V]
}Pmsm_dq_s;

/***************************************************************************//*!
@brief          Data structure for currents measures
******************************************************************************/
typedef struct{
	Pmsm_abc_raw_s Iabc_raw;
	Pmsm_abc_float_s Iabc;
	Pmsm_alphaBeta_s IalphaBeta;
	Pmsm_dq_s Idq;
}Pmsm_current_s;

/***************************************************************************//*!
@brief          Data structure for voltage commands
******************************************************************************/
typedef struct{
	Pmsm_abc_float_s Uabc;
	Pmsm_alphaBeta_s UalphaBeta;
	Pmsm_dq_s Udq;
}Pmsm_voltageOutput_s;

/***************************************************************************//*!
@brief          Data structure for commands
******************************************************************************/
typedef struct{
	volatile float Id;		// current for field weakening in [A]
	volatile float Iq;      // current for torque control in [A]
	volatile float omega;   // angular speed in rad
	volatile float alpha;   // angular position in rad
}Pmsm_controlCommands_s;


/***************************************************************************//*!
@brief          Data structure to configure control state
******************************************************************************/
typedef struct{
	volatile Pmsm_ctrlState_e state;			//!< state of the position controller
	float speedCommand;							//!< speed command [rad/s]
	float outputTorque;							//!< torque command [Nm]
	Control_piControl_s regPI;					//!< control of position loop
}Pmsm_ctrlSpeed_s;

/***************************************************************************//*!
@brief          Data structure for current regulator
******************************************************************************/
typedef struct{
	volatile Pmsm_ctrlState_e state;		//!< state of the current controller
	Pmsm_voltageOutput_s outputV;			//!< voltage commands
	Pmsm_dq_s cCurrent;                     //!< current command at entry of current control system
	Control_piControl_s controlD;           //!< control of D current loop
	Control_piControl_s controlQ;           //!< control of Q current loop
}Pmsm_ctrlCurrent_s;

/***************************************************************************//*!
@brief          Data structure for sensor measures
******************************************************************************/
typedef struct{
	Pmsm_sensorsPosition_s pos;				//!< Data of position and speed sensors
	Pmsm_vBus_s    dcBus;					//!< Data of DCBus
	Pmsm_current_s currents;  				//!< Currents measured of the motor in different spaces
}Pmsm_measure_s;


/***************************************************************************//*!
@brief          Data structure for 1 PMSM motor
******************************************************************************/
typedef struct{
	Pmsm_config_s config;					//!< List of pads to control the Motor
	Pmsm_measure_s meas;					//!< Measures of sensors			
	Pmsm_ctrlCurrent_s ctrlCurrent;         //!< Data for current control
	Pmsm_ctrlSpeed_s ctrlSpeed;				//!< Data for position control
}Pmsm_motor_s;




#endif /* PMSM_MOTORS_TYPES_H_ */
