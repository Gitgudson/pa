/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     pmsm_posSensors.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2016 Jan 02
* 
* @brief    Library to use the positions sensors of the 2 PMSM motors.
* 
* @copyright (C) Nicolas Broch
*
*
* This library is part of the pmsmMotors library and allows to configure and use the resolvers 
* and the quadrature encoders of the 2 PMSM motors
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* 
* 
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
* 
*
*/


#ifndef MPC5643L_PMSM_POSSENSORS_H_
#define MPC5643L_PMSM_POSSENSORS_H_

#include "MPC5643L.h"
#include <typedefs.h> 

#include "drivers/pmsm_motors.h"
#include "drivers/pmsm_posSensors_types.h"
#include "drivers/math_trigo.h"
#include "HAL/mpc5643l_etimer.h"
#include "HAL/mpc5643l_adc.h"




//-----------------------------------------------------------------------------
// Driver parameters
//-----------------------------------------------------------------------------
#define PMSM_RES_MID     0x07FF   //!< Value of ADC for 0V in resolver input
#define PMSM_TOL_NULL_RES 20	  //!< Tolerance around PMSM_RES_MID to consider measure of Resolver as null
#define PMSM_LIM_ERR_SENS 0.1	  //!< Limit of error between the 2 sensors to consider that there is a fail of 1 sensor




/*******************************************************************************
* Macros
*******************************************************************************/

/*******************************************************************************
* Functions
*******************************************************************************/

/***************************************************************************//*!
@brief         Initialise parameters for position sensors structure

@param		   mot				Pointer to the structure of the first motor data
@param		   encETimerReg		Register in eTimer in which is saved the position of the encoder
@param		   maxValueEnc		The encoder register has values between -maxValueEnc and maxValueEnc
@param		   resADCReg_sin	ADC register in which is saved the measure of sine sense
@param		   resADCReg_cos	ADC register in which is saved the measure of cosine sense
@param		   Fe_pos			Sample frequency of position measure
@param		   paramSpeedFilter	Parameters of filter on speed signal

@return  	   void

@details       This function configures the data structure of a motor position
			   sensors.
******************************************************************************/
void pmsmMotors_initPos(Pmsm_sensorsPosition_s* mot,
						const vuint16_t *encETimerReg, const float maxValueEnc,
						const vuint32_t *resADCReg_sin, const vuint32_t *resADCReg_cos,
						const float Fe_pos, const Control_IIRfilter_param_s *paramSpeedFilter);


/***************************************************************************//*!
@brief         Update the value of the 2 encoders in the motor data structure

@param		   mot0		Pointer to the structure of the first motor data

@param		   mot1		Pointer to the structure of the second motor data

@return  	   void

@details       This function reads the values of the 2 quadrature encoders and
			   save the result into the motor structures pmsm_s* given. This
			   function is useful to synchronise the reading of the position
			   of the 2 sensors. Otherwise, as there is not hardware trigger,
			   a jitter occurs between the reading of the 2 motor sensors
******************************************************************************/
void pmsmMotors_read2Enc(Pmsm_sensorsPosition_s* mot0, Pmsm_sensorsPosition_s* mot1);

/***************************************************************************//*!
@brief         Update the value of the 1 encoder in the motor data structure

@param		   mot		Pointer to the structure of the motor position sensors

@return  	   void

@details       This function reads the values of the 1 quadrature encoders and
			   save the result into the motor structures pmsm_s* given.
******************************************************************************/
void pmsmMotors_readEnc(Pmsm_sensorsPosition_s* mot);


/***************************************************************************//*!
@brief         Update the value of the resolver in the motor data structure

@param		   mot		Pointer to the structure of the motor data sensors



@return  		int32_t\n
				0 if no problem detected
				-1 if the data read in the ADC registers are not valid
				-2 if both SIN and COS signal of the resolver are null.

@details       This function reads the values of the 2 resolvers and
			   save the result into the motor sensors structure given.
******************************************************************************/
int32_t pmsmMotors_readResolv(Pmsm_sensorsPosition_s* mot);


/***************************************************************************//*!
@brief         Define the current angle in the system point of view

@param		   mot		Pointer to the structure of the first motor data

@param		   angle_mot	Current motor angle of the system

@return  		void

@details       Configure the sensors and functions for "homing". After this
			   function, the sensors will consider the current position as
			   the angle "angle". This function must be called when the motor
			   is stopped to be useful.\n
			   This function doesn't perform a measurement of resolvers. So,
			   a previous measure of the resolver position must be performed
			   before calling this function!
******************************************************************************/
void pmsmMotors_setAngleSys(Pmsm_sensorsPosition_s* mot, float angle_mot);


/***************************************************************************//*!
@brief         Define angular position and speed of sensor
				
@param		   mot			Pointer to the structure of the PMSM motor

@return  		0  if there is no error
				-1 if the state machine of position has an error
				-2 if the 2 position sensor give different positions

@details        Select which sensor is used for position and compute the
				speed of motor
******************************************************************************/
int32_t pmsmMotors_setPositionSpeed(Pmsm_sensorsPosition_s *mot);


#endif /* MPC5643L_PMSM_POSSENSORS_H_ */
