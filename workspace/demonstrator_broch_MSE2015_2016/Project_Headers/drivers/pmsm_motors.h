/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
*
* @file     pmsm_motors.h
*
* @author   Nicolas Broch
* 
* @version  0.1
* 
* @date     2016 Jan 02
* 
* @brief    Driver to configure and command the 2 PMSM motors of the board
* 			MTRCKTDPS5643L.
* 
* @copyright (C) Nicolas Broch
*
*
* This driver allows to configure and use the 2 PMSM motors with SIN/COS resolvers and
* encoders.
* 
* This HAL is only compatible with the Freescale MPC5643L MCU, cut 2 or higher.
* 
* How to use it
* -------------
* 
* Without any information, the function of this library are acting on the 2 
* The function pmsmMotors_init() must be called first during init. Then, the function
* pmsmMotors_setAngle() can be used for homing of the 2 sensors of each motor (set the
* angle 0).
* 
* The function pmsmMotors_readEnc() and pmsmMotors_readResolv() can be used during
* runtime to update the position pos_rad in the data motor structures.
* 
* To not generate an error, the interrupt number 206 "CTU_0 - ADC_I" must be implemented in
* the system at priority 10 for the current control loop.
* 
* The interrupt 206 is generated after conversion of the resolvers signals but before the
* conversion of currents and voltages measures. This is proposed to compute the angular
* position of the rotor while the ADCs converts the other signals. To know when the
* other signals are converted, use the function pmsmMotors_endOfConv().
* 
* The function ctu_IRQ_adc_ack() must be called to acknowledge the interrupt.
* 
* 
* Pad configured in this library
* ------------------------------
* 
* |Pad       | Function                   | When is it configure?
* |:---------|:---------------------------|:------------------------------
* 
*
*/


#ifndef MPC5643L_PMSM_MOTORS_H_
#define MPC5643L_PMSM_MOTORS_H_

#include "MPC5643L.h"
#include <typedefs.h> 

#include "drivers/pmsm_motors_types.h"
#include "drivers/pmsm_posSensors.h"
#include "drivers/pmsm_currentVoltage.h"
#include "drivers/MC33937A.h"
#include "drivers/control.h"
#include "drivers/filter.h"

#include "drivers/math_trigo.h"

#include "HAL/mpc5643l_ctu.h"
#include "HAL/mpc5643l_adc.h"
#include "HAL/mpc5643l_flexpwm.h"
#include "HAL/mpc5643l_etimer.h"
#include "HAL/mpc5643l_siul.h"
#include "HAL/mpc5643l_dspi.h"



/*******************************************************************************
* Global variables
*******************************************************************************/
extern Pmsm_motor_s motorPMSM0; //!< Data structure for motor 0
extern Pmsm_motor_s motorPMSM1; //!< Data structure for motor 1
extern uint32_t nbIterIRQ;		//!< Number of iteration of the control IRQ (counted with function pmsmMotors_endOfConv() calls)


//-----------------------------------------------------------------------------
// Driver parameters
//-----------------------------------------------------------------------------
// Motor parameters
#define PMSM_NUM      2U				//!< Number of motor controlled by this library
#define PMSM_NB_POLES 3U				//!< Number of pair of poles of each motor
#define PMSM_KT_CORR  0.0F				//!< Value to correct the Kt in order to have static gain in current loop of 1
#define PMSM_KT       0.044F			//!< Torque constant of the PMSM motors
#define PMSM_KT_INV   22.73F			//!< Inverse of the torque constant of the PMSM motors
#define PMSM_MAX_I    5.0F  			//!< Max current command to be applied to motor

// Error parameters
#define PMSM_DIFF_P_ERR 0.017F	//!< Maximal allowed value between 2 position sensors before considering an error

// DCBus scale
#define DCBUS_SENS_SCALE   36UL     	   //!< Value of DCBus in [V] when raw ADC measure = 0xFFF.\n This value is used to convert the raw measure of DCBUS into DCBUS voltage
#define DCBUS_MAX_VOLTAGE  28.0F		   //!< Maximum voltage allowed on DC-bus [V]
#define MAX_PHASE_VOLT     12.0F		   //!< Maximum voltage on a phase [V]

// Encoder steps per half-rotation
#define PMSM_ENC_PRF     2048.0F  //!< Number of steps of encoders per half rotation in floating point
#define PMSM_ENC_PRI     2048U    //!< Number of steps of encoders per half rotation in integer

// Current sensor scale
#define CURRENT_SENS_MAX 10.0F        //!< Value of Current in [A] when raw ADC measure = 0xFFF.\n This value is used to convert the raw measure in measure in amperes

// Channels which are converted in each ADC
#define PMSM_ADC0_CONV_CH 0x0000204FUL //!< ADC0, channels converted during 1 cycle. The structure follows the register ADC_CEOCFR0, MPC5643LRM 8.3.3.2
#define PMSM_ADC1_CONV_CH 0x00005845UL //!< ADC1, channels converted during 1 cycle. The structure follows the register ADC_CEOCFR0, MPC5643LRM 8.3.3.2


//-----------------------------------------------------------------------------
// ADC register of measures values
//----------------------------------------------------------------------------
// resolvers
#define PMSM_ADC_RESOL0_SIN (&ADC0.CDR0.R)   //!< Resolver of motor 0, SIN signal
#define PMSM_ADC_RESOL0_COS (&ADC1.CDR0.R)   //!< Resolver of motor 0, COS signal
#define PMSM_ADC_RESOL1_SIN (&ADC0.CDR6.R)   //!< Resolver of motor 1, SIN signal
#define PMSM_ADC_RESOL1_COS (&ADC1.CDR6.R)   //!< Resolver of motor 1, COS signal

#define PMSM_RESOL0_ALIGN_V  0.0155F		 //!< Value for alignment of magnets on resolver of motor 0
#define PMSM_RESOL1_ALIGN_V  -1.87855F		 //!< Value for alignment of magnets on resolver of motor 1

// DCBUS
#define DCBUS0     (&ADC0.CDR1.R)   //!< DCBus voltage measure of motor 0
#define DCBUS1     (&ADC0.CDR3.R)   //!< DCBus voltage measure of motor 0

// currents
#define MOT0_IA    (&ADC1.CDR11.R)  //!< Current of phase A of motor 0
#define MOT0_IB    (&ADC1.CDR12.R)  //!< Current of phase B of motor 0
#define MOT0_IC    (&ADC0.CDR2.R)   //!< Current of phase C of motor 0

#define MOT1_IA    (&ADC0.CDR13.R)  //!< Current of phase A of motor 1
#define MOT1_IB    (&ADC1.CDR14.R)  //!< Current of phase B of motor 1
#define MOT1_IC    (&ADC1.CDR2.R)   //!< Current of phase C of motor 1

//-----------------------------------------------------------------------------
// eTimer register of encoders
//----------------------------------------------------------------------------
#define PMSM_ETIMER_ENC0 (&ETIMER_0.CNTR0.R) //!< Encoder of motor 0
#define PMSM_ETIMER_ENC1 (&ETIMER_1.CNTR0.R) //!< Encoder of motor 1



//-----------------------------------------------------------------------------
// Current control parameters
//-----------------------------------------------------------------------------
#define PMSM_TE_I    1e-4F   //!< Period of current loop
#define PMSM_FE_I    1e4F    //!< Frequency of current loop = 1/PMSM_TE_I

#define PMSM_KP_IQ  1.26F    //!< Kp parameter of current controller Q
#define PMSM_GI_IQ  1909.0F  //!< Gi = 1/Ti parameter of current controller Q

#define PMSM_KP_ID  1.26F     //!< Kp parameter of current controller D
#define PMSM_GI_ID  1909.0F  //!< Gi = 1/Ti parameter of current controller D

//-----------------------------------------------------------------------------
// Speed control parameters
//-----------------------------------------------------------------------------
#define PMSM_TE_IT_W 10U	   //!< Number of Current iteration for 1 speed iteration

#define PMSM_TE_W    1e-3F     //!< Period of position loop
#define PMSM_FE_W    1e3F      //!< Frequency of position loop = 1/PMSM_TE_P
#define PMSM_KP_W    0.012F    //!< Kp parameter of current controller
#define PMSM_GI_W    0.1F   //!< Gi = 1/Ti parameter of current controller


//-----------------------------------------------------------------------------
// Filter on speed measure
//-----------------------------------------------------------------------------
#define PMSM_SPEED_FILT_A0  -0.8465F //!< A1 parameter of speed filter
#define PMSM_SPEED_FILT_A1  1.0F     //!< A2 parameter of speed filter
#define PMSM_SPEED_FILT_B0  0.1535F  //!< B1 parameter of speed filter
#define PMSM_SPEED_FILT_B1  0        //!< B2 parameter of speed filter



/*******************************************************************************
* Functions
*******************************************************************************/

/***************************************************************************//*!
@brief         Configure the 2 motors

@return  	   int32_t\n
			   0  If the success was successful.\n
			   -1 If an error occurred during 33937A initialisation

@details       This function must be called before calling any other
   	   	   	   function of this library. It configures the 2 PMSM motors to
   	   	   	   be able to control them.
@warning	   The system interrupts can occurred before the end of this function,
			   please add a system mechanisms to avoid to run the control
			   interrupt before the end of this function
******************************************************************************/
int32_t pmsmMotors_init();


/***************************************************************************//*!
@brief         Configure the speed controller for 1 PMSM motor

@param		   motor		Pointer to the structure of the PMSM motor

@return  	   void

@details       This function configures the speed controller for 1 motor

******************************************************************************/
void pmsmMotors_controlWInit(Pmsm_motor_s *motor);

/***************************************************************************//*!
@brief         Configure the current controller for 1 PMSM motor

@param		   motor		Pointer to the structure of the PMSM motor

@return  	   void

@details       This function configures the current controller for 1 motor

******************************************************************************/
void pmsmMotors_controlIInit(Pmsm_motor_s *motor);

/***************************************************************************//*!
@brief         Configure the system of the position and speed sensors of
			   the 2 motors;

@return  	   void

@details       This function configures the position and speed sensors

******************************************************************************/
void pmsmMotors_posSpeedInit();

/***************************************************************************//*!
@brief         Compute the speed control algorithm for 1 motor

@param		   motor		Data and parameters of the motor
@param		   angle		Angle command in [rad] on 1 turn

@return  		0  if the controller works correctly
				-1 if an error occured

@details       Compute the current control algorithm for 1 motor

******************************************************************************/
int32_t pmsmMotors_speedControl(Pmsm_motor_s *motor, float speed);

/***************************************************************************//*!
@brief         Compute the torque control algorithm for 1 motor

@param		   motor		Data and parameters of the motor
@param		   torque		Torque command of the motor [Nm]
@param		   Id			Current command to reduce magnetic field of magnets

@return  		0  if the controller works correctly
				-1 if an error occurred

@details       Compute the torque control and Id control algorithms for 1 motor

******************************************************************************/
int32_t pmsmMotors_torqueControl(Pmsm_motor_s *motor, float torque, 
		                          float Id);


/***************************************************************************//*!
@brief          End of CTU_0 ADC_I interrupt

@return  		void

@details        This function must be called at the end of the CTU_0 - ADC_I
				interrupt function to clear interrupt flags and reset registers.
******************************************************************************/
void pmsmMotors_endIRQ();

/***************************************************************************//*!
@brief          Check if the ADC conversions are finished.

@return  		0 if the conversions are not finished
				1 if the conversions are finished

@details        This function checks if all the ADC conversions are over.
******************************************************************************/
int32_t pmsmMotors_endOfConv();

/***************************************************************************//*!
@brief          Check if the DC-bus voltage is over DCBUS_MAX_VOLTAGE and
				activate braking resistor if it is the case

@return  		0 if the DC-bus if below DCBUS_MAX_VOLTAGE
				1 if the DC-bus if over or equal to DCBUS_MAX_VOLTAGE

@details        If the DC-bus voltage is over DCBUS_MAX_VOLTAGE, activate
				the braking resistor. This function must be called once
				per current control loop.
******************************************************************************/
int32_t pmsmMotors_checkDCbusVoltage(Pmsm_motor_s *motor);




#endif /* MPC5643L_PMSM_MOTORS_H_ */
