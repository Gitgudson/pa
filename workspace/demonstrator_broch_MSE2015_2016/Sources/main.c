/******************************************************************************
*
* Master thesis of Nicolas Broch
* Development of Demonstrator of Lockstep Microcontroller for Automotive 
* Application (DLMAA)
*
***************************************************************************//*!
 *
 *  Created on        : 2015 Oct 27
 *	Last modification : 2019 May 5
 *      Author        : Nicolas Broch
 *      Author		  : Jan Huber
 */

#include "MPC5643L.h"

#include "HAL/mpc5643l_hal.h"
#include "HAL/mpc5643l_siul.h"
#include "drivers/pmsm_motors.h"
#include "drivers/button.h"
#include "system_control.h"
#include "error_codes.h"
#include "state_machine.h"
#include "scheduler.h"
#include "task.h"


//-----------------------------------------------------------------------------
// LEDs pads
//-----------------------------------------------------------------------------
#define LED_SAFE PCR_A_13    //!< Pad of MCU to indicate that the MCU is in error
#define LED_OKAY PCR_A_12    //!< Pad of MCU to indicate that the MCU runs without failure

//-----------------------------------------------------------------------------
// Speed command tables
//-----------------------------------------------------------------------------
#define MOTOR_NB_COMMAND  11
#define MOTOR_I_COMMAND_0 5

float command_m0[MOTOR_NB_COMMAND] = { 100.0F,  60.0F,  40.0F,  15.0F,   5.0F,  0.0F,  -5.0F, -15.0F,  -40.0F, -60.0F, -100.0F};
float command_m1[MOTOR_NB_COMMAND] = {-133.3F, -80.0F, -53.3F, -20.0F, -6.67F,  0.0F,  6.67F,  20.0F,   53.3F,  80.0F,  133.3F};

static indexCommand = MOTOR_I_COMMAND_0; //!< Index of the current speed command

//-----------------------------------------------------------------------------
// State of buttons/switch
//-----------------------------------------------------------------------------
static uint32_t stateSwitch = 0;
static uint32_t stateUpButton = 0, prevStateUpBut = 0;
static uint32_t stateDownButton = 0, prevStateDownBut = 0;

//-----------------------------------------------------------------------------
// External structure for motors
//-----------------------------------------------------------------------------
extern Pmsm_motor_s motorPMSM0;
extern Pmsm_motor_s motorPMSM1;

//-----------------------------------------------------------------------------
// State machine
//-----------------------------------------------------------------------------
state_machine_e sysState;

void state_align();
void state_ctrl_2_mot();
void state_error();

//-----------------------------------------------------------------------------
// Task table
//-----------------------------------------------------------------------------
#define MAX_NBER_OF_TASKS 10

static TaskStruct_t taskTable[MAX_NBER_OF_TASKS];

void fsm_task(void*);


int main(void) {
	volatile uint32_t i = 0;
	int32_t test = 0;
	uint16_t  spiCommand   = 0; // command to send
	uint32_t  spiReceive32 = 0; // received command in 32 bits
	int32_t errorCode = 0; 		// variable to stock the return code of functions
	uint32_t lastCnt = 0;       // value of IRQ counter to to a tempo
	
	/*====================================================
	 Initialise the state machine
	====================================================*/
	sysState = STATE_INIT;
	
	/*====================================================
	 Configure pad to say that the system init is done
	====================================================*/
	siul_config_pad(LED_OKAY, HAL_YES, HAL_NO,
				SIUL_MODE_GPIO, SIUL_OUTPUT,
				SIUL_SLOW, SIUL_TYPE_NONE, HAL_YES);

	/*====================================================
	  Initialise the basis of HAL layer
	====================================================*/
	hal_init_sys(LED_SAFE);
	
	/*====================================================
	  Initialise the system level of motor controllers
	====================================================*/
	system_control_init();
	
	/*====================================================
	  Configure the motor control system
	====================================================*/
	errorCode = pmsmMotors_init();
	
	//DEBUG
	//errorCode = 0;
	
	/*====================================================
	  An error has occurred, shows information to user
	====================================================*/
	if(errorCode != 0)
	{
		/*====================================================
		  Reset MCU in error mode
		====================================================*/
		mode_sw_error();
	}
	/* no error */
	else
	{
		/*====================================================
		  Enable interrupts
		====================================================*/
		interrupts_enable();
		
		/*====================================================
		Indicate a end of initialisation without error
		====================================================*/
		//siul_write_fast_pad(LED_OKAY, SIUL_HIGH);
		
		/*====================================================
		Once here, the state machine can be changed to
		STATE_ALIGN state.
		====================================================*/
		sysState = STATE_ALIGN;
		
		/*====================================================
		Initialise the scheduler
		====================================================*/
	    schedulerInit(taskTable, MAX_NBER_OF_TASKS);

		/*====================================================
		Add tasks
		====================================================*/
	    schedulerAddTask(fsm_task, 1, NULL_PTR);
	    schedulerAddTask(freq_meas_task, 10000, NULL_PTR);
	    //schedulerAddTask(test_task, 1000, NULL_PTR);
	    //schedulerAddTask(nce_injection_task, 5000, NULL_PTR);
	    //schedulerAddTask(obe_injection_task, 5000, NULL_PTR);
	    //schedulerAddTask(ecsm_err_check, 5000, NULL_PTR);
	}

	lastCnt = 0;
	
	/* Loop forever */
	for(;;) {
		/* Tempo */
		while(nbIterIRQ - lastCnt < 10) {}
		lastCnt = nbIterIRQ; // update lastCnt for temporisation
		
		// Run the scheduler
		schedulerRun();
	}
	  
}

void fsm_task(void*)
{
	if(sysState == STATE_ALIGN)
	{
		state_align();
	}
	else if(sysState == STATE_CTRL_2_MOT)
	{
		state_ctrl_2_mot();
	}
	else if(sysState == STATE_ERROR)
	{
		state_error();
	}
}

void state_align()
{
	/*====================================================
	  Align the magnets with the sensors angular position
	  Wait at least 5 control IRQ iteration before
	  initialising the magnet alignment
	====================================================*/
	if(nbIterIRQ > 5)
	{
		/*
		 * set the current position in such way that the "angle0" parameter
		 * is equal to PMSM_RESOL0_ALIGN_V
		 */ 
		pmsmMotors_setAngleSys(&motorPMSM0.meas.pos, (floatmod(-PMSM_RESOL0_ALIGN_V + 
				               motorPMSM0.meas.pos.resolver.meas.pos_rad_raw + PI, 2 * PI) - PI));
		pmsmMotors_setAngleSys(&motorPMSM1.meas.pos, (floatmod(-PMSM_RESOL1_ALIGN_V + 
				               motorPMSM1.meas.pos.resolver.meas.pos_rad_raw + PI, 2 * PI) - PI));
		
		/*====================================================
		  Set the resolver as position sensor
		====================================================*/
		motorPMSM0.meas.pos.posAndSpeed.state = PMSM_POS_RESOLVER;
		motorPMSM1.meas.pos.posAndSpeed.state = PMSM_POS_RESOLVER;
		
		/*====================================================
		  Switch to next state with error mode handled
		====================================================*/
		if(critFaultM0 == 0 && critFaultM1 == 0){ // no error
			sysState = STATE_CTRL_2_MOT;
		}
		else{ // fault in both motors
			sysState = STATE_ERROR;
		}
	}
}

void state_ctrl_2_mot()
{
	static firstEntry = 0;    // variable to know if it's the first time this function is called
	int32_t errorHBridge = 0; // variable to save the number of h-bridge driver in error
	
	/*====================================================
	  Entry in this state - Set all commands to 0
	====================================================*/
	if(firstEntry == 0)
	{
		indexCommand = MOTOR_I_COMMAND_0;
		
		id_motor0 = 0.0F;
		id_motor1 = 0.0F;
		t_motor0  = 0.0F;
		t_motor1  = 0.0F;
		w_motor0  = command_m0[indexCommand];
		w_motor1  = command_m1[indexCommand];
		
		firstEntry = 1;
	}
	else{} // MISRA-C compliance
	
	/*====================================================
	  Read switch and buttons
	====================================================*/
	prevStateUpBut   = stateUpButton;   // save the previous state to have rising or falling detection
	prevStateDownBut = stateDownButton; // save the previous state to have rising or falling detection
	stateSwitch      = button_state(&switchOnOff);
	stateUpButton    = button_state(&buttonUp);
	stateDownButton  = button_state(&buttonDown);
	
	/*====================================================
	  Enable the current and speed controllers if switch
	  in on, disable controllers if switch off
	====================================================*/
	if(stateSwitch == 0){
		// start controllers
		motorPMSM0.ctrlCurrent.state = CTRL_ON;
		motorPMSM1.ctrlCurrent.state = CTRL_ON;
		motorPMSM0.ctrlSpeed.state = CTRL_ON;
		motorPMSM1.ctrlSpeed.state = CTRL_ON;
		
		// start PWM
		flexpwm_run(FLEXPWM_ALL);
		
		/*====================================================
		  Change speed command regarding the state of 
		  pushbuttons
		====================================================*/
		// falling edge pushbutton up
		if(stateUpButton == 0 && prevStateUpBut > 0 && stateDownButton > 0)
		{
			if(indexCommand < (MOTOR_NB_COMMAND - 1))
			{
				indexCommand++;
			}
		}
		// falling edge pushbutton down
		else if(stateUpButton > 0 && stateDownButton == 0 && prevStateDownBut > 0)
		{
			if(indexCommand > 0)
			{
				indexCommand--;
			}
		}
		else{} // nothing to do if both buttons are pressed
		
		/*====================================================
		  Update speed command
		====================================================*/
		w_motor0  = command_m0[indexCommand];
		w_motor1  = command_m1[indexCommand];
	}
	else
	{
		/*====================================================
		  Stop controller
		====================================================*/
		// stop PWM
		flexpwm_stop(FLEXPWM_ALL);
		
		motorPMSM0.ctrlCurrent.state = CTRL_OFF;
		motorPMSM1.ctrlCurrent.state = CTRL_OFF;
		motorPMSM0.ctrlSpeed.state = CTRL_OFF;
		motorPMSM1.ctrlSpeed.state = CTRL_OFF;
		
		/*====================================================
		  Reset controller commands
		====================================================*/
		indexCommand = MOTOR_I_COMMAND_0;
		w_motor0  = command_m0[indexCommand];
		w_motor1  = command_m1[indexCommand];
	}
	
	/*====================================================
	  Check whether one H-bridge driver is in error mode
	====================================================*/
	errorHBridge = MC33937A_checkFault(&motorPMSM0, &motorPMSM1);
	
	/*====================================================
	  Next state - error state
	====================================================*/
	if(critFaultM0 > 0 || critFaultM1 > 0 || errorHBridge != 0){
		sysState = STATE_ERROR;
	}
	else{} // MISRA-C compliance
}

void state_error()
{
	/*====================================================
	  Stop controller
	====================================================*/
	// stop PWM
	flexpwm_stop(FLEXPWM_ALL);
	
	motorPMSM0.ctrlCurrent.state = CTRL_OFF;
	motorPMSM1.ctrlCurrent.state = CTRL_OFF;
	motorPMSM0.ctrlSpeed.state = CTRL_OFF;
	motorPMSM1.ctrlSpeed.state = CTRL_OFF;
	
	/*====================================================
	  Reset controller commands
	====================================================*/
	indexCommand = MOTOR_I_COMMAND_0;
	w_motor0  = command_m0[indexCommand];
	w_motor1  = command_m1[indexCommand];
	
	/*====================================================
	  Reset the MCU in error mode
	====================================================*/
	mode_sw_error();
}


