# PA notes

### Week 1
* Definition of objectives
* Planning, tasks and time necessary for tasks
* GIT
* Critical stuff for safety
* Use mechanisms to protect against faults
* IEEE publication (accessible on NEBIS)
* Fix norm level (ag. A/B/C/D)
  * Implement functions/solutions/mechanisms
* Standards, non-interference (spacial, temporal, execution?)


### Week 2
* All the code for demonstrator was created by master Student
  * Why?
  * compare with code from microcontroller manufacturer
* Mechanisms implemented for the norm are described in microcontroller datasheet
* Send invites for weekly reunions!
* Add mile stones to the Planning
* Failure rate does not need to be calculated (focus more on SW-HW)
* Norms in ROSAS cloud
  * ISO 26262
    * Part 5, p. 52
  * IEC 615080
    * Part 2, p. 54
    * Part 3, Annex F (non.interference)
* Talk about MISRAC (use, principles, etc.)
* Use C as a programming language (apparently not recommended by ISO norm)
* List of processors
* Mechanisms implemented for norm


### Week 3
* GIT Jean-Roland Schuler
* E-Mail invitation
  * What was done ?
  * What to discuss
  * GIT link to Weekly Report
* Look up Memory Protection Unit region (MPU)
* Explain (difference) of the features of the MCUs
* E-Mail for work place


## Week 4
* Check registers with compiler?
* Check stack over/under flow with compiler?
  * Problem with tow buffers (only first one is protected)
    ---------------
    |   canary    |
    ---------------
    |   buffer 1  |
    ---------------
    |   buffer 2  |
    ---------------
* Choose necessary safety methods to implement
  * Which were already implemented?
  * Which only require configuration of the MCU?
  * Which need to implemented in SW?
  * Which depend on external HW?
* Check for MISRA C compliance
  * [CppCheck](https://en.wikipedia.org/wiki/Cppcheck)
    * [Eclipse plugin](https://github.com/kwin/cppcheclipse)


## Week 5
* Further tools to check for MISRA C compliance
  * Mathworks Polyspace
    * [Code Prover](https://ch.mathworks.com/de/products/polyspace-code-prover.html)
    * [Bug Finder](https://ch.mathworks.com/de/products/polyspace-bug-finder.html)
  * [PC-Lint](https://www.gimpel.com/)
* How does the Scheduler prioritise tasks?
* How is the memory protected (ECC)?
  * Does it need to be activated?
  * Is it integrated in the memory?
* Find example program for demonstrator
* Make list of tools used during project
  * Version
  * Purpose
  * Classify from T1 to T3
    * T1: No influence on final code/product
    * T3: Major influence on final code/product
* Next PA reunion at ROSAS
* Can't connect demonstrator board to PC
  * Try with USB to CAN/LIN/Serial
  * Try with new JTAG debugger


## Week 6
* Scheduler / Task manager
  * Periodic Interrupt Timer
* CRC
* ECC
* oscillator supervision

## Week 7
* Code loads into RAM or Flash?
* Inform about state of project/demonstrator
* Write annex for demonstrator
  * How to connect/power?
  * How to load program?
* JTAG debugger software
  * [Multilink](http://www.pemicro.com/multilink/)
    * Quick Downloads -> Software & Resources
  * [PEDriver](http://www.pemicro.com/osbdm/)
    * Install drivers
  * [Firmware](http://www.pemicro.com/osbdm/)
    * Firmware Updates & Recovery

## Week 8
* List of tools used
Tool | Manufacturer | Impact (T1-T3) | Use | Why acceptable?
* Scheduler
  * Execution time
  * Priorities
* Tasks
  * Execution time
* Documentation for each mechanism
  * Why implemented ?
  * How is it implemented ?
  * Does it work ?
* CodeWarrior License
  * Check before Friday
  * Else reinstall or install new version
  * Else ask if license available at school
  * Else buy license
* Implement mechanisms for after Easter

## Week 9
* Verify which input clock is used for the PIT module
  * Determine systick period in function of this
* Verify execution time of tasks
  * Use oscilloscope
  * Compare with SW measurement?
* Scheduler priorities
  * Use position in task table, first task is executed first
  * Use execution periods with no common denominators
* PIT uses "peripheral set 0 clock"
  * defined as 60 MHz in "clock_.xlsx"
  * LDVAL = (period/clock_period)-1 = 59'999 = 0xEA5F
* Broch report p.80 and p.186

## Week 10
* Can take measurements with a SALEAE Logic Analyzer
    * There's one at Rosas
* More precise planning for the end of the project
    * Especially for the mechanisms
* Hand-in of the report: 31.05.19
* Presentation: 19.06.19 at 8h00 in C10.15/C00.11
    * Not yet definitive
* ECC non correctable fault injection problems
    * Injection is possible
    * Detection is not possible (MPC5643LRM 21.4.2.10)
* Look into IVOR exception handling

## Week 11
* ECC problem (Why?, Workaround)
    * MPC5643LRM 21.4.2.10
    * Deactivate direct error reporting and handle IVOR1 exception
        * [2bit error injection and handling](https://community.nxp.com/docs/DOC-103300)
    * Not necessary to implement
* How does ECC work?
    * [How ECC works](https://www.computerworld.com/article/2568163/sidebar--how-ecc-works.html)
    * MPC5643LRM 7.4 & 46.4
    * 7-bit/32 SEC/DED ECC (for RAM)
        * does not detect all errors greater than 2 bits.
    * 8-bit/64 parity SEC/DED (for FLASH)
* Next reunion
    * Monday 8h00
    * Rosas
* Measurements of function duration
    * Use GPIO pins
    * MPC5643LDMCBUG 2.11
    * Clock supervision
        * 101 us
    * FSM
        * align 7.25 us
        * ctrl 1st 8.16 us
        * ctrl 7.91 us
    * Scheduler Overhead
        *  3.2 us
