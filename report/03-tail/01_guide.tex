\chapter{Set-up guide}
\label{annex:guide}

This is a step-by-step guide for the \acrfull{demonstrator}, detailing how to install programs and drivers as well as how to put the demonstrator into a functional state. However, this guide is not an explanation of the code written for \acrshort{demonstrator} nor is it a troubleshooting guide for the aforementioned code.

\minitoc

\newpage

\section{Requirements}
No prior knowledge or set-up is required to follow this guide but it is assumed that you have:

\begin{itemize}[noitemsep]
	\item An installation of the Windows 10 operating system
	\item A \acrlong{probe}
	\item Access to a \acrshort{demonstrator} demonstrator
\end{itemize}

This guide was tested and is verified to work with Windows 10 and there is no guarantee that it will work for older versions of this operating system.


\section{Driver and Program Installation}
\label{sec:install}
This section details the installation of the drivers and programs needed to load code onto the demonstrator.

\subsection{JTAG Debugger}
The \acrshort{jtag} debugger is needed to load code onto the \acrshort{demonstrator} and to debug said code. There are two steps needed to set up the debugger:

\subsubsection{Drivers}
This step is necessary for the debugger to work. First, download the drivers from \url{http://www.pemicro.com/multilink/} (USB Multilink $\to$ Quick Downloads $\to$ Software \& Resources) and \url{http://www.pemicro.com/osbdm/} (Install Drivers $\to$ Click for Windows). Once the drivers are downloaded, install them by following the on-screen instructions.

Now you should check if the driver is installed correctly by connecting the debugger to the PC  with a USB cable (see \autoref{sec:connect}). Then open the device manager in Windows and check if the debugger is present.

\subsubsection{Firmware}
This step is only necessary if the debugger is new and has not been used before or if the debugger has been used to program another \acrshort{cpu} not compatible with \acrshort{demonstrator}. Either way, it is recommended that this step is followed regardless of how the debugger was used before. This ensures that the debugger is working correctly when it needs to be used.

First, go to \url{http://www.pemicro.com/osbdm/} (Firmware Updates \& Recovery) and download the tool. Next, follow the instructions to install it and then run the program (PEFirmwareConfig.exe), you should see the window shown in \autoref{fig:jtag_firmware}.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.75\textwidth]{jtag_firmware_update}
	\caption{Updating the firmware of the debugger}
	\label{fig:jtag_firmware} 
\end{figure}

Select the options shown in \autoref{fig:jtag_firmware} and click on \textit{Update Firmware}. If the program does not launch or the firmware update fails, consider deactivating your anti-virus as it may interfere with this step.

\subsection{IDE}
The \acrfull{ide} used is the \acrlong{cw} (hereafter only called \acrshort{cw}) from \acrlong{nxp}. An evaluation version can be downloaded by navigating to the software centre on the NXP website and clicking on the "Eval" button of the correct \acrshort{ide}. Please note that you need an account to start the download of this product.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=\textwidth]{license_select_product}
	\caption{Select the evaluation version of the desired \acrshort{ide}}
	\label{fig:select_eval}
\end{figure}


\section{Connecting the demonstrator}
\label{sec:connect}
This section shows how to power the \acrshort{demonstrator} and how to connect the \acrshort{jtag} probe to it. \textbf{It is important that you first connect the debugger to the demonstrator before powering it.}

\subsection{JTAG Debugger}
First, connect the debugger to the PC with a USB (male A to male B) cable. Check if the the LED on the debugger marked with "USB" shines blue. Please also check if the PC recognises the debugger (described in \autoref{sec:install}).\\

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.3\textwidth]{jtag_cable}
	\caption{The cable to connect the debugger to the demonstrator}
	\label{fig:jtag_cable} 
\end{figure}

Now take the cable with the 14-pin connector, connect it to the debugger Port A (\autoref{fig:jtag_open}) and make sure that the red part of the cable (\autoref{fig:jtag_cable}) is connected to pin number 1. Next, connect the other end of this cable to the \acrshort{jtag} port on the demonstrator (\autoref{fig:demonstrator}) and again with the red part connecting to pin number 1.

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{jtag_closed}
		\caption{Closed}
		\label{fig:jtag_closed}
	\end{subfigure}
	~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[width=\textwidth]{jtag_open}
		\caption{Open}
		\label{fig:jtag_open}
	\end{subfigure}
	\caption{Connection of the \acrshort{jtag} debugger}
	\label{fig:debugger}
\end{figure}

\subsection{Power}
Once the debugger is connected, you can power on the demonstrator. On the bottom left in \autoref{fig:demonstrator} you can see the power connector which is located on the motor driver. There are two drivers stacked on top of each other and both have a power connector. It is not important which one is used.

\textbf{Warning: Please don't use the power connector located on the microcontroller board (Bottom right in \autoref{fig:demonstrator}).}

Once the board has power the corresponding LEDs should light up and the LED on the debugger marked with "TGTPWR" should light up orange.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=\textwidth]{demonstrator}
	\caption{Connection of the demonstrator}
	\label{fig:demonstrator} 
\end{figure}


\section{Debugging}
This section describes the steps needed to start a debugging session and launch the program on the demonstrator.\\

Once \acrshort{cw} is launched, click the arrow to the right of the green "bug" and select the option containing "FLASH" in the name. This option should already be the default and directly clicking the green "bug" should suffice.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=\textwidth]{cw}
	\caption{Load the program onto the \acrshort{mcu} and start a debugging session}
	\label{fig:cw} 
\end{figure}

When the program is loaded, you can use the buttons highlighted in \autoref{fig:cw_dbg}. The following list describes the most important buttons.

\begin{description}[align=right,labelwidth=1.5cm,noitemsep]
	\item [Green] Start, pause and stop debugging
	\item [Yellow] Advance step-by-step in the program
	\item [Red] Restart the program
\end{description}

\begin{figure}[H] 
	\centering 
	\includegraphics[width=\textwidth]{cw_dbg}
	\caption{Start the program on the \acrshort{mcu}}
	\label{fig:cw_dbg} 
\end{figure}


\section{License expiring}
If you only have an evaluation license for \acrlong{cw}, it will expire within 30 days. This means you need to renew the license, or redownload and reinstall the \acrshort{ide} after this period. The following text and images describe all the steps needed to generate a new license.\\

First, open \acrlong{cw} and click on \textit{Help -> Freescale Licenses}, a pop-up listing you licenses should appear. Select the license you want to renew and click on \textit{Register}. A website should open within \acrshort{cw} where you need to navigate to the location shown in \autoref{fig:license_nav}. Once there, select the product shown in \autoref{fig:license_sel} and click on \textit{Evaluation}.

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics[width=.3\textwidth]{license_navigate_to}
		\caption{Navigate}
		\label{fig:license_nav} 
	\end{subfigure}
	~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=\textwidth]{license_select_product}
		\caption{Select desired product}
		\label{fig:license_sel}
	\end{subfigure}
	~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=.8\textwidth]{license_select_software}
		\caption{Select desired version}
		\label{fig:license_sel_2}
	\end{subfigure}
	\caption{Selecting the product for which to generate a license}
	\label{fig:license_nav_sel}
\end{figure}

Before you can continue, you need to accept the terms and conditions. Now you should see the product list with the product information where you need to select the desired version of your product (\autoref{fig:license_sel_2}).\\

The next page is shown in \autoref{fig:license_gen_1} where you need to click on \textit{License Keys} to get to the page of \autoref{fig:license_gen_2}. On this page, select the check box and click the button \textit{Generate} to create your new license.

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=.8\textwidth]{license_generate_1}
		\caption{Step 1}
		\label{fig:license_gen_1} 
	\end{subfigure}
	~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
	%(or a blank line to force the subfigure onto a new line)
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=.9\textwidth]{license_generate_2}
		\caption{Step 2}
		\label{fig:license_gen_2}
	\end{subfigure}
	\caption{Generate the license}
	\label{fig:license_gen}
\end{figure}

Once this is done, you arrive on the page of \autoref{fig:license_save} where you can save the newly generated license by clicking on \textit{Save All}.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.4\textwidth]{license_save}
	\caption{Save the license file}
	\label{fig:license_save}  
\end{figure}

Now you have the new license on your PC which should be called \textit{license.dat}. To tell \acrshort{cw} to use the new license instead of the old one, navigate to the folder \textit{<Path to CW installation>/MCU/} and replace the old \textit{license.dat} file with the new one.\\

If you were not able to generate a new license, try creating a new account on the website of \acrshort{nxp}. If this also fails, try redownloading and reinstalling the \acrshort{ide}.


%\section{Troubleshooting}



