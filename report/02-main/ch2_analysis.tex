\chapter{Analysis}
\label{chap:analysis}

This is the chapter dedicated to the analysis of the previous master project which is the basis of this project. One part of this is the analysis of the hardware used for this project which includes the search for possible replacement processors satisfying certain criteria, carried out during this project. Furthermore, the \acrlong{sw} written for the demonstrator during the last master project is studied and some important parts are highlighted and briefly described.\\

\minitoc

\newpage

% -----------------------------------------------------------------------------
\section{Safety}
During the last project a detailed analysis of the \textit{ISO 26262} norm was conducted which was an integral part of the corresponding master thesis. This norm will also be studied during this project, although in less detail, as there is not enough time available. The last project conducted a thorough analysis of this norm and the safety life cycle which was the applied in a simplified form during the development. Whereas the present project uses the safety norms more as an indicated of which safety mechanisms should be used, why they should be used and how they should be implemented.


% -----------------------------------------------------------------------------
\section{Hardware}
This section includes a description of the processor and the demonstrator chosen during the last master project. It also includes the analysis of different processors done during this project with the goal of choosing one or more processors which could replace the one currently in use.

\subsection{Current Processor}
The processor currently in use was imposed by the project description of the previous master project which stated that the \acrlong{hw} needs to be based on the \textit{\acrlong{proc}} microcontroller. This is in part due to the fact that this processor was already studied in a project preceding the master project and also because it is certified for a an appropriate safety level, namely ASIL D.\\

\subsection{Current Demonstrator}
The processor  chosen is only one part of the \acrlong{hw} as there also needs to be a visual aspect to the whole project. During the last project, the goal was to show this demonstrator during exhibition, meaning the people this is being shown to do not necessarily have a background in engineering or science. This fact makes the visual aspect even more important and plays a primary part in the choice of the demonstrator.\\

One way to choose a demonstrator is by building a custom one form scratch with all the important features needed. This would certainly be the best approach for a final product but the fact that the time available of the master project and this project are limited, it is not a feasible option.\\
The obvious second approach is finding a demonstrator designed for visual demonstrations which is equipped with the desired processor and features. During the time of the master project only one such product was available on the marked. This product is a development kit form \acrshort{nxp} called \acrlong{demonstrator}.\\

The \autoref{fig:demonstrator_full} below shows the complete demonstrator, with the controller board (where the \acrshort{cpu} is located), the two power stages and the two 3-phase permanent magnet synchronous motors (PMSMS) driven by these power stages.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.8\textwidth]{demonstrator_full}
	\caption{The current demonstrator (Source: \cite{demonstrator})}
	\label{fig:demonstrator_full} 
\end{figure}

The following figure (\autoref{fig:controllerboard_full}) shows the controller board. The processor is located in the middle of the board and is surrounded by various components, interfaces and connectors.\\
The connectors on the left (UNI-3 interface and MC33937A Interface) are used to connect the controller board to the two power stages. The connectors on the top right connect the encoder and the resolver for both motors. The \acrshort{jtag} connector is located on the right side of the board besides the buttons and the switch which are used to control the speed and direction of the motors. Finally, the connectors on the bottom are used to interface with other components that could be added to expand the functionality of this demonstrator. The power supply connector of the controller board is not important because it gets the power from the power stage through the UNI-3 interface.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.8\textwidth]{controllerboard_full}
	\caption{The controller board on the demonstrator with the processor (Source: \cite{demonstrator})}
	\label{fig:controllerboard_full} 
\end{figure}

The third figure (\autoref{fig:powerstage_full}) shows one of the power stages. The other power stage is exactly the same and requires no additional explanation.\\
The power supply connector and terminal are located on the bottom of the power stage. Both stages have a power supply connector but only one needs to be connected as the second one gets its power through the terminal which connects the two stages together. On the top is the motor connector which provides the three phases for the motors. Finally, on the right are the connectors which connect the power stage to the controller board. Like already mentioned above, the power for the controller board is provided by the power stage through the UNI-3 interface.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.8\textwidth]{powerstage_full}
	\caption{The 3-phase power stage of the demonstrator to control the two brushless AC motors (Source: \cite{demonstrator})}
	\label{fig:powerstage_full} 
\end{figure}

\subsection{Choosing a new Microcontroller}
This section describes the search for a new microcontroller which should have the same level of functionality as the one currently in use. It should also be certified for the same safety level, so that it could potentially be used in the same domain without lowering the safety of the whole system.\\
The search for a new microcontroller is mostly an exercise, as it would not be feasible to develop a new demonstrator and to rewrite the whole code in the time available for this project. But this could still be valuable information for a future project that aims to create a demonstrator form scratch where it would be beneficial to use another microcontroller.\\

The first step was to make a list of the features available on the \acrshort{proc}, which is the current microcontroller used. Then, new microcontrollers can be searched by comparing their features with this list. The final list contains four potential candidates: \textit{SPC570S50E3}, \textit{SPC56EL60L5}, \textit{SPC564L60L5} and \textit{TMS570LS1115}.\\
The next step was to choose one of these potential candidates and to compare the supported safety functionalities. For this analysis, the \textit{TMS570LS1115} from \textit{Texas Instruments} was chosen because the features were most similar to the current microcontroller.\\

The two tables created during this analysis can be found in \autoref{annex:processors}. The first table shows the comparison of the features and the second one shows the comparison of the safety functionalities.


% -----------------------------------------------------------------------------
\section{Software}
The previous master project developed the software form scratch. The \acrfull{hal}, the driver layer and the application layer have all been implemented. To simplify the development and to have maximum control over the software, it was decided not to use a \acrfull{rtos} \cite{broch}.\\
The \acrshort{hal} implements the different modules available on the microcontroller and directly communicates with them. The driver layer implements the functions needed to drive the motors of the demonstrator and the application layer contains the \acrfull{fsm} which is used to control the motors.\\
The \autoref{fig:mpc_block_diagram} taken from the report of the master project, shows the block diagram of the \acrshort{proc}, highlighting the already implemented modules.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.75\textwidth]{mpc_block_diagram}
	\caption{The block diagram with all the modules implemented during the last master project (Source: \cite{broch})}
	\label{fig:mpc_block_diagram} 
\end{figure}

 The flowchart in \autoref{fig:fsm} shows the implemented \acrshort{fsm} which is running in an infinite loop to control the motor. This state machine has fours states: One to initialise the peripherals (state init), one to initialise the motors by aligning them properly (state align), one running continuously to control the speed of the motors (state control) and one handling errors if any occur (state error).

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.65\textwidth]{fsm}
	\caption{The state machine controlling the motors}
	\label{fig:fsm} 
\end{figure}

\subsection{Measurements}
The following figures show the execution time of the \acrshort{fsm} which was measured using the SALEAE Logic Analyser. The first figure (\autoref{fig:fsm_align_meas}) shows the execution time of the alignment state of the \acrshort{fsm} which takes 7.25 \si{\micro\second}.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.75\textwidth]{meas_fsm_align}
	\caption{Measurement of the \acrshort{fsm} state aligning the motors}
	\label{fig:fsm_align_meas} 
\end{figure}

The second figure (\autoref{fig:fsm_ctrl_meas}) shows the execution time of the control state when it gets called the first time. This measurement was done because the first time the \acrshort{fsm} enters this state, all the commands are set to "0" to stop the motors. The execution time measured is 8.16 \si{\micro\second}.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.75\textwidth]{meas_fsm_ctrl_1st}
	\caption{Measurement of the first pass of the \acrshort{fsm} state controlling the motors}
	\label{fig:fsm_ctrl_meas} 
\end{figure}

The last figure (\autoref{fig:fsm_ctrl_meas}) shows the execution time of the control state every subsequent time it gets called where the reset of the commands is skipped. The execution time measured is 7.91 \si{\micro\second}.

\begin{figure}[H] 
	\centering 
	\includegraphics[width=0.75\textwidth]{meas_fsm_ctrl}
	\caption{Measurement of the \acrshort{fsm} state controlling the motors}
	\label{fig:fsm_ctrl_1_meas} 
\end{figure}


% -----------------------------------------------------------------------------
\section{Safety Mechanisms}
This section describes the safety mechanisms that were already implemented during the last master project. It also provides a brief description of these mechanisms.\\
If one of these mechanisms detects a fault, the \acrshort{mcu} goes into safe mode which is indicated with a LED to the operator. In the safe mode all the peripheral devices are deactivated, meaning the motors cannot run when the microcontroller is in this mode. The only way to exit this mode is by performing a power cycle, by completely turning off the microcontroller and then turning it on again.

\subsection{Clock Supervision}
This mechanism is integrated in the microcontroller with the \acrfull{cmu}. It is possible to configure this module to supervise the clocks of different components as not all of them use the same clock. All the clocks for these components are generated using the external oscillator (XOSC @ 8 MHz). There is also a second clock signal generated by the internal RC-oscillator (IRCOSC @ 16 MHz). This clock is used in the \acrshort{cmu} to check the other clocks.\\
A second part of this mechanism is the periodic supervision of the IRCOSC, which was not implemented during the last project. This is however one of the mechanisms which is implemented in this project and described in detail in \autoref{chap:implementation}.

\subsection{Power Supply Supervision}
The voltage of the power supply can be monitored with the \acrfull{pmu}. This module checks the voltage of the main power supply and the voltage for the processor. If the power supply voltage is lower than 2.6 V or the processor voltage is not in the range of 1.1 V to 1.3 V \cite{broch}, the microcontroller enters safe mode.

\subsection{Position Sensor Supervision}
The motors on the demonstrator each have two position sensors which are redundant. These two sensors are compared and if the angle measured is grater than 0.1 rad an error is reported. As explained in the report of the previous master project, this comparison can only be done when the motors are running \cite{broch}.

\subsection{DC-bus Voltage and Motor Current}
The motors are controlled by using \acrfull{pwm}. These \acrshort{pwm} signals are generated by the FlexPWM module which also includes a safety mechanism that allows the detection of over voltage on the DC-bus and over current on the motor phases. When this is detected, the \acrshort{pwm} signals are deactivated and the microcontroller goes into safe mode.\\
The maximum allowable values of the voltage and current can be configured with potentiometers located on the controller board. These values are set up to correspond to the maximum allowed voltage for the DC-bus (32 V) and the maximum allowed current for the motor phases (10 A) \cite{broch}.



% -----------------------------------------------------------------------------
%\section{Conclusion}

