\chapter{Introduction}
\label{chap:introduction}

The project described, analysed and discussed in this report is introduced in this chapter. This project was proposed by CertX, a company active in functional security and cyber security for critical applications, such as Industrial Machinery, Automotive, Railway, and much more. These critical applications can be life-threatening and require the use of special processors to achieve extremely high safety levels. It is therefore required that these processors have mechanisms that add redundancy, isolation of some pieces of the software from others, replication and error correction. These are only a few examples of various mechanisms which are commonly found on such processors.\\

\minitoc

\newpage

% -----------------------------------------------------------------------------
\section{Structure of this Report}
This report is divided into five chapters, excluding the annexe.

Chapter 1 provides a short introduction to this project. It includes a description of the project itself, its objectives and the task that were set at the beginning.

Chapter 2 is the analysis of the current state of this project. This chapter describes what has already been done during a previous project, provides a description of the hardware used, explains some parts of the software and analyses possible replacement processors.

Chapter 3 provides an overview of the safety aspect of this project. It describes what safety standards were used, why they are important for this project and which safety mechanisms are recommended to be implemented.

Chapter 4 describes the work that directly concerns the expansion of already existing features as well as the implementation of new ones. This chapter also includes a description of some important mechanisms which have not yet been implemented.

Chapter 5 provides a conclusion to the project. It contains a summary of the work dine during this project, a comparison with the initial objectives, the difficulties encountered, a future perspective of this project and a personal opinion of this project.

Finally, the annexe contains a guide intended to help future students setting up the \acrshort{hw} and \acrshort{sw} without wasting time. It also contains a table comparing processors, all the weekly reports written during the project andd the planing showing the anticipated time and the real time spent on various tasks.

% -----------------------------------------------------------------------------
\section{Tools used}
The table in this sections lists all the tools used for this project. Every tool has a description detailing why it is used and why it is acceptable to use it in this context. Additionally, each tool receives a rating describing the impact it has on the final product/code. This rating can have the following three values: T1 (low impact), T2 (moderate impact) and T3 (high impact).\\

\begin{longtabu} to \linewidth
	{
%		|	Tool		|		Version		|	Impact (T1-T3)	|		Use		|		Why acceptable?		|
		| X				| l					| l					| X				| X							|
	}
	\hline %----------------------------------------------------------------------------------------------
	%\taburowcolor{lightgray}
	\textbf{Tool}		& \textbf{Version}	& \textbf{Impact}	& \textbf{Use}	& \textbf{Why acceptable?}	\\
	\hline %----------------------------------------------------------------------------------------------
	\endhead % all the lines above this will be repeated on every page
	
%	|		Tool			| Version	| Impact	|		Use			|		Why acceptable?			|
	TeXstudio				& 2.12.x	& T1		& Writing the report for this project.
																		& This tool has no impact on the final \acrlong{hw} or \acrlong{sw}, and is therefore no safety concern.						\\
	\hline %----------------------------------------------------------------------------------------------
	Atom					& 1.34.x	& T1		 & Taking notes and writing the weekly reports for this project.
																		& This tool has no impact on the final \acrlong{hw} or \acrlong{sw}, and is therefore of no safety concern.						\\
	\hline %----------------------------------------------------------------------------------------------
	LibreOffice				& 6.2.3		& T1		& Making and maintaining the planning for this project.
																		& This tool has no impact on the final \acrlong{hw} or \acrlong{sw}, and is therefore of no safety concern.						\\
	\hline %----------------------------------------------------------------------------------------------
	Git for Windows			& 2.21.x	& T1 		& Version controlling of the code and the documentation.
																		& This tool has no impact on the final \acrlong{hw} or \acrlong{sw}, and is therefore of no safety concern.						\\
	\hline %----------------------------------------------------------------------------------------------
	cppcheck				& 1.87		& T2 		& Checking the code for MISRA-C compliance.
																		& This tool has no direct impact on the final \acrlong{hw} or \acrlong{sw}. But depending on what this tool suggests, the developer could be lead to change some aspects of the code. This means that the checks and suggestions of this tool need to be accurate.			\\
	\hline %----------------------------------------------------------------------------------------------
	Doxywizard for Doxygen	& 1.8.15	& T2 		& Generating documentation of the code written for the
	\acrshort{mcu}.			
																		& This tool has no impact on the final \acrlong{hw} or \acrlong{sw}. However, it is necessary to integrate comments in the code serving as documentation, which means changing the code. Though this is no safety concern as the function and execution of the code is not altered in any way.							\\
	\hline %----------------------------------------------------------------------------------------------
	SALEAE Logic			& 1.2.18	& T2 		& Recording measurements taken with the SALEAE Logic analyser to determine the execution time of certain important functionalities in the program.
																		& This tool has no impact on the final \acrlong{hw} or \acrlong{sw}. However, it was necessary to add code to turn pins on or off which could then be measured. This is no safety concern, as these code sections will be removed in the final code which means the actual execution time could potentially be shorter than the measurements suggest but can never be longer.								\\
	\hline %----------------------------------------------------------------------------------------------
	\acrlong{cw}			& 10.6		& T3 		& Writing code, loading the program onto the demonstrator and debugging
																		& This tool can have a very serious impact on the final code loaded onto the demonstrator. It has to be made sure that the code is transmitted correctly and not modified in the process. The debugging capabilities of this tool also have to work properly, or else errors might be included or overlooked during debugging. However, it is acceptable to use this tool as it was designed to be used for safety related applications.	\\
	\hline %----------------------------------------------------------------------------------------------
	compiler				& -			& T3 		& Compile code written by a developer into machine code that can be executed by a processor.
																		& This tool can have a very serious impact on the final code loaded onto the demonstrator. It has to be made sure that the machine code generate behaves exactly how the developer intended. This compiler is acceptable to use because it is provided by the \acrshort{ide} which is designed to be used for safety related applications.	\\
	\hline %----------------------------------------------------------------------------------------------	
	\caption[A table detailing the tools used]{A table detailing the tools used during this project and the impact they have on the result}
	\label{tab:tools}
\end{longtabu}


% -----------------------------------------------------------------------------
\section{Context}
This project builds upon two already completed projects. The first one was a study of high-security processors done in the year 2014 \cite{schnarrenberger}. The second one, which is more important for this project as it already built upon the first project, is the master project done Mr. Nicolas Broch in the year 2015 called "Functional Safety by Redundant Processor" \cite{broch}. There were two main points of focus: The study of the safety process described in the norm ISO 26262 and the development of a demonstrator, integrating various safety features.\\

The goal of this project, or the final purpose of this application running on a demonstrator, is the use for education. This will allow professors to easily and, more importantly, visually demonstrate the importance of safety mechanisms and norms during exercises.\\
Another idea is the use of this demonstrator for laboratories, where students will be able to interact directly with the demonstrator and the application running on it. During such a laboratory session they can for example modify some code snippets and observe the impact of these modifications on the whole application.\\


\subsection{Description and Objectives}
The primary objective of this project is to understand the details of the safety mechanisms that are available on of the processor used. It is also important to detect the limits of certain mechanisms and to implement an application, or to modify an already existing one, on a development board. This application depends on three things: The direction set during the master project which is the basis for this project, the possibilities and limits of the different mechanisms, as well as the collaboration with the company CertX.\\

A second objective is the study and choice of an appropriate safety norm and level. Essentially, this was already defined during the previous master project with the choice of the demonstrator. This is still an important objective, as it allows for a deeper understanding of the safety aspect and the reason why this norm was chosen.\\

A third objective is the search for an appropriate processor on which the developed application could also be deployed with as little effort as possible. This choice naturally also depends on the desired safety level wanted for this application. This objective is more of an exercise with the purpose of defining a replacement processor if a future project aims to make a more definitive and fully custom demonstrator.\\

\subsection{Tasks}
The following list describes the tasks defined at the beginning of this project, with the exception of the unforeseen task which would be defined as the project was taking its course. Each task has a brief description, detailing the work that is associated with it.

\begin{description}[align=left,noitemsep]
	\item [Introduction:] This is the first task of the project which includes many different things. One  essential part is the creation of a planning for the project which separates it into the tasks described here. This planning then assigns a time span to each task and some mile stones for certain essential ones that absolutely need to be completed before a certain date.\\
	A second part of the introduction is the study of the previous master thesis and the definition of objectives for this project. These two are somewhat related, as the study of the previous project gives some insight to what was already done and what could be done next.\\
	Some minor parts of the introduction, which are important nevertheless, are the definition of a context for the project and the setup of a \acrshort{git}, which is used to manage the whole project.\\
	
	\item [Safety norms:] This task is dedicated to the safety aspect of this project which plays an important role. The primary focus of this task is the study, analysis and comprehension of some safety norms and standards which are applicable for this project.\\
	
	\item [Implementation:] This task is the biggest one, as it consists of many different sub-tasks related to the implementation of the various elements of this project. It includes: The search for possible replacement processors, the selection of the mechanisms that should be implemented in the code, the actual implementation of these mechanisms, and also the testing of these mechanisms.\\
	
	\item [Documentation:] One very important aspect of every project is the documentation. This task includes the  writing of the report, the preparation of the final presentation, and the documentation of the code.\\
	
	\item [Unforeseen tasks:] There are two unforeseen task that arose during the course of the project. The first one is the set-up of the demonstrator which, at the beginning of the project, didn't seem like it would take much time and therefore did not have its own task. The set-up is described in the guide in \autoref{annex:guide} and the problems are mentioned in the conclusion.\\
	The second task which was not planned at the beginning of the project, is the measurement of the execution time of different parts of the program. In retrospect, this task should have been included at the start.
\end{description}


%* Setup GIT
%* Define objectives of the project
%* Planning (time of the different tasks)
%* Think of context of the project
%* Analyse different processors
%* Study norm and its different levels (eg. A/B/C/D)
%* Study the different mechanism to mitigate errors
%* Read IEEE publications (optional)
%* Study "non interference" (spatial, temporal, execution) in standard


%Planning and Tasks
%Definition of objectives
%Set-up GIT
%Study previous project
%Define  project context
%Choose norm
%Choose norm levels
%Study mitigation mechanisms
%Study "non interference" standard
%Choose processor
%Choose critical safety criteria
%Choose mechanisms to implement
%Implement mechanisms
%Implement Scheduler
%Test mechanisms
%Report
%doxygen
%Presentation
%Read IEEE publications
%Setup of the demonstrator



