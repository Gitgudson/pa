\chapter{Safety}
\label{chap:safety}

This chapter discusses the different safety standards which were studied during this project. Mainly two standards are important: \textit{ISO 26262} and \textit{IEC 61508}. Each standard is described in its own section and important parts are highlighted and explained in more detail. Moreover, this chapter also explains what the MISRA-C directive is and provides some examples illustrating its use.

\minitoc

\newpage

% -----------------------------------------------------------------------------
\section{IEC 61508}
%Part 2, p. 54
%Part 3, Annex F (non.interference)
This is an international standard which applies to all software of electrical systems which perform safety related functions. This is more of a general approach to these types of problems, the more specific norm (related to the automotive sector) is the ISO 26262 which will be discussed in the next section of this chapter \cite{iec}.

\subsection{Mechanisms}
This norm also defines mechanisms that need to be implemented to achieve a certain safety level. For each mechanism listed, there is a description explaining its goal and suggesting how it could be implemented with actual hardware or software. Some of these mechanisms are listed and described in the following sections which  talks about the ISO 26262 norm, which is essentially an adaptation of this norm.

\subsection{Non-Interference Standard}
%Only this is really important
This subsection describes methods mentioned in this standard, which have the goal of ensuring that different elements of a program which are executed on the same system do not interfere with each other. This principle of non-interference is most important for safety related functions. Mainly two elements of a program need to be prevented from interfering with each other: Two elements which are associated to the same safety function, or elements related to safety functionality and elements which are not safety related.\\

There are essentially two domains in which the program elements should not interfere with each other:

\begin{description}[align=left,noitemsep]
	\item [Spacial:] This refers to the data used and modified by one program element. Another program element, especially if it is not safety related, should not be able to change this data.
	\item [Temporal:] This refers to the execution time of a program element. Some other element of the program should not prevent a safety critical element to function incorrectly by using too many resources or by blocking a shared resource.
\end{description}

The following factors are listed by the norm, that could be the cause of interferences between program elements:

\begin{itemize}[noitemsep]
	\item Shared memory
	\item Shared peripheral devices
	\item Shared processor
	\item Communication between elements
	\item Errors occurring in an element
\end{itemize}

\subsubsection{Spacial Independence}
This section discusses what measures can be taken to ensure spacial independence between different program elements. One technique is the use of memory protection by including hardware memory protection and using a \acrfull{mmu}. With the help of a \acrshort{mmu}, every process can be executed in its own virtual address space and the \acrshort{mmu} translates these virtual addresses into the corresponding physical addresses.\\
Another technique is the protection of the software which executes safety related functions. Non-safety related software should not be able to modify safety related software and it should also be avoided to send data from a non-safety to a safety part of the program. If it is absolutely necessary to send data between elements of the program which should be independent, only uni-directional interfaces (e.g. pipes) should be used and shared memory should be avoided.

\subsubsection{Temporal Independence}
This section discusses the techniques which should be used to maintain temporal independence between elements of a program. One technique is the implementation of scheduling which should either be deterministic (each task has a dedicated amount of \acrshort{cpu} time available) or be priority based (Task with a higher safety rating have a higher priority).\\
A second technique is the implementation of a watchdog timer (called time fences in this norm), which detect when an element takes longer to execute than expected.\\

\subsection{Why is it important?}
The non-interference standard, which is part of this norm and is described above, is one of the most important concepts of programming, be it safety related or not. As more and more depends on computers and the tasks performed by them get evermore diverse and complex, the programs written naturally also get bigger and more complex. Such a program has many different parts that are --- or should be --- independent from each other. This non-interference standard provides a way of ensuring this independence between those different program elements. Of course this is more important for safety related programs which can potentially harm people if they misbehave. It is even more important for programs or systems which contain a mix of safety and non-safety functionalities, because these are more vulnerable to bugs which can potentially affect critical parts of the system. These types of errors and interference have to be avoided and can be avoided by following this norm and the non-interference standard.


% -----------------------------------------------------------------------------
\section{ISO 26262}
%more focus on this than IEC
%asil
%Part 5, p. 52
This norm concerns the safety in road vehicles, in other words, it is a norm for the automotive industry. The ISO 26262 is an adaptation of the IEC 61508 norm defined by the International Electrotechnical Commission, which was introduced in at the beginning of this chapter. This adaptation is done to address the safety needs of programs in cars and other vehicles.\\
This section talks about part 5 of this norm and provides an overview of the pieces which are important for this project. It also outlines the \acrfull{asil} and talks about the various safety mechanisms that are recommended to be implemented by this norm.\\

This norm is only applicable to vehicles which do not have a special purpose (e.g. vehicles for people with disabilities) and do not surpass 3500 kg. It also only covers hazards that can directly be caused by en error in an electronic system \cite{iso}.

\subsection{Why is it important?}
As with everything, existing technology gets more complex as time goes on and new technology gets developed at an increasingly fast rate and gets adopted equally quickly. The speed at which new technology is integrated into already existing products is visible in every day life but especially so in the automotive industry. Not so long ago, cars only needed to drive and every thing safety related was concerned with the mechanical aspect as there were almost no electronics present. Now, every car is full of electronics. Almost everything relies on software, from the most critical safety features like brakes and steering, to the non-critical features like the entertainment system. As a consequence, there are many more points of potential failure present in a modern car  than in an old one.\\
Due to this trend of steadily increasing complexity and the ever stricter safety criteria, internationally standardised norms are as important as never before.

\subsection{\acrlong{asil}}
The \acrfull{asil} is a safety level classification similar to SIL (general/railway) and DAL (aviation). There are four \acrshort{asil} levels: ASIL-A, ASIL-B, ASIL-C and ASIL-D. Where ASIL-D represents the highest safety level with the most restricting requirements.


\subsection{Mechanisms}
This subsection shows some of the more important mechanisms mentioned in the ISO 26262 norm. The table below lists these mechanisms and provides a short description of each one.

\begin{longtabu} to \linewidth {|l|X|}
	\hline %------------------------------------------------------------------------------------------------------
	Mechanism							& Description															\\ 
	\hline %------------------------------------------------------------------------------------------------------
							\multicolumn{2}{|c|}{System} 														\\
	\hline
	Comparator and redundancy			& Signals and information form independent hardware and software are compared. For software comparison a lock-step mode could be used, which runs the same program on two independent cores of a processor. If a comparison of more than two sources is made, a majority voter should be implemented, deciding which result is trustworthy.\\
	\hline
	Self-test (HW/SW)					& Self-test performed either by software, e.g. by detecting errors in registers and memory, or by hardware. Hardware self-test are performed by a specialised hardware element and are typically only done during boot or reset of the processor.\\
	\hline
	Register test						& Regularly reading and comparing registers to a known value. If the values differ, either the correct value should be restored or the processor needs to enter a safe mode.\\
	\hline
	Stack over/under flow				& Check if stack over or under flows occur. This can be done by implementing so-called "canaries", which are placed before or after a memory region. It can then be detected, if a "canary" is overwritten. "Canaries" can usually be implemented using the compiler.\\
	\hline
	Hardware consistency				& Detect when illegal op-codes are used or illegal operations are performed in a program (e.g. division by zero).\\
	\hline %------------------------------------------------------------------------------------------------------
							\multicolumn{2}{|c|}{Memory} 														\\
	\hline
	\acrlong{edc}						& Protect memory by detecting every 1-bit and 2-bit errors. Usually the 1-bit errors can be corrected and the program can continue operating normally. This mechanism is often implemented by using \acrshort{ecc} memory.\\
	\hline
	Memory signature/checksum			& Calculate a checksum of a memory block and append the result to the data or store it elsewhere in memory. This checksum can then periodically by recalculated and compared to the stored checksum. This is often implemented by using a \acrfull{crc}.\\
	\hline
	Block replication					& The whole memory, or a safety critical part of it, is duplicated and stored in a separate memory which is accessed in parallel to the first one. The values contained in the two memories can then dynamically be compared.\\
	\hline %------------------------------------------------------------------------------------------------------
							\multicolumn{2}{|c|}{\acrshort{io}} 												\\
	\hline
	Test pattern						& Check if the data is still being transmitted correctly by periodically sending a predefined test pattern and testing if the expected data is received.\\
	\hline
	Code protection						& Detect failures of inputs and outputs by adding redundant information to the data. For example, before sending data, a checksum can be calculated. The receiver performs the same operation and checks if the calculation matches.\\
	\hline
	Parallel output						& Monitor parallel outputs that are compared by an external comparator. The values of both outputs need to stay in a defined range and should not be delayed in time. If one criterion fails, the system can be put into safe mode to prevent a dangerous situation. If more than two parallel outputs are compared, a majority voting system can be used to determine the correct output value.\\
	\hline %------------------------------------------------------------------------------------------------------
							\multicolumn{2}{|c|}{Communication bus} 											\\
	\hline
	Hardware redundancy					& The goal is the detection of errors during communication by adding redundant lines to the communication bus. Each added line corresponds to an additional bit in the transferred data stream which is exclusively used for redundancy. This can also be extended by adding a second, completely independent bus which is then compared with the first one to detect differences in the data.\\
	\hline
	Information redundancy				& This mechanism either adds redundant information directly to the data (e.g. by calculating a checksum) or the same data is sent multiple times back-to-back (transmission redundancy).\\
	\hline
	Read back							& The sender of a message reads it back and compares it with the message that was sent.\\
	\hline
	Test patterns						& Check if the data is still being transmitted correctly by periodically sending a predefined test pattern and testing if the expected data is received.\\
	\hline
	Frame counter						& Add additional information to the data in form of a counter. The receiver can then determine if any data is lost during transmission by checking this counter.\\
	\hline
	Time out monitoring					& Make sure data is transmitted by implementing a timer which makes sure data is received in a predefined period. If the timer elapses, it could mean that communication has been lost.\\
	\hline %------------------------------------------------------------------------------------------------------
							\multicolumn{2}{|c|}{Power supply} 													\\
	\hline
	Voltage and current control			& Detect wrong voltage or current values on the input or output of the power supply by monitoring them.\\
	\hline %------------------------------------------------------------------------------------------------------
							\multicolumn{2}{|c|}{Program sequence} 												\\
	\hline
	Watchdog							& Add a watchdog timer to the program, which should periodically be reset. If some part of the program takes too long to execute and the timer is not reset in time, an interrupt is generated which can then be used to treat this error appropriately.\\
	\hline
	Program sequence monitoring			& Implement software measures that monitor the correct execution of the program. This can be combined with a watchdog timer to achieve the highest coverage of error detection.\\
	\hline %------------------------------------------------------------------------------------------------------
							\multicolumn{2}{|c|}{Sensors} 														\\
	\hline
	On-line monitoring					& Monitor the sensors and check for illogical values that should not be possible in the current context.\\
	\hline
	Test pattern						& Check if the data is still being transmitted correctly by periodically sending a predefined test pattern and testing if the expected data is received.\\
	\hline
	Valid range							& Limit the valid range of a sensor and check if the output value is outside of this defined range. A value outside of the specified range could mean the sensor or its connections are damaged.\\
	\hline
	Correlation							& Use redundant sensors of the same type and compare their output values.\\
	\hline
	Rationality							& Use redundant sensors of different types and check if the measurements agree with each other.\\
	\hline %-----------------------------------------------------------------------------------------------------
	\caption{Safety mechanisms proposed by ISO 26262 (Source: \cite{iso})}
	\label{tab:mechanisms}
\end{longtabu}


% -----------------------------------------------------------------------------
\section{Microcontroller Safety Guide}
The safety guide provided by \acrlong{nxp} \cite{safety_man} is an important help for the development and implementation of the safety mechanisms for this demonstrator. This document provides additional safety information for every module in the microcontroller which is not present in the reference manual \cite{ref_man}.\\
This document helps particularly in choosing and listing the safety mechanisms mentioned in the ISO 26262 norm, because the information given is less general and more specific to the microcontroller used. This provides a more concrete idea of how the different mechanisms can be implemented using the available modules.


% -----------------------------------------------------------------------------
\section{MIRA-C}
MISRA-C is a safety guideline and represents a subset of the C programming language. The aim of this guideline is to minimise the amount of errors and increase the safety/reliability of a program by avoiding some functions and properties of the C language which are deemed unsafe (e.g. \texttt{printf}, \texttt{malloc}, etc.). Another goal is to keep a code maintainable by providing naming conventions for functions, variables and other elements.\\
The compliance with these guidelines can be checked with tools designed for this purpose. The tool used during this project is called \textit{cppcheck}.

\subsection{Examples}
This subsection shows some examples of rules given in the 2004 version of the MISRA-C guideline \cite{misra}.

\subsubsection{Comments}
Only use \mintinline{c}{/*...*/} for comments. The reason is, that comments with \mintinline{c}{//} are not permitted in C90. Some compilers might allow this, even for C90 code but the behaviour is not consistent.

\subsubsection{Variables}
A variable inside a new scope should never have the same name as a variable outside of this scope (shown in \autoref{lst:misra_scope}). If this is not respected, the code quickly gets confusing and is more difficult to maintain.

\begin{listing}[H]
	\begin{ccode}
	int16_t i;
	
	{
		int16_t i;
	}
	\end{ccode}
	\caption{Two different variables with the same name}
	\label{lst:misra_scope}
\end{listing}

\subsubsection{Types}
Only use types which clearly indicate the size of a variable with this type, like: \texttt{int8\_t}, \texttt{uint16\_t}, \texttt{float32\_t}, etc. The use of types which do not have a clearly defined size (\texttt{short}, \texttt{int}, \texttt{float}, etc.) is discouraged.

\subsubsection{Constants}
It is discouraged to use octal numbers (integers starting with "0"), as this can be confusing and could lead to unexpected behaviour.

\subsubsection{Conversion}
Some type conversions should be avoided:

\begin{itemize}[noitemsep]
	\item Loss of value: The value range of the first type cannot be represented in the second type (e.g. \texttt{int32\_t} to \texttt{int16\_t}).
	\item Loss of sign: The first type represents the sign (+/-) of the value and the second type does not represent this sign (e.g. \texttt{int16\_t} to \texttt{uint16\_t}).
	\item Loss of precision: The first type represents a value with a higher precision than the second type (e.g. \texttt{float32\_t} to \texttt{int32\_t}).
\end{itemize}

\subsubsection{Conversion}
The variable used as the counter for a loop should not be modified in this loop.

\begin{listing}[H]
	\begin{ccode}
	for(i = 0; i < 10; i++)
	{
		i = i + 3;
	}
	\end{ccode}
	\caption{Modifying the loop counter in the loop}
	\label{lst:misra_loop}
\end{listing}

\subsubsection{Program flow}
A program should not contain any code which can never be reached.

\begin{listing}[H]
	\begin{ccode}
		if(false)
		{
			foo();
		}
	\end{ccode}
	\caption{A part of the program which cannot be reached}
	\label{lst:misra_unreacheble}
\end{listing}

Code which is part of an \textit{if}, \textit{for}, \textit{while} or other such statement needs to be enclosed in braces.

\begin{listing}[H]
	\begin{ccode}
	if(false)
		foo();	/* Wrong */

	if(false)
	{
		foo();	/* Correct */
	}
	\end{ccode}
	\caption{Code after an \textit{if} statement}
	\label{lst:misra_statement}
\end{listing}

\subsubsection{Libraries}
The library \textit{stdio.h}, \textit{signal.h} and \textit{time.h} should not be used. Other libraries can be used but some functions are not permitted, like \texttt{atoi()} from the the library \textit{stdlib.h}.


% -----------------------------------------------------------------------------
%\section{Conclusion}

