var union_d_s_p_i___p_u_s_h_r__32_b__tag =
[
    [ "B", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#acaed07bd7376a74a9866799317feefc3", null ],
    [ "CONT", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a4b568dc50c682cd850f240842423014e", null ],
    [ "CTAS", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a4a0130d3a50664c0d155a9ad327c9944", null ],
    [ "CTCNT", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#aff0b39afc859b4acd6b804626429b692", null ],
    [ "EOQ", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a10ccb45a2f6a141e3b8d442f903181d7", null ],
    [ "PCS0", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a3deaf3c0103d4602b530bc43436dc80a", null ],
    [ "PCS1", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#ade1ec6b8f19560d2eb403b96bf560965", null ],
    [ "PCS2", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a0db34bc7e789273607e97c75f1a57848", null ],
    [ "PCS3", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#aa1377eb7bac27c9bcc0a7723cf58d114", null ],
    [ "PCS4", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#ad1e1056fc082ef2d45ed36ee5e149bf8", null ],
    [ "PCS5", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a0e9be86377a7e8e81562bd00994ed235", null ],
    [ "PCS6", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a363fd0a094c6f321b1dbf6b6d4a7a0bd", null ],
    [ "PCS7", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a79dea711f87f695a6b26cf27d652a553", null ],
    [ "PUSHR_PE", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a3850c346b9e1a1735c4f9aa30dd2d23b", null ],
    [ "PUSHR_PP", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a46a47eb15dda2ff97d368e210c840d0c", null ],
    [ "R", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ],
    [ "TXDATA", "union_d_s_p_i___p_u_s_h_r__32_b__tag.html#a8b7137d33bcfd26d210bbfe7578079ae", null ]
];