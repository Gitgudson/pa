var union_f_r___c_i_f_r__16_b__tag =
[
    [ "__pad0__", "union_f_r___c_i_f_r__16_b__tag.html#aef1e5a0d60139f638a7fab4e35c01b15", null ],
    [ "B", "union_f_r___c_i_f_r__16_b__tag.html#a2bccad261ac761560f0867643a55a822", null ],
    [ "CHIF", "union_f_r___c_i_f_r__16_b__tag.html#a4691f5ebc3141de17acbfaf03c4cf019", null ],
    [ "FAFAIF", "union_f_r___c_i_f_r__16_b__tag.html#ae0b9945cdbec056238b027c3d5060235", null ],
    [ "FAFBIF", "union_f_r___c_i_f_r__16_b__tag.html#abc5e8393e8a4a0b4b2523fa411a27dcf", null ],
    [ "MIF", "union_f_r___c_i_f_r__16_b__tag.html#a472d688ddc98536bd48ea9a8dfd6d141", null ],
    [ "PRIF", "union_f_r___c_i_f_r__16_b__tag.html#a46d8e9a8ac42983cc59a037b9bf4a61d", null ],
    [ "R", "union_f_r___c_i_f_r__16_b__tag.html#a46d27991aececcda6046a07b32c865b4", null ],
    [ "RBIF", "union_f_r___c_i_f_r__16_b__tag.html#a3bc2e10e95d5a77051eeba00d6da9e5c", null ],
    [ "TBIF", "union_f_r___c_i_f_r__16_b__tag.html#acf4f3046b1be6dd1cde5ace165f8def3", null ],
    [ "WUPIF", "union_f_r___c_i_f_r__16_b__tag.html#a0fd8947a0c213e1c867bbd3443df4ee3", null ]
];