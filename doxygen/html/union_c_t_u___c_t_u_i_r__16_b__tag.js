var union_c_t_u___c_t_u_i_r__16_b__tag =
[
    [ "__pad0__", "union_c_t_u___c_t_u_i_r__16_b__tag.html#aef1e5a0d60139f638a7fab4e35c01b15", null ],
    [ "B", "union_c_t_u___c_t_u_i_r__16_b__tag.html#a44f1686307ad7e629b03fcb5528692b3", null ],
    [ "DMA_DE", "union_c_t_u___c_t_u_i_r__16_b__tag.html#ae983334ddb0e2eebc390424e00eff71f", null ],
    [ "IEE", "union_c_t_u___c_t_u_i_r__16_b__tag.html#a787c0861d5db8ff59b61d088cf11b27e", null ],
    [ "MRS_DMAE", "union_c_t_u___c_t_u_i_r__16_b__tag.html#a879fa4ac7563201d24fc12198840ae0f", null ],
    [ "MRS_IE", "union_c_t_u___c_t_u_i_r__16_b__tag.html#aa6cf4108a49653326e6ffd5541f997cf", null ],
    [ "R", "union_c_t_u___c_t_u_i_r__16_b__tag.html#a46d27991aececcda6046a07b32c865b4", null ],
    [ "SAF_CNT_A_EN", "union_c_t_u___c_t_u_i_r__16_b__tag.html#adb7111efba1abd2cb2405851a5771a88", null ],
    [ "SAF_CNT_B_EN", "union_c_t_u___c_t_u_i_r__16_b__tag.html#a3e04689fb07768455ce886c3bf5b4b88", null ],
    [ "T0_I", "union_c_t_u___c_t_u_i_r__16_b__tag.html#a38f1b54414561c55e13b7be3c641f8b1", null ],
    [ "T1_I", "union_c_t_u___c_t_u_i_r__16_b__tag.html#ad9bfd011fe0db53dc4b497bc262b8d63", null ],
    [ "T2_I", "union_c_t_u___c_t_u_i_r__16_b__tag.html#a66dbe9d28f4bd07a8175c9e20d3d6aaf", null ],
    [ "T3_I", "union_c_t_u___c_t_u_i_r__16_b__tag.html#ad23ef6f193cc0c75ca923bd22bda800b", null ],
    [ "T4_I", "union_c_t_u___c_t_u_i_r__16_b__tag.html#a3c09356c36eea263b7959ca363d90f4f", null ],
    [ "T5_I", "union_c_t_u___c_t_u_i_r__16_b__tag.html#ab365c44cddc9900dcac82ade72c77e83", null ],
    [ "T6_I", "union_c_t_u___c_t_u_i_r__16_b__tag.html#ac9a7553ea7a8eaf8ca49b8145893b7a9", null ],
    [ "T7_I", "union_c_t_u___c_t_u_i_r__16_b__tag.html#a959bd888ab9ca5244ee82249f8a0c1e6", null ]
];