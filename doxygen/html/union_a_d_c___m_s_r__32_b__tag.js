var union_a_d_c___m_s_r__32_b__tag =
[
    [ "__pad0__", "union_a_d_c___m_s_r__32_b__tag.html#ac4f9ca5e396ec05019375a4f19eee234", null ],
    [ "__pad1__", "union_a_d_c___m_s_r__32_b__tag.html#ad012213e9c4495669847868d50afe47d", null ],
    [ "__pad2__", "union_a_d_c___m_s_r__32_b__tag.html#a943e82f4ca5c9a07f1c07a8ed06c7486", null ],
    [ "__pad3__", "union_a_d_c___m_s_r__32_b__tag.html#a4a48290df7cadf8e7a567c7a811528ec", null ],
    [ "ACKO", "union_a_d_c___m_s_r__32_b__tag.html#ad739d17cbd570aa6086aaf5eb488e6b4", null ],
    [ "ADCSTATUS", "union_a_d_c___m_s_r__32_b__tag.html#a4a240e80ef2f1e67697c3d6f74a1d0f2", null ],
    [ "B", "union_a_d_c___m_s_r__32_b__tag.html#a1358ef4b5aba6e788ba45b5b807a73fc", null ],
    [ "CHADDR", "union_a_d_c___m_s_r__32_b__tag.html#ac736b6bdc171a196aee501649a0d62c9", null ],
    [ "CTUSTART", "union_a_d_c___m_s_r__32_b__tag.html#a458ac3a3ae8098e51e8119d673fd5511", null ],
    [ "JABORT", "union_a_d_c___m_s_r__32_b__tag.html#adc488aff27f3707fde0b38d7e1c8703a", null ],
    [ "JSTART", "union_a_d_c___m_s_r__32_b__tag.html#a06f40c145718fc9eb8738307088d98fb", null ],
    [ "NSTART", "union_a_d_c___m_s_r__32_b__tag.html#a39093e7c4601c5a5a055cc0821b9cf60", null ],
    [ "OFFCANC", "union_a_d_c___m_s_r__32_b__tag.html#a36d326f5f8fed35ac4b52ad4082bc266", null ],
    [ "OFFREFRESH", "union_a_d_c___m_s_r__32_b__tag.html#a0391abd81b3989b98282b6f83d9aefe3", null ],
    [ "R", "union_a_d_c___m_s_r__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ]
];