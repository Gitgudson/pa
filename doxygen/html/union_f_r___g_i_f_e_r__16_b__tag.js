var union_f_r___g_i_f_e_r__16_b__tag =
[
    [ "B", "union_f_r___g_i_f_e_r__16_b__tag.html#a1eb67e43853b3b5d11d3a001b7375d3e", null ],
    [ "CHIE", "union_f_r___g_i_f_e_r__16_b__tag.html#a20337108b19740ad2142637db092fba3", null ],
    [ "CHIF", "union_f_r___g_i_f_e_r__16_b__tag.html#a4691f5ebc3141de17acbfaf03c4cf019", null ],
    [ "FAFAIF", "union_f_r___g_i_f_e_r__16_b__tag.html#ae0b9945cdbec056238b027c3d5060235", null ],
    [ "FAFBIF", "union_f_r___g_i_f_e_r__16_b__tag.html#abc5e8393e8a4a0b4b2523fa411a27dcf", null ],
    [ "FNEAIE", "union_f_r___g_i_f_e_r__16_b__tag.html#ab1ecd08161e58f0c49bd627115b96763", null ],
    [ "FNEBIE", "union_f_r___g_i_f_e_r__16_b__tag.html#a816ae6b6fbc903ed84fb3500a4b7fe2d", null ],
    [ "MIE", "union_f_r___g_i_f_e_r__16_b__tag.html#af6c3c77278cae6c7cd6cc221ce09d49b", null ],
    [ "MIF", "union_f_r___g_i_f_e_r__16_b__tag.html#a472d688ddc98536bd48ea9a8dfd6d141", null ],
    [ "PRIE", "union_f_r___g_i_f_e_r__16_b__tag.html#aa893e1ace97bf83b053ef540f69fb204", null ],
    [ "PRIF", "union_f_r___g_i_f_e_r__16_b__tag.html#a46d8e9a8ac42983cc59a037b9bf4a61d", null ],
    [ "R", "union_f_r___g_i_f_e_r__16_b__tag.html#a46d27991aececcda6046a07b32c865b4", null ],
    [ "RBIE", "union_f_r___g_i_f_e_r__16_b__tag.html#aebd7799e803a1ac84339224eda48ce0e", null ],
    [ "RBIF", "union_f_r___g_i_f_e_r__16_b__tag.html#a3bc2e10e95d5a77051eeba00d6da9e5c", null ],
    [ "TBIE", "union_f_r___g_i_f_e_r__16_b__tag.html#a88399d6f1693e66ac01cac84f4b6fea8", null ],
    [ "TBIF", "union_f_r___g_i_f_e_r__16_b__tag.html#acf4f3046b1be6dd1cde5ace165f8def3", null ],
    [ "WUPIE", "union_f_r___g_i_f_e_r__16_b__tag.html#a256dfd94588320db030b61c761a6899b", null ],
    [ "WUPIF", "union_f_r___g_i_f_e_r__16_b__tag.html#a0fd8947a0c213e1c867bbd3443df4ee3", null ]
];