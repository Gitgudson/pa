var pmsm__current_voltage_8h =
[
    [ "PMSM_CORR_OFFSET_M0_IA", "pmsm__current_voltage_8h.html#a74d6134842bd3e379bd6b19d0231d8d4", null ],
    [ "PMSM_CORR_OFFSET_M0_IB", "pmsm__current_voltage_8h.html#ad6468f9814814c896b0dfb199735ac43", null ],
    [ "PMSM_CORR_OFFSET_M0_IC", "pmsm__current_voltage_8h.html#a92a315cb3ad3cf76253d84b5349095a3", null ],
    [ "PMSM_CORR_OFFSET_M1_IA", "pmsm__current_voltage_8h.html#a9746ae94f54ba4edbdb79f5c74a44cb9", null ],
    [ "PMSM_CORR_OFFSET_M1_IB", "pmsm__current_voltage_8h.html#ad6e81d8dd590598eadb95121f8206087", null ],
    [ "PMSM_CORR_OFFSET_M1_IC", "pmsm__current_voltage_8h.html#a8a8279542a787024f4241a362ec41753", null ],
    [ "PMSM_CURRENT_MID", "pmsm__current_voltage_8h.html#a79b9075e33737e424d22de49b12a6869", null ],
    [ "pmsmMotors_readCurrents", "pmsm__current_voltage_8h.html#aeac1a232f4ce68facd2c19a09282bc29", null ],
    [ "pmsmMotors_readVoltage", "pmsm__current_voltage_8h.html#a4644beb32239396b88146e3bd1170323", null ],
    [ "pmsmMotors_setVoltage", "pmsm__current_voltage_8h.html#a198d5be0a4327fb92c006771dff606a2", null ]
];