var struct_p_i_t___r_t_i__struct__tag =
[
    [ "CH", "struct_p_i_t___r_t_i__struct__tag.html#ac4ef05685864b614920b5bedbd13c6ab", null ],
    [ "CHANNEL", "struct_p_i_t___r_t_i__struct__tag.html#a9b520e41f66c9e2cdd0b839ec3ae00df", null ],
    [ "CVAL0", "struct_p_i_t___r_t_i__struct__tag.html#a7995cb00abbe0f65fd4ac64e4c54691e", null ],
    [ "CVAL1", "struct_p_i_t___r_t_i__struct__tag.html#aaeafee9f0a631ffe3686e16cad76a628", null ],
    [ "CVAL2", "struct_p_i_t___r_t_i__struct__tag.html#ad8f2957a9fc24bba7f71c508b84d012e", null ],
    [ "CVAL3", "struct_p_i_t___r_t_i__struct__tag.html#a7f13de66586cb85114043d943a9fb27d", null ],
    [ "LDVAL0", "struct_p_i_t___r_t_i__struct__tag.html#ac4d26450e0c5a8419f56cd65cdbf5609", null ],
    [ "LDVAL1", "struct_p_i_t___r_t_i__struct__tag.html#a111138605deefb9b0c6c23e77b166087", null ],
    [ "LDVAL2", "struct_p_i_t___r_t_i__struct__tag.html#a12defb5bcae82610ec011b0720e16ca9", null ],
    [ "LDVAL3", "struct_p_i_t___r_t_i__struct__tag.html#acd12a8dfd7b2db6a20fd60f2f401d14b", null ],
    [ "PIT_RTI_reserved_0004", "struct_p_i_t___r_t_i__struct__tag.html#a8260dbdfae16d2c78a2d35c0075639be", null ],
    [ "PIT_RTI_reserved_0140", "struct_p_i_t___r_t_i__struct__tag.html#ae8f286a838e1cfc47a31a4ba192f5ad6", null ],
    [ "PITMCR", "struct_p_i_t___r_t_i__struct__tag.html#ac7831725f86f39786e8ad404c5b90f02", null ],
    [ "TCTRL0", "struct_p_i_t___r_t_i__struct__tag.html#a4fc9c8c23cd6dea6bdc77808800a6a5f", null ],
    [ "TCTRL1", "struct_p_i_t___r_t_i__struct__tag.html#adfe52ce558a8398df0497caa99e11943", null ],
    [ "TCTRL2", "struct_p_i_t___r_t_i__struct__tag.html#aac8bb3edbdce3a8d8757d708e6ad39d5", null ],
    [ "TCTRL3", "struct_p_i_t___r_t_i__struct__tag.html#aec8c96de5957cca934e0153244a3de49", null ],
    [ "TFLG0", "struct_p_i_t___r_t_i__struct__tag.html#aa53c2b5946a89ad1eaf13166db0a86ec", null ],
    [ "TFLG1", "struct_p_i_t___r_t_i__struct__tag.html#ae915ebbf3d8d7a5a3f72d73b7b09e15f", null ],
    [ "TFLG2", "struct_p_i_t___r_t_i__struct__tag.html#a11a8fd1908530e7a845e4f1f3a60523c", null ],
    [ "TFLG3", "struct_p_i_t___r_t_i__struct__tag.html#a363e25d262d62a3ab13f98757ab1c767", null ]
];