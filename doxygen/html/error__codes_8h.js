var error__codes_8h =
[
    [ "error_code_e", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6", [
      [ "NO_ERROR", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6abf350750d0d4fabd8954c0f1e9bbae94", null ],
      [ "ERROR_RESOL_MOTOR_0", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a4de421fbf0305e269bddab2950f80020", null ],
      [ "ERROR_RESOL_MOTOR_1", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a4ecbcea0f5b3b66af194556ec6e7f5b1", null ],
      [ "ERROR_POS_MOTOR_0", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6af9b2579dd4f68b0b0530052b5aaabf7a", null ],
      [ "ERROR_POS_MOTOR_1", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6ad3f93e7a5e42f4cb01157dbfc981b253", null ],
      [ "ERROR_READ_VOLTAGE", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a3444c69be50771979b566688d9b87767", null ],
      [ "ERROR_READ_CURRENT", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a0e7d04711db99b7b3a3c7b5ab4a556e4", null ],
      [ "ERROR_STATE_SPEED_CTRL_M0", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a196c03fb5e4c603e6a31b404d9b53f2f", null ],
      [ "ERROR_STATE_SPEED_CTRL_M1", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a3fa33e5759f854ba1e7b1d3a4fb368b3", null ],
      [ "ERROR_STATE_CURR_CTRL_M0", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a42e344b4e7f14e8831cfedf419721679", null ],
      [ "ERROR_STATE_CURR_CTRL_M1", "error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a1f2d396071844e6111b1e9e0bbffaa15", null ]
    ] ]
];