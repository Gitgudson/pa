var union_c_t_u___c_t_u_c_r__16_b__tag =
[
    [ "B", "union_c_t_u___c_t_u_c_r__16_b__tag.html#acc5e09ad57815f07fd0aacb641f92af0", null ],
    [ "CGRE", "union_c_t_u___c_t_u_c_r__16_b__tag.html#af7009125ba0943ae1638bd15ba627df7", null ],
    [ "CTU_ADC_RESET", "union_c_t_u___c_t_u_c_r__16_b__tag.html#ab9a9e42c3f353da729e47c144d43ecef", null ],
    [ "CTU_ODIS", "union_c_t_u___c_t_u_c_r__16_b__tag.html#aee46c0167895ab765350901455fb9dd6", null ],
    [ "FGRE", "union_c_t_u___c_t_u_c_r__16_b__tag.html#a571fd667a6d647d6668a3a7d4ce894bb", null ],
    [ "FILTER_EN", "union_c_t_u___c_t_u_c_r__16_b__tag.html#a9f4c204d447ca95b6df0929b7098853d", null ],
    [ "GRE", "union_c_t_u___c_t_u_c_r__16_b__tag.html#a3b1d56ee82e8a7bc97b7eab79d46e050", null ],
    [ "MRS_SG", "union_c_t_u___c_t_u_c_r__16_b__tag.html#ae11ede3a59f2cb17134bfd22adb3d318", null ],
    [ "R", "union_c_t_u___c_t_u_c_r__16_b__tag.html#a46d27991aececcda6046a07b32c865b4", null ],
    [ "T0_SG", "union_c_t_u___c_t_u_c_r__16_b__tag.html#a0e1d315976cdf005cb17c05adf02de36", null ],
    [ "T1_SG", "union_c_t_u___c_t_u_c_r__16_b__tag.html#aad313f6033bde7c3acb4e93173ce4e4c", null ],
    [ "T2_SG", "union_c_t_u___c_t_u_c_r__16_b__tag.html#ab880f1bf413c77888678073431f8f1c3", null ],
    [ "T3_SG", "union_c_t_u___c_t_u_c_r__16_b__tag.html#a5faf08b5c226580dedf71883525699fc", null ],
    [ "T4_SG", "union_c_t_u___c_t_u_c_r__16_b__tag.html#af9f275d3ec4f3b481001a08f9d2a7fd3", null ],
    [ "T5_SG", "union_c_t_u___c_t_u_c_r__16_b__tag.html#ac4320ecb733e6e87625e71d6c14c7fe3", null ],
    [ "T6_SG", "union_c_t_u___c_t_u_c_r__16_b__tag.html#a17a8fe4630630f4fb52eecb66ed802ba", null ],
    [ "T7_SG", "union_c_t_u___c_t_u_c_r__16_b__tag.html#a192dcddd5b6d26f7a00b7ef968c49e70", null ],
    [ "TGSISR_RE", "union_c_t_u___c_t_u_c_r__16_b__tag.html#a4eb9405d0e72cc220e26aed9e785fff8", null ]
];