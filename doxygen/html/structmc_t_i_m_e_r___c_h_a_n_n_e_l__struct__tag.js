var structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag =
[
    [ "CAPT1", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#ae22a29933304ef01f4352ad55723757b", null ],
    [ "CAPT2", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#ae811921b63384398f89d39893de58a74", null ],
    [ "CCCTRL", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#a763fe4be008b5f84bfcb31f48030ec7d", null ],
    [ "CMPLD1", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#ab59bb2b507f23efd196124051cef2588", null ],
    [ "CMPLD2", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#a499d8c50173a309bbd4b1dbec806836e", null ],
    [ "CNTR", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#af3b9e0e702d0abe8dc699bdb6e79e48e", null ],
    [ "COMP1", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#abf571ebbade0aff43eb18084a824ca50", null ],
    [ "COMP2", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#a3124193cb9f076bff52e2563bb2b791c", null ],
    [ "CTRL", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#abd4885e652a135ff62dfa48a42b7b600", null ],
    [ "CTRL1", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#a4ba1a084c143d278aa527116fb4691e7", null ],
    [ "CTRL2", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#a7b9da1ff539640468e3d440258dbe5a6", null ],
    [ "CTRL3", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#adb332dca39393ef9f879df063b44ffce", null ],
    [ "FILT", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#af9c5080a1d42a145f97b4ec9f92b4f87", null ],
    [ "HOLD", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#aefe2d93387cc20866c3e1635a07af83d", null ],
    [ "INTDMA", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#a50b6131854d8c4452088667952a670bf", null ],
    [ "LOAD", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#a7fbead3631ff5d6b30f5d6482f8f07ca", null ],
    [ "STS", "structmc_t_i_m_e_r___c_h_a_n_n_e_l__struct__tag.html#ad61ef90e070e8ff76093e19838e5b579", null ]
];