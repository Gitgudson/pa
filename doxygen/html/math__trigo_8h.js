var math__trigo_8h =
[
    [ "PI", "math__trigo_8h.html#a598a3330b3c21701223ee0ca14316eca", null ],
    [ "PI_2", "math__trigo_8h.html#a7c7671235d3f0d09bc5c011920fe0d5b", null ],
    [ "atan4", "math__trigo_8h.html#ac49d4d5f3ec2c693ff1823f2f4f6ac85", null ],
    [ "cos_opt", "math__trigo_8h.html#a60e58d079be4fd9cd7728afdbb303721", null ],
    [ "fastsqrt", "math__trigo_8h.html#a0d30d2a6bbb6b39c18e9cfb99731b933", null ],
    [ "floatmod", "math__trigo_8h.html#a03e779a509703569cf89cab2815578a0", null ],
    [ "sin_cos_opt", "math__trigo_8h.html#a97b4e876b81edba7aad96a7c512b4738", null ],
    [ "sin_opt", "math__trigo_8h.html#a0d8912b8ef1b8bdc04238c8b1894f80d", null ]
];