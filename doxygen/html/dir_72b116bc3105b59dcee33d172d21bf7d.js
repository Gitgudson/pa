var dir_72b116bc3105b59dcee33d172d21bf7d =
[
    [ "button.h", "button_8h.html", "button_8h" ],
    [ "control.h", "control_8h.html", "control_8h" ],
    [ "control_types.h", "control__types_8h.html", [
      [ "Control_piControl_param_s", "struct_control__pi_control__param__s.html", "struct_control__pi_control__param__s" ],
      [ "Control_piControl_data_s", "struct_control__pi_control__data__s.html", "struct_control__pi_control__data__s" ],
      [ "Control_piControl_s", "struct_control__pi_control__s.html", "struct_control__pi_control__s" ]
    ] ],
    [ "filter.h", "filter_8h.html", "filter_8h" ],
    [ "filter_types.h", "filter__types_8h.html", [
      [ "Control_IIRfilter_data_s", "struct_control___i_i_rfilter__data__s.html", "struct_control___i_i_rfilter__data__s" ],
      [ "Control_IIRfilter_param_s", "struct_control___i_i_rfilter__param__s.html", "struct_control___i_i_rfilter__param__s" ],
      [ "Control_IIRfilter_s", "struct_control___i_i_rfilter__s.html", "struct_control___i_i_rfilter__s" ]
    ] ],
    [ "math_tables.h", "math__tables_8h.html", "math__tables_8h" ],
    [ "math_transforms.h", "math__transforms_8h.html", "math__transforms_8h" ],
    [ "math_trigo.h", "math__trigo_8h.html", "math__trigo_8h" ],
    [ "MC33937A.h", "_m_c33937_a_8h.html", "_m_c33937_a_8h" ],
    [ "pmsm_currentVoltage.h", "pmsm__current_voltage_8h.html", "pmsm__current_voltage_8h" ],
    [ "pmsm_motors.h", "pmsm__motors_8h.html", "pmsm__motors_8h" ],
    [ "pmsm_motors_types.h", "pmsm__motors__types_8h.html", "pmsm__motors__types_8h" ],
    [ "pmsm_posSensors.h", "pmsm__pos_sensors_8h.html", "pmsm__pos_sensors_8h" ],
    [ "pmsm_posSensors_types.h", "pmsm__pos_sensors__types_8h.html", "pmsm__pos_sensors__types_8h" ]
];