var union_c_t_u___s_t_s__32_b__tag =
[
    [ "B", "union_c_t_u___s_t_s__32_b__tag.html#a139db34eaef67e5111220557d6eacc79", null ],
    [ "FIFO_EMPTY0", "union_c_t_u___s_t_s__32_b__tag.html#a380f364abaf8df9363f851a58ef71a05", null ],
    [ "FIFO_EMPTY1", "union_c_t_u___s_t_s__32_b__tag.html#a2344819ff16f8b752a54520243d3dec2", null ],
    [ "FIFO_EMPTY2", "union_c_t_u___s_t_s__32_b__tag.html#af6d673773755fe6025d5a6936af79189", null ],
    [ "FIFO_EMPTY3", "union_c_t_u___s_t_s__32_b__tag.html#aeaf43e3e1d008a185c07bd1716721551", null ],
    [ "FIFO_EMPTY4", "union_c_t_u___s_t_s__32_b__tag.html#aa288b765c0f50c4e8c8fea4ad80e69ba", null ],
    [ "FIFO_EMPTY5", "union_c_t_u___s_t_s__32_b__tag.html#af462cf6e608f61480388f9277e4d806d", null ],
    [ "FIFO_EMPTY6", "union_c_t_u___s_t_s__32_b__tag.html#ad8b49eb758122eeb832f014a76941229", null ],
    [ "FIFO_EMPTY7", "union_c_t_u___s_t_s__32_b__tag.html#a646fc926bedd0b7cf9a25e48d65dfa2d", null ],
    [ "FIFO_FULL0", "union_c_t_u___s_t_s__32_b__tag.html#a59866601f5c3a7a7b2f8f20cad8fc4ec", null ],
    [ "FIFO_FULL1", "union_c_t_u___s_t_s__32_b__tag.html#a900480001a5201564492e3d9340556b6", null ],
    [ "FIFO_FULL2", "union_c_t_u___s_t_s__32_b__tag.html#aa555cc661011668289565835a3c9257a", null ],
    [ "FIFO_FULL3", "union_c_t_u___s_t_s__32_b__tag.html#a4f53a3b6984d7ff1170943765747813e", null ],
    [ "FIFO_FULL4", "union_c_t_u___s_t_s__32_b__tag.html#a969e0d3afbdfddc94f4e076a45435ae8", null ],
    [ "FIFO_FULL5", "union_c_t_u___s_t_s__32_b__tag.html#a3a3e2281fdf64661183e67d65a653243", null ],
    [ "FIFO_FULL6", "union_c_t_u___s_t_s__32_b__tag.html#afa2ef35442b5eda6340e5e8cac51d2f8", null ],
    [ "FIFO_FULL7", "union_c_t_u___s_t_s__32_b__tag.html#a100abbc4573fa7ce1f143c3a2f27f406", null ],
    [ "FIFO_OVERFLOW0", "union_c_t_u___s_t_s__32_b__tag.html#a1d5168d6ffede6c685dffe923c0d802a", null ],
    [ "FIFO_OVERFLOW1", "union_c_t_u___s_t_s__32_b__tag.html#af40bfc4aa285c82aead048c11a97bf9d", null ],
    [ "FIFO_OVERFLOW2", "union_c_t_u___s_t_s__32_b__tag.html#a706c46a5d2eeb2dd3384709243233717", null ],
    [ "FIFO_OVERFLOW3", "union_c_t_u___s_t_s__32_b__tag.html#ac1ff3569052e77e947bb144f10ad63d7", null ],
    [ "FIFO_OVERFLOW4", "union_c_t_u___s_t_s__32_b__tag.html#a0bc6c3e5ab2396b34572f9950b3a32c4", null ],
    [ "FIFO_OVERFLOW5", "union_c_t_u___s_t_s__32_b__tag.html#a4244a17f9530d014fdc1d59813c81f68", null ],
    [ "FIFO_OVERFLOW6", "union_c_t_u___s_t_s__32_b__tag.html#a2379a5470ceaf16fb39f1d26b3a4fe87", null ],
    [ "FIFO_OVERFLOW7", "union_c_t_u___s_t_s__32_b__tag.html#a68a1d70705f001192996258a8084fda5", null ],
    [ "FIFO_OVERRUN0", "union_c_t_u___s_t_s__32_b__tag.html#a198c68540881bf49f662f2308179b3e7", null ],
    [ "FIFO_OVERRUN1", "union_c_t_u___s_t_s__32_b__tag.html#ab4fe2f9ddc12bb3cf605ae9d8a1fa476", null ],
    [ "FIFO_OVERRUN2", "union_c_t_u___s_t_s__32_b__tag.html#a4badf58a3d4922c267605a226655d72c", null ],
    [ "FIFO_OVERRUN3", "union_c_t_u___s_t_s__32_b__tag.html#a60d5b13c88687a33c8d448c18104c8ab", null ],
    [ "FIFO_OVERRUN4", "union_c_t_u___s_t_s__32_b__tag.html#a71f74b0b6bd01e52778025d3f3e05f1a", null ],
    [ "FIFO_OVERRUN5", "union_c_t_u___s_t_s__32_b__tag.html#abee4a16e03910c062716974569e14604", null ],
    [ "FIFO_OVERRUN6", "union_c_t_u___s_t_s__32_b__tag.html#a464fe3a03e72e9e0804b07e418875198", null ],
    [ "FIFO_OVERRUN7", "union_c_t_u___s_t_s__32_b__tag.html#ab059723dbaf736670453328b61a07d7a", null ],
    [ "R", "union_c_t_u___s_t_s__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ]
];