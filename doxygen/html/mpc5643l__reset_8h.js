var mpc5643l__reset_8h =
[
    [ "RESET_MASK_JTAG", "mpc5643l__reset_8h.html#a1d9c9306e8d1a7670c063ff589ec70ac", null ],
    [ "RESET_MASK_POR", "mpc5643l__reset_8h.html#a92a2d31e13e2285343db2ed559e08a18", null ],
    [ "RESET_MASK_ST", "mpc5643l__reset_8h.html#a1c59c7b9750736b7ebe89b3d7e1724be", null ],
    [ "RESET_MASK_SWRES", "mpc5643l__reset_8h.html#a25c340240cce8617b6117877c710d0eb", null ],
    [ "RESET_RGM_FBRE", "mpc5643l__reset_8h.html#a2e81cf990bc843933a86e00d2f775fc2", null ],
    [ "RESET_RGM_FEAR", "mpc5643l__reset_8h.html#af63b8cbbb111a566025f6cfa9e7e0397", null ],
    [ "RESET_RGM_FERD", "mpc5643l__reset_8h.html#a5cbbbd3e0ac46afa8e70dc84b53b2e40", null ],
    [ "RESET_RGM_FESS", "mpc5643l__reset_8h.html#a1b018d5e7263d43caeeb95d8f7ea9533", null ],
    [ "reset_clear_reset", "mpc5643l__reset_8h.html#a22fa78ada633a23862138ae31d68522d", null ],
    [ "reset_init", "mpc5643l__reset_8h.html#a5ca3f6a2dad14d8a98cdc79ef1206e1a", null ],
    [ "reset_read_reset", "mpc5643l__reset_8h.html#a66e4807ef90840bd41e8f55a612d3c71", null ]
];