var mpc5643l__dspi__types_8h =
[
    [ "Dspi_ctar_e", "mpc5643l__dspi__types_8h.html#ae91d51fae74bfcdae43a7f8cf172681b", [
      [ "CTAR0", "mpc5643l__dspi__types_8h.html#ae91d51fae74bfcdae43a7f8cf172681ba0454628d3ab2ed6a7c1b98da9c33d4b9", null ],
      [ "CTAR1", "mpc5643l__dspi__types_8h.html#ae91d51fae74bfcdae43a7f8cf172681ba6b235ba5a98ebceb27e554e6c4a2fe61", null ],
      [ "CTAR2", "mpc5643l__dspi__types_8h.html#ae91d51fae74bfcdae43a7f8cf172681badb9b08173909df62cec676721f1f0713", null ],
      [ "CTAR3", "mpc5643l__dspi__types_8h.html#ae91d51fae74bfcdae43a7f8cf172681ba32a451c661d0d7c8d7916f5e6fc110d2", null ]
    ] ],
    [ "Dspi_nbr_e", "mpc5643l__dspi__types_8h.html#a6ead6742386a54834f0792c02855edd8", [
      [ "DSPI0", "mpc5643l__dspi__types_8h.html#a6ead6742386a54834f0792c02855edd8a8f0f1e78f4b6b6b18cf1cb5efc9c3b19", null ],
      [ "DSPI1", "mpc5643l__dspi__types_8h.html#a6ead6742386a54834f0792c02855edd8a5fca2e67ecc9be0fceb483cd0aa5b237", null ],
      [ "DSPI2", "mpc5643l__dspi__types_8h.html#a6ead6742386a54834f0792c02855edd8ace634647680316db6773604ada5ef8eb", null ]
    ] ],
    [ "Dspi_pcs_e", "mpc5643l__dspi__types_8h.html#af27058431ad3c62c2d11d3837b7d132b", [
      [ "PCS0", "mpc5643l__dspi__types_8h.html#af27058431ad3c62c2d11d3837b7d132ba35dc730e3860e4794fd47c5d59bcd1fb", null ],
      [ "PCS1", "mpc5643l__dspi__types_8h.html#af27058431ad3c62c2d11d3837b7d132bae9a4239adb749963ccff30a76d40e611", null ],
      [ "PCS2", "mpc5643l__dspi__types_8h.html#af27058431ad3c62c2d11d3837b7d132ba6062be716a5cef9b4c8741c777820f29", null ],
      [ "PCS3", "mpc5643l__dspi__types_8h.html#af27058431ad3c62c2d11d3837b7d132bac0dc1b7f6f4d7511e80450b4103985ce", null ],
      [ "PCS4", "mpc5643l__dspi__types_8h.html#af27058431ad3c62c2d11d3837b7d132bac79b083b7bf6949045461bac94a42a9e", null ],
      [ "PCS5", "mpc5643l__dspi__types_8h.html#af27058431ad3c62c2d11d3837b7d132baafbe4f0e2aba73cb1ef9209a8da31d11", null ],
      [ "PCS6", "mpc5643l__dspi__types_8h.html#af27058431ad3c62c2d11d3837b7d132ba6e2c3d9d33c5837c9530855a5d4d8267", null ],
      [ "PCS7", "mpc5643l__dspi__types_8h.html#af27058431ad3c62c2d11d3837b7d132ba47a09a9e622f9c321b32213c1a77ebc2", null ]
    ] ]
];