var unionmc_p_w_m___c_t_r_l2__16_b__tag =
[
    [ "B", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#a23d28796909b135253b4462ba0fe2738", null ],
    [ "CLK_SEL", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#aa00c899f362211b03a7f07f88394f7b0", null ],
    [ "DBGEN", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#a5634de64cf6b04f43c65cf6ccae0667c", null ],
    [ "FORCE", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#a1dd2101615b509904fd86dc85b2d1466", null ],
    [ "FORCE_SEL", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#aa96ebdae40225934ec58d5c87245bef9", null ],
    [ "FRCEN", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#ab988c2605ca1d9ad0fc7e01d49b0a03c", null ],
    [ "INDEP", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#af7f8799d16ba32d5d2842b584b005217", null ],
    [ "INIT_SEL", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#af0fa7b58680514e78748ef8521b184df", null ],
    [ "PWM23_INIT", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#a90d367dc24c723e32e98724cccab7273", null ],
    [ "PWM45_INIT", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#a9a42000368cd324c916bc011d28a147a", null ],
    [ "PWMX_INIT", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#acae644936b5a48f4cda7d401de7b20e5", null ],
    [ "R", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#a46d27991aececcda6046a07b32c865b4", null ],
    [ "RELOAD_SEL", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#aa96c8e0076d7086a803df1cf00f9de86", null ],
    [ "WAITEN", "unionmc_p_w_m___c_t_r_l2__16_b__tag.html#a68d6fc98a82410e72200c244be0245d2", null ]
];