var union_d_s_p_i___c_t_a_r__32_b__tag =
[
    [ "ASC", "union_d_s_p_i___c_t_a_r__32_b__tag.html#a44e5e1d16a1be44b5b623faa6a488e96", null ],
    [ "B", "union_d_s_p_i___c_t_a_r__32_b__tag.html#a41a347187d9c016c90250ee03731ae0b", null ],
    [ "BR", "union_d_s_p_i___c_t_a_r__32_b__tag.html#affc55934a83968b789383ca5e51f3bfb", null ],
    [ "CPHA", "union_d_s_p_i___c_t_a_r__32_b__tag.html#ad1435d90e41d7bae4cc3172e87272fb0", null ],
    [ "CPOL", "union_d_s_p_i___c_t_a_r__32_b__tag.html#a431e3b5f979f160c97993bebe4143dca", null ],
    [ "CSSCK", "union_d_s_p_i___c_t_a_r__32_b__tag.html#a72673261ebe18261944a4ee6153dc4cb", null ],
    [ "DBR", "union_d_s_p_i___c_t_a_r__32_b__tag.html#a7b5f14bd5339b9d000c825956756e92b", null ],
    [ "DT", "union_d_s_p_i___c_t_a_r__32_b__tag.html#aab6795339d9bc09928d23c94a6b174ef", null ],
    [ "FMSZ", "union_d_s_p_i___c_t_a_r__32_b__tag.html#a33249431643e06ea24823ab6ca9537f7", null ],
    [ "LSBFE", "union_d_s_p_i___c_t_a_r__32_b__tag.html#a7eb20c54d668d7e419a93e1868388097", null ],
    [ "PASC", "union_d_s_p_i___c_t_a_r__32_b__tag.html#ac6ff84d8c6d397b0699a27e824c5bc8f", null ],
    [ "PBR", "union_d_s_p_i___c_t_a_r__32_b__tag.html#ac89c63f43a942eecdd0d8226e6cb30c8", null ],
    [ "PCSSCK", "union_d_s_p_i___c_t_a_r__32_b__tag.html#ae6caf67c3607f78e0c50c13d7313e8b1", null ],
    [ "PDT", "union_d_s_p_i___c_t_a_r__32_b__tag.html#a9d6ad59d6cd9801cf921221d2fef8ffa", null ],
    [ "R", "union_d_s_p_i___c_t_a_r__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ]
];