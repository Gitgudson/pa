var searchData=
[
  ['adc0_5fctr0',['ADC0_CTR0',['../mpc5643l__adc_8h.html#aa828bed498f1c806b109cfe5b24bc1c6',1,'mpc5643l_adc.h']]],
  ['adc0_5fmcr',['ADC0_MCR',['../mpc5643l__adc_8h.html#a7ecdcd28c0bc7df4ff02cd9fe423cc71',1,'mpc5643l_adc.h']]],
  ['adc1_5fctr0',['ADC1_CTR0',['../mpc5643l__adc_8h.html#a3c0e0603ffd991975ba1b9d755bd9b95',1,'mpc5643l_adc.h']]],
  ['adc1_5fmcr',['ADC1_MCR',['../mpc5643l__adc_8h.html#a58e8014bcc2f988ab06950dc405d75e3',1,'mpc5643l_adc.h']]],
  ['adc_5fbi_5fmax_5fval',['ADC_BI_MAX_VAL',['../mpc5643l__adc_8h.html#a313c01797e06222321eb7392c027640a',1,'mpc5643l_adc.h']]],
  ['adc_5fcdata_5fmask',['ADC_CDATA_MASK',['../mpc5643l__adc_8h.html#a1c8e18c18ff16d60a0eaff83d9c0a790',1,'mpc5643l_adc.h']]],
  ['adc_5fclr_5fceocfr0',['ADC_CLR_CEOCFR0',['../mpc5643l__adc_8h.html#a73b75403d3a3485f083318f7c4a9943a',1,'mpc5643l_adc.h']]],
  ['adc_5fmax_5fval',['ADC_MAX_VAL',['../mpc5643l__adc_8h.html#a3a63fb6f00694ff9f4318b4142c5c868',1,'mpc5643l_adc.h']]],
  ['adc_5fvalid',['ADC_VALID',['../mpc5643l__adc_8h.html#acf4e83fac5a8e2ac5cd9394eb4c9542f',1,'mpc5643l_adc.h']]]
];
