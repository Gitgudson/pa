var searchData=
[
  ['error_5fpos_5fmotor_5f0',['ERROR_POS_MOTOR_0',['../error__codes_8h.html#ae251e459221035ce9052137ad76a34e6af9b2579dd4f68b0b0530052b5aaabf7a',1,'error_codes.h']]],
  ['error_5fpos_5fmotor_5f1',['ERROR_POS_MOTOR_1',['../error__codes_8h.html#ae251e459221035ce9052137ad76a34e6ad3f93e7a5e42f4cb01157dbfc981b253',1,'error_codes.h']]],
  ['error_5fread_5fcurrent',['ERROR_READ_CURRENT',['../error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a0e7d04711db99b7b3a3c7b5ab4a556e4',1,'error_codes.h']]],
  ['error_5fread_5fvoltage',['ERROR_READ_VOLTAGE',['../error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a3444c69be50771979b566688d9b87767',1,'error_codes.h']]],
  ['error_5fresol_5fmotor_5f0',['ERROR_RESOL_MOTOR_0',['../error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a4de421fbf0305e269bddab2950f80020',1,'error_codes.h']]],
  ['error_5fresol_5fmotor_5f1',['ERROR_RESOL_MOTOR_1',['../error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a4ecbcea0f5b3b66af194556ec6e7f5b1',1,'error_codes.h']]],
  ['error_5fstate_5fcurr_5fctrl_5fm0',['ERROR_STATE_CURR_CTRL_M0',['../error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a42e344b4e7f14e8831cfedf419721679',1,'error_codes.h']]],
  ['error_5fstate_5fcurr_5fctrl_5fm1',['ERROR_STATE_CURR_CTRL_M1',['../error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a1f2d396071844e6111b1e9e0bbffaa15',1,'error_codes.h']]],
  ['error_5fstate_5fspeed_5fctrl_5fm0',['ERROR_STATE_SPEED_CTRL_M0',['../error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a196c03fb5e4c603e6a31b404d9b53f2f',1,'error_codes.h']]],
  ['error_5fstate_5fspeed_5fctrl_5fm1',['ERROR_STATE_SPEED_CTRL_M1',['../error__codes_8h.html#ae251e459221035ce9052137ad76a34e6a3fa33e5759f854ba1e7b1d3a4fb368b3',1,'error_codes.h']]]
];
