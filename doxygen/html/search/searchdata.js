var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvwxy",
  1: "abcdfilmoprstuw",
  2: "bcefmpstw",
  3: "abcdefhimnoprstuw",
  4: "abcdefgiklmnopqrstvwxy",
  5: "t",
  6: "defhpsu",
  7: "cdefhnpsu",
  8: "abcdefimoprsuw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

