var searchData=
[
  ['dspi0_5finterrupt',['dspi0_interrupt',['../mpc5643l__dspi_8h.html#a892ceef81b4ae6b32a4901341ec8e44d',1,'mpc5643l_dspi.c']]],
  ['dspi1_5finterrupt',['dspi1_interrupt',['../mpc5643l__dspi_8h.html#a06a5c1e2947e0ac543bfd4fd1e36a527',1,'mpc5643l_dspi.c']]],
  ['dspi2_5finterrupt',['dspi2_interrupt',['../mpc5643l__dspi_8h.html#ab73957774b05958279bbdc5da3b114f0',1,'mpc5643l_dspi.c']]],
  ['dspi_5finit',['dspi_init',['../mpc5643l__dspi_8h.html#aa46ae1d598006792650dafc286fc8d05',1,'mpc5643l_dspi.c']]],
  ['dspi_5fsend',['dspi_send',['../mpc5643l__dspi_8h.html#a1e8a62cdb7b1e4abbca8b6b48e1575a6',1,'mpc5643l_dspi.c']]],
  ['dspi_5fsend_5freceive',['dspi_send_receive',['../mpc5643l__dspi_8h.html#a5b6dfd0eff4c83855b47a52762ccb399',1,'mpc5643l_dspi.c']]]
];
