var struct_s_s_c_m__struct__tag =
[
    [ "CLEN", "struct_s_s_c_m__struct__tag.html#a24825d23d28b840ceed56d843358eb9e", null ],
    [ "DEBUGPORT", "struct_s_s_c_m__struct__tag.html#a6555799826fee472d262d0b292440250", null ],
    [ "DPMBOOT", "struct_s_s_c_m__struct__tag.html#ac14445998afe8f30e43e3f07a87ab2ff", null ],
    [ "DPMKEY", "struct_s_s_c_m__struct__tag.html#a61683ec81cd1eb84d049ba48896d4190", null ],
    [ "ERROR", "struct_s_s_c_m__struct__tag.html#a0e33055d904c3afb249abd49dfbd9477", null ],
    [ "MEMCONFIG", "struct_s_s_c_m__struct__tag.html#a100db1d922d695ff96f19996839c6c9a", null ],
    [ "PSA", "struct_s_s_c_m__struct__tag.html#a07c577079e1a7d7237c0e9f8a0286d44", null ],
    [ "PWCMPH", "struct_s_s_c_m__struct__tag.html#aec41874e5fd1bcd438c67de88c3850fb", null ],
    [ "PWCMPL", "struct_s_s_c_m__struct__tag.html#a7ba1d0621fa951593bba7fb7ab6e1cad", null ],
    [ "SCTR", "struct_s_s_c_m__struct__tag.html#a148ef36f9ab3f0c7393baf372b03e9cb", null ],
    [ "SSCM_reserved_0004", "struct_s_s_c_m__struct__tag.html#aadcf96faaad523d123f995ed8391da9b", null ],
    [ "SSCM_reserved_000A", "struct_s_s_c_m__struct__tag.html#a1ca1ba1c70fca7576be3ea5a9756df87", null ],
    [ "SSCM_reserved_0014", "struct_s_s_c_m__struct__tag.html#a716ce6680bd6967e52a9f0228ee77bf0", null ],
    [ "SSCM_reserved_0030", "struct_s_s_c_m__struct__tag.html#a90c88a64028d41f76e9ffeedae4cbc6a", null ],
    [ "STATUS", "struct_s_s_c_m__struct__tag.html#ac1354ba7a821c88f56cd9fe9706491d8", null ],
    [ "UOPS", "struct_s_s_c_m__struct__tag.html#a1208883346aaee7f4c8242b9c886d8d5", null ]
];