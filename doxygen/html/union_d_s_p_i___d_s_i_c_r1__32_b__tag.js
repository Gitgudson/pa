var union_d_s_p_i___d_s_i_c_r1__32_b__tag =
[
    [ "__pad0__", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#ac4f9ca5e396ec05019375a4f19eee234", null ],
    [ "__pad1__", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#ad012213e9c4495669847868d50afe47d", null ],
    [ "__pad2__", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a943e82f4ca5c9a07f1c07a8ed06c7486", null ],
    [ "B", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a5bf6dd9d60fda1147afdfee445c81c46", null ],
    [ "DPCS1_0", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a9478c96cbafddd024eaf56f6b27b3586", null ],
    [ "DPCS1_1", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#abef785f163db28978d100c775e16afbc", null ],
    [ "DPCS1_2", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a03b8fcfb005e8629ebbe45170bf23fbe", null ],
    [ "DPCS1_3", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a78ff3f4a7dfbb4cdb12812796c73e340", null ],
    [ "DPCS1_4", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a6f642f2794ba581d2f19d8074e7fb724", null ],
    [ "DPCS1_5", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a6adcb198b637632bad2163f9e78b2840", null ],
    [ "DPCS1_6", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a332bb7e640db6550d05da89af5751ed5", null ],
    [ "DPCS1_7", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a7932b440b9699532333c3fb184e5cadf", null ],
    [ "DSE0", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#ad491c6565abe39d0d4319105ff85e8d8", null ],
    [ "DSE1", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a54f7802ad38f073571fe04b0d3cc7c2d", null ],
    [ "R", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ],
    [ "TSBCNT", "union_d_s_p_i___d_s_i_c_r1__32_b__tag.html#a045bfeae702c1473a5d81bd835f3cef0", null ]
];