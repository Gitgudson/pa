var struct_c_r_c__struct__tag =
[
    [ "CFG0", "struct_c_r_c__struct__tag.html#a3c3430b4c57655be780f6cd14c3ab2d6", null ],
    [ "CFG1", "struct_c_r_c__struct__tag.html#a9a79d323df7d03f5179ef24822d1b336", null ],
    [ "CFG2", "struct_c_r_c__struct__tag.html#a8626357e32b790df439e5c7d036ee643", null ],
    [ "CNTX", "struct_c_r_c__struct__tag.html#a255dd4bfa76182b7dcf61cf7a978e3ec", null ],
    [ "CRC_reserved_0030", "struct_c_r_c__struct__tag.html#a59808ddd0001ce844ed6c8437f4b837a", null ],
    [ "CSTAT0", "struct_c_r_c__struct__tag.html#abe3aab4c531e6282c97ab53b93929adb", null ],
    [ "CSTAT1", "struct_c_r_c__struct__tag.html#a0ab83dc6ad7dde23111475758f23e2e6", null ],
    [ "CSTAT2", "struct_c_r_c__struct__tag.html#a8fcb1d77c252ef3b6ae2b2a1a1cb59c9", null ],
    [ "INP0", "struct_c_r_c__struct__tag.html#a8b77ed3d0c1f786b8ecc8d0a8d69a549", null ],
    [ "INP1", "struct_c_r_c__struct__tag.html#acd37ee5a69264e3924267a7e13fcb903", null ],
    [ "INP2", "struct_c_r_c__struct__tag.html#a408717359786c16619be3f3b29a030bc", null ],
    [ "OUTP0", "struct_c_r_c__struct__tag.html#a96bd285fef5bee4b004daf5b067e787f", null ],
    [ "OUTP1", "struct_c_r_c__struct__tag.html#abe4207e84532938cf3b687960432d11c", null ],
    [ "OUTP2", "struct_c_r_c__struct__tag.html#af9f7000ab7386295a70b77e4c0775d06", null ]
];