var pmsm__pos_sensors__types_8h =
[
    [ "Pmsm_speed_s", "struct_pmsm__speed__s.html", "struct_pmsm__speed__s" ],
    [ "Pmsm_enc_s", "struct_pmsm__enc__s.html", "struct_pmsm__enc__s" ],
    [ "Pmsm_res_s", "struct_pmsm__res__s.html", "struct_pmsm__res__s" ],
    [ "Pmsm_pos_s", "struct_pmsm__pos__s.html", "struct_pmsm__pos__s" ],
    [ "Pmsm_pos_speed_s", "struct_pmsm__pos__speed__s.html", "struct_pmsm__pos__speed__s" ],
    [ "Pmsm_encoder_param_s", "struct_pmsm__encoder__param__s.html", "struct_pmsm__encoder__param__s" ],
    [ "Pmsm_resolv_param_s", "struct_pmsm__resolv__param__s.html", "struct_pmsm__resolv__param__s" ],
    [ "Pmsm_encoder_s", "struct_pmsm__encoder__s.html", "struct_pmsm__encoder__s" ],
    [ "Pmsm_resolver_s", "struct_pmsm__resolver__s.html", "struct_pmsm__resolver__s" ],
    [ "Pmsm_sensorsPosition_s", "struct_pmsm__sensors_position__s.html", "struct_pmsm__sensors_position__s" ],
    [ "Pmsm_selectSensPos_e", "pmsm__pos_sensors__types_8h.html#ad9cce07eb7661957c792367b852bcdf3", [
      [ "PMSM_POS_RESOLVER", "pmsm__pos_sensors__types_8h.html#ad9cce07eb7661957c792367b852bcdf3abbad2ff405169df8c2403d07dbce9cd4", null ],
      [ "PMSM_POS_ENCODER", "pmsm__pos_sensors__types_8h.html#ad9cce07eb7661957c792367b852bcdf3aba374636a55f25d50e2c22b46cca66be", null ],
      [ "PSMS_POS_FIX", "pmsm__pos_sensors__types_8h.html#ad9cce07eb7661957c792367b852bcdf3aa5507c1339f751099397fdf2d45e2efb", null ]
    ] ]
];