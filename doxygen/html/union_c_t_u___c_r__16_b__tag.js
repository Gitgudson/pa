var union_c_t_u___c_r__16_b__tag =
[
    [ "B", "union_c_t_u___c_r__16_b__tag.html#af6f7a1e2689ad460bf0fe1b834998424", null ],
    [ "DMA_EN0", "union_c_t_u___c_r__16_b__tag.html#aff86483466b85bd9b1a63db0d6f65c88", null ],
    [ "DMA_EN1", "union_c_t_u___c_r__16_b__tag.html#a2bd320aa70102c5e49d9c339ba7fe1ce", null ],
    [ "DMA_EN2", "union_c_t_u___c_r__16_b__tag.html#acc15d7b2bece78c0688dca31edffb9f4", null ],
    [ "DMA_EN3", "union_c_t_u___c_r__16_b__tag.html#aaab68d64e434894d2d413dff2fbc2bf3", null ],
    [ "DMA_EN4", "union_c_t_u___c_r__16_b__tag.html#aa4563845d5773a8df66c9fb764ccd3f6", null ],
    [ "DMA_EN5", "union_c_t_u___c_r__16_b__tag.html#abff70c3d77dd19a519201bff29f5aa1c", null ],
    [ "DMA_EN6", "union_c_t_u___c_r__16_b__tag.html#a49d391458150051dbbe2fd82716f32cd", null ],
    [ "DMA_EN7", "union_c_t_u___c_r__16_b__tag.html#a84c0f4d6113a33e0f8de09d449e141eb", null ],
    [ "EMPTY_CLR0", "union_c_t_u___c_r__16_b__tag.html#a0a6dda04f69095490c9dcc524b29620b", null ],
    [ "EMPTY_CLR1", "union_c_t_u___c_r__16_b__tag.html#a4944ab6653890a2d1c5c50d6a1c69cb4", null ],
    [ "EMPTY_CLR2", "union_c_t_u___c_r__16_b__tag.html#ad2e3990cddcd7f4903b77312e93ec2ee", null ],
    [ "EMPTY_CLR3", "union_c_t_u___c_r__16_b__tag.html#a41dbfc3d6baaa8c8b9d5253cca777c4e", null ],
    [ "EMPTY_CLR4", "union_c_t_u___c_r__16_b__tag.html#ab60c2c065668ec2f7e8104308f73b6a5", null ],
    [ "EMPTY_CLR5", "union_c_t_u___c_r__16_b__tag.html#aeb87651dada9a0c4e7bbbd81fb5304d9", null ],
    [ "EMPTY_CLR6", "union_c_t_u___c_r__16_b__tag.html#aeee558af504e6df97fb58c23ab05a4db", null ],
    [ "EMPTY_CLR7", "union_c_t_u___c_r__16_b__tag.html#a45afc0d3c69703fcf8a5d84ae2a2908d", null ],
    [ "R", "union_c_t_u___c_r__16_b__tag.html#a46d27991aececcda6046a07b32c865b4", null ]
];