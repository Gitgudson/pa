var union_f_l_e_x_c_a_n___e_s_r__32_b__tag =
[
    [ "__pad0__", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#ac4f9ca5e396ec05019375a4f19eee234", null ],
    [ "__pad1__", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#ad012213e9c4495669847868d50afe47d", null ],
    [ "ACK_ERR", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#a178d1b668ae4021433b68aa024e14cbc", null ],
    [ "B", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#aa0c30a540c8aba7691a042fec12e1e5e", null ],
    [ "BIT0_ERR", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#ac0f57d9510a1df52712103d5d951d410", null ],
    [ "BIT1_ERR", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#ae614bde9339e90af0883ffcf398caf9a", null ],
    [ "BOFF_INT", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#ab0586f2b70f41818844b2c03cda4aebf", null ],
    [ "CRC_ERR", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#ae6417aae5b92af7291509dfd4b7b4bf4", null ],
    [ "ERR_INT", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#af14c336994bd2cdc1c785a37c19dd7e9", null ],
    [ "FLT_CONF", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#afe941f56494e0d4a85ea41df5d59874b", null ],
    [ "FRM_ERR", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#a054c37b388ec50e9ef1e96d902bd865b", null ],
    [ "IDLE", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#abdcc1bfe0dd94b7139dcb792101a67d1", null ],
    [ "R", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ],
    [ "RWRN_INT", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#a0f3ad0cbf36254f0f4b5e4ec5b47b3cb", null ],
    [ "RX_WRN", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#aabf3671e2c340c266f12ed79e5d9ba19", null ],
    [ "STF_ERR", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#a3475c232979c99099c3b517af26ffabb", null ],
    [ "TWRN_INT", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#a5f9fb4006e210effc2f67382acc6d4d7", null ],
    [ "TX_WRN", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#a37749fe91449928b82028a61fde1bda3", null ],
    [ "TXRX", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#a77257135287920f6e7a541f4cbd42d99", null ],
    [ "WAK_INT", "union_f_l_e_x_c_a_n___e_s_r__32_b__tag.html#ad439e240870b4f1b704a5f227516e48e", null ]
];