var scheduler_8h =
[
    [ "TaskStruct_t", "struct_task_struct__t.html", "struct_task_struct__t" ],
    [ "NULL_PTR", "scheduler_8h.html#a530f11a96e508d171d28564c8dc20942", null ],
    [ "Task_t", "scheduler_8h.html#adec0b2ec8c805c5b5b13e6ae0a2b650d", null ],
    [ "PERIOD_LIST", "scheduler_8h.html#afab3ec5036289501c3b540a393ed20e5", [
      [ "TASK_PERIOD_1ms", "scheduler_8h.html#afab3ec5036289501c3b540a393ed20e5a54d0db3bdc7f97f63aaf7f4f3743ce21", null ],
      [ "TASK_PERIOD_2ms", "scheduler_8h.html#afab3ec5036289501c3b540a393ed20e5aa35d399dcac9aed7f67fdb4e139f4289", null ],
      [ "TASK_PERIOD_5ms", "scheduler_8h.html#afab3ec5036289501c3b540a393ed20e5ad8437d956a972b3666b57b477af19e9f", null ],
      [ "TASK_PERIOD_10ms", "scheduler_8h.html#afab3ec5036289501c3b540a393ed20e5a01f919bf96b46afe42924b1e799ab772", null ],
      [ "TASK_PERIOD_20ms", "scheduler_8h.html#afab3ec5036289501c3b540a393ed20e5ad9b8a8782c592261b47a84ff288649f6", null ],
      [ "TASK_PERIOD_50ms", "scheduler_8h.html#afab3ec5036289501c3b540a393ed20e5ae9b5879e527608cc32369f9aaa55efe0", null ],
      [ "TASK_PERIOD_100ms", "scheduler_8h.html#afab3ec5036289501c3b540a393ed20e5a9f06563d070ed86aef7b8b7fa1d10dc8", null ],
      [ "TASK_PERIOD_200ms", "scheduler_8h.html#afab3ec5036289501c3b540a393ed20e5aed929f22975f306e1f8faedf5a5737b3", null ],
      [ "TASK_PERIOD_500ms", "scheduler_8h.html#afab3ec5036289501c3b540a393ed20e5a463438dbbb2c7c690e8c6b061f715bd0", null ],
      [ "TASK_PERIOD_1s", "scheduler_8h.html#afab3ec5036289501c3b540a393ed20e5a2e1469a0d83a3b6a8fb0a290f61d65fa", null ]
    ] ],
    [ "schedulerAddTask", "scheduler_8h.html#a444bbe7fe86b335d5fe8ae72891ca069", null ],
    [ "schedulerInit", "scheduler_8h.html#a95118a58fb25c554be85889f5aa44086", null ],
    [ "schedulerIsTaskRunning", "scheduler_8h.html#a0acca6d365c6b923e5798a89c9c871f6", null ],
    [ "schedulerPauseTask", "scheduler_8h.html#acfafde134082ffc52de4ba7297ca12f3", null ],
    [ "schedulerRemoveTask", "scheduler_8h.html#a8d051879d9ae69b125aafee145af57fd", null ],
    [ "schedulerResumeTask", "scheduler_8h.html#ab9276f632a52d0d3a156d91ff411f3f1", null ],
    [ "schedulerRun", "scheduler_8h.html#a9123c17a30a321621a23510c48c6dec1", null ]
];