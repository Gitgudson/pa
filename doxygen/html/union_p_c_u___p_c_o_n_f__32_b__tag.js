var union_p_c_u___p_c_o_n_f__32_b__tag =
[
    [ "__pad0__", "union_p_c_u___p_c_o_n_f__32_b__tag.html#ac4f9ca5e396ec05019375a4f19eee234", null ],
    [ "__pad1__", "union_p_c_u___p_c_o_n_f__32_b__tag.html#ad012213e9c4495669847868d50afe47d", null ],
    [ "__pad2__", "union_p_c_u___p_c_o_n_f__32_b__tag.html#a943e82f4ca5c9a07f1c07a8ed06c7486", null ],
    [ "B", "union_p_c_u___p_c_o_n_f__32_b__tag.html#a53c56412fe8f4c7b9ba2ba7b60457dd2", null ],
    [ "DRUN", "union_p_c_u___p_c_o_n_f__32_b__tag.html#abbe11b50e7585aaa533a5ab0c3758d76", null ],
    [ "HALT0", "union_p_c_u___p_c_o_n_f__32_b__tag.html#a8eaf345180a4cbad4e2bca8985e8c196", null ],
    [ "R", "union_p_c_u___p_c_o_n_f__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ],
    [ "RST", "union_p_c_u___p_c_o_n_f__32_b__tag.html#ad01095607616aa5621642cf04ac6a7b9", null ],
    [ "RUN0", "union_p_c_u___p_c_o_n_f__32_b__tag.html#a9ecfe70c51856f7b6b6c18ad31911259", null ],
    [ "RUN1", "union_p_c_u___p_c_o_n_f__32_b__tag.html#a1569ded8410c1833ab086ae4aa417975", null ],
    [ "RUN2", "union_p_c_u___p_c_o_n_f__32_b__tag.html#ad525855e96034c8347d81cb5070d1e21", null ],
    [ "RUN3", "union_p_c_u___p_c_o_n_f__32_b__tag.html#a9d5a69fcc0e16242b955315091ea1980", null ],
    [ "SAFE", "union_p_c_u___p_c_o_n_f__32_b__tag.html#aa8d2e0102f34f85b7711c640af22d423", null ],
    [ "STBY0", "union_p_c_u___p_c_o_n_f__32_b__tag.html#a2fa93691148a231680031daf3afa9c2a", null ],
    [ "STOP0", "union_p_c_u___p_c_o_n_f__32_b__tag.html#a94a4a943ca94d3674af33f04dd12f61f", null ],
    [ "TEST", "union_p_c_u___p_c_o_n_f__32_b__tag.html#a5a6e1b1224f28a346750dc769281c56a", null ]
];