var mpc5643l__interrupts_8h =
[
    [ "INTERRUPTS_HIGH_PRIO", "mpc5643l__interrupts_8h.html#a7fe828336d799d44b1c082f096154a11", null ],
    [ "INTERRUPTS_LOW_PRIO", "mpc5643l__interrupts_8h.html#af6f1c16ff0d9779ebca441de14c6303c", null ],
    [ "INTERRUPTS_NBR", "mpc5643l__interrupts_8h.html#a68f811dfe107e67ae8000fd7304f1f6d", null ],
    [ "interrupts_disable", "mpc5643l__interrupts_8h.html#a3a4ad967355a2dfd3f9740d0ba9054b6", null ],
    [ "interrupts_enable", "mpc5643l__interrupts_8h.html#a8c2f37c814d053cffc105ffb0c4c91b7", null ],
    [ "interrupts_init", "mpc5643l__interrupts_8h.html#a5c99cba5ccc698fc7feb8902442176ea", null ],
    [ "interrupts_IRQ_error", "mpc5643l__interrupts_8h.html#a88a90e7247b392a1c58276fb710334f8", null ]
];