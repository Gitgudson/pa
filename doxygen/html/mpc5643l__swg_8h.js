var mpc5643l__swg_8h =
[
    [ "SWG_CHECK_CLEAR_ERROR", "mpc5643l__swg_8h.html#a11fbffcb4af5f30741c16982bd1c180d", null ],
    [ "SWG_CTRL_DEF", "mpc5643l__swg_8h.html#a7a6267c0625646141bdca97e3c0a76b5", null ],
    [ "SWG_INT_ERR_EN", "mpc5643l__swg_8h.html#a488bc064570f7404f5d4af5da55486d0", null ],
    [ "swg_clear_error", "mpc5643l__swg_8h.html#ad35a8656c5a4490457f03fe7fd08e3b8", null ],
    [ "swg_init", "mpc5643l__swg_8h.html#a3e06057648f867009e2f6503bb266bb4", null ],
    [ "swg_IRQ_error", "mpc5643l__swg_8h.html#aeac51f4e3f1c9097683e86b5f33d2c23", null ],
    [ "swg_is_error", "mpc5643l__swg_8h.html#ae60e1ae3f30d3957653a01aea9af20d1", null ],
    [ "swg_start", "mpc5643l__swg_8h.html#ae7bc8f6dbd7c7febae5fc6011bc00121", null ],
    [ "swg_stop", "mpc5643l__swg_8h.html#a27822ea9887f1f22e3ce622cbceeebce", null ]
];