var mpc5643l__siul__types_8h =
[
    [ "SIUL_HIGH", "mpc5643l__siul__types_8h.html#a399b59d26c94476883f8d303d9392acc", null ],
    [ "SIUL_LOW", "mpc5643l__siul__types_8h.html#a45605222264a65f4a874bb869ca63eb8", null ],
    [ "Siul_gpio_mode_e", "mpc5643l__siul__types_8h.html#a9540c9e88a474fa9909e839a1c164879", [
      [ "SIUL_MODE_GPIO", "mpc5643l__siul__types_8h.html#a9540c9e88a474fa9909e839a1c164879a3a8f0cda4096f9041dc2ed25a3c553b9", null ],
      [ "SIUL_MODE_ALTER_1", "mpc5643l__siul__types_8h.html#a9540c9e88a474fa9909e839a1c164879a190ee94f9f8c8f82a2f2e9baaa90eb93", null ],
      [ "SIUL_MODE_ALTER_2", "mpc5643l__siul__types_8h.html#a9540c9e88a474fa9909e839a1c164879a0978bb69890348ebc300c8f1de0298ff", null ],
      [ "SIUL_MODE_ALTER_3", "mpc5643l__siul__types_8h.html#a9540c9e88a474fa9909e839a1c164879a2686a14577cc8e0de1c19402770013bc", null ]
    ] ],
    [ "Siul_inputOutput_e", "mpc5643l__siul__types_8h.html#a03d2fd583a629b4ad629dc33e5b86cc1", [
      [ "SIUL_INPUT", "mpc5643l__siul__types_8h.html#a03d2fd583a629b4ad629dc33e5b86cc1ad4c00705daf6a1b93c8f55f53d92b360", null ],
      [ "SIUL_OUTPUT", "mpc5643l__siul__types_8h.html#a03d2fd583a629b4ad629dc33e5b86cc1a928ec66d1ea50ef1bcee23433bbc332d", null ],
      [ "SIUL_INOUT_NONE", "mpc5643l__siul__types_8h.html#a03d2fd583a629b4ad629dc33e5b86cc1a275ea5a6b5240e38a5f525abecfdfc77", null ]
    ] ],
    [ "Siul_inputOutputType_e", "mpc5643l__siul__types_8h.html#ab16f598420a5d21ff19d7032e076ac5a", [
      [ "SIUL_TYPE_NONE", "mpc5643l__siul__types_8h.html#ab16f598420a5d21ff19d7032e076ac5aa5422ea3ac940f757b39ef437227a51ca", null ],
      [ "SIUL_OUTPUT_OPEN_DRAIN", "mpc5643l__siul__types_8h.html#ab16f598420a5d21ff19d7032e076ac5aa4c438f441835c52e6325e2836b9b0e44", null ],
      [ "SIUL_INPUT_PULL_UP", "mpc5643l__siul__types_8h.html#ab16f598420a5d21ff19d7032e076ac5aa13a8126deb806fc538f81f94a7ef8f3c", null ],
      [ "SIUL_INPUT_PULL_DOWN", "mpc5643l__siul__types_8h.html#ab16f598420a5d21ff19d7032e076ac5aa712d1b157532a4d3394801930ef73621", null ]
    ] ],
    [ "Siul_interruptType_e", "mpc5643l__siul__types_8h.html#a74464c249be950ede65083b5569674ef", [
      [ "SIUL_RISING_EDGE", "mpc5643l__siul__types_8h.html#a74464c249be950ede65083b5569674efa46eb2433749700fe602598d2dc284d45", null ],
      [ "SIUL_FALLING_EDGE", "mpc5643l__siul__types_8h.html#a74464c249be950ede65083b5569674efacbb249668eae8dda33cf159bf75fc657", null ]
    ] ],
    [ "Siul_pcr_e", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9", [
      [ "PCR_A_0", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a6db25b484fb45b1967314c465da3edab", null ],
      [ "PCR_A_1", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a81f0d867c2a69d4c725e2bbbc754b8fa", null ],
      [ "PCR_A_2", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ab32c8cb0049632e4f9ab9fa338a1944e", null ],
      [ "PCR_A_3", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a541df09472c5239384f5f92e04821900", null ],
      [ "PCR_A_4", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ab513bc405a168fc8e2ce189cccf3c139", null ],
      [ "PCR_A_5", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a22abb4ec9e6729d0261e8f6e92c105e8", null ],
      [ "PCR_A_6", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ab8ae1a39aba26dc02f7083143e973209", null ],
      [ "PCR_A_7", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a8d7756ebb71ef0abafb773b7c54c7d7f", null ],
      [ "PCR_A_8", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a34e4f82b5c653429ad962a76e36775f0", null ],
      [ "PCR_A_9", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aeda7399bae3c2daadbc5ed36b27c04c6", null ],
      [ "PCR_A_10", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a414ebb73d4e7849bdd41f72c238f4555", null ],
      [ "PCR_A_11", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a86ac7995711b6fef366792f2eddd37e0", null ],
      [ "PCR_A_12", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9abf49d1e471fafcbfb67713e67b35049e", null ],
      [ "PCR_A_13", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ad7b0dc7a17869e4c132f2a567fdbe8ae", null ],
      [ "PCR_A_14", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a412a9067f98117ca4be6011fa8256551", null ],
      [ "PCR_A_15", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ac14c9f0b53bd7671118936b6cd7f4cad", null ],
      [ "PCR_B_0", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ac82141b26f7fc51ca7f5d391c97b9e40", null ],
      [ "PCR_B_1", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a37dab837d3c456cd2dba47ccf1e15b51", null ],
      [ "PCR_B_2", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9adb36194a3e97f9cb3b584d32ed285b6b", null ],
      [ "PCR_B_3", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ab35f94562fa7fd98896228358d4c53e3", null ],
      [ "PCR_B_4", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a8187f0be0aa42af2f38b4c8046639ede", null ],
      [ "PCR_B_5", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a509aee79def80e09eccf9bea45b88681", null ],
      [ "PCR_B_6", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ae660f9c75549a932ba8dede7ab35125f", null ],
      [ "PCR_B_7", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a1541f8d9d33c6c20668916e066ce83da", null ],
      [ "PCR_B_8", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a8dee49396ee06b526689ec667290f028", null ],
      [ "PCR_B_9", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9af74812cbc8d3b049eb7d72e7acac4174", null ],
      [ "PCR_B_10", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ab5ca3fb81b7d352a5b184714223f7d3e", null ],
      [ "PCR_B_11", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aba082e3ecf05a00333af69fad1e68f4f", null ],
      [ "PCR_B_12", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a40b124e089f3439c50aab1cfcf260a7a", null ],
      [ "PCR_B_13", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a64433d4c3df4a0caf058d5e026f8b640", null ],
      [ "PCR_B_14", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a559bfc00b0a8134ae64b8fffab0ed321", null ],
      [ "PCR_B_15", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a437c5bf382a8a53736ce7355cb0eef51", null ],
      [ "PCR_C_0", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a99ff0d4b019d994f2a37541337d6a9a7", null ],
      [ "PCR_C_1", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aad75c442c5dd58f34fe5c3056f0f4e18", null ],
      [ "PCR_C_2", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a2c7ea1333140ba3416554464357fb3c0", null ],
      [ "PCR_C_3", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a81d8d202e2c044c142f89860dc4b93d6", null ],
      [ "PCR_C_4", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a86df8daa56bf130b891f706644adb4ef", null ],
      [ "PCR_C_5", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ae2a8ff696b76d48be1b577d71d50d6b7", null ],
      [ "PCR_C_6", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aaba9c5f401f048e8483923a46e494ef7", null ],
      [ "PCR_C_7", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aef64c41e87104560b554d1d65656ca12", null ],
      [ "PCR_C_10", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9af931c5c8990e1b08d9ce41ddfffa72a0", null ],
      [ "PCR_C_11", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a90ffcb2b205a823b4b672753b6b50be3", null ],
      [ "PCR_C_12", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a540f0608619072a39134a2e744948194", null ],
      [ "PCR_C_13", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a4ce45bbf8f8dee35404ad62dd12181fb", null ],
      [ "PCR_C_14", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aea830fec39d9bad6148025b2b0735edb", null ],
      [ "PCR_C_15", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a346e6118d4b44fef238391c3554b63cd", null ],
      [ "PCR_D_0", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9adec0b943c2f93ee39a3380afbd043bed", null ],
      [ "PCR_D_1", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ac337700ddd8a61e686cd1a20f1361fe0", null ],
      [ "PCR_D_2", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a5b4291cd215a29c9743e1c197a562d70", null ],
      [ "PCR_D_3", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a63cd43f36e78ac03ff68cc18a3547f8a", null ],
      [ "PCR_D_4", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9acf68e3a4add1d72d2e55d77f22399fba", null ],
      [ "PCR_D_5", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a5903b7e22abf1006ed28fc4c86d6294a", null ],
      [ "PCR_D_6", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9af2e6f0d9298a85085b1d80acfb83a895", null ],
      [ "PCR_D_7", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a8477f636cfeb396a3e1558b647dfa313", null ],
      [ "PCR_D_8", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a8c2f092b772d8eff356cc40cc0cf92f5", null ],
      [ "PCR_D_9", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aa192efca2e24f1ec3c3b174bcfd9fbf4", null ],
      [ "PCR_D_10", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9adb0a7f84e4088a79a4056acba784a5a3", null ],
      [ "PCR_D_11", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a9f1f486169bed37c767b890cc2f80839", null ],
      [ "PCR_D_12", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9adfcadcbbb0e3188785282c1d5abfc4f3", null ],
      [ "PCR_D_14", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ae530a13c3666a4bc0999c159a823688b", null ],
      [ "PCR_E_0", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9acf2ffda7ad8ae7bddd36819889b34d2d", null ],
      [ "PCR_E_2", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ad47b6da4bccb17f58d4f51fca1359081", null ],
      [ "PCR_E_4", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ac2d89ca59d13b053ca338d68d3ca3122", null ],
      [ "PCR_E_5", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9af5bd99ab2d7375196cd89998eb4822d7", null ],
      [ "PCR_E_6", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9af5c55b5379baa3b417c4a93770b184a2", null ],
      [ "PCR_E_7", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a33d896bbf3017014bbe6a224d9bc94a1", null ],
      [ "PCR_E_9", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a8b1f7446c7f08fe9c363f0e9dde3fc31", null ],
      [ "PCR_E_10", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a1051bf03a69a5fd346d8b3167d0bbdee", null ],
      [ "PCR_E_11", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a7434a72aa0887e8b56c076d2f494f6f7", null ],
      [ "PCR_E_12", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a525b15d195d0a525b63ad07f4617f9eb", null ],
      [ "PCR_E_13", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aa10a656051d0758fb75cf8e1b543249d", null ],
      [ "PCR_E_14", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9abdc3f2793f8ef164bcc02553c8594121", null ],
      [ "PCR_E_15", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a283b17bf0ddc613fc47395f0e114e8da", null ],
      [ "PCR_F_0", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a1e9ef3ed5cbcbba4b0e77e4aff3888e2", null ],
      [ "PCR_F_3", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a563000791a684fa4a9659ead0b43814e", null ],
      [ "PCR_F_4", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a1b50e508e0d937681fdd240445a64b92", null ],
      [ "PCR_F_5", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a62f3d0b14b232aea7218bc614d7e4a6f", null ],
      [ "PCR_F_6", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a43bbb13131022c83a733969627f57acc", null ],
      [ "PCR_F_7", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a8a28c3943f71c0567e25b2851c12e7b6", null ],
      [ "PCR_F_8", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9afaa25c3f23987d412c6bee5c7f991359", null ],
      [ "PCR_F_9", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aa543dfac5536cbf5e4f01770e7b2eec6", null ],
      [ "PCR_F_10", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a95cb1765c07f890966b28b4bea78bca7", null ],
      [ "PCR_F_11", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a87c5b56b36a9126ae0c0a2e63091fe97", null ],
      [ "PCR_F_12", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a4cf8da87606c999770ac64a15db755ab", null ],
      [ "PCR_F_13", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ae7b58b0a25b04e08c4c49268a57e656e", null ],
      [ "PCR_F_14", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a6c728dc34e86218ddf5d819f8e684ea0", null ],
      [ "PCR_F_15", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a9cabf8537f941085184955eeed1bab99", null ],
      [ "PCR_G_2", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ae18654e8f19528a4288b0af1172f67f3", null ],
      [ "PCR_G_3", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ad4fa284cf845a570565dd46b2347842a", null ],
      [ "PCR_G_4", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a96fcfb2cca8e6c09a10891f5d146a64f", null ],
      [ "PCR_G_5", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a52c2035a8b9d247afb14429eaba3734c", null ],
      [ "PCR_G_6", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a234f9c91c608990d83c87978e24d5f74", null ],
      [ "PCR_G_7", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ace58492280a710e5214e42756286d0c2", null ],
      [ "PCR_G_8", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ae7ed23d7a2a949c0ed74d5c86eff45e0", null ],
      [ "PCR_G_9", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a0c09322b27df9e337d75eee5f4225dde", null ],
      [ "PCR_G_10", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a0ba9f774e0537e458a7a7bd34cdfb0a3", null ],
      [ "PCR_G_11", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a79829d66dbe80faa3577f2964b887c17", null ],
      [ "PCR_G_12", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9abecb06e99e7766fe440956f1b154d360", null ],
      [ "PCR_G_13", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a7509763e759a03fe6337cb58d7cfcfa5", null ],
      [ "PCR_G_14", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aff1a017eb13f9d5fbea916bec00a382e", null ],
      [ "PCR_G_15", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9abd18f356f9dd629ade218d90d8ce6d80", null ],
      [ "PCR_H_0", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a676633e6486af92695c413a8c8a61dae", null ],
      [ "PCR_H_1", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9af86b687fa55605c09656952ab988e5d0", null ],
      [ "PCR_H_2", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9af0d688fe9308bdde01bd98cdab7e938e", null ],
      [ "PCR_H_3", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ae4b0a15cb8ac0846f34f8d7c48929474", null ],
      [ "PCR_H_4", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a169de32bed5327042e99fe20d244869a", null ],
      [ "PCR_H_5", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a1b02d406b03ee97c90964da3bfcdbeaa", null ],
      [ "PCR_H_6", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ad4e8a595e0f092aabfea0ed1b07720a1", null ],
      [ "PCR_H_7", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ac4726d37d9263bc21ef543b7a5e429e4", null ],
      [ "PCR_H_8", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a58eb3710091c60184c5b56ea04694f38", null ],
      [ "PCR_H_9", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aa608846f48df859731d288cfda7f02b1", null ],
      [ "PCR_H_10", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a58b67b60c68b7a0e11b049b9478cff37", null ],
      [ "PCR_H_11", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a9ee341af036d7ac85fc7518c1e297e8b", null ],
      [ "PCR_H_12", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a2e6c165a8edb79684ccbf7774172af8f", null ],
      [ "PCR_H_13", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ae2c2c9d24763342ce7aa10fa69b9be61", null ],
      [ "PCR_H_14", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a995a54c748e5276d7e9e856d058cac90", null ],
      [ "PCR_H_15", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a2d5cc50241e70c697d29c4c2136a9a45", null ],
      [ "PCR_I_0", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9aad89db6ef8cfc6a96e0ab6ee028e52a5", null ],
      [ "PCR_I_1", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a6cb6539d9d7dd9c5771a087961af7c3b", null ],
      [ "PCR_I_2", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9acd215878810d609d2cdc34a26f88baea", null ],
      [ "PCR_I_3", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ae17e76a516069aceba1bcc6c10365f04", null ],
      [ "PCR_RDY", "mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9a707402e9f087d272609c4138726b2b1d", null ]
    ] ],
    [ "Siul_slewRate_e", "mpc5643l__siul__types_8h.html#a20b2e69682d9ffbd5ddf88df1ddef911", [
      [ "SIUL_SLOW", "mpc5643l__siul__types_8h.html#a20b2e69682d9ffbd5ddf88df1ddef911a2c1428cf4fbdf58178e23221c9541d1f", null ],
      [ "SIUL_FAST", "mpc5643l__siul__types_8h.html#a20b2e69682d9ffbd5ddf88df1ddef911ad6684cdbd4d2758eded5567d3ed22d6f", null ]
    ] ]
];