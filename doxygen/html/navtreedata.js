/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Secure processor for critical applications", "index.html", [
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_exceptions_8h_source.html",
"mpc5643l__dspi_8h.html#ae11f2fa06cbac4a13944ac789f12ef6b",
"mpc5643l__etimer_8h.html#af3ebadf7a19075d60465f2b4b35ca104",
"mpc5643l__interrupts_8h.html",
"mpc5643l__siul__types_8h.html#a363be5a7971934c1f0fb20d493f5d5d9ae18654e8f19528a4288b0af1172f67f3",
"struct_a_d_c__struct__tag.html#a21842dfc51161d784f0dae7b150eea5e",
"struct_c_f_l_a_s_h___s_h_a_d_o_w__struct__tag.html#a2561a2884132cd8da305f73540fda97e",
"struct_c_f_l_a_s_h___s_h_a_d_o_w__struct__tag.html#abe97fbf166902aa37d705595b5eac4d0",
"struct_c_t_u__struct__tag.html#a17c2ea1082de28a3fe050c33fe53af7f",
"struct_f_c_c_u__struct__tag.html#a773d72e48649ef55a16db5f96cc63029",
"struct_f_l_e_x_c_a_n__struct__tag.html#a80aaeca4ec4c61dfe4b569e574165d87",
"struct_f_r__struct__tag.html#a1a4b2e7b86c6992ebaa759dd3b88dbf7",
"struct_f_r__struct__tag.html#ac4f4eaaffec10e742e49eb78ea74f3ce",
"struct_i_n_t_c__struct__tag.html#a2ca68a4e87600253a2f9c9755bccf7d7",
"struct_i_n_t_c__struct__tag.html#a85163ce4c30eac4212dda8b45247d9a1",
"struct_i_n_t_c__struct__tag.html#af3ec5f17f30318d391b0b87fe11b6233",
"struct_m_p_u__struct__tag.html#a77859b465fa7e48dcd4cdbbc73eb2eb5",
"struct_r_g_m__struct__tag.html#a656534d7444f0df7c4e175146a8dd7bb",
"struct_s_i_u_l__struct__tag.html#a1830646ffa678e4ff962310d630fdaee",
"struct_s_i_u_l__struct__tag.html#a352bf25bfe018e776b3856f3f520f281",
"struct_s_i_u_l__struct__tag.html#a5262cd6b67eec71998e46f69cfdf7e81",
"struct_s_i_u_l__struct__tag.html#a6e7ebd5fecef9940c02e9367404bc427",
"struct_s_i_u_l__struct__tag.html#a8a65c1066de678b10dd0fa85c765021e",
"struct_s_i_u_l__struct__tag.html#aa5ec7306e8625a6ebef2133edd15c889",
"struct_s_i_u_l__struct__tag.html#ac0c630c63c8bfb65c6533aa9f458e6f7",
"struct_s_i_u_l__struct__tag.html#ade5418083d13d2760be39bbd1a32e492",
"struct_s_i_u_l__struct__tag.html#af9e2b77c0860ea96d211fe83f436a2e5",
"struct_s_p_p___d_m_a2__struct__tag.html#afdb330df05422ecf782c92b2066c1f59",
"structmc_p_w_m__struct__tag.html#ae01e9014e129a2875f06984e0a773dd4",
"union_a_d_c___c_e_o_c_f_r0__32_b__tag.html#abda0d418da86be887867078e259fa99a",
"union_a_d_c___m_c_r__32_b__tag.html#afbe9832c3115f1bdf086fb94a2fc39c1",
"union_a_d_c___s_t_s_r3__32_b__tag.html#a50fcd418168922527a122ef5bf9cd553",
"union_c_g_m___o_c___e_n__32_b__tag.html#ac584aa1db3f5e7c96594886eecde3762",
"union_c_t_u___c_t_u_i_r__16_b__tag.html#adb7111efba1abd2cb2405851a5771a88",
"union_d_s_p_i___a_s_d_r__32_b__tag.html#afae436ebf54c04fdc0145fc731557ba8",
"union_d_s_p_i___t_x_f_r__32_b__tag.html#aff7ddf9d26263b0019fa1038e7072ecd",
"union_f_c_c_u___c_f___s2__32_b__tag.html#ace43cd21ed00db1f129c40cd793cb609",
"union_f_c_c_u___i_r_q___s_t_a_t__32_b__tag.html#a2513876b78d3d7ca37c34e2755b02c7d",
"union_f_c_c_u___n_c_f___e2__32_b__tag.html#a3e621cd6c1be9791ac7f60a384ceceb1",
"union_f_c_c_u___n_c_f___t_o_e1__32_b__tag.html#a26314c83ec6e180fb0c46ba36db59e90",
"union_f_c_c_u___n_c_f_s___c_f_g7__32_b__tag.html#a7e0d00c594cc40152d1cc3c7daa35fc4",
"union_f_l_e_x_c_a_n___m_c_r__32_b__tag.html#a0e36cfbde1c4c5a88a709abf2b3fa6f6",
"union_f_r___m_b_s_s_u_t_r__16_b__tag.html#a9595b6e360ac2872a02e85e861885134",
"union_f_r___p_e_d_r_a_r__16_b__tag.html#a6e03ec22b238b7d8104300c235cd20ab",
"union_f_r___s_f_i_d_r_f_r__16_b__tag.html#a7a0e07dd308edda246f4f7f405cf595c",
"union_l_i_n_f_l_e_x___b_i_d_r__32_b__tag.html#ac4f9ca5e396ec05019375a4f19eee234",
"union_m_e___d_m_t_s__32_b__tag.html#a2d989cb4469f86685dbcc9eed813a789",
"union_m_e___t_e_s_t___m_c__32_b__tag.html#a4a48290df7cadf8e7a567c7a811528ec",
"union_p_b_r_i_d_g_e___o_p_a_c_r64__71__32_b__tag.html#a7ea6be71da339c5ff690242ea0a7a6cc",
"union_p_m_u_c_t_r_l___i_r_q_e__32_b__tag.html#a4a305f05232703c186d02cae8c5e77b3",
"union_s_g_e_n_d_i_g___i_r_q_e__32_b__tag.html",
"union_s_i_u_l___m_p_g_p_d_o__32_b__tag.html#aded89ab720d31aa4d7c085e13d94c9cc",
"union_s_p_p___d_m_a2___t_c_d_w_o_r_d28____32_b__tag.html#a8516fc5e73b3664fdb1573e0279c9170",
"union_s_t_m___c_r__32_b__tag.html",
"unionmc_p_w_m___o_u_t_e_n__16_b__tag.html#aef1e5a0d60139f638a7fab4e35c01b15"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';