var mpc5643l__uart__type_8h =
[
    [ "Uart_RxBuffer", "struct_uart___rx_buffer.html", "struct_uart___rx_buffer" ],
    [ "UART_SIZE_BUFFER", "mpc5643l__uart__type_8h.html#a2dcb62a15f1dd53b5a1548a8d6a4f628", null ],
    [ "Uart_flexlin", "mpc5643l__uart__type_8h.html#ac744f54172af9927784dc0c8cba054bb", [
      [ "UART_FLEXLIND0", "mpc5643l__uart__type_8h.html#ac744f54172af9927784dc0c8cba054bba64c4db22e405c17da61b1f348f6b9b93", null ],
      [ "UART_FLEXLIND1", "mpc5643l__uart__type_8h.html#ac744f54172af9927784dc0c8cba054bba88d9c8aa70c7280894aa00dbf0c3cad1", null ]
    ] ]
];