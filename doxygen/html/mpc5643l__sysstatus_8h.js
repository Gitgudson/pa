var mpc5643l__sysstatus_8h =
[
    [ "SYSSTATUS_ERROR", "mpc5643l__sysstatus_8h.html#a78a82e5a01a6649d41b73e7f8039b2cc", null ],
    [ "SYSSTATUS_MEMCONFIG", "mpc5643l__sysstatus_8h.html#aeeb0566c80d89ce82a0e62361f611284", null ],
    [ "SYSSTATUS_MEMCONFIG_MASK", "mpc5643l__sysstatus_8h.html#a9de4d0f2cc6902f1090c732455e0461d", null ],
    [ "SYSSTATUS_MIDR1", "mpc5643l__sysstatus_8h.html#abbff38008d419eca5dbbe7fead915b50", null ],
    [ "SYSSTATUS_MIDR2", "mpc5643l__sysstatus_8h.html#aee6fa5a0507cc9ae5535e9fb8d65ba3b", null ],
    [ "SYSSTATUS_STATUS", "mpc5643l__sysstatus_8h.html#aba9648eeaef8e330846fc4dcae093130", null ],
    [ "SYSSTATUS_STATUS_MASK", "mpc5643l__sysstatus_8h.html#af6ba84646aadc5553f98f3e1d9c3ab3a", null ],
    [ "sysstatus_check", "mpc5643l__sysstatus_8h.html#af4b9e52d35fb630e35491a7bf6b32097", null ]
];