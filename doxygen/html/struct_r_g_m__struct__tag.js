var struct_r_g_m__struct__tag =
[
    [ "DERD", "struct_r_g_m__struct__tag.html#a20d9a7f676ea1d74e366a149f22fba6c", null ],
    [ "DES", "struct_r_g_m__struct__tag.html#abdee4c7d6dfa5cd8fe0967070fdbab57", null ],
    [ "FBRE", "struct_r_g_m__struct__tag.html#a95157b64664ca148a50bf05a2fcd2d36", null ],
    [ "FEAR", "struct_r_g_m__struct__tag.html#a38f06223863eaa2eea925b2d45bcf3f8", null ],
    [ "FERD", "struct_r_g_m__struct__tag.html#a60e94f787b5755ba3e673f6a2cd53dd9", null ],
    [ "FES", "struct_r_g_m__struct__tag.html#adc203b3f6383ff5c0636c5cfff52ed75", null ],
    [ "FESS", "struct_r_g_m__struct__tag.html#af6f2d19931fdbb926440cf5c7cf51753", null ],
    [ "RGM_reserved_0008", "struct_r_g_m__struct__tag.html#ac60891b2463e6cc004b44853ff3bcc26", null ],
    [ "RGM_reserved_0012", "struct_r_g_m__struct__tag.html#a656534d7444f0df7c4e175146a8dd7bb", null ],
    [ "RGM_reserved_001A", "struct_r_g_m__struct__tag.html#a4a2dbc385d221850f4c76710766987d0", null ],
    [ "RGM_reserved_001E", "struct_r_g_m__struct__tag.html#a90e7b9420e5023fae033f262a1469ba9", null ]
];