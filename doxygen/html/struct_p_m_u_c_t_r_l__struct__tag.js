var struct_p_m_u_c_t_r_l__struct__tag =
[
    [ "CTRL", "struct_p_m_u_c_t_r_l__struct__tag.html#a9c634b9daefb2dbba73cfebaef753f4a", null ],
    [ "FAULT", "struct_p_m_u_c_t_r_l__struct__tag.html#ae34e2ffbb1d2ade2611ee3a2e3f0152b", null ],
    [ "IRQE", "struct_p_m_u_c_t_r_l__struct__tag.html#ae8f13f4d0385d5c2e9b0402ed8ab4970", null ],
    [ "IRQS", "struct_p_m_u_c_t_r_l__struct__tag.html#af8bbc99e25650c21823e0eba066a12a6", null ],
    [ "MASKF", "struct_p_m_u_c_t_r_l__struct__tag.html#acacddc64bcd2e136f7b66ab196a085d1", null ],
    [ "PMUCTRL_reserved_0000", "struct_p_m_u_c_t_r_l__struct__tag.html#a493ae617bdcdc02e09114c77c759b013", null ],
    [ "PMUCTRL_reserved_000C", "struct_p_m_u_c_t_r_l__struct__tag.html#aed36eae0cc676d10f2410466f0e25768", null ],
    [ "PMUCTRL_reserved_0028", "struct_p_m_u_c_t_r_l__struct__tag.html#ab7e8a48fffbc6ccd424b07e92aec3b43", null ],
    [ "PMUCTRL_reserved_0048", "struct_p_m_u_c_t_r_l__struct__tag.html#ac5c8c331d7497d084fec3b53ae299532", null ],
    [ "PMUCTRL_reserved_0080", "struct_p_m_u_c_t_r_l__struct__tag.html#a22e164e85bbf0bd3d12b3f01be5a5f2a", null ],
    [ "STATEREG", "struct_p_m_u_c_t_r_l__struct__tag.html#aed245da50aa5b54f70cd2500e3401943", null ],
    [ "STATHVD", "struct_p_m_u_c_t_r_l__struct__tag.html#a799fa55eaaacfad4cdfc8e43f980ad52", null ],
    [ "STATIREG", "struct_p_m_u_c_t_r_l__struct__tag.html#aae8f7f7a250bb5357a5fe49017889d66", null ],
    [ "STATLVD", "struct_p_m_u_c_t_r_l__struct__tag.html#a6ba84806334d882568961b694261024f", null ],
    [ "STATUS", "struct_p_m_u_c_t_r_l__struct__tag.html#a089e8d5f85ef919675e5af6deb40fb76", null ]
];