var union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag =
[
    [ "__pad0__", "union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag.html#ac4f9ca5e396ec05019375a4f19eee234", null ],
    [ "__pad1__", "union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag.html#ad012213e9c4495669847868d50afe47d", null ],
    [ "__pad2__", "union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag.html#a943e82f4ca5c9a07f1c07a8ed06c7486", null ],
    [ "B", "union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag.html#af625af7c962569a22c7bc6099db33a01", null ],
    [ "IOAMPL", "union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag.html#ad7849b88537ef8a73455e568af46ac7d", null ],
    [ "IOFREQ", "union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag.html#a4c69adde123c79d82c00cc4805f11bfb", null ],
    [ "LDOS", "union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag.html#a088bce1d63166237c3ba679f37b5fd87", null ],
    [ "PDS", "union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag.html#a5092ad59cb4694c8268ad35658eff086", null ],
    [ "R", "union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ],
    [ "S0H1", "union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag.html#aa91d414154bf9634ebd7fd862f4c083d", null ],
    [ "SEMASK", "union_s_g_e_n_d_i_g___c_t_r_l__32_b__tag.html#aefe52f571a427cfd8f3086fbe1ba4597", null ]
];