var pmsm__pos_sensors_8h =
[
    [ "PMSM_LIM_ERR_SENS", "pmsm__pos_sensors_8h.html#a39fb8fca00f5ec030924ca0ff545b44f", null ],
    [ "PMSM_RES_MID", "pmsm__pos_sensors_8h.html#ad5b416fa16b241e88853d32f85bc22bb", null ],
    [ "PMSM_TOL_NULL_RES", "pmsm__pos_sensors_8h.html#aa7fbddc8e43bb6a3eef8cfece7bcfa0f", null ],
    [ "pmsmMotors_initPos", "pmsm__pos_sensors_8h.html#aba9852b81b631a50b5f6a93b8545fc7f", null ],
    [ "pmsmMotors_read2Enc", "pmsm__pos_sensors_8h.html#af6536c1439992fa97f20c9e3dc89b275", null ],
    [ "pmsmMotors_readEnc", "pmsm__pos_sensors_8h.html#a7b330916f37fe6cf9f4f8c7e295002bc", null ],
    [ "pmsmMotors_readResolv", "pmsm__pos_sensors_8h.html#a3a8006c8f77e78d15de4a2b7d9b5e351", null ],
    [ "pmsmMotors_setAngleSys", "pmsm__pos_sensors_8h.html#a11b5accfbd3748fb100fa616af96aca4", null ],
    [ "pmsmMotors_setPositionSpeed", "pmsm__pos_sensors_8h.html#a39be06679574e3f97e634ee6017ae0e6", null ]
];