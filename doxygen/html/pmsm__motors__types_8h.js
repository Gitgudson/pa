var pmsm__motors__types_8h =
[
    [ "Pmsm_mc33937a_s", "struct_pmsm__mc33937a__s.html", "struct_pmsm__mc33937a__s" ],
    [ "Pmsm_config_s", "struct_pmsm__config__s.html", "struct_pmsm__config__s" ],
    [ "Pmsm_vBus_s", "struct_pmsm__v_bus__s.html", "struct_pmsm__v_bus__s" ],
    [ "Pmsm_abc_raw_s", "struct_pmsm__abc__raw__s.html", "struct_pmsm__abc__raw__s" ],
    [ "Pmsm_abc_float_s", "struct_pmsm__abc__float__s.html", "struct_pmsm__abc__float__s" ],
    [ "Pmsm_alphaBeta_s", "struct_pmsm__alpha_beta__s.html", "struct_pmsm__alpha_beta__s" ],
    [ "Pmsm_dq_s", "struct_pmsm__dq__s.html", "struct_pmsm__dq__s" ],
    [ "Pmsm_current_s", "struct_pmsm__current__s.html", "struct_pmsm__current__s" ],
    [ "Pmsm_voltageOutput_s", "struct_pmsm__voltage_output__s.html", "struct_pmsm__voltage_output__s" ],
    [ "Pmsm_controlCommands_s", "struct_pmsm__control_commands__s.html", "struct_pmsm__control_commands__s" ],
    [ "Pmsm_ctrlSpeed_s", "struct_pmsm__ctrl_speed__s.html", "struct_pmsm__ctrl_speed__s" ],
    [ "Pmsm_ctrlCurrent_s", "struct_pmsm__ctrl_current__s.html", "struct_pmsm__ctrl_current__s" ],
    [ "Pmsm_measure_s", "struct_pmsm__measure__s.html", "struct_pmsm__measure__s" ],
    [ "Pmsm_motor_s", "struct_pmsm__motor__s.html", "struct_pmsm__motor__s" ],
    [ "Pmsm_ctrlState_e", "pmsm__motors__types_8h.html#a1744b34b055a6d463921b9a6e4ac7e90", [
      [ "CTRL_OFF", "pmsm__motors__types_8h.html#a1744b34b055a6d463921b9a6e4ac7e90a6f2df1a08d365028749035ea9e36d60a", null ],
      [ "CTRL_ON", "pmsm__motors__types_8h.html#a1744b34b055a6d463921b9a6e4ac7e90a0140102f1b745ca7bb2dfff1b4cda532", null ]
    ] ],
    [ "Pmsm_select_motor_e", "pmsm__motors__types_8h.html#a269257dc3aaf54e70e898bd88a02552b", [
      [ "PMSM_motor0", "pmsm__motors__types_8h.html#a269257dc3aaf54e70e898bd88a02552ba63fbed30d1a02cd470b01a63317473c1", null ],
      [ "PMSM_motor1", "pmsm__motors__types_8h.html#a269257dc3aaf54e70e898bd88a02552ba82ab08076c85abfa1e73d26864deb5c2", null ]
    ] ]
];