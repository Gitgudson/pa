var union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag =
[
    [ "__pad0__", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#ac4f9ca5e396ec05019375a4f19eee234", null ],
    [ "__pad1__", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#ad012213e9c4495669847868d50afe47d", null ],
    [ "__pad2__", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#a943e82f4ca5c9a07f1c07a8ed06c7486", null ],
    [ "__pad3__", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#a4a48290df7cadf8e7a567c7a811528ec", null ],
    [ "B", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#a4adf2edd19d00679bf30050129c3a663", null ],
    [ "PACR0_SP", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#a9de940ac98927e598593c000c90e7a09", null ],
    [ "PACR0_TP", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#ae0465d5e4afe926dcc6f95686d30afe5", null ],
    [ "PACR0_WP", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#ab47ff04082e0c6ed5f83a3b55b642553", null ],
    [ "PACR1_SP", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#a83b5c3a97de2804dacedc9aecd304295", null ],
    [ "PACR1_TP", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#ad75632a2e016486de83d6eacf4f8f9f5", null ],
    [ "PACR1_WP", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#a9562572aa18af6e4d83348967fb85ab1", null ],
    [ "PACR4_SP", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#a6d797fca7dcf86a6985d3cea3b5d8030", null ],
    [ "PACR4_TP", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#a27254bc449ea88d227cc8e2727f13265", null ],
    [ "PACR4_WP", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#adccb41c695a844afe1a1ac334e2ccc63", null ],
    [ "R", "union_p_b_r_i_d_g_e___p_a_c_r0__7__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ]
];