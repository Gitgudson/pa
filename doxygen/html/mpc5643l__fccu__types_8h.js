var mpc5643l__fccu__types_8h =
[
    [ "Fccu_errors_s", "struct_fccu__errors__s.html", "struct_fccu__errors__s" ],
    [ "Fccu_ctrl_e", "mpc5643l__fccu__types_8h.html#a3543771cd0729650dd3299cc76af10f3", [
      [ "FCCU_SWITCH_CONFIG", "mpc5643l__fccu__types_8h.html#a3543771cd0729650dd3299cc76af10f3a5dd461e503cf47e2364fb0b032562385", null ],
      [ "FCCU_SWITCH_NORMAL", "mpc5643l__fccu__types_8h.html#a3543771cd0729650dd3299cc76af10f3a66dba6cc2547ed34ec69462c29073bf6", null ],
      [ "FCCU_READ_STATE", "mpc5643l__fccu__types_8h.html#a3543771cd0729650dd3299cc76af10f3ad5577bc3a10274696510813b8213cb61", null ],
      [ "FCCU_LOCKCONF", "mpc5643l__fccu__types_8h.html#a3543771cd0729650dd3299cc76af10f3a108ed85bab471b60300f9cc2d31b28bc", null ],
      [ "FCCU_READ_CF", "mpc5643l__fccu__types_8h.html#a3543771cd0729650dd3299cc76af10f3a88842dda970db4bf73450f6d332fad70", null ],
      [ "FCCU_READ_NCF", "mpc5643l__fccu__types_8h.html#a3543771cd0729650dd3299cc76af10f3a7057ee5bb6126ba05182df6062ce7106", null ]
    ] ]
];