var struct_d_m_a___c_h___m_u_x__struct__tag =
[
    [ "CHCONFIG", "struct_d_m_a___c_h___m_u_x__struct__tag.html#a21dd908f3fbd023a16ba16ed0561001c", null ],
    [ "CHCONFIG0", "struct_d_m_a___c_h___m_u_x__struct__tag.html#a7ce2db913e340629d56fe71a4701040c", null ],
    [ "CHCONFIG1", "struct_d_m_a___c_h___m_u_x__struct__tag.html#ac966a176024fb0dd14a7306c059b8afa", null ],
    [ "CHCONFIG10", "struct_d_m_a___c_h___m_u_x__struct__tag.html#a8eaa2acdb39a476d2654e00f2589d6a8", null ],
    [ "CHCONFIG11", "struct_d_m_a___c_h___m_u_x__struct__tag.html#a5f952afe24fc144b04448e6e9099498b", null ],
    [ "CHCONFIG12", "struct_d_m_a___c_h___m_u_x__struct__tag.html#affb13fbef5c35079c3e0eac01b2d62b2", null ],
    [ "CHCONFIG13", "struct_d_m_a___c_h___m_u_x__struct__tag.html#ab65281c14a52aa73e323bcd11bd15e78", null ],
    [ "CHCONFIG14", "struct_d_m_a___c_h___m_u_x__struct__tag.html#aac9c84d88efed01545986e6e795e466e", null ],
    [ "CHCONFIG15", "struct_d_m_a___c_h___m_u_x__struct__tag.html#a69b0ac47064c91e8b3565a57a66acc6e", null ],
    [ "CHCONFIG2", "struct_d_m_a___c_h___m_u_x__struct__tag.html#a63f0efad8718daa649d70ba74c57be6b", null ],
    [ "CHCONFIG3", "struct_d_m_a___c_h___m_u_x__struct__tag.html#a4b55e002f1f63c134b7166c9f4b00dd2", null ],
    [ "CHCONFIG4", "struct_d_m_a___c_h___m_u_x__struct__tag.html#a5a5223b114fcebdca666fb48a575cb73", null ],
    [ "CHCONFIG5", "struct_d_m_a___c_h___m_u_x__struct__tag.html#ae8d34ab545ee37506369111d657c1f38", null ],
    [ "CHCONFIG6", "struct_d_m_a___c_h___m_u_x__struct__tag.html#ab900e9e0450231d366ec891e0d223264", null ],
    [ "CHCONFIG7", "struct_d_m_a___c_h___m_u_x__struct__tag.html#aab0bfec9ecc8c5977097ec9283f864bf", null ],
    [ "CHCONFIG8", "struct_d_m_a___c_h___m_u_x__struct__tag.html#a6fcf9b8a0670e792af5fd16e206f9bdc", null ],
    [ "CHCONFIG9", "struct_d_m_a___c_h___m_u_x__struct__tag.html#a8e3ccaac1094547cefa08f4c42d16cf8", null ],
    [ "DMA_CH_MUX_reserved_0010", "struct_d_m_a___c_h___m_u_x__struct__tag.html#a99cd736dcf10e8a9c2f31274d09959b0", null ]
];