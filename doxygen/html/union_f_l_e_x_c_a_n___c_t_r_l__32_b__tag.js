var union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag =
[
    [ "__pad0__", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#ac4f9ca5e396ec05019375a4f19eee234", null ],
    [ "B", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a9eb19968206749d460b0ce2dd8a30eb1", null ],
    [ "BOFF_MSK", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#af913a009132b8bd0471e4cc761c8cf8f", null ],
    [ "BOFF_REC", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a4dbb91381366a7862bca128b99d1e02a", null ],
    [ "CLK_SRC", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a3a7c60bff492179725bd9694983cd8d6", null ],
    [ "ERR_MSK", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#ad126dfaf4559a5a288af0f8bd0c9f4e8", null ],
    [ "LBUF", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a4ab33dc4da05dedaa41fc9223008cbae", null ],
    [ "LOM", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a184585d02a7a45b4c81607793a857ec9", null ],
    [ "LPB", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a2a30ee6ead480a7ec10147c429e3880d", null ],
    [ "PRESDIV", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#ab5b28e6447e92deebf4cece3c8ace197", null ],
    [ "PROPSEG", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a36022569d22ad67201749296ba473d37", null ],
    [ "PSEG1", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a8717cc49954ece34d739e2d6a3751245", null ],
    [ "PSEG2", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a7118cfe201fe64ac4130bfb44d519cc4", null ],
    [ "R", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ],
    [ "RJW", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a7b4fc6ffce75e32f24d3598077e82338", null ],
    [ "RWRN_MSK", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#aba1e470f9cdb03770c6f7d4f77c87505", null ],
    [ "SMP", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a53c87f67684cbb9ea9da9891e078bea9", null ],
    [ "TSYN", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#ae5c503fb5953756656b20159e3651bf0", null ],
    [ "TWRN_MSK", "union_f_l_e_x_c_a_n___c_t_r_l__32_b__tag.html#a7727f55881302cbd3b78cdac3f857ce8", null ]
];