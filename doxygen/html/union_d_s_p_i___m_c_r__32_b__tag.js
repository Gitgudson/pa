var union_d_s_p_i___m_c_r__32_b__tag =
[
    [ "__pad0__", "union_d_s_p_i___m_c_r__32_b__tag.html#ac4f9ca5e396ec05019375a4f19eee234", null ],
    [ "B", "union_d_s_p_i___m_c_r__32_b__tag.html#a3844e6dc19aed4914c80ed87bcee9703", null ],
    [ "CLR_RXF", "union_d_s_p_i___m_c_r__32_b__tag.html#af5906e12f8603222f73f337838f98f5f", null ],
    [ "CLR_TXF", "union_d_s_p_i___m_c_r__32_b__tag.html#aa124e317ded434358d37c14d477c6534", null ],
    [ "CONT_SCKE", "union_d_s_p_i___m_c_r__32_b__tag.html#aa6174160cc2e37d4c730651bb7b4babd", null ],
    [ "DCONF", "union_d_s_p_i___m_c_r__32_b__tag.html#ae3d7f7218e86282b9149da35d77196dd", null ],
    [ "DIS_RXF", "union_d_s_p_i___m_c_r__32_b__tag.html#a7c98356a5a90ea5e821f0df0e8cc893e", null ],
    [ "DIS_TXF", "union_d_s_p_i___m_c_r__32_b__tag.html#a8fa2055dd53c38d53f6c1bba0127edba", null ],
    [ "DOZE", "union_d_s_p_i___m_c_r__32_b__tag.html#afd143d9da5fa06ab92a466002a9e6f93", null ],
    [ "FRZ", "union_d_s_p_i___m_c_r__32_b__tag.html#a56d8544a83b8120660558f33fc23ca14", null ],
    [ "HALT", "union_d_s_p_i___m_c_r__32_b__tag.html#aa4267ea60e1b3e7d6e7ac59a673d9feb", null ],
    [ "MDIS", "union_d_s_p_i___m_c_r__32_b__tag.html#a71ca2d41ed4874aabc788a9d6ec0723e", null ],
    [ "MSTR", "union_d_s_p_i___m_c_r__32_b__tag.html#a17ccdbca0fd546e86e937a1cf131f658", null ],
    [ "MTFE", "union_d_s_p_i___m_c_r__32_b__tag.html#aacd42e021845da25d851353f1e92b1f5", null ],
    [ "PCSIS0", "union_d_s_p_i___m_c_r__32_b__tag.html#a6fc0a5b110c18a2d78358c7c96d229ae", null ],
    [ "PCSIS1", "union_d_s_p_i___m_c_r__32_b__tag.html#a54bcef2e8d6a0b861439a461e215895e", null ],
    [ "PCSIS2", "union_d_s_p_i___m_c_r__32_b__tag.html#ab9d154462e6662ce91026c55368fc7a3", null ],
    [ "PCSIS3", "union_d_s_p_i___m_c_r__32_b__tag.html#a2d0cbad928b7bf162f150f7859212888", null ],
    [ "PCSIS4", "union_d_s_p_i___m_c_r__32_b__tag.html#a3185fc564bc9724207f691f33985216b", null ],
    [ "PCSIS5", "union_d_s_p_i___m_c_r__32_b__tag.html#aff12287af5841277aa8acd8c6b98d5cd", null ],
    [ "PCSIS6", "union_d_s_p_i___m_c_r__32_b__tag.html#ae1ad00c4944b37e3a68d6641746f3691", null ],
    [ "PCSIS7", "union_d_s_p_i___m_c_r__32_b__tag.html#a7902acbac60a7901abe453432b7a3c08", null ],
    [ "PCSSE", "union_d_s_p_i___m_c_r__32_b__tag.html#abae0d9c7c0b5a052bcee2e40e9d4b20b", null ],
    [ "PES", "union_d_s_p_i___m_c_r__32_b__tag.html#a56fff63287a2fb7c38885da3246e14f1", null ],
    [ "R", "union_d_s_p_i___m_c_r__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ],
    [ "ROOE", "union_d_s_p_i___m_c_r__32_b__tag.html#a65c74bb1c325b5a0940d11f1e5d4046e", null ],
    [ "SMPL_PT", "union_d_s_p_i___m_c_r__32_b__tag.html#aa0650d351954a189bac4b7f2c405181d", null ]
];