var union_d_s_p_i___s_r__32_b__tag =
[
    [ "__pad0__", "union_d_s_p_i___s_r__32_b__tag.html#ac4f9ca5e396ec05019375a4f19eee234", null ],
    [ "__pad1__", "union_d_s_p_i___s_r__32_b__tag.html#ad012213e9c4495669847868d50afe47d", null ],
    [ "__pad2__", "union_d_s_p_i___s_r__32_b__tag.html#a943e82f4ca5c9a07f1c07a8ed06c7486", null ],
    [ "__pad3__", "union_d_s_p_i___s_r__32_b__tag.html#a4a48290df7cadf8e7a567c7a811528ec", null ],
    [ "__pad4__", "union_d_s_p_i___s_r__32_b__tag.html#a4bc805a6e6b8da9797071eaf16fd1c71", null ],
    [ "B", "union_d_s_p_i___s_r__32_b__tag.html#a9d29850cda85f641e9b46edb2613c945", null ],
    [ "DDIF", "union_d_s_p_i___s_r__32_b__tag.html#adb21b4354d72d8ac181c129769cb7d35", null ],
    [ "DPEF", "union_d_s_p_i___s_r__32_b__tag.html#ae395b3b5a087e607cbb396ced444d9e6", null ],
    [ "EOQF", "union_d_s_p_i___s_r__32_b__tag.html#ad52fa4ca1d6f9d452b6abd52fb90ee48", null ],
    [ "POPNXTPTR", "union_d_s_p_i___s_r__32_b__tag.html#a6ff9ece6fdfa23fc57a676551e6c9bc4", null ],
    [ "R", "union_d_s_p_i___s_r__32_b__tag.html#a5771b9f4924dfe9b09a9b38582e9b4c9", null ],
    [ "RFDF", "union_d_s_p_i___s_r__32_b__tag.html#a67795e0e85e90910a04d2566a87a8290", null ],
    [ "RFOF", "union_d_s_p_i___s_r__32_b__tag.html#ab670cf3a42406821b60ecbab8de07d11", null ],
    [ "RXCTR", "union_d_s_p_i___s_r__32_b__tag.html#a3f4d30d59db33fa1514520fd17257015", null ],
    [ "SPEF", "union_d_s_p_i___s_r__32_b__tag.html#a83ed8dbd0650d60f420de55f32d7d077", null ],
    [ "TCF", "union_d_s_p_i___s_r__32_b__tag.html#a3fdb24f2f9b23bc314925bd15375ddc3", null ],
    [ "TFFF", "union_d_s_p_i___s_r__32_b__tag.html#af41f3254dca34f55d4dab83d8481f3ac", null ],
    [ "TFUF", "union_d_s_p_i___s_r__32_b__tag.html#a6358246d0e6b6a5bc991dbb9f4e54a37", null ],
    [ "TXCTR", "union_d_s_p_i___s_r__32_b__tag.html#af9d200aa87eff8de80dfe49bccc7a88e", null ],
    [ "TXNXTPTR", "union_d_s_p_i___s_r__32_b__tag.html#a733e3bf0d3943221531130dfe126d1e9", null ],
    [ "TXRXS", "union_d_s_p_i___s_r__32_b__tag.html#a403370481a1f929667db3d1e9b32d13b", null ]
];