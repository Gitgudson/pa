var mpc5643l__siul_8h =
[
    [ "SIUL_PCR_IBE", "mpc5643l__siul_8h.html#acf74b12fc24a93040260a723cf94ad6a", null ],
    [ "SIUL_PCR_OBE", "mpc5643l__siul_8h.html#aa29833b9ff773083fb27962f78adc7ff", null ],
    [ "siul_config_pad", "mpc5643l__siul_8h.html#a5a003a0ebce9c52d6f47a13616c6f797", null ],
    [ "siul_config_padSelection", "mpc5643l__siul_8h.html#ab01ac4aca4394f8a87f5e6e643bc7912", null ],
    [ "siul_deconfig_pad", "mpc5643l__siul_8h.html#ab8ac5c50d6f91709b40cabe2ed8b1e72", null ],
    [ "siul_init", "mpc5643l__siul_8h.html#a8c75e35058d21ae2394ec2c4c156f75c", null ],
    [ "siul_read_pad", "mpc5643l__siul_8h.html#a51ff1397a1f645f84d8ff86fd39d62ba", null ],
    [ "siul_SWTEST_REGCRC", "mpc5643l__siul_8h.html#ad5352954d4ad9fb040d523b5439de03d", null ],
    [ "siul_toggle_pad", "mpc5643l__siul_8h.html#afe155047f7ede5c8c48329983427e176", null ],
    [ "siul_write_fast_pad", "mpc5643l__siul_8h.html#a2b48dd878ed5a409797b63c8f3812087", null ],
    [ "siul_write_pad", "mpc5643l__siul_8h.html#add63de0dd2e06bb614a322a08e033c56", null ]
];