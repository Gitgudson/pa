var mpc5643l__adc_8h =
[
    [ "ADC0_CTR0", "mpc5643l__adc_8h.html#aa828bed498f1c806b109cfe5b24bc1c6", null ],
    [ "ADC0_MCR", "mpc5643l__adc_8h.html#a7ecdcd28c0bc7df4ff02cd9fe423cc71", null ],
    [ "ADC1_CTR0", "mpc5643l__adc_8h.html#a3c0e0603ffd991975ba1b9d755bd9b95", null ],
    [ "ADC1_MCR", "mpc5643l__adc_8h.html#a58e8014bcc2f988ab06950dc405d75e3", null ],
    [ "ADC_BI_MAX_VAL", "mpc5643l__adc_8h.html#a313c01797e06222321eb7392c027640a", null ],
    [ "ADC_CDATA_MASK", "mpc5643l__adc_8h.html#a1c8e18c18ff16d60a0eaff83d9c0a790", null ],
    [ "ADC_CLR_CEOCFR0", "mpc5643l__adc_8h.html#a73b75403d3a3485f083318f7c4a9943a", null ],
    [ "ADC_MAX_VAL", "mpc5643l__adc_8h.html#a3a63fb6f00694ff9f4318b4142c5c868", null ],
    [ "ADC_VALID", "mpc5643l__adc_8h.html#acf4e83fac5a8e2ac5cd9394eb4c9542f", null ],
    [ "adc_init", "mpc5643l__adc_8h.html#a1e942fcd91f79d49e4ddb8d7e9d3ef95", null ],
    [ "adc_reset_endOfConv", "mpc5643l__adc_8h.html#abf6915153a3a14b80d3a640f7fe71f06", null ]
];