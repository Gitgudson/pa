var struct_p_b_r_i_d_g_e__struct__tag =
[
    [ "MPROT0_7", "struct_p_b_r_i_d_g_e__struct__tag.html#a0751f18caf6d5ce1f7ab549c20a5388d", null ],
    [ "MPROT8_15", "struct_p_b_r_i_d_g_e__struct__tag.html#aba11755413e4b5a2dbcc2a4d49de078c", null ],
    [ "OPACR0_7", "struct_p_b_r_i_d_g_e__struct__tag.html#a0bc7600e3a5f05e36e0c7935e6c557fe", null ],
    [ "OPACR16_23", "struct_p_b_r_i_d_g_e__struct__tag.html#add5a0f94b5176cf9cd1bc2f0ba43844f", null ],
    [ "OPACR24_31", "struct_p_b_r_i_d_g_e__struct__tag.html#a1a3d751018dc01650fed8d4b5faa3a75", null ],
    [ "OPACR32_39", "struct_p_b_r_i_d_g_e__struct__tag.html#addcdca50bf32f1879b704d4d6b091b00", null ],
    [ "OPACR40_47", "struct_p_b_r_i_d_g_e__struct__tag.html#ad2bea15980f9eace9cd1363e804e672f", null ],
    [ "OPACR48_55", "struct_p_b_r_i_d_g_e__struct__tag.html#aa4708d5e11b2952e81b23b25b1fdb95e", null ],
    [ "OPACR56_63", "struct_p_b_r_i_d_g_e__struct__tag.html#aa44373f1b031eb36c3a9bd1512e13f40", null ],
    [ "OPACR64_71", "struct_p_b_r_i_d_g_e__struct__tag.html#a38ac2d6d72691956b3badd6e1a96d664", null ],
    [ "OPACR80_87", "struct_p_b_r_i_d_g_e__struct__tag.html#a2f6086ef08f7d991cbd3fe928bf9b3b7", null ],
    [ "OPACR88_95", "struct_p_b_r_i_d_g_e__struct__tag.html#ac7ab0f02efacc7863d1f29085f860d2f", null ],
    [ "PACR0_7", "struct_p_b_r_i_d_g_e__struct__tag.html#a6ce096f9844a926d0dd9a53a278f5aef", null ],
    [ "PACR16_23", "struct_p_b_r_i_d_g_e__struct__tag.html#a6dfeb589640d2bde95860673f9ccbd54", null ],
    [ "PACR8_15", "struct_p_b_r_i_d_g_e__struct__tag.html#aefd8e2778d56408865a814f8c53fe32a", null ],
    [ "PBRIDGE_reserved_0008", "struct_p_b_r_i_d_g_e__struct__tag.html#aacd5e12c28053def538685423e587c91", null ],
    [ "PBRIDGE_reserved_002C", "struct_p_b_r_i_d_g_e__struct__tag.html#a04c60999fade49d17a05c276902c469c", null ],
    [ "PBRIDGE_reserved_0044", "struct_p_b_r_i_d_g_e__struct__tag.html#a3a59d2c2df42b8c81ee28b381dec7208", null ],
    [ "PBRIDGE_reserved_0064", "struct_p_b_r_i_d_g_e__struct__tag.html#a0d5c5a565846fa9d69e0ccc1ef881164", null ],
    [ "PBRIDGE_reserved_0070", "struct_p_b_r_i_d_g_e__struct__tag.html#a1bf472a0c637967c676e47dcf4186b20", null ]
];