# Weekly Report 3

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Done
* Short test of mechanisms
    * Clock Supervision
    * Register lock
* Implemented ECC for the RAM
    * Artificial 1-bit error injection
    * Detection of error
* Make more precise planning



#### Problems
* Interrupt for non-correctable error not working
    * According to the documentation this does not work with the MPC5643L
    * MPC5643LRM 21.4.2.10


#### Solutions
* Only inject and detect 1-bit error


## To Do
* Measurements with SALEAE
* Test mechanisms
* Recheck CodeWarrior license when expiring again
* Report


## Questions / Ideas
