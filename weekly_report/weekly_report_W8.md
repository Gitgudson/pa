# Weekly Report 8

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Done
* Take pictures for the report/guide
* Implement / activate the scheduler
* Test scheduler
  * Added dummy task to make a LED blink
* Add the state machine controlling the  motor as a task
* Vary the timing of the systick for the scheduler
* Check CodeWarrior license
  * Was able to generate a new evaluation license
  * Can this be repeated?



#### Problems
* Function "pit_init()" not working in "mpc5643l_mode.c"



#### Solutions
* "mpc5643l_pit.h" include guard was the same as in "mpc5643l_clock.h"
  * change include guard
  * Thanks to Jonathan Hendriks



## To Do
* Check the timing of the systick
* Implement mechanisms
* Add priorities to scheduler
* Recheck CodeWarrior license when expiring again


## Questions / Ideas
