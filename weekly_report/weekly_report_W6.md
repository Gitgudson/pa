# Weekly Report 6

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Done
* Begin report
* Familiarise myself with program written for the demonstrator
* Search solution to connect the demonstrator with the PC
  * Order new JTAG debugger


#### Problems



#### Solutions



## To Do
* Continue report
* Put demonstrator board into functional state
* Check already implemented safety Mechanisms
* Try and implement additional mechanisms


## Questions / Ideas
* Make list of tools used during project
  * Example
