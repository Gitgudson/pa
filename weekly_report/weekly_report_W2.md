# Weekly Report 2

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Done
* Added mile stones to planning
* Finished studying previous project
* Compose a list of potential microcontrollers
  * Based on microcontroller used for previous project
* Study of non-interference standard
* Basic study of IEC norm
* Basic study of ISO norm


#### Problems



#### Solutions



## To Do
* More in depth comparison of potential microcontrollers
* Look into ISO / IEC norms
* Further study of non-interference standard
* Norm comparison



## Questions / Ideas
* Work place
