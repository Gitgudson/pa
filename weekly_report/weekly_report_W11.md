# Weekly Report 3

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Done
* How does ECC work
* Check on ECC problem
* Measurements with SALEAE
    * Scheduler
    * FSM
    * Clock supervision
* Recheck CodeWarrior license


#### Problems



#### Solutions



## To Do
* Report


## Questions / Ideas
* Report
    * Format (bind or not)
