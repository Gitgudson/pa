# Weekly Report 4

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Done
* Compile list of safety mechanisms
  * Already implemented
  * Could be implemented
  * How to implement
  * How to check
* Look into MISRAC
  * Find tool to check for compliance



#### Problems



#### Solutions



## To Do
* Implement first safety mechanisms
* Think of how some mechanisms could be tested
* Look into compiler options



## Questions / Ideas
