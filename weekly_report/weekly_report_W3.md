# Weekly Report 3

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Done
* More in depth comparison of potential microcontrollers
  * Updated comparison of specs
  * Began comparison of methods implemented for safety
* Looked up Memory Protection Unit (MPU)
  * meaning of regions
* Look into ISO norms
* Look into IEC norms
* Study of non-interference standard


#### Problems



#### Solutions



## To Do
* Norm comparison
* Choose norm
* Look into MISRAC



## Questions / Ideas
* Work place
* Master Project
