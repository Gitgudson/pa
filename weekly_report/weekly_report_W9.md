# Weekly Report 3

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Done
* Check the timing of the systick
  * Clock used is 60 MHz
* Implement measurement/verification of IRCOSC
  * Calculate min and max value returned by the measurement
  * clock_.xlsx Configuration -> Frequency meter, MPC5643LRM.pdf 12.4.1.4
* Began locking registers
  * MPC5643LRM 40.5.2 & 40.6



#### Problems



#### Solutions



## To Do
* Implement mechanisms
* Recheck CodeWarrior license when expiring again


## Questions / Ideas
