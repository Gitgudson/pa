# Weekly Report 1

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Done
* GIT setup
* List of tasks
* Planning
* Study previous project
* Search for other microcontrollers
  * [TI Hercules](https://www.ti.com/microcontrollers/hercules-safety-mcus/products.html#p1498=Automotive)
  * [ST S Line](https://www.st.com/content/st_com/en/products/automotive-microcontrollers/spc5-32-bit-automotive-mcus/spc5-mcus-for-safety-critical-applications-and-motor-control/spc56-l-line-mcus.html?querycriteria=productId=LN1829)
  * [ST L Line](https://www.st.com/content/st_com/en/products/automotive-microcontrollers/spc5-32-bit-automotive-mcus/spc5-mcus-for-safety-critical-applications-and-motor-control/spc57-s-line-mcus.html?querycriteria=productId=LN1870)



#### Problems



#### Solutions



## To Do
* Define safety norm / level / features
* Examine other microcontroller solutions
* Define project context
* Study previous project
* Study non-interference standard




## Questions / Ideas
* Report language
* Presentation language
* Weekly report
* Document access
  * GIT
  * ROSAS cloud
  * other
