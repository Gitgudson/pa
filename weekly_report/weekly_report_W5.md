# Weekly Report 5

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Done
* Familiarise myself with demonstrator board
* Test with different JTAG debugger
  * Not working
* Communicate with minicom
  * Not working


#### Problems
* JTAG debugger is most likely broken
  * Tested on different PCs
  * Tested with different cables
* Board can't be connected to the PC via USB
  * Seen by OS but communication fails
  * No response seen on minicom


#### Solutions
* Potential solutions
  * Get different JTAG debugger
  * Get same JTAG debugger
  * try with USB to CAN/LIN/Serial cable
  * Try to communicate directly (e.g. via minicom)
  * Ask Nicolas Broch


## To Do
* Put demonstrator board into functional state
* Check already implemented safety Mechanisms
* Try and implement one additional mechanism


## Questions / Ideas
