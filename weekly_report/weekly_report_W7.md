# Weekly Report 7

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Done
* Put demonstrator board into functional state
* Program the scheduler



#### Problems
* JTAG debugger not able to program the demonstrator



#### Solutions
* Reinstall JTAG debugger drivers
* Reprogram JTAG debugger firmware



## To Do
* Implement / activate the scheduler
* Test scheduler



## Questions / Ideas
* Make list of tools used during project
  * Example
