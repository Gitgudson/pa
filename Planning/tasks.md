# PA Tasks and Objectives

###### Student: Jan Huber
###### Supervision: Jean-Roland Schuler, Eric Silva


## Important dates

* Start of project: 18.02.2019
* Delivery of the report: 31.05.2019
* Presentation: 10.06.2019 to 21.06.2019

## Tasks

* Setup GIT
* Define objectives of the project
* Planning (time of the different tasks)
* Think of context of the project
* Analyse different processors
* Study norm and its different levels (eg. A/B/C/D)
* Study the different mechanism to mitigate errors
* Read IEEE publications (optional)
* Study "non interference" (spatial, temporal, execution) in standard


## Objectives

* Choose appropriate processor
* Choose critical safety criteria
* Define safety level (eg. according to ASIL or SIL)
* Implement mechanism required by norm
* Test the different mechanism / try to bypass them / induce artificial faults
* Define optional and mandatory mechanisms to implement
* Redact report and presentation


## Useful links

* [IEEE publications](https://ieeexplore.ieee.org/Xplore/home.jsp)
* [ROSAS cloud](https://cloud.rosas.center)
* [GIT repository](https://gitlab.com/Gitgudson/pa)
